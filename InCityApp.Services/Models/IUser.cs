﻿using System;

namespace InCityApp.Services
{
    public interface IUser
    {
        object Id { get; }
        string Username { get; }
    }
}

