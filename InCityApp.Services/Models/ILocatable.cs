﻿using System;

namespace InCityApp.Models
{
    public interface ILocatable
    {
        double Latitude { get; }
        double Longitude { get; }
    }
}

