﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InCityApp.Models
{
    public class Annotation : MapLocation, IAnnotation
    {
        public Annotation () : base () {}
        public Annotation (ILocation location) : base (location) {}
    }
}
