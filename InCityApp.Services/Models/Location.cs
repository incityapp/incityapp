﻿using System.Collections.Generic;
using InCityApp.Services;
using System.ComponentModel;
using System;


namespace InCityApp.Models
{
    public class MapLocation : ILocation, ILocatable, INotifyPropertyChanged
    {
        readonly ILocation innerLocation;

        public const int EQUATOR_RADIUS = 6378137;

        public MapLocation () : this(null) { }

        public MapLocation(ILocation userLocation = null)
        {
            innerLocation = userLocation ?? new DefaultUserLocation();
        }

        public MapLocation(double latitude, double longitude) : this(null)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        protected ILocation InnerLocation {  get { return innerLocation; } }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged (string propertyName)
        {
            var evt = PropertyChanged;
            if (evt == null) return;
            evt(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region ILocatable implementation

        public double Latitude { get; set; }
        public double Longitude { get; set; }

        #endregion

        #region ILocation implementation

        public Address Address {
            get {
                return InnerLocation.Address ;
            }
            set {
                InnerLocation.Address = value;
            }
        }

        public IUser User {
            get {
                return InnerLocation.User;
            }
            set {
                InnerLocation.User = value;
                OnPropertyChanged("User");
            }
        }


        public double DistanceBetween(ILocatable a, ILocatable b)
        {
            //double distance = Math.Acos(
            //    (Math.Sin(a.Latitude) * Math.Sin(b.Latitude)) +
            //    (Math.Cos(a.Latitude) * Math.Cos(b.Latitude))
            //    * Math.Cos(b.Longitude - a.Longitude));

            double earthRadius = EQUATOR_RADIUS; //6371

            double dLat = ConvertToRadians(b.Latitude - a.Latitude);
            double dLng = ConvertToRadians(b.Longitude - a.Longitude);

            double sindLat = Math.Sin(dLat / 2);
            double sindLng = Math.Sin(dLng / 2);

            double ang = Math.Pow(sindLat, 2) + Math.Pow(sindLng, 2)
                * Math.Cos(ConvertToRadians(a.Latitude)) * Math.Cos(ConvertToRadians(b.Latitude));

            double c = 2 * Math.Atan2(Math.Sqrt(ang), Math.Sqrt(1 - ang));

            double dist = earthRadius * c;

            return dist;

        }

        public double DistanceFrom(ILocatable other)
        {
            return DistanceBetween(this, other);
        }


        #endregion
    
        public override string ToString ()
        {
            return string.Format ("{0} <{1},{2}>", User == null ? "" : User.Username, Latitude, Longitude);
        }

        private double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
    }

    public class DefaultUserLocation : ILocation, INotifyPropertyChanged
    {
        public DefaultUserLocation()
        {
            Address = new Address();
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;


        protected void OnPropertyChanged (string propertyName)
        {
            var evt = PropertyChanged;
            if (evt == null) return;
            evt(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region IUserLocation implementation

        public Address Address {
            get;
            set;
        }

        IUser user;
        public IUser User {
            get {
                return user;
            }
            set {
                user = value;
                OnPropertyChanged("User");
            }
        }
        #endregion
    }
}

