using System;
using System.Collections;
using System.Collections.Generic;
using InCityApp.Services;

namespace InCityApp.Models
{
	public interface ILocation
	{
        Address Address { get; set; }

        IUser User { get; set; }
	}
}

