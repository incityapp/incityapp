﻿using System;
using InCityApp.Services;

namespace InCityApp.Services
{
    public interface IAuthenticationResponse
    {
        IAuthenticationService Sender { get; }
        bool IsAuthenticated { get; }
        string Message { get; }
    }
}

