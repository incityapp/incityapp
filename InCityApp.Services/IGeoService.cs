using System;
using InCityApp.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InCityApp.Services
{
	public interface IGeoService
	{
        IEnumerable<Address> ValidateAddress(string address);
	}

}

