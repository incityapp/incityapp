﻿using System;
using System.Threading.Tasks;

namespace InCityApp.Services
{
    public interface IAuthenticationService
    {
        Task<IAuthenticationResponse> TryCredentials(string identifier, string secret);
    }
}
