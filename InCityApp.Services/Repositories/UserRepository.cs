﻿using System;
using InCityApp.Models;
using Xamarin.Forms;
using InCityApp.Services;

[assembly:Dependency(typeof(UserRepository))]

namespace InCityApp.Services
{
    public class UserRepository : UserInMemoryRepository<User>, IRepository<User> 
    {
        public UserRepository ()
        {
            Add(new User(new Contact { FirstName = "Javier", LastName = "Vasquez" }) { Username = "jvasquez" });
            Add(new User(new Contact { FirstName = "Davon", LastName = "Smith" }) { Username = "dsmith" });
        }
    }
}

