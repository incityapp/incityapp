using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using InCityApp.Services;
using InCityApp.Models;

[assembly:Dependency(typeof(AnnotationRepository))]

namespace InCityApp.Services
{

    public class AnnotationRepository : LocationInMemoryRepository<Annotation>, IRepository<Annotation> 
    {
        public AnnotationRepository()
        {
            // donghi
            Add(new Annotation { Address = new Address { Street = "Via Donghi 40", City = "Genova", State = "IT", PostalCode = "16100", Latitude = 44.412050, Longitude = 8.969441 }, Latitude = 44.412050, Longitude = 8.969441 });
            Add(new Annotation { Address = new Address { Street = "Via Donghi 40", City = "Genova", State = "IT", PostalCode = "16100", Latitude = 44.411050, Longitude = 8.968441 }, Latitude = 44.411050, Longitude = 8.968441 });
            Add(new Annotation { Address = new Address { Street = "Via Donghi 40", City = "Genova", State = "IT", PostalCode = "16100", Latitude = 44.410050, Longitude = 8.967441 }, Latitude = 44.410050, Longitude = 8.967441 });
            // bombrini
            Add(new Annotation { Address = new Address { Street = "Villa Bombrini per grandi e piccini!", City = "Genova", State = "IT", PostalCode = "16100", Latitude = 44.415254, Longitude = 8.875491 }, Latitude = 44.415254, Longitude = 8.875491 });
        }
    }
    
}
