

using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

using Xamarin.Forms;
﻿using Xamarin.Forms.Maps;

using InCityApp.Entities;
using InCityApp.Pages;
using InCityApp.Shared.Views;
using InCityApp.DataProviders;
using InCityApp.Helpers;
using System.Threading.Tasks;
using System.Diagnostics;
using InCityApp.DataModels;
using System.Collections.Concurrent;
using Android.Content;
using Android.Util;
using Android.Runtime;

namespace InCityApp.Views
{
    public class CustomMapView : Map
    {
        static object _lock = new object();

        public EventHandler OnAnnotationCreated;
        public EventHandler OnAnnotationModified;
        public EventHandler OnAnnotationDismissed;
        public EventHandler OnCreateAnnotationAborted;
        public EventHandler OnUpdateRender;

        private AnnotationDialog _annotationDialog;

        public enum State
        {
            Ready,
            InsertingAnnotation,
        }

        private readonly ConcurrentBag<Annotation> _items = new ConcurrentBag<Annotation>();
        private MapSpan _visibleRegion;
        private State _state;
        private BasePage _page;

        public bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                if (_isLoading == true && _page != null)
                    _page.ShowLoadingIndicator();
                else if (_isLoading == false && _page != null)
                    _page.RemoveLoadingIndicator();

            }
        }

        public CustomMapView(BasePage page, MapSpan region)
            : base(region)
        {
            _page = page;
            LastMoveToRegion = region;
        }

        //public CustomMapView(IntPtr javaReference, JniHandleOwnership jniHandleOwnership)
        //    : base()
        //{
        //}

        public static readonly BindableProperty SelectedPinProperty = BindableProperty.Create<CustomMapView, CustomPinView>(x => x.SelectedPin, null);
        
        public CustomPinView SelectedPin
        {
            get { return (CustomPinView)base.GetValue(SelectedPinProperty); }
            set { base.SetValue(SelectedPinProperty, value); }
        }

        public Annotation SelectedPinAnnotation
        {
            get;
            set;
        }

        public MapSpan LastMoveToRegion { get; set; }

        public new MapSpan VisibleRegion
        {
            get { return _visibleRegion; }
            set
            {
                if (_visibleRegion == value)
                {
                    return;
                }
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                OnPropertyChanging("VisibleRegion");
                _visibleRegion = value;
                OnPropertyChanged("VisibleRegion");
            }
        }

        public State CurrentState
        {
            get { return _state; }
            set { _state = value; }
        }

        public ConcurrentBag<Annotation> Items
        {
            get
            {
                    return _items;
            }
        }

        public void UpdatePins(IEnumerable<Annotation> items)
        {
            if (Items != null)
            {
                Annotation someItem;
                while (!Items.IsEmpty) 
                {
                    Items.TryTake(out someItem);
                }
            }

            if (items != null)
            {
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }

            if (OnUpdateRender != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    OnUpdateRender(this, null);
                });
            }
        }

        public async Task ShowAnnotationDialog(bool createNew)
        {
            if (IsLoading)
                return;

            List<Page> pages = new List<Page>(Navigation.NavigationStack.ToArray());
            if (pages.Find(x => x is AnnotationDialog) != null)
                return;

            IsLoading = true;

            //AnnotationExplorationAward explorationAwardDlg = new AnnotationExplorationAward(SelectedPinAnnotation);
            //await Navigation.PushAsync(explorationAwardDlg);

            bool isOurWSConnected = await Connectivity.Instance.IsOurWSConnected();
            if (isOurWSConnected)
            {
                if (SelectedPinAnnotation.CreatedFromCurrentUser || SelectedPinAnnotation.VotedFromCurrentUser) //Already voted or mine
                {
                    try
                    {
                        await OpenDetailForAnnotation(SelectedPinAnnotation, createNew);
                    }
                    catch (Exception exc)
                    {
                        Debug.Print(exc.Message);
                    }
                }
                else
                {
                    AnnotationValidation anvl = await AnnotationsModel.GetValidationByUserId(SelectedPinAnnotation.Id, UsersModel.CurrentUser.Id); //Update info about validation of this annotation

                    if (anvl == null || anvl.GuessVote <= 0) //Not already voted
                    {
                        AnnotationQuizPage qzPage = new AnnotationQuizPage(SelectedPinAnnotation);
                        qzPage.OnAnnotationModified += HandleOnAnnotationModified;
                        await Navigation.PushAsync(qzPage);
                    }
                    else
                        await OpenDetailForAnnotation(SelectedPinAnnotation, createNew);
                }
            }

            IsLoading = false;
        }

        private async Task OpenDetailForAnnotation(Annotation a, bool createNew)
        {
            _annotationDialog = new AnnotationDialog(a, createNew);
            _annotationDialog.OnCreateAnnotationAborted += HandleOnCreateAnnotationAborted;
            _annotationDialog.OnAnnotationCreated += HandleOnAnnotationCreated;
            _annotationDialog.OnAnnotationModified += HandleOnAnnotationModified;
            _annotationDialog.OnAnnotationDismissed += HandleOnAnnotationDismissed;

            try
            {
                await Navigation.PushAsync(_annotationDialog);
            }
            catch (Exception exc)
            {
                Debug.Print(exc.Message);
            }
        }

        private void HandleOnCreateAnnotationAborted(object sender, EventArgs e)
        {
            if (OnCreateAnnotationAborted != null)
                OnCreateAnnotationAborted(this, e);
        }

        private void HandleOnAnnotationCreated(object sender, Annotation annotation)
        {
            if (OnAnnotationCreated != null)
                OnAnnotationCreated(this, new EventArgs());

            SelectedPinAnnotation = annotation;

            //Annotation annotation = await AnnotationsModel.GetAnnotationById(SelectedPinAnnotation.Id.ToString());
            //if (annotation != null)
            //    if (annotation.IsTheFirstInArea)
            if (SelectedPinAnnotation.ExplorationPoints > 0)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    try
                    {
                        AnnotationExplorationAward explorationAwardDlg = new AnnotationExplorationAward(SelectedPinAnnotation);
                        explorationAwardDlg.OnClosed += async () => { await Navigation.PopModalAsync(); };
                        await Navigation.PushModalAsync(explorationAwardDlg);
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message);
                    }
                });
            }
        }

        private void HandleOnAnnotationModified(object sender, EventArgs e)
        {
            if (OnAnnotationModified != null)
                OnAnnotationModified(this, e);
        }

        private void HandleOnAnnotationDismissed(object sender, EventArgs e)
        {
            if (OnAnnotationDismissed != null)
                OnAnnotationDismissed(this, e);
        }
    }
}
