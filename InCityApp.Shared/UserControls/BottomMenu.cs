using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Shared.Views
{
	public class BottomMenu : StackLayout
	{
        public System.Action OnMapItemSelected;
        public System.Action OnNoisePollutionItemSelected;
        public System.Action OnMoveStatsItemSelected;
        public System.Action OnProfileSettingsItemSelected;

        public BottomMenu()
		{
            Orientation = StackOrientation.Horizontal;
            HorizontalOptions = LayoutOptions.Center;
            BackgroundColor = Color.FromHex("333333");

            FixedButton mapButton = new FixedButton()
            {
                Text = "Map",
                TextColor = Color.White,
                BackgroundColor = Color.FromHex("333333"),
                WidthRequest = 64,
                HeightRequest = 64
            };
            Children.Add(mapButton);

            FixedButton noisePollutionButton = new FixedButton()
            {
                Text = "Noiz",
                TextColor = Color.White,
                BackgroundColor = Color.FromHex("333333"),
                WidthRequest = 64,
                HeightRequest = 64
            };
            Children.Add(noisePollutionButton);

            FixedButton moveStatsButton = new FixedButton()
            {
                Text = "Stats",
                TextColor = Color.White,
                BackgroundColor = Color.FromHex("333333"),
                WidthRequest = 64,
                HeightRequest = 64
            };
            Children.Add(moveStatsButton);

            FixedButton profileSettingsButton = new FixedButton()
            {
                Text = "Settings",
                TextColor = Color.White,
                BackgroundColor = Color.FromHex("333333"),
                WidthRequest = 64,
                HeightRequest = 64
            };
            Children.Add(profileSettingsButton);
		}

	}
}
