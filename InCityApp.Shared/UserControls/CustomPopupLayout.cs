using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using Xamarin.Forms;

namespace InCityApp.Views
{
    public class CustomPopupLayout : RelativeLayout
    {
        private readonly Object _lock = new Object();

        /// <summary>
        /// Popup location options when relative to another view
        /// </summary>
        public enum PopupLocation
        {
            /// <summary>
            ///     Will show popup on top of the specified view
            /// </summary>
            Top,
            /// <summary>
            ///     Will show popup below of the specified view
            /// </summary>
            Bottom
            /// <summary>
            /// Will show popup left to the specified view
            /// </summary>
            //Left,
            /// <summary>
            /// Will show popup right of the specified view
            /// </summary>
            //Right
        }

        private View _content;
        private View _popup;

        public View PopupView
        {
            get { return _popup; }
            set { _popup = value; }
        }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        public View Content
        {
            get { lock (_lock) { return this._content; } }
            set
            {
                lock (_lock)
                {
                    if (_content != null)
                    {
                        Children.Remove(this._content);
                    }

                    _content = value;
                    Children.Add(this._content, () => this.Bounds);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is popup active.
        /// </summary>
        /// <value><c>true</c> if this instance is popup active; otherwise, <c>false</c>.</value>
        public bool IsPopupActive
        {
            get { lock (_lock) { return this._popup != null; } }
        }

        /// <summary>
        /// Shows the popup centered to the parent view.
        /// </summary>
        /// <param name="popupView">The popup view.</param>
        public void ShowPopup(View popupView)
        {
            lock (_lock)
            {
                ShowPopup(
                    popupView,
                    Constraint.RelativeToParent(p => (this.Width - _popup.WidthRequest) / 2),
                    Constraint.RelativeToParent(p => (this.Height - _popup.HeightRequest) / 2)
                    );
            }
        }

        /// <summary>
        /// Shows the popup with constraints.
        /// </summary>
        /// <param name="popupView">The popup view.</param>
        /// <param name="xConstraint">X constraint.</param>
        /// <param name="yConstraint">Y constraint.</param>
        /// <param name="widthConstraint">Optional width constraint.</param>
        /// <param name="heightConstraint">Optional height constraint.</param>
        public void ShowPopup(View popupView, Constraint xConstraint, Constraint yConstraint, Constraint widthConstraint = null, Constraint heightConstraint = null)
        {
            lock (_lock)
            {
                DismissPopup();
                _popup = popupView;

                try
                {
                    _content.InputTransparent = true;
                    Children.Add(_popup, xConstraint, yConstraint, widthConstraint, heightConstraint);
                }
                catch (Exception exc)
                { 
                    string innerMsg = string.Empty;
                    if (exc.InnerException != null)
                        innerMsg = exc.InnerException.Message;
                    Debug.Print(exc.Message + " | INNER : " + innerMsg);
                }

                UpdateChildrenLayout();
            }
        }


        /// <summary>
        /// Shows the popup.
        /// </summary>
        /// <param name="popupView">The popup view.</param>
        /// <param name="presenter">The presenter.</param>
        /// <param name="location">The location.</param>
        /// <param name="paddingX">The padding x.</param>
        /// <param name="paddingY">The padding y.</param>
        public void ShowPopup(View popupView, View presenter, PopupLocation location, float paddingX = 0, float paddingY = 0)
        {
            lock (_lock)
            {
                DismissPopup();
                _popup = popupView;

                Constraint constraintX = null, constraintY = null;

                switch (location)
                {
                    case PopupLocation.Bottom:
                        constraintX = Constraint.RelativeToParent(parent => presenter.X + (presenter.Width - _popup.WidthRequest) / 2);
                        constraintY = Constraint.RelativeToParent(parent => parent.Y + presenter.Y + presenter.Height + paddingY);
                        break;
                    case PopupLocation.Top:
                        constraintX = Constraint.RelativeToParent(parent => presenter.X + (presenter.Width - _popup.WidthRequest) / 2);
                        constraintY = Constraint.RelativeToParent(parent =>
                            parent.Y + presenter.Y - _popup.HeightRequest / 2 - paddingY);
                        break;
                    //case PopupLocation.Left:
                    //    constraintX = Constraint.RelativeToView(presenter, (parent, view) => ((view.X + view.Height / 2) - parent.X) + this.popup.HeightRequest / 2);
                    //    constraintY = Constraint.RelativeToView(presenter, (parent, view) => parent.Y + view.Y + view.Width + paddingY);
                    //    break;
                    //case PopupLocation.Right:
                    //    constraintX = Constraint.RelativeToView(presenter, (parent, view) => ((view.X + view.Height / 2) - parent.X) + this.popup.HeightRequest / 2);
                    //    constraintY = Constraint.RelativeToView(presenter, (parent, view) => parent.Y + view.Y - this.popup.WidthRequest - paddingY);
                    //    break;
                }

                this.ShowPopup(popupView, constraintX, constraintY);
            }
        }

        /// <summary>
        /// Dismisses the popup.
        /// </summary>
        public void DismissPopup()
        {
            lock (_lock)
            {
                if (_popup != null)
                {
                    this.Children.Remove(_popup);
                    _popup = null;
                }

                _content.InputTransparent = false;
            }
        }
    }
}
