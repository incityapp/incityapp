﻿using InCityApp.DataModels;
using InCityApp.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Views
{
    public class TagListView : ContentView
    {
        public delegate bool OnNewTagValidationHandler(string tagName);
        //public OnNewTagValidationHandler OnNewTagValidating;
        //public EventHandler<Tag> OnNewTagValidated;
        public EventHandler<int> OnTagVoted;

        private StackLayout _layout;
        private StackLayout _addOrCancelRowLayout;
        private ScrollView _scrollView;
        private FixedButton _btnAddCustomTag, _btnConfirmCustomTag, _btnCancelInsertingCustomTag;
        private Entry _txtCustomTagName;

        private Annotation _annotation;
        private List<TagListRow> _rows = new List<TagListRow>();
        private bool _creatingNew;

        public TagListView(Annotation annotation, bool creatingNew)
        {
            _annotation = annotation;
            _creatingNew = creatingNew;

            _scrollView = new ScrollView()
            {
                Orientation = ScrollOrientation.Vertical,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            _layout = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
            };

            foreach (Tag tag in _annotation.Tags)
            {
                TagListRow row = new TagListRow(tag, creatingNew);
                row.OnVoted = (object sender, int vote) => { if (OnTagVoted != null) OnTagVoted(sender, vote); };
                _rows.Add(row);
                _layout.Children.Add(row);
            }

            //if (creatingNew)
            //{
            //    // --- Add and Cancel buttons
            //    _addOrCancelRowLayout = new StackLayout() 
            //    { 
            //        Orientation = StackOrientation.Horizontal 
            //    };
            //    _btnAddCustomTag = new FixedButton() 
            //    { 
            //        Text = "Aggiungi Tag", 
            //        HorizontalOptions = LayoutOptions.Start, 
            //        BackgroundColor = Color.FromHex("52B296"), 
            //        TextColor = Color.White,
            //    };
            //    _btnAddCustomTag.Clicked += btnAddCustomTag_Clicked;
            //    _addOrCancelRowLayout.Children.Add(_btnAddCustomTag);

            //    _btnCancelInsertingCustomTag = new FixedButton() 
            //    { 
            //        Text = "Annulla", 
            //        IsEnabled = false, 
            //        IsVisible = false, 
            //        HorizontalOptions = LayoutOptions.End, 
            //        BackgroundColor = Color.FromHex("52B296"),
            //        TextColor = Color.White,
            //    };
            //    _btnCancelInsertingCustomTag.Clicked += btnCancelInsertingCustomTag_Clicked;
            //    _addOrCancelRowLayout.Children.Add(_btnCancelInsertingCustomTag);
              
            //    _layout.Children.Add(_addOrCancelRowLayout);
            //    // ---

            //    // tag entry
            //    _txtCustomTagName = new Entry() 
            //    { 
            //        IsEnabled = false, 
            //        IsVisible = false, 
            //        HorizontalOptions = LayoutOptions.Fill 
            //    };
            //    _txtCustomTagName.TextChanged += txtCustomTagName_TextChanged;
            //    _txtCustomTagName.Focused += txtCustomTagName_Focused;
            //    _layout.Children.Add(_txtCustomTagName);
            //    // confirm button
            //    _btnConfirmCustomTag = new FixedButton() 
            //    { 
            //        Text = "Ok", 
            //        IsEnabled = false,
            //        IsVisible = false, 
            //        HorizontalOptions = LayoutOptions.Fill,
            //        BackgroundColor = Color.FromHex("52B296") ,
            //        TextColor = Color.White,
            //    };
            //    _btnConfirmCustomTag.Clicked += btnConfirmCustomTag_Clicked;
            //    _layout.Children.Add(_btnConfirmCustomTag);
            //}

            _scrollView.Content = _layout;
            Content = _scrollView;
            
            //Content = _layout;
        }

        public void AddRow(TagListRow row)
        {
            _layout.Children.Add(row);
        }

        public void SetGuessMode(bool guessMode, bool readOnly = false)
        {
            foreach (TagListRow row in _rows)
                row.SetGuessMode(guessMode, readOnly);
        }

        public void ShowVoteOptions(bool show)
        {
            foreach (TagListRow row in _rows)
                row.ShowVoteOptions(show);
        }

        //void btnCancelInsertingCustomTag_Clicked(object sender, EventArgs e)
        //{
        //    _txtCustomTagName.IsEnabled = false;
        //    _txtCustomTagName.IsVisible = false;
        //    _txtCustomTagName.Text = string.Empty;

        //    _btnConfirmCustomTag.IsEnabled = false;
        //    _btnConfirmCustomTag.IsVisible = false;

        //    _btnCancelInsertingCustomTag.IsEnabled = false;
        //    _btnCancelInsertingCustomTag.IsVisible = false;

        //    _btnAddCustomTag.IsEnabled = true;
        //    _btnAddCustomTag.BackgroundColor = Color.FromHex("52B296");
        //}

        //void btnAddCustomTag_Clicked(object sender, EventArgs e)
        //{
        //    _txtCustomTagName.IsEnabled = true;
        //    _txtCustomTagName.IsVisible = true;
        //    _txtCustomTagName.Text = "Inserisci il nome del nuovo tag...";

        //    _btnConfirmCustomTag.IsEnabled = false;
        //    _btnConfirmCustomTag.BackgroundColor = Color.Gray;
        //    _btnConfirmCustomTag.IsVisible = true;

        //    _btnCancelInsertingCustomTag.IsEnabled = true;
        //    _btnCancelInsertingCustomTag.IsVisible = true;

        //    _btnAddCustomTag.IsEnabled = false;
        //    _btnAddCustomTag.BackgroundColor = Color.Gray;
        //    _scrollView.ScrollToAsync(0, _btnConfirmCustomTag.Y, true);
        //}

        //void txtCustomTagName_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    _txtCustomTagName.TextColor = Color.Black;
        //    if (string.IsNullOrWhiteSpace(e.NewTextValue))
        //    {
        //        _btnConfirmCustomTag.IsEnabled = false;
        //        _btnConfirmCustomTag.BackgroundColor = Color.Gray;
        //    }
        //    else
        //    {
        //        _btnConfirmCustomTag.IsEnabled = true;
        //        _btnConfirmCustomTag.BackgroundColor = Color.FromHex("52B296");
        //    }
        //}

        //void txtCustomTagName_Focused(object sender, FocusEventArgs e)
        //{
        //    if (_txtCustomTagName.Text == "Inserisci il nome del nuovo tag...")
        //        _txtCustomTagName.Text = string.Empty;
        //}

        //void btnConfirmCustomTag_Clicked(object sender, EventArgs e)
        //{
        //    bool isValid = false;
        //    if (OnNewTagValidating != null)
        //        isValid = OnNewTagValidating(_txtCustomTagName.Text);

        //    if (isValid)
        //    {
        //        Tag tag = new Tag() { Id = -1, UserId = UsersModel.CurrentUser.Id, AnnotationId = _annotation.Id, Name = _txtCustomTagName.Text, Tagged = 1, Vote = -1 };
        //        _addOrCancelRowLayout.Children.Remove(_btnAddCustomTag);
        //        _addOrCancelRowLayout.Children.Remove(_btnCancelInsertingCustomTag);
        //        _layout.Children.Remove(_addOrCancelRowLayout);
        //        _layout.Children.Remove(_txtCustomTagName);
        //        _layout.Children.Remove(_btnConfirmCustomTag);
                
        //        _layout.Children.Add(new TagListRow(tag, _creatingNew));

        //        _addOrCancelRowLayout.Children.Add(_btnAddCustomTag);
        //        _addOrCancelRowLayout.Children.Add(_btnCancelInsertingCustomTag);
        //        _layout.Children.Add(_addOrCancelRowLayout);
        //        _layout.Children.Add(_txtCustomTagName);
        //        _layout.Children.Add(_btnConfirmCustomTag);

        //        _txtCustomTagName.IsEnabled = false;
        //        _txtCustomTagName.IsVisible = false;
        //        _txtCustomTagName.Text = string.Empty;

        //        _btnConfirmCustomTag.IsEnabled = false;
        //        _btnConfirmCustomTag.IsVisible = false;

        //        _btnCancelInsertingCustomTag.IsEnabled = false;
        //        _btnCancelInsertingCustomTag.IsVisible = false;

        //        _btnAddCustomTag.IsEnabled = true;
        //        _btnAddCustomTag.BackgroundColor = Color.FromHex("52B296");

        //        if (OnNewTagValidated != null)
        //            OnNewTagValidated(this, tag);
        //    }
        //    else
        //    {
        //        _txtCustomTagName.TextColor = Color.Red;
        //    }
        //}
    }
}
