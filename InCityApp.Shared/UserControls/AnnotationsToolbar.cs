using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Shared.Views
{
	public class AnnotationsToolbar : StackLayout
	{
        private double _width, _height;
        private FixedButton _btnAdd, _btnVote, _btnResetSteps;
        private Label _lblStepsDescription;
        private Label _lblSteps;
        private string _steps = "";

        public System.Action OnButtonAddClicked;
        public System.Action OnButtonResetStepsClicked;

        public string Steps
        {
            get { return _steps; }
            set { _steps = value; OnPropertyChanged(); }
        }

        public AnnotationsToolbar()
		{
           
		}

        public void Initialize()
        {
            Orientation = StackOrientation.Horizontal;
            _btnAdd = new FixedButton 
            { 
                Text = "Aggiungi Annotazione", 
                HorizontalOptions = LayoutOptions.Start, 
                BackgroundColor = Color.FromHex("52B296"),
                WidthRequest = 120,
                HeightRequest = 30
            };
            _btnAdd.Clicked += BtnAdd_Clicked; 

            _btnResetSteps = new FixedButton 
            { 
                Text = "Azzera Passi", 
                HorizontalOptions = LayoutOptions.End,
                WidthRequest = 120,
                HeightRequest = 30
            };

            _btnResetSteps.Clicked += BtnResetSteps_Clicked;
            Children.Add(_btnAdd);
            Children.Add(_btnResetSteps); 

            _lblStepsDescription = new Label() 
            {
                HorizontalOptions = LayoutOptions.End,
                WidthRequest = 120,
                HeightRequest = 30
            };
            _lblStepsDescription.Text = "Passi Fatti : ";
            Children.Add(_lblStepsDescription);

            _lblSteps = new Label() 
            { 
                HorizontalOptions = LayoutOptions.End,
                WidthRequest = 120,
                HeightRequest = 30
            };
            _lblSteps.SetBinding(Label.TextProperty, "Steps");
            _lblSteps.BindingContext = this;
            Children.Add(_lblSteps);
        }

        void BtnAdd_Clicked(object sender, EventArgs e)
        {
            if (OnButtonAddClicked != null)
                OnButtonAddClicked();
        }

        void BtnResetSteps_Clicked(object sender, EventArgs e)
        {
            if (OnButtonResetStepsClicked != null)
                OnButtonResetStepsClicked();
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            _width = width;
            _height = height;
        }
	}
}
