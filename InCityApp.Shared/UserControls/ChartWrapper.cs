﻿using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.UserControls
{
    public class ChartWrapper : StackLayout
    {
        public class DataModel
        {
            public ObservableCollection<ChartDataPoint> HighTemperature;

            public DataModel()
            {
                HighTemperature = new ObservableCollection<ChartDataPoint>();

                HighTemperature.Add(new ChartDataPoint("Jan", 42));
                HighTemperature.Add(new ChartDataPoint("Feb", 44));
                HighTemperature.Add(new ChartDataPoint("Mar", 53));
                HighTemperature.Add(new ChartDataPoint("Apr", 64));
                HighTemperature.Add(new ChartDataPoint("May", 75));
                HighTemperature.Add(new ChartDataPoint("Jun", 83));
                HighTemperature.Add(new ChartDataPoint("Jul", 87));
                HighTemperature.Add(new ChartDataPoint("Aug", 84));
                HighTemperature.Add(new ChartDataPoint("Sep", 78));
                HighTemperature.Add(new ChartDataPoint("Oct", 67));
                HighTemperature.Add(new ChartDataPoint("Nov", 55));
                HighTemperature.Add(new ChartDataPoint("Dec", 45));

            }

        }

        public ChartWrapper()
        {
            StackLayout chartLayout = new StackLayout();

            SfChart chart = new Syncfusion.SfChart.XForms.SfChart();
            chart.Title = new ChartTitle() { Text = "Weather Analysis" };
            //Initializing Primary Axis
            CategoryAxis primaryAxis = new CategoryAxis();
            primaryAxis.Title = new ChartAxisTitle() { Text = "Month" };

            chart.PrimaryAxis = primaryAxis;

            //Initializing Secondary Axis
            NumericalAxis secondaryAxis = new NumericalAxis();
            secondaryAxis.Title = new ChartAxisTitle() { Text = "Temperature" };

            chart.SecondaryAxis = secondaryAxis;
            
            DataModel dataModel = new DataModel();

            chart.Series.Add(new ColumnSeries()
            {
                ItemsSource = dataModel.HighTemperature
            });

            chartLayout.Children.Add(chart);
           
        }

    }
}
