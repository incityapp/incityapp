﻿

using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

using Xamarin.Forms;
﻿using Xamarin.Forms.Maps;

using InCityApp.Entities;
using InCityApp.Pages;
using InCityApp.Shared.Views;
using System.Threading.Tasks;
using InCityApp.DependencyServiceInterfaces;

namespace InCityApp.Views
{
    public class EditableImage : Image
    {
        //public static readonly BindableProperty EditModeProperty = BindableProperty.Create<EditableImage, EditMode>(x => x.CurrentEditMode, EditMode.None);

        public Action<int, int, int, int> OnClick;

        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }

        public double ContainerWidth { get; set; }
        public double ContainerHeight { get; set; }

        public ICommand TapCommand { protected set; get; }

        public enum EditMode
        {
            None,
            HotSpot,
            // DrawLine,
            // etc...
        }

        private EditMode _currentEditMode = EditMode.None;

        public EditMode CurrentEditMode
        {
            get { return _currentEditMode; }
            set 
            { 
                _currentEditMode = value;
                DependencyService.Get<IEditableImage>().SetEditMode(_currentEditMode);
            }
            //get { return (EditMode)base.GetValue(EditModeProperty); }
            //set { base.SetValue(EditModeProperty, value); }
        }

        public void Tap(int screenX, int screenY, int imageX, int imageY)
        {
            if (OnClick != null)
                OnClick(screenX, screenY, imageX, imageY);
        }

        public EditableImage()
        {
            
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            ContainerWidth = width;
            ContainerHeight = height;
        }

    }
}
