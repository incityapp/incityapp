﻿using InCityApp.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Linq;

namespace InCityApp.Views
{
    public class TagListRow : StackLayout
    {
        public EventHandler<int> OnVoted;

        private Tag _tag;
        private FiveStarsRatingView _fiveStarsRating;

        private string[] defaultTags = new string[7] { "Bellezza", "Odore", "Aria", "Food&Drink", "Arte e Cultura", "Vivibilita'", "Acustica" };

        public TagListRow(Tag tag, bool editable)
        {
            _tag = tag;

            Orientation = StackOrientation.Horizontal;
            //Spacing = 5;

            _fiveStarsRating = new FiveStarsRatingView(tag.Vote, editable) { HorizontalOptions = LayoutOptions.End }; // Spacing = 5
            _fiveStarsRating.OnVoted = (object sender, int vote) => { _tag.Vote = vote; if (OnVoted != null) OnVoted(sender, vote); };
            FixedLabel fxLbl = new FixedLabel() { Text = _tag.Name, XAlign = TextAlignment.Center, WidthRequest = 120, VerticalOptions = LayoutOptions.Center };
            
            if (defaultTags.Contains(_tag.Name))
                fxLbl.FontAttributes = FontAttributes.Bold;

            Children.Add(fxLbl);
            Children.Add(_fiveStarsRating);
        }

        public void SetGuessMode(bool guessMode, bool readOnly = false)
        {
            _fiveStarsRating.SetGuessMode(guessMode, readOnly);
        }

        public void ShowVoteOptions(bool show)
        {
            _fiveStarsRating.ShowVoteOptions(show);
        }
    }
}
