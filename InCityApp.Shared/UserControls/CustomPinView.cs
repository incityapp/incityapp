using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;
﻿using Xamarin.Forms.Maps;

using InCityApp.Entities;

namespace InCityApp.Views
{
    public class CustomPinView
    {
        public double Latitude { set; get; }
        public double Longitude { set; get; }
        public CustomPinView(string name, string details, double latitude, double longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }


    }
}
