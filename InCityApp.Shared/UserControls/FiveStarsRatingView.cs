using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace InCityApp.Views
{
    public class FiveStarsRatingView : StackLayout
    {
        public EventHandler<int> OnVoted;

        const string VoteDefaultIcon = "starBigGrey.png";
        const string VoteSelectedIcon = "starBigRed.png";
        const string GuessDefaultIcon = "starBigGrey.png";
        const string GuessSelectedIcon = "starBigRed.png";

        private Dictionary<Guid, int> _buttonsVoteValue = new Dictionary<Guid, int>();
        private bool _editable;
        private bool _editableStateBeforeGuessMode;
        private int _initialRating;
        private bool _guessMode;

        public FiveStarsRatingView(int initialRating, bool editable)
        {
            _initialRating = initialRating;
            _editable = editable;

            if (initialRating < 0)
                initialRating = 0;
            if (initialRating > 5)
                initialRating = 5;

            Orientation = StackOrientation.Horizontal;
            HorizontalOptions = LayoutOptions.Center;
            VerticalOptions = LayoutOptions.Center;

            //int startWidth = InCityApplication.ScreenWidth / 3;
            //startWidth /= 10;
            int i;
            for (i = 0; i < initialRating; i++)
            {
                ImageButton btnStar = new ImageButton { WidthRequest = 48, HeightRequest = 32, Image = (FileImageSource)FileImageSource.FromFile("starBigRed.png"), BackgroundColor = Color.Transparent };
                btnStar.Clicked += btnStar_Clicked;
                Children.Add(btnStar);
                _buttonsVoteValue.Add(btnStar.Id, i + 1);
            }

            for (int j = i; j < 5; j++)
            {
                ImageButton btnStar = new ImageButton { WidthRequest = 48, HeightRequest = 32, Image = (FileImageSource)FileImageSource.FromFile("starBigGrey.png"), BackgroundColor = Color.Transparent };
                btnStar.Clicked += btnStar_Clicked;
                Children.Add(btnStar);
                _buttonsVoteValue.Add(btnStar.Id, j + 1);
            }
        }

        public void SetGuessMode(bool guessMode, bool readOnly = false)
        {
            _guessMode = guessMode;
            if (_guessMode)
            {
                // reset buttons
                List<View> buttons = new List<View>(Children);
                foreach (View button in buttons)
                    (button as ImageButton).Image = (FileImageSource)FileImageSource.FromFile(GuessDefaultIcon);
 
                _editableStateBeforeGuessMode = _editable;
                _editable = !readOnly;
            }
            else
            {
                // reset buttons
                List<View> buttons = new List<View>(Children);
                foreach (View button in buttons)
                    (button as ImageButton).Image = (FileImageSource)FileImageSource.FromFile(VoteDefaultIcon);

                // select the previous buttons
                List<View> buttonsToHighlight = buttons.FindAll(x => _buttonsVoteValue[x.Id] <= _initialRating);

                foreach (View button in buttonsToHighlight)
                    (button as ImageButton).Image = (FileImageSource)FileImageSource.FromFile(VoteSelectedIcon);

                _editable = _editableStateBeforeGuessMode;
            }
        }

        public void ShowVoteOptions(bool show)
        {
            List<View> buttons = new List<View>(Children);

            if (show)
            {
                foreach (View button in buttons)
                    (button as ImageButton).IsVisible = true;
            }
            else
            {
                foreach (View button in buttons)
                    (button as ImageButton).IsVisible = false;
            }
        }

        void btnStar_Clicked(object sender, EventArgs e)
        {
            if (!_editable)
                return;

            string defaultIcon = string.Empty;
            string selectedIcon = string.Empty;

            if (_guessMode)
            {
                defaultIcon = GuessDefaultIcon;
                selectedIcon = GuessSelectedIcon;
            }
            else
            {
                defaultIcon = VoteDefaultIcon;
                selectedIcon = VoteSelectedIcon;            
            }

            // reset buttons
            List<View> buttons = new List<View>(Children);
            foreach (View button in buttons)
                (button as ImageButton).Image = (FileImageSource)FileImageSource.FromFile(defaultIcon);

            // set the selected one
            (sender as ImageButton).Image = (FileImageSource)FileImageSource.FromFile(selectedIcon);
            int vote = _buttonsVoteValue[(sender as ImageButton).Id];

            // select the previous buttons
            List<View> buttonsToHighlight = buttons.FindAll(x => _buttonsVoteValue[x.Id] < vote);

            foreach(View button in buttonsToHighlight)
                (button as ImageButton).Image = (FileImageSource)FileImageSource.FromFile(selectedIcon);

            if (OnVoted != null)
                OnVoted(this, vote);
        }

    }
}
