using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace InCityApp.DataProviders
{
    /// <summary>
    /// Static class for global Data Providers settings.
    /// </summary>
    public static class DataProviderSettings
    {
        // TODO > serialize/deserialize this to/from xml

        // --- Database Global Settings ---
        private static string _databaseFileName = "InCityTestDB";
        private static bool _installDatabaseIfNotExists = true;
        private static bool _overwriteDatabase = true;

        public static string DatabaseFileName { get { return DataProviderSettings._databaseFileName; } }

        public static bool OverwriteDatabase { get { return _overwriteDatabase; } }

        public static bool InstallDatabaseIfNotExists { get { return _installDatabaseIfNotExists; } }

        // --- Web Services Global Settings ---
        // We use two different WS, the first from XTribe to perform the user authentication
        // and the second one to access the app data stored on a remote DB
        

        private static Dictionary<string, CookieContainer> _cookieContainers = new Dictionary<string, CookieContainer>();

        /// <summary>
        /// Cookie containers used to make requests from each WS, the key is the type name(GetType().ToString()) of the class derived from WSDataProvider 
        /// </summary>
        public static Dictionary<string, CookieContainer> CookieContainers
        {
            get { return DataProviderSettings._cookieContainers; }
            set { DataProviderSettings._cookieContainers = value; }
        }


        private static Dictionary<string, bool> _isWSReachable = new Dictionary<string, bool>();
        public static Dictionary<string, bool> IsWSReachable
        {
            get { return DataProviderSettings._isWSReachable; }
            set { DataProviderSettings._isWSReachable = value; }
        }

        private static int _wsTimeout = 5000; 

        /// <summary>
        /// Timeout for the first call to the WS to check if is it available.
        /// In milliseconds.
        /// </summary>
        public static int TimeOut { get { return DataProviderSettings._wsTimeout; } }
      
        private static int[] _retryTime = new int [3] { 2, 2, 2 };

        /// <summary>
        /// Every web-service call is retried three time, 
        /// this array contains the three values representing the time (in seconds) to wait for each call to the web-sevice.
        /// By default is set to two seconds per call.
        /// </summary>
        public static int[] RetryTime { get { return _retryTime; } }

    }
}
