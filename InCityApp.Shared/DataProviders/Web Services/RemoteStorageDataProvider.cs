using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;

using InCityApp.DataInterfaces;
using System.Net;
using System.Threading.Tasks;

namespace InCityApp.DataProviders
{
    public class RemoteStorageDataProvider : WSDataProvider
    {
        // XTribe user auth URL 
        private string _wsTestEndPoint = "http://www.clagame.com/rest/1.2/index.php/Connection/isConnected"; 
        private string _wsBaseEndPoint = "http://www.clagame.com/rest/1.2/index.php/";
        private string _wsAlternativeEndPoint = "http://www.clagame.com/rest/1.2/index.php/";

        public override async Task<bool> Connect()
        {
            BaseEndPoint = _wsBaseEndPoint;
            TestEndPoint = _wsTestEndPoint;
            AlternativeEndPoint = _wsAlternativeEndPoint;

            if (!DataProviderSettings.CookieContainers.ContainsKey(GetType().ToString()))
                DataProviderSettings.CookieContainers.Add(GetType().ToString(), new CookieContainer());

            if (DataProviderSettings.IsWSReachable[this.GetType().ToString()] == true)
                return true;

            Debug.WriteLine("RemoteStorage Data Provider - Ping to Web Service at " + TestEndPoint);

            IsReachable = DataProviderSettings.IsWSReachable[this.GetType().ToString()] = true; //await base.Connect();
            if (IsReachable)
            {
                Debug.WriteLine("RemoteStorage Data Provider - Web Service reachable.");
            }
            else
                Debug.WriteLine("RemoteStorage Data Provider - Web Service doesn't respond.");

            return IsReachable;
        }
    }
}
