using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;

using InCityApp.DataInterfaces;
using System.Net;
using System.Threading.Tasks;

namespace InCityApp.DataProviders
{
    public class XTribeDataProvider : WSDataProvider
    {
        // XTribe user auth URL 
        private string _wsTestEndPoint = "http://lab.xtribe.eu:8080/xtribe_api_2/user/login"; // this should be test end point
        private string _wsBaseEndPoint = "http://lab.xtribe.eu:8080/xtribe_api_2/";
        private string _wsAlternativeEndPoint = "http://lab.xtribe.eu:8080/xtribe_api/";

        public override async Task<bool> Connect()
        {
            if (IsReachable)
                return IsReachable;
            
            BaseEndPoint = _wsBaseEndPoint;
            TestEndPoint = _wsTestEndPoint;
            AlternativeEndPoint = _wsAlternativeEndPoint;

            if (!DataProviderSettings.CookieContainers.ContainsKey(GetType().ToString()))
                DataProviderSettings.CookieContainers.Add(GetType().ToString(), new CookieContainer());

            Debug.WriteLine("XTribe Data Provider - Ping to Web Service at " + TestEndPoint);

            bool ret = true;// await base.Connect();
            if (ret)
            {
                Debug.WriteLine("XTribe Data Provider - Web Service reachable.");

            }
            else
                Debug.WriteLine("XTribe Data Provider - Web Service doesn't respond.");

            return ret;
        }
    }
}
