
using System;
using System.Collections.Generic;
using System.Text;

using InCityApp.DataProviders;
using System.Net;
using System.IO;
using System.Json;
using System.Threading.Tasks;
using System.Diagnostics;

namespace InCityApp.DataInterfaces
{
    public class WSDataProvider : IDataProvider
    {
        public Action<string> OnRequestFailed;

        /// <summary>
        /// Connection Test URL.
        /// Must be set before Connect() it's called.
        /// </summary>
        public string TestEndPoint
        {
            get;
            set;
        }

        public string BaseEndPoint
        {
            get;
            set;
        }

        public string AlternativeEndPoint
        {
            get;
            set;
        }

        public bool IsReachable { get; set; }

        public WSDataProvider()
        {
            Connect();
        }

        public HttpWebRequest MakeHttpPostRequest(string endpoint, PostData postParamaters = null)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(endpoint));
            request.ContentType = "application/x-www-form-urlencoded"; // "application/json; charset=utf-8";  
            request.Method = "POST";
            request.Accept = "application/json";
            request.CookieContainer = DataProviderSettings.CookieContainers[this.GetType().ToString()];
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            request.Timeout = DataProviderSettings.TimeOut;

            if (postParamaters != null && !string.IsNullOrEmpty(postParamaters.GetFormattedString()))
            {
                string postDataString = postParamaters.GetFormattedString();
                byte[] dataStream = Encoding.UTF8.GetBytes(postDataString);
                Stream postDataStream = request.GetRequestStream();
                postDataStream.Write(dataStream, 0, dataStream.Length);
                postDataStream.Close();
            }

            return request;
        }

        public HttpWebRequest MakeHttpMultipartFormPostRquest(string endpoint, Dictionary<string, object> formPostParameters)
        {
            string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
            string contentType = "multipart/form-data; boundary=" + formDataBoundary;

            byte[] formData = GetMultipartFormData(formPostParameters, formDataBoundary);

            return PostForm(endpoint, contentType, formData);
        }

        private HttpWebRequest PostForm(string endpoint, string contentType, byte[] formData)
        {
            HttpWebRequest request = WebRequest.Create(endpoint) as HttpWebRequest;

            if (request == null)
            {
                throw new NullReferenceException("request is not a http request");
            }

            // Set up the request properties.
            request.Method = "POST";
            request.ContentType = contentType;
            //request.UserAgent =
            request.CookieContainer = DataProviderSettings.CookieContainers[this.GetType().ToString()];
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            request.Timeout = DataProviderSettings.TimeOut;
            request.ContentLength = formData.Length;

            // Send the form data to the request.
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(formData, 0, formData.Length);
                requestStream.Close();
            }

            return request;
            //.GetResponse() as HttpWebResponse;
        }

        private byte[] GetMultipartFormData(Dictionary<string, object> postParameters, string boundary)
        {
            Stream formDataStream = new System.IO.MemoryStream();
            bool needsCLRF = false;

            foreach (var param in postParameters)
            {
                // add a CRLF to allow multiple parameters to be added.
                // Skip it on the first parameter, add it to subsequent parameters.
                if (needsCLRF)
                    formDataStream.Write(Encoding.UTF8.GetBytes("\r\n"), 0, Encoding.UTF8.GetByteCount("\r\n"));

                needsCLRF = true;

                if (param.Value is PostFileParameter)
                {
                    PostFileParameter fileToUpload = (PostFileParameter)param.Value;

                    // Add just the first part of this param, since we will write the file data directly to the Stream
                    string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n",
                        boundary,
                        param.Key,
                        fileToUpload.FileName ?? param.Key,
                        fileToUpload.ContentType ?? "application/octet-stream");

                    formDataStream.Write(Encoding.UTF8.GetBytes(header), 0, Encoding.UTF8.GetByteCount(header));

                    // Write the file data directly to the Stream, rather than serializing it to a string.
                    formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
                }
                else
                {
                    string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                        boundary,
                        param.Key,
                        param.Value);
                    formDataStream.Write(Encoding.UTF8.GetBytes(postData), 0, Encoding.UTF8.GetByteCount(postData));
                }
            }

            // Add the end of the request. Start with a newline
            string footer = "\r\n--" + boundary + "--\r\n";
            formDataStream.Write(Encoding.UTF8.GetBytes(footer), 0, Encoding.UTF8.GetByteCount(footer));

            // Dump the Stream into a byte[]
            formDataStream.Position = 0;
            byte[] formData = new byte[formDataStream.Length];
            formDataStream.Read(formData, 0, formData.Length);
            formDataStream.Close();

            return formData;
        }

        public HttpWebRequest MakeHttpPostRequest(string endpoint, string postParameters)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(endpoint));
            request.ContentType = "application/x-www-form-urlencoded"; // "application/json; charset=utf-8";  
            request.Method = "POST";
            request.Accept = "application/json";
            request.CookieContainer = DataProviderSettings.CookieContainers[this.GetType().ToString()];
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            request.Timeout = DataProviderSettings.TimeOut;

            if (!string.IsNullOrEmpty(postParameters))
            {
                string postDataString = postParameters;
                byte[] dataStream = Encoding.UTF8.GetBytes(postDataString);
                Stream postDataStream = request.GetRequestStream();
                postDataStream.Write(dataStream, 0, dataStream.Length);
                postDataStream.Close();
            }

            return request;
        }

        public HttpWebRequest MakeHttpGetRequest(string endpoint, GetData getParamaters = null)
        {
            try
            {
                if (getParamaters != null)
                {
                    string parameters = getParamaters.GetFormattedString();
                    if (getParamaters != null && !string.IsNullOrEmpty(parameters))
                    {
                        endpoint += parameters;
                    }
                }

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(endpoint));
                request.ContentType = "application/x-www-form-urlencoded"; // "application/json; charset=utf-8";  
                request.Method = "GET";
                request.Accept = "application/json";
                request.CookieContainer = DataProviderSettings.CookieContainers[this.GetType().ToString()];
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                request.Timeout = DataProviderSettings.TimeOut;

                return request;
            }
            catch (Exception e)
            {
                Debug.Print(e.Message);
                throw e;
            }
        }

        public async Task<JsonValue> AsyncJSONPostRequest(string endpoint, PostData postParamaters = null)
        {
            HttpWebRequest request = MakeHttpPostRequest(endpoint, postParamaters);
            return await AsyncJSONPostRequest(request);
        }

        public async Task<JsonValue> AsyncJSONMultiformPostRequest(string endpoint, Dictionary<string, object> multiformPostParameters)
        {
            HttpWebRequest request = MakeHttpMultipartFormPostRquest(endpoint, multiformPostParameters);
            return await AsyncJSONPostRequest(request);
        }

        public async Task<JsonValue> AsyncJSONPostRequest(string endpoint, string postParameters)
        {
            HttpWebRequest request = MakeHttpPostRequest(endpoint, postParameters);
            return await AsyncJSONPostRequest(request);
        }

        public async Task<JsonValue> AsyncJSONGetRequest(string endpoint, GetData getParameters = null)
        {
            HttpWebRequest request = MakeHttpGetRequest(endpoint, getParameters);
            return await AsyncJSONGetRequest(request);
        }

        public async Task<JsonValue> AsyncJSONGetRequestArray(string endpoint, GetData getParameters = null)
        {
            HttpWebRequest request = MakeHttpGetRequest(endpoint, getParameters);
            return await AsyncJSONGetRequestArray(request);
        }

        public JsonValue JSONPostRequest(string endpoint, PostData postParamaters = null)
        {
            HttpWebRequest request = MakeHttpPostRequest(endpoint, postParamaters);
            return JSONPostRequest(request);
        }

        public async Task<JsonValue> AsyncJSONPostRequest(HttpWebRequest request)
        {
            WebResponse response = null;
            try
            {
                // send the request to the server and wait for the response
                response = await request.GetResponseAsync();
                
                // get a stream representation of the HTTP web response
                using (Stream stream = response.GetResponseStream())
                {
                    // use this stream to build a JSON document object
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    response.Close();
                    return jsonDoc;
                }
            }
            catch (TimeoutException)
            {

                return null;
            }
            catch (Exception e)
            {
                if (OnRequestFailed != null)
                    OnRequestFailed(e.Message);
                Debug.WriteLine(e.Message);
                return null;
            }
            finally
            {
                if (response != null)
                    response.Close();
            }
        }

        public async Task<JsonValue> AsyncJSONGetRequest(HttpWebRequest request)
        {
            WebResponse response = null;
            try
            {
                // send the request to the server and wait for the response
                response = await request.GetResponseAsync();

                // get a stream representation of the HTTP web response
                using (Stream stream = response.GetResponseStream())
                {
                    // use this stream to build a JSON document object
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    response.Close();
                    return jsonDoc;
                }
            }
            catch (TimeoutException)
            {

                return null;
            }
            catch (Exception e)
            {
                if (OnRequestFailed != null)
                    OnRequestFailed(e.Message);
                Debug.WriteLine(e.Message);
                return null;
            }
            finally
            {
                if (response != null)
                    response.Close();
            }
        }

        //public async Task<JsonValue> AsyncJSONMultiformPostRquest(HttpWebRequest request)
        //{ 
        
        //}

        public async Task<JsonValue> AsyncJSONGetRequestArray(HttpWebRequest request)
        {
            WebResponse response = null;
            try
            {
                // send the request to the server and wait for the response
                response = await request.GetResponseAsync();

                // get a stream representation of the HTTP web response
                using (Stream stream = response.GetResponseStream())
                {
                    // use this stream to build a JSON document object
                    JsonValue jsonDoc = await Task.Run(() => JsonArray.Load(stream));
                    //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    response.Close();
                    return jsonDoc;
                }

            }
            catch (TimeoutException)
            {

                return null;
            }
            catch (Exception e)
            {
                if (OnRequestFailed != null)
                    OnRequestFailed(e.Message);
                Debug.WriteLine(e.Message);
                return null;
            }
            finally
            {
                if (response != null)
                    response.Close();
            }
        }


        public JsonValue JSONPostRequest(HttpWebRequest request)
        {
            WebResponse response = null;
            try
            {
                // send the request to the server and wait for the response
                response = request.GetResponse();

                // get a stream representation of the HTTP web response
                using (Stream stream = response.GetResponseStream())
                {
                    // use this stream to build a JSON document object
                    JsonValue jsonDoc = JsonObject.Load(stream);
                    //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    response.Close();
                    return jsonDoc;
                }

            }
            catch (TimeoutException)
            {

                return null;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
            finally
            {
                if (response != null)
                    response.Close();
            }
        }

        #region IDataProvider Implementation

        public virtual async Task<bool> Connect() 
        {
            if (string.IsNullOrEmpty(TestEndPoint))
                return false;

            // create an HTTP web request using the test end point
            HttpWebRequest request = MakeHttpPostRequest(TestEndPoint);

            // check for any response
            try
            {
                await request.GetResponseAsync();
            }
            catch (TimeoutException)
            {
                IsReachable = false;
                return IsReachable;
            }
            catch (System.Net.WebException e)
            {
                IsReachable = true;
                return IsReachable;
            }

            IsReachable = true;
            return true;
        }

        public virtual void Disconnect() { }

        public virtual async Task<bool> TestConnection()
        {
            HttpWebResponse response = null;
            try
            {
                HttpWebRequest request = MakeHttpGetRequest(TestEndPoint);
                response = (HttpWebResponse)request.GetResponse();
                if (response == null || response.StatusCode != HttpStatusCode.OK)
                {
                    if (response != null)
                        response.Close();
                    return false;
                }
                else
                {
                    response.Close();
                    return true;
                }
            }
            catch (Exception exc)
            {
                Debug.Print(string.Format("TestConnection to {0} failed : {1}", TestEndPoint, exc.Message));
                return false;
            }
            finally
            {
                if (response != null)
                    response.Close();
            }
        }

        #endregion
    }

    public class PostData
    {
        private string _formattedPostData;

        public void AddParameter(string parameterName, string value)
        {
            _formattedPostData += parameterName + "=";
            _formattedPostData += value + "&";
        }

        public string GetFormattedString()
        {
            if (_formattedPostData.Length == 0)
                return null;

            if (_formattedPostData[_formattedPostData.Length - 1] == '&')
                return _formattedPostData.Substring(0, _formattedPostData.Length - 1);
            else
                return _formattedPostData;
        }
    }

    public class PostFileParameter
    {
        public byte[] File { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }

        public PostFileParameter(byte[] file) : this(file, null) { }

        public PostFileParameter(byte[] file, string filename) : this(file, filename, null) { }

        public PostFileParameter(byte[] file, string filename, string contenttype)
        {
            File = file;
            FileName = filename;
            ContentType = contenttype;
        }
    }

    public class GetData
    {
        private string _formattedPostData;

        public void AddParameter(string parameterName, string value)
        {
            if (string.IsNullOrWhiteSpace(_formattedPostData))
                _formattedPostData += "/";

            _formattedPostData += parameterName + "/";
            _formattedPostData += value + "/";
        }

        public string GetFormattedString()
        {
            if (_formattedPostData.Length == 0)
                return null;

            string x = _formattedPostData.Replace(',', '.');
            return x.Substring(0, x.Length - 1);
        }
    }

}
