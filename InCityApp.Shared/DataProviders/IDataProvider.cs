using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace InCityApp.DataProviders
{
    public interface IDataProvider
    {
        Task<bool> Connect();
        void Disconnect();
    }
}
