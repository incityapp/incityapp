
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Text;
//using System.Diagnostics;

//using SQLite.Net;
//using SQLiteNetExtensions;
//using SQLite.Net.Interop;
//using SQLiteNetExtensions.Extensions;
//#if __ANDROID__
//using SQLite.Net.Platform.XamarinAndroid;
//#else
//using SQLite.Net.Platform.XamarinIOS;
//#endif

//using InCityApp.DataProviders;
//using System.Threading.Tasks;

//namespace InCityApp.DataProviders
//{
//    public class SQLiteDataProvider : DBDataProvider
//    {
//        private static SQLiteConnection _db;

//        public SQLiteConnection DB
//        {
//            get { return SQLiteDataProvider._db; }
//        }

//        ISQLitePlatform PlatformInterface
//        {
//            get
//            {
//#if __IOS__
//                return new SQLitePlatformIOS();
//#elif __ANDROID__
//                return new SQLitePlatformAndroid();
//#elif WINDOWS_PHONE
//                        return null;
//#endif
//            }
//        }

//        public override async Task<bool> Connect()
//        {
////            Debug.WriteLine("SQLite Data Provider -  Attempting to connect to DB...");
//            try
//            {
//                if (_db == null)
//                {
//                    if (DataProviderSettings.InstallDatabaseIfNotExists)
//                    {
//                        // TODO > Avoid DataProviderSettings.OverwriteDatabase : find a way to get creation datetime of the DB in the assets folder,
//                        //        and compare it to datetime of the DB already installed.
//                        if (!File.Exists(DatabasePath) || DataProviderSettings.OverwriteDatabase)
//                        {
//                            Debug.WriteLine("SQLite Data Provider - Database not found on device.");
//                            Debug.WriteLine("SQLite Data Provider - Attempting to install the database from Assets.");
//                            if (InstallDatabase())
//                            {
//                                Debug.WriteLine("SQLite Data Provider - Database installed on device.");
//                            }
//                            else
//                            {
//                                Debug.WriteLine("SQLite Data Provider - Unable to install the database on device.");
//                                return false;
//                            }
//                        }

//                        Debug.WriteLine("SQLite Data Provider - Connection estabilished.");
//                        _db = new SQLiteConnection(PlatformInterface, DatabasePath);
//                        return true;
//                    }
//                    else
//                    {
//                        Debug.WriteLine("SQLite Data Provider - Database not found on device.");
//                        Debug.WriteLine("SQLite Data Provider - Connection failed.");
//                        return false;
//                    }
//                }
//                else
//                {
////                    Debug.WriteLine("SQLite Data Provider - Data Provider already connected to the database.");
//                    return true;
//                }
//            }
//            catch (Exception e)
//            {
//                Debug.WriteLine("SQLite Data Provider - Failure : " + e.Message);
//                return false;
//            }
//            finally
//            {
//                if (_db == null)
//                    Debug.WriteLine("SQLite Data Provider - Unable to connect to SQLite Database.");
//            }
//        }

//    } // end class

//} // end namespace
