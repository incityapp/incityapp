//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Text;
//using System.Diagnostics;
//using System.Reflection;

//using InCityApp.Helpers;
//using SQLite.Net.Interop;
//using System.Threading.Tasks;

//#if __ANDROID__
//using SQLite.Net.Platform.XamarinAndroid;
//#else
//using SQLite.Net.Platform.XamarinIOS;
//#endif

//namespace InCityApp.DataProviders
//{
//    public class DBDataProvider : IDataProvider
//    {
//        protected bool _isDBInstalled;

//        public bool IsDatabaseInstalled
//        {
//            get { return _isDBInstalled; }
//        }

//        public string DatabasePath
//        {
//            get
//            {
//                var sqliteFilename = DataProviderSettings.DatabaseFileName;
//                var path = string.Empty;
//#if __IOS__
//                string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
//                string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
//                path = Path.Combine(libraryPath, sqliteFilename);
//#elif __ANDROID__
//                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
//                path = Path.Combine(documentsPath, sqliteFilename);
//#elif WINDOWS_PHONE
//                // WinPhone
//                path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);
//#endif
//                return path;
//            }
//        }

//        public DBDataProvider()
//        {
//            Connect();
//        }

//        #region IDataProvider Implementation

//        public virtual async Task<bool> Connect() { return false; }
//        public virtual void Disconnect() { }

//        #endregion

//        public virtual bool InstallDatabase()
//        {
//            Stream streamDB = ResourceLoader.GetEmbeddedResourceStream(Assembly.GetAssembly(typeof(ResourceLoader)), DataProviderSettings.DatabaseFileName);
//            FileStream streamOutput = new FileStream(DatabasePath, FileMode.OpenOrCreate, FileAccess.Write);

//            if (streamDB != null && streamOutput != null)
//            {
//                if (CopyDatabaseToDevice(streamDB, streamOutput))
//                    _isDBInstalled = true;
//                else
//                    _isDBInstalled = false;
//            }

//            return _isDBInstalled;
//        }

//        private bool CopyDatabaseToDevice(Stream streamDB, Stream streamOutput)
//        {
//            bool _isSuccess = false;
//            int _length = 256;
//            Byte[] _buffer = new Byte[_length];

//            try
//            {
//                // read all
//                int _bytesRead = streamDB.Read(_buffer, 0, _length);
//                while (_bytesRead > 0)
//                {
//                    // write back to the output stream
//                    streamOutput.Write(_buffer, 0, _bytesRead);
//                    _bytesRead = streamDB.Read(_buffer, 0, _length);
//                }
//                _isSuccess = true;
//            }
//            catch (Exception exc)
//            {
//                Debug.WriteLine(exc.Message);
//            }
//            finally
//            {
//                streamDB.Close();
//                streamOutput.Close();
//            }

//            return _isSuccess;
//        }
//    }
//}
