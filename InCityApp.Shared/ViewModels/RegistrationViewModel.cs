using InCityApp.DataModels;
using InCityApp.Entities;
using InCityApp.Pages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace InCityApp.Shared.ViewModels
{
    public class RegistrationViewModel : BaseViewModel
    {
        public RegistrationViewModel(BasePage page)
        {
            PageRef = page;
        }

        public const string UsernamePropertyName = "Username";
        private string _username = string.Empty;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value, UsernamePropertyName); }
        }

        public const string PasswordPropertyName = "Password";
        private string _password = string.Empty;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value, PasswordPropertyName); }
        }

        public const string ConfPasswordPropertyName = "ConfPassword";
        private string _confPassword = string.Empty;
        public string ConfPassword
        {
            get { return _confPassword; }
            set { SetProperty(ref _confPassword, value, ConfPasswordPropertyName); }
        }

        public const string MailPropertyName = "Mail";
        private string _mail = string.Empty;
        public string Mail
        {
            get { return _mail; }
            set { SetProperty(ref _mail, value, MailPropertyName); }
        }

        public const string ConfirmMailPropertyName = "ConfirmMail";
        private string _confirmMail = string.Empty;
        public string ConfirmMail
        {
            get { return _confirmMail; }
            set { SetProperty(ref _confirmMail, value, ConfirmMailPropertyName); }
        }

        public const string AgreementPropertyName = "Agreement";
        private bool _agreement = false;
        public bool Agreement
        {
            get { return _agreement; }
            set { SetProperty(ref _agreement, value, AgreementPropertyName); }
        }

        public const string DisclaimerPropertyName = "Disclaimer";
        private string _disclaimer = string.Empty;
        public string Disclaimer
        {
            get { return _disclaimer; }
            set { SetProperty(ref _disclaimer, value, DisclaimerPropertyName); }
        }

        public const string LogMessagePropertyName = "LogMessage";
        private string _logMessage = string.Empty;
        public string LogMessage
        {
            get { return _logMessage; }
            set { SetProperty(ref _logMessage, value, LogMessagePropertyName); }
        }

        public const string RegisterCommandPropertyName = "RegisterCommand";
        private Command _registerCommand;
        public Command RegisterCommand
        {
            get
            {
                return _registerCommand ?? (_registerCommand = new Command(async () => await ExecuteRegisterCommand()));
            }
        }

        protected async Task ExecuteRegisterCommand()
        {
            LogMessage = "";

            if (Password == "" || ConfPassword == "" || ConfirmMail == "" || ConfPassword == "" || Mail == "" || Username == "")
            {
                LogMessage = "Tutti i campi sono obligatori per poter procedere";
                return;
            }

            if (ConfPassword != Password)
            {
                LogMessage = "La password e la conferma password non coincidono";
                return;
            }

            if (ConfirmMail != Mail)
            {
                LogMessage = "L' e-mail e la conferma dell'e-mail non coincidono";
                return;
            }

            if (!Agreement)
            {
                LogMessage = "Devi accettare le condizioni per poter andare avanti";
                return;
            }

            if (IsLoading)
                return;
            
            IsLoading = true;

            User.AuthorizationData regData = await UsersModel.Register(_username, _mail, _confirmMail, _password, _agreement);
            switch (regData.State)
            { 
                case User.AuthorizationState.OK:
                    LogMessage = "";
                    
                    IsLoading = false;
                    PageRef.ShowRegistrationSuccessPopup();

                    // we use this flag in the native project to know if we are in the login page,
                    // to disable the hardware back button on android
                    InCityApplication.InRegistrationPage = false;
                    break;

                case User.AuthorizationState.Error:
                    IsLoading = false;
                    LogMessage = regData.ErrorMessage;
                    break;

                case User.AuthorizationState.CannotConnect:
                    IsLoading = false;
                    LogMessage = "Impossibile connettersi al servizio di registrazione. Verifica la tua connessione";
                    Debug.WriteLine("Unable to connect to registration service. Please check your internet connection.");
                    break;
            }
        }
    }
}
