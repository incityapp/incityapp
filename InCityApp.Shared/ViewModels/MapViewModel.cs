using System.Collections.Generic;
using System.Linq;
using InCityApp.Entities;
using Xamarin.Forms.Maps;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using InCityApp.Services;
using InCityApp.DataModels;
using System.Diagnostics;
using System;
using System.Threading.Tasks;

namespace InCityApp.Shared.ViewModels
{
    public class MapViewModel : BaseViewModel
    {
        public static readonly Position NullPosition = new Position(0, 0);
        private List<Annotation> _dataModels = new List<Annotation>();

        public List<Annotation> Models { get { return _dataModels; } }

        public MapViewModel()
        {
            Title = "Map";
            //Icon = "map.png";
        }

        public async Task LoadAnnotationsInArea()
        {
            _dataModels.Clear();
            _dataModels = new List<Annotation>();
            try
            {
                _dataModels = await AnnotationsModel.GetAllInUserArea(); 
            }
            catch (Exception exc)
            {
                Debug.WriteLine("MapViewModel : Cannot retrieve annotations for current user. " + exc.Message);
            } 
        }

        public async Task<List<Pin>> GetPins()
        {
            if (UsersModel.CurrentUser != null)
            {
                try
                {
                    await LoadAnnotationsInArea();
                }
                catch (Exception e)
                {

                }
            }

            //if (UserLocator.Instance != null && UserLocator.Instance.CurrentLocation != null)
            //    LoadAnnotationsInArea();

            var pins = _dataModels.Select(model =>
            {
                var item = (Annotation)model;
               // var address = item.Address;

                var position = new Position(item.Latitude, item.Longitude); //address != null ? new Position(address.Latitude, address.Longitude) : NullPosition;
                var pin = new Pin
                    {
                        Type = PinType.Place,
                        Position = position,
                        Label = item.Title
                 //       Address = address.ToString()
                    };
                return pin;
            }).ToList();

            return pins; 
        }
    }
}
