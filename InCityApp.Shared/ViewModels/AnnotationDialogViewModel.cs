using System.Collections.Generic;
using System.Linq;
using InCityApp.Entities;
using Xamarin.Forms.Maps;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using InCityApp.Services;
using InCityApp.DataModels;
using System.Diagnostics;
using System;
using System.ComponentModel;
using XLabs.Forms.Charting.Controls;
using System.Threading.Tasks;
using InCityApp.Views;
using InCityApp.Pages;

namespace InCityApp.Shared.ViewModels
{
    public class AnnotationDialogViewModel : BaseViewModel
    {
        //// This is a violation of the MVVM design pattern, 
        //// but the PopUp in Xamarin cannot be handled by the navigation system 
        //// (by default there isn't a custom popup in xamarin, so I took the only valid implematation I found, from Xamarin.Forms.Labs project),
        //// we need to keep the popup definition in the "view side" and then we need a reference to the view to call the PopUp.
        //private AnnotationDialog _view;

        private Annotation _annotation;
        //private EventHandler<int> _lastPopupHandler;
        //private string _lastPopupTitle;
        //private bool _voteChanged;

        //private string _vote;
        //public const string VotePropertyName = "Vote";
        //public string Vote
        //{
        //    get { return _vote; }
        //    set { SetProperty(ref _vote, value, VotePropertyName); }
        //}

        private ImageSource _photoImageSource;
        public const string PhotoImageSourcePropertyName = "PhotoImageSource";
        public ImageSource PhotoImageSource
        {
            get { return _photoImageSource; }
            set { SetProperty(ref _photoImageSource, value, PhotoImageSourcePropertyName); }
        }

        //public CustomPopupLayout PopUpLayout { get; set; }

        public AnnotationDialogViewModel(AnnotationDialog view, Annotation annotation, bool createNew)
        {
            //_view = view;
            _annotation = annotation;
            //if (string.IsNullOrWhiteSpace(_annotation.Title))
            //    Title = "No Title";
            //else
            //    Title = _annotation.Title;
        }

        //public const string VoteAnnotationCommandPropertyName = "VoteAnnotationCommand";
        //private Command _voteAnnotationCommand;
        //public Command VoteAnnotationCommand
        //{
        //    get
        //    {
        //        return _voteAnnotationCommand ?? (_voteAnnotationCommand = new Command(() => ExecuteVoteAnnotationCommand()));
        //    }
        //}

        //protected void ExecuteVoteAnnotationCommand()
        //{
        //    _view.OpenVotePopUp(HandleOnAnnotationVoted, "Vote this annotation :");
        //}

        //private void HandleOnAnnotationVoted(object sender, int vote)
        //{
        //    if (PopUpLayout.IsPopupActive)
        //        PopUpLayout.DismissPopup();

        //    _annotation.Vote = vote;
        //    int average = AnnotationsModel.ComputeAnnotationVotesAverage(_annotation);
        //    _annotation.VotesAverage = average;
        //    _vote = string.Format("({0})", average);

        //    _voteChanged = true;
        //}

    }
}
