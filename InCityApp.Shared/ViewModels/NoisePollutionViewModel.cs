using System.Collections.Generic;
using System.Linq;
using InCityApp.Entities;
using Xamarin.Forms.Maps;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using InCityApp.Services;
using InCityApp.DataModels;
using System.Diagnostics;
using System;
using System.ComponentModel;
using XLabs.Forms.Charting.Controls;
using Syncfusion.SfChart.XForms;
using System.Threading;
using System.Collections.Concurrent;

namespace InCityApp.Shared.ViewModels
{
    public class NoisePollutionViewModel : BaseViewModel
    {
        public object Locker = new object();
        public EventHandler<List<ChartDataPoint>> OnSeriesChanged;
        public EventHandler<List<ChartDataPoint>> OnInitialSeriesPrepared;

        private List<ChartDataPoint> updatePoints = new List<ChartDataPoint>();

        private static List<ChartDataPoint> _originalDataPoints = new List<ChartDataPoint>();

        private static int _oldMaxSamplesPerSeries = 180;
        private static int _maxSamplesPerSeries = 180;
        private static int  _maxSamples = 180;

        public static int XAxisInterval { get; set; }

        public static int MaxSamplesPerSeries 
        {
            get { return _maxSamplesPerSeries; } 
            set
            {
                _oldMaxSamplesPerSeries = _maxSamplesPerSeries;
                _maxSamplesPerSeries = value;
            }
        }

        public static int MaxSamples
        {
            get { return _maxSamples; }
            set
            {
                _maxSamples = value;
            }
        }

        public const string DataModelsPropertyName = "DataModels";
        public List<ChartDataPoint> DataPoints
        {
            get { return _originalDataPoints; }
            set { SetProperty(ref _originalDataPoints, value, DataModelsPropertyName); }
        }

        public NoisePollutionViewModel()
        {
            XAxisInterval = 10;
        }

        public void Init(bool firstTime = true)
        {
            if (OnInitialSeriesPrepared != null)
            {
                List<ChartDataPoint> oldData = new List<ChartDataPoint>();
                if (AudioRecorder.AvaragedChartDataPoints.Count > 0)
                    oldData = AudioRecorder.AvaragedChartDataPoints.ToList();
                else if (AudioRecorder.AvaragedChartDataPoints.Count > MaxSamplesPerSeries)
                    oldData = AudioRecorder.AvaragedChartDataPoints.ToList().GetRange(AudioRecorder.AvaragedChartDataPoints.Count - MaxSamplesPerSeries, MaxSamplesPerSeries);

                OnInitialSeriesPrepared(null, oldData);
            }

            if ( AudioRecorder.OnOneSecondVolume != null)
                AudioRecorder.OnOneSecondVolume -= OnOneSecondVolume;

            if (AudioRecorder.OnOneSecondVolume == null)
                AudioRecorder.OnOneSecondVolume += OnOneSecondVolume;
        }

        public void ReInit()
        {
            Init(false);
        }

        private void OnOneSecondVolume(object sender, double decibel)
        {
            //lock (Locker)
            {
                ComputeAvaragedData(decibel);
            }
        }

        public void ComputeAvaragedData(double decibel)
        {
            //lock (Locker)
            {
                if (AudioRecorder.AvaragedChartDataPoints.Count >= _maxSamples)
                    AudioRecorder.AvaragedChartDataPoints.RemoveAt(0);

                if (updatePoints.Count >= _maxSamplesPerSeries)
                    updatePoints.RemoveAt(0);

                if (_originalDataPoints.Count >= _maxSamples)
                    _originalDataPoints.RemoveAt(0);

                if (_maxSamplesPerSeries != _oldMaxSamplesPerSeries && 
                    _maxSamplesPerSeries < _oldMaxSamplesPerSeries &&
                    _originalDataPoints.Count > _maxSamplesPerSeries)
                {
                    int lastToKeep = _originalDataPoints.Count - _maxSamplesPerSeries;
                    _originalDataPoints.RemoveRange(0, lastToKeep);   
                }

                if (_originalDataPoints.Count >= 5)
                {
                    List<ChartDataPoint> lastFourValues = _originalDataPoints.GetRange(_originalDataPoints.Count - 5, 4);
                    double avgDecibel = 0;
                    foreach (ChartDataPoint dp in lastFourValues)
                        avgDecibel += dp.YValue;

                    avgDecibel += decibel;
                    avgDecibel /= 5;

                    _originalDataPoints.Add(new ChartDataPoint("", decibel));

                    AudioRecorder.AvaragedChartDataPoints.Add(new ChartDataPoint("", avgDecibel));

                    List<ChartDataPoint> avarageSubRangePoints = new List<ChartDataPoint>();
                    updatePoints.Clear();

                    int count = AudioRecorder.AvaragedChartDataPoints.Count;

                    if (AudioRecorder.AvaragedChartDataPoints.Count > 0)
                        avarageSubRangePoints = AudioRecorder.AvaragedChartDataPoints;
                    else if (AudioRecorder.AvaragedChartDataPoints.Count > _maxSamplesPerSeries)
                        avarageSubRangePoints = AudioRecorder.AvaragedChartDataPoints.GetRange(AudioRecorder.AvaragedChartDataPoints.Count - _maxSamplesPerSeries, _maxSamplesPerSeries);

                    foreach (ChartDataPoint dp in avarageSubRangePoints)
                        updatePoints.Add(new ChartDataPoint(dp.XValue, dp.YValue));

                    if (updatePoints.Count > _maxSamplesPerSeries)
                    {
                        int lastToKeepAvg = updatePoints.Count - _maxSamplesPerSeries;
                        updatePoints.RemoveRange(0, lastToKeepAvg);
                    }

                    updatePoints[0] = new ChartDataPoint("-" + MaxSamplesPerSeries.ToString(), updatePoints[0].YValue);

                    int n = 1;
                    for (int i = 0; i < updatePoints.Count; i++)
                    {
                        if (i % XAxisInterval == 0)
                        {
                            string labelValue = ((MaxSamplesPerSeries - (n * XAxisInterval)) + XAxisInterval).ToString();
                            if (labelValue == "0")
                                labelValue = "";
                            else
                                labelValue = "-" + labelValue + "s";

                            updatePoints[i] = new ChartDataPoint(labelValue, updatePoints[i].YValue);
                            n++;
                        }
                        else
                            updatePoints[i] = new ChartDataPoint("", updatePoints[i].YValue);
                    }

                    // send a clone to prevent the same reference to be used from multiple threads
                    List<ChartDataPoint> updatePointsClone = new List<ChartDataPoint>();
                    foreach (ChartDataPoint dp in updatePoints)
                        updatePointsClone.Add(new ChartDataPoint(dp.XValue, dp.YValue));

                    if (OnSeriesChanged != null)
                        OnSeriesChanged(null, updatePoints);

                    //Debug.Print("Last Decibel Value : " +  updatePoints[updatePoints.Count - 1].YValue.ToString());

                }
                else
                {
                    ChartDataPoint dataPoint = new ChartDataPoint("", decibel);
                    _originalDataPoints.Add(dataPoint);
                }
            }
        }
    }
}
