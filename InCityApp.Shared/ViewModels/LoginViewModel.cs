using InCityApp.DataModels;
using InCityApp.Entities;
using InCityApp.Pages;
using InCityApp.Shared.Pages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace InCityApp.Shared.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public System.Action OnLoginCompleted;

        public LoginViewModel(BasePage refPage)
        {
           PageRef = refPage;
        }

        public const string UsernamePropertyName = "Username";
        private string _username = string.Empty;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value, UsernamePropertyName); }
        }

        public const string PasswordPropertyName = "Password";
        private string _password = string.Empty;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value, PasswordPropertyName); }
        }

        public const string LoginCommandPropertyName = "LoginCommand";
        private Command _loginCommand;
        public Command LoginCommand
        {
            get
            {
                return _loginCommand ?? (_loginCommand = new Command(async () => await ExecuteLoginCommand()));
            }
        }

        public const string RegistrationCommandPropertyName = "RegisterCommand";
        private Command _registerCommand;
        public Command RegisterCommand
        {
            get
            {
                return _registerCommand ?? (_registerCommand = new Command(() => ExecuteRegisterCommand()));
            }
        }

        protected async Task ExecuteLoginCommand()
        {
            Subtitle = "";

            if (Username == "" || Password == "")
            {
                Subtitle = "Compila i campi per procedere";
                return;
            }

            if (IsLoading)
                return;

            IsLoading = true;

            User.AuthorizationData authData = await UsersModel.Login(_username, _password);
            switch (authData.State)
            {
                case User.AuthorizationState.OK:
                    Subtitle = "";

                    IsLoading = false;

                    InCityApp.InCityApplication.App.HandleOnUserVerified( true );

                    // we use this flag in the native project to know if we are in the login page,
                    // to disable the hardware back button on android
                    InCityApplication.InLoginPage = false;
                    
                    break;
                case User.AuthorizationState.Error:
                    IsLoading = false;
                    Subtitle = authData.ErrorMessage;
                    break;
                case User.AuthorizationState.CannotConnect:
                    IsLoading = false;
                    Subtitle = "Impossibile connettersi al servizio di registrazione. Verifica la tua connessione";
                    Debug.WriteLine("Unable to connect to login service. Please check your internet connection.");
                    break;
            }
        }

        protected void ExecuteRegisterCommand()
        {
            InCityApplication.InLoginPage = false;

            InCityApp.InCityApplication.App.SetRegistrationAsRootPage();
        }
    }
}
