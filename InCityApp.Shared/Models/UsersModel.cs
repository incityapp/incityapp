
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

using InCityApp.Entities;
using InCityApp.DataInterfaces;
using InCityApp.Helpers;
using Xamarin.Forms;
using InCityApp.DependencyServiceInterfaces;

namespace InCityApp.DataModels
{
    public static class UsersModel 
    {
        static object _lockable = new object();

        static User _currentUser = null;

        public static Action<bool> OnUserVerified;

        public static User CurrentUser
        {
            get 
            {
                return _currentUser; 
            }
        }

        public static async Task<User.AuthorizationData> Login(string username, string password)
        {
            UsersDataInterface usersData = new UsersDataInterface();

            string deviceId = string.Empty;
            try
            {
                deviceId = DependencyService.Get<IUniqueIdGenerator>().GetDeviceUniqueId();
            }
            catch (Exception exc)
            {
                Debug.Print(exc.Message);
                return null;
            }

            User.AuthorizationData authData = await usersData.Login(deviceId, username, password);

            if (authData.State == InCityApp.Entities.User.AuthorizationState.OK )
                _currentUser = new User() { Id = Settings.UserId, Username = Settings.UserName, Token = Settings.UserToken };

            return authData;
        }

        public static async void VerifyAccessToken(string userId)
        {
            if (string.IsNullOrEmpty(Settings.UserToken))
            {
                if (OnUserVerified != null)
                    OnUserVerified(false);
                return;
            }

            string deviceId = string.Empty;
            try
            {
                deviceId = DependencyService.Get<IUniqueIdGenerator>().GetDeviceUniqueId();
            }
            catch (Exception exc)
            {
                Debug.Print(exc.Message);
                return;
            }

            UsersDataInterface usersData = new UsersDataInterface();
            bool ret = await usersData.IsUserLogged(deviceId, userId, Settings.UserToken);

            //if (ret)
                _currentUser = new User() { Id = Settings.UserId, Username = Settings.UserName , Token = Settings.UserToken };

            if (OnUserVerified != null)
                OnUserVerified(ret);
        }

        //public static async Task<User.AuthorizationData> Login(string username, string password)
        //{
        //    UsersDataInterface usersData = new UsersDataInterface();
        //    User.AuthorizationData authData = await usersData.Authenticate(username, password);

        //    if (!string.IsNullOrEmpty(authData.UserId))
        //    {
        //        try
        //        {
        //            _currentUser = await usersData.GetById(authData.UserId);
        //        }
        //        catch
        //        {
        //            //authData.State = User.AuthorizationState.Error;
        //            //return authData;
        //            _currentUser = null;
        //        }

        //        if (_currentUser == null)
        //        {
        //            User newUser = new User() { Id = authData.UserId, Username = username, Token = Settings.UserToken };

        //            try
        //            {
        //                Insert(newUser);
        //            }
        //            catch (Exception e)
        //            {
        //                Debug.WriteLine(string.Format("Insert of user {0} failed : {1}", username, e.Message));
        //                return authData;
        //            }

        //            _currentUser = newUser;
        //        }
        //    }

        //    return authData;
        //}

        public static async Task<int> GetUserScore()
        {
            if (_currentUser == null)
                return -1;

            UsersDataInterface userData = new UsersDataInterface();
            return await userData.GetUserScore(_currentUser.Id);
        }

        public static async Task<User.AuthorizationData> Register(string userName, string mail, string confirmMail, string password, bool agreement)
        {
            UsersDataInterface userData = new UsersDataInterface();
            return await userData.Register(userName, mail, confirmMail, password, agreement);            
        }

        public static async Task<bool> Logout()
        {
            string deviceId = string.Empty;
            try
            {
                deviceId = DependencyService.Get<IUniqueIdGenerator>().GetDeviceUniqueId();
            }
            catch (Exception exc)
            {
                Debug.Print(exc.Message);
                return false;
            }

            UsersDataInterface userData = new UsersDataInterface();
            return await userData.Logout(deviceId, _currentUser.Id, _currentUser.Token);
        }

        //public static async Task<User.AuthorizationData> CheckIfAuthenticated(string userToken)
        //{
        //    UsersDataInterface usersData = new UsersDataInterface();
        //    return await usersData.CheckIfAuthenticated(userToken);
        //}

        //public static void Insert(User user)
        //{
        //    lock (_lockable)
        //    {
        //        UsersDataInterface userData = new UsersDataInterface();
        //        userData.Insert(user);
        //    }
        //}

        //public static async Task<User> GetCurrentUser()
        //{
        //    if (_currentUser == null)
        //    {
        //        string userToken = Settings.UserToken;
        //        // TODO > enable this block when the user authentication is fixed on XTribe or from us.
        //        //if (!string.IsNullOrEmpty(userToken))
        //        //{
        //        //    User.AuthorizationData authData = await CheckIfAuthenticated(userToken);
        //        //    switch (authData.State)
        //        //    {
        //        //        case User.AuthorizationState.OK: _currentUser = authData.AlreadyLoggedUser; break;
        //        //        case User.AuthorizationState.Error: break;
        //        //        case User.AuthorizationState.CannotConnect: break;
        //        //    }
        //        //}
        //    }

        //    return _currentUser; 
        //}

    }
}
