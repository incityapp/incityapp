﻿using InCityApp.DataInterfaces;
using InCityApp.DataModels;
using InCityApp.Entities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace InCityApp.Models
{
    public class SoundsModel
    {
        public static async Task<List<SoundAtPosition>> GetAllByDate(DateTime date)
        {
            if (UsersModel.CurrentUser == null)
                throw new InvalidOperationException("CurrentUser is NULL.");

            SoundsDataInterface soundData = new SoundsDataInterface();
            try
            {
                List<SoundAtPosition> sounds = await soundData.GetAllByData(UsersModel.CurrentUser.Id, date);
                return sounds;
            }
            catch
            {
                throw new InvalidOperationException("Unable to retrieve volume data from web-service");
            }
        }

        public static async Task<List<SoundAtPosition>> GetLastFiveMinutes()
        {
            if (UsersModel.CurrentUser == null)
                throw new InvalidOperationException("CurrentUser is NULL.");

            SoundsDataInterface soundData = new SoundsDataInterface();
            try
            {
                List<SoundAtPosition> sounds = await soundData.GetLastFiveMinutes(UsersModel.CurrentUser.Id);
                return sounds;
            }
            catch
            {
                throw new InvalidOperationException("Unable to retrieve sound data from web-service");
            }
        }

        public static async Task<SoundStats> GetStats(DateTime date)
        {
            if (UsersModel.CurrentUser == null)
                throw new InvalidOperationException("CurrentUser is NULL.");

            SoundsDataInterface soundData = new SoundsDataInterface();
            SoundStats soundStats = await soundData.GetStats(UsersModel.CurrentUser.Id, date);
            if (soundStats == null)
                Debug.Print("GetStats >> Unable to retrieve sound statistic data from web-service");

            return soundStats;
        }

        public static async Task<double> GetMaxVolumeByDate(DateTime date)
        {
            if (UsersModel.CurrentUser == null)
                throw new InvalidOperationException("CurrentUser is NULL.");

            SoundsDataInterface soundData = new SoundsDataInterface();
            double volume = await soundData.GetMaxVolumeByDate(UsersModel.CurrentUser.Id, date);
            if (volume < 0)
                throw new InvalidOperationException("Unable to retrieve volume data from web-service");

            return volume;
        }

        public static async Task<double> GetAvarageVolumeByDate(DateTime date)
        {
            if (UsersModel.CurrentUser == null)
                throw new InvalidOperationException("CurrentUser is NULL.");

            SoundsDataInterface soundData = new SoundsDataInterface();
            double volume = await soundData.GetAvarageVolumeByDate(UsersModel.CurrentUser.Id, date);
            if (volume < 0)
                throw new InvalidOperationException("Unable to retrieve volume data from web-service");

            return volume;
        }

        public static async Task<double> GetMinVolumeByDate(DateTime date)
        {
            if (UsersModel.CurrentUser == null)
                throw new InvalidOperationException("CurrentUser is NULL.");

            SoundsDataInterface soundData = new SoundsDataInterface();
            double volume = await soundData.GetMinVolumeByDate(UsersModel.CurrentUser.Id, date);
            if (volume < 0)
                throw new InvalidOperationException("Unable to retrieve volume data from web-service");

            return volume;
        }

        public static async Task<bool> Insert(SoundAtPosition[] soundSamples)
        {
            if (UsersModel.CurrentUser == null)
                return false;

            SoundsDataInterface soundData = new SoundsDataInterface();
            return await soundData.Insert(soundSamples);
        }
    }
}
