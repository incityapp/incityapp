
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using InCityApp.Entities;
using InCityApp.DataInterfaces;
using System.Threading.Tasks;
using InCityApp.DataModels;

namespace InCityApp.Models
{
    public static class StepsModel
    {
        static object _lock = new object();

		public static async Task<List<StepSession>> GetStepSession(DateTime time)
		{
            if (UsersModel.CurrentUser == null)
                return null;

            StepsDataInterface stepsData = new StepsDataInterface();
            return await stepsData.GetByDateTime(UsersModel.CurrentUser.Id, time);
		}

        //public static List<StepSession> GetAll()
        //{
        //    lock (_lock)
        //    {
        //        StepsDataInterface stepsData = new StepsDataInterface();
        //        return stepsData.GetAll();
        //    }
        //}

        //public static List<StepSession> GetRange(int count)
        //{
        //    lock (_lock)
        //    {
        //        StepsDataInterface stepsData = new StepsDataInterface();
        //        return stepsData.GetRange(count);
        //    }
        //}
		
		public static async Task<bool> Save(StepSession stepSession, bool insert)
		{
            if (UsersModel.CurrentUser == null)
                return false;

            StepsDataInterface stepsData = new StepsDataInterface();
            if (insert)
                return await stepsData.Insert(UsersModel.CurrentUser.Id, stepSession);
            else
                return await stepsData.Update(UsersModel.CurrentUser.Id, stepSession);
		}

        //public static int Delete(StepSession stepSession)
        //{
        //    lock (_lock)
        //    {
        //        StepsDataInterface stepsData = new StepsDataInterface();
        //        return stepsData.Delete(stepSession);
        //    }
        //}

        //public static int Delete(int id)
        //{
        //    lock (_lock)
        //    {
        //        StepsDataInterface stepsData = new StepsDataInterface();
        //        return stepsData.DeleteById(id);
        //    }
        //}
    }
}
