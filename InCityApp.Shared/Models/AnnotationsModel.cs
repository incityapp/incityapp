
using System;
using System.Collections.Generic;
using System.Text;

using InCityApp.Entities;
using InCityApp.DataInterfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Net;

namespace InCityApp.DataModels
{
    public static class AnnotationsModel 
    {
        static object _lock = new object();

        /// <summary>
        /// Gets all the annotations in the current user area.
        /// </summary>
        /// <remarks>
        /// Throws InvalidOperationException if user informations cannot be retrieved.
        /// </remarks>
        /// <returns></returns>
        public static async Task<List<Annotation>> GetAllInUserArea()
        {
            if (UserLocator.Instance == null || UserLocator.Instance.CurrentLocation == null)
                throw new InvalidOperationException();

            AnnotationsDataInterface annotationsData = new AnnotationsDataInterface();
                
            List<Annotation> annotations = await annotationsData.GetAllNearTo(UsersModel.CurrentUser.Id, UserLocator.Instance.CurrentLocation, UserLocator.Instance.UserAreaRadius / 1000);
            return annotations;
        }

        public static async Task<Annotation> IsThereAnnotationInUserArea()
        {
            if (UserLocator.Instance == null || UserLocator.Instance.CurrentLocation == null || UsersModel.CurrentUser == null)
                throw new InvalidOperationException();

            AnnotationsDataInterface annotationsData = new AnnotationsDataInterface();

            try
            {
                Annotation annotation = await annotationsData.IsThereAnnotationInRange(UsersModel.CurrentUser.Id, UserLocator.Instance.CurrentLocation, UserLocator.Instance.UserAreaRadius / 1000);
                return annotation;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public static async Task<Annotation> GetAnnotationById(string annotationId)
        {
            AnnotationsDataInterface annotationsData = new AnnotationsDataInterface();

            try
            {
                Annotation annotation = await annotationsData.GetById(annotationId);
                return annotation;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public static async Task<AnnotationValidation> GetValidationByUserId(int annotationId, string userId)
        {
            AnnotationValidationDataInterface annotationValidationData = new AnnotationValidationDataInterface();
            return await annotationValidationData.GetByUserId(annotationId, userId);   
        }

        public static async Task<byte[]> GetPhoto(Annotation annotation)
        {
            if (annotation == null || string.IsNullOrWhiteSpace(annotation.PhotoURL))
                return null;

            using (WebClient webClient = new WebClient())
            {
                return await webClient.DownloadDataTaskAsync(new Uri(annotation.PhotoURL));
            }
        }

        public static async Task<Annotation> Insert(Annotation annotation)
        {
            AnnotationsDataInterface annotationData = new AnnotationsDataInterface();
            return await annotationData.Insert(annotation);
        }

        public static async Task<int> InsertValidation(Annotation annotation, int guessVote)
        {
            AnnotationValidationDataInterface tagVotesData = new AnnotationValidationDataInterface();
            return await tagVotesData.Insert(new AnnotationValidation(annotation, guessVote));   
        }
    }
}
