
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using InCityApp.Entities;
using InCityApp.DataInterfaces;
using System.Threading.Tasks;

namespace InCityApp.DataModels
{
    public static class TagsModel 
    {
        public static async Task<List<PredefinedTag>> GetPredefinedTagsName ()
        {
            PredefinedTagsDataInterface definedTagsData = new PredefinedTagsDataInterface();
            List<PredefinedTag> predefinedTags = await definedTagsData.GetAllByUser(UsersModel.CurrentUser);

            return predefinedTags;
        }
    }
}
