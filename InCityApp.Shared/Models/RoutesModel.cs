
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using InCityApp.Entities;
using InCityApp.DataInterfaces;
using InCityApp.DataModels;
using System.Threading.Tasks;

namespace InCityApp.Models
{
    public static class RoutesModel
    {
        static object _lock = new object();

        public static async Task<List<RoutePosition>> GetInDateRange(DateTime fromDate, DateTime toDate)
        {
            if (UsersModel.CurrentUser == null)
                return null;

            RoutesDataInterface routeData = new RoutesDataInterface();
            return await routeData.GetInDateRange(UsersModel.CurrentUser.Id, fromDate, toDate);
        }

        public static async Task<ActivityStats> GetDistinctActivities(DateTime fromDate, DateTime toDate)
        {
            if (UsersModel.CurrentUser == null)
                return null;

            RoutesDataInterface routeData = new RoutesDataInterface();
            return await routeData.GetDistinctActivities(UsersModel.CurrentUser.Id, fromDate, toDate);
        }


        public static async Task<bool> Insert(RoutePosition routePosition)
		{
            if (UsersModel.CurrentUser == null)
                return false;

            RoutesDataInterface routeData = new RoutesDataInterface();
            return await routeData.Insert(UsersModel.CurrentUser.Id, routePosition);
		}
    }
}
