﻿using InCityApp.DependencyServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using XLabs.Enums;
using XLabs.Forms.Controls;

namespace InCityApp.Views
{
	public class CustomNavigationView : ContentView
	{
        int spacing = 20;

        Image navBg;

        ImageButton noiseBtn;
        ImageButton activitiesBtn;
        ImageButton settingsBtn;

        Image mainBtnImage;
        Button mainBtn;

        Image backBtnImage;
        Button backBtn;

        Label titleLbl;

        AbsoluteLayout customNav;

        StackLayout buttonsStack;

		public CustomNavigationView ()
		{
            navBg = new Image
            {
                Source = (FileImageSource)FileImageSource.FromFile("NavBarBg"),
                Aspect = Aspect.Fill
            };

            noiseBtn = new ImageButton()
            {
                Source = (FileImageSource)FileImageSource.FromFile("noiseIcon")
            };

            activitiesBtn = new ImageButton()
            {
                Source = (FileImageSource)FileImageSource.FromFile("activitiesIcon")
            };

            settingsBtn = new ImageButton()
            {
                Source = (FileImageSource)FileImageSource.FromFile("settingsIcon")
            };

            buttonsStack = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.End,

                Children = 
                { 
                    noiseBtn,
                    activitiesBtn,
                    settingsBtn
                }
            };

            mainBtn = new Button()
            {
                BackgroundColor = Color.Transparent
            };

            mainBtnImage = new Image
            {
                Source = (FileImageSource)FileImageSource.FromFile("NavBarMainIcon"),
                Aspect = Aspect.AspectFit
            };

            customNav = new AbsoluteLayout()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,

                Children =
                {
                    navBg,
                    mainBtnImage,
                    mainBtn,
                    buttonsStack
                }
            };

            Content = customNav;

            noiseBtn.Clicked += noiseButtonClicked;
            activitiesBtn.Clicked += ActivitiesButtonClicked;
            settingsBtn.Clicked += optionsButtonClicked;

            this.SizeChanged += OnSizeChangedEvent;

		}

        public CustomNavigationView( string title )
        {
            navBg = new Image
            {
                Source = (FileImageSource)FileImageSource.FromFile("NavBarBg"),
                Aspect = Aspect.Fill
            };

            titleLbl = new Label
            {
                Text = title,
                XAlign = TextAlignment.End,
                YAlign = TextAlignment.Center,
                TextColor = Color.FromHex("#333333"),
                FontAttributes = FontAttributes.Bold
            };

            backBtnImage = new Image
            {
                Source = (FileImageSource)FileImageSource.FromFile("NavBarBackIcon"),
                Aspect = Aspect.AspectFit
            };


            backBtn = new Button()
            {
                BackgroundColor = Xamarin.Forms.Color.Transparent
            };

            customNav = new AbsoluteLayout()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,

                Children =
                {
                    navBg,
                    backBtnImage,
                    backBtn,
                    titleLbl
                }
            };

            Content = customNav;

            backBtn.Clicked += backButtonClicked;

            this.SizeChanged += OnSizeChangedEvent;
        }

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentView page = (ContentView)sender;

            double wndWidth = page.Width;

            double screenRatio = (page.Width / 1080);

            customNav.HeightRequest = 144 * screenRatio;

            navBg.WidthRequest = 1080 * screenRatio;
            navBg.HeightRequest = 144 * screenRatio;

            AbsoluteLayout.SetLayoutBounds(navBg, new Rectangle(0f, 0f, navBg.Width, navBg.Height));

            double specialRatio = (screenRatio < 0.6 ? 0.6 : screenRatio);

            LayoutForNavButton(noiseBtn, screenRatio);
            LayoutForNavButton(activitiesBtn, screenRatio);
            LayoutForNavButton(settingsBtn, screenRatio);

            if (buttonsStack != null)
            {
                buttonsStack.Spacing = 0;

                AbsoluteLayout.SetLayoutBounds(buttonsStack, new Rectangle(page.Width - buttonsStack.Width - (spacing * screenRatio), (page.Height - buttonsStack.Height) * 0.5, buttonsStack.Width, buttonsStack.Height));
            }

            if (titleLbl != null)
            {
                titleLbl.FontSize = 50 * screenRatio;
                AbsoluteLayout.SetLayoutBounds(titleLbl, new Rectangle(page.Width - titleLbl.Width - (spacing * screenRatio), (page.Height - titleLbl.Height) * 0.5, titleLbl.Width, titleLbl.Height));
            }

            if (mainBtnImage != null)
            {
                mainBtnImage.WidthRequest = 148 * screenRatio;
                mainBtnImage.HeightRequest = 123 * screenRatio;

                AbsoluteLayout.SetLayoutBounds(mainBtnImage, new Rectangle((spacing * screenRatio), (page.Height - mainBtnImage.Height), mainBtnImage.Width, mainBtnImage.Height));
            }

            if (mainBtn != null)
            {
                mainBtn.WidthRequest = 148 * screenRatio;
                mainBtn.HeightRequest = 123 * screenRatio;

                AbsoluteLayout.SetLayoutBounds(mainBtn, new Rectangle((spacing * screenRatio), (page.Height - mainBtn.Height), mainBtn.Width, mainBtn.Height));
            }

            if (backBtnImage != null)
            {
                backBtnImage.WidthRequest = 207 * screenRatio;
                backBtnImage.HeightRequest = 123 * screenRatio;

                AbsoluteLayout.SetLayoutBounds(backBtnImage, new Rectangle((spacing * screenRatio), (page.Height - backBtnImage.Height), backBtnImage.Width, backBtnImage.Height));
            }

            if (backBtn != null)
            {
                backBtn.WidthRequest = 207 * screenRatio;
                backBtn.HeightRequest = 123 * screenRatio;

                AbsoluteLayout.SetLayoutBounds(backBtn, new Rectangle((spacing * screenRatio), (page.Height - backBtn.Height), backBtn.Width, backBtn.Height));
            }
        }

        public void LayoutForNavButton(ImageButton btn, double screenRatio)
        {
            if (btn == null)
                return;

            double specialRatio = (screenRatio < 0.6 ? 0.6 : screenRatio);

            btn.BackgroundColor = Xamarin.Forms.Color.Transparent;

            btn.WidthRequest = 88 * (1 + screenRatio*0.2) * specialRatio;
            btn.HeightRequest = 88 * screenRatio;
            btn.ImageWidthRequest = Convert.ToInt32(88 * specialRatio);
            btn.ImageHeightRequest = Convert.ToInt32(88 * specialRatio);
        }

        private void noiseButtonClicked(object sender, EventArgs e)
        {
            InCityApplication.App.SetNoiseAsRootPage();
        }

        private void ActivitiesButtonClicked(object sender, EventArgs e)
        {
            InCityApplication.App.SetActivitiesAsRootPage();
        }
        private void optionsButtonClicked(object sender, EventArgs e)
        {
            InCityApplication.App.SetSettingsAsRootPage();
        }

        private void backButtonClicked(object sender, EventArgs e)
        {
            InCityApplication.App.SetMapAsRootPage();
        }
	}
}
