using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Resources;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

using InCityApp.Entities;
using InCityApp.Services;
using InCityApp.Shared.Pages;
using InCityApp.DataInterfaces;
using InCityApp.DependencyServiceInterfaces;
using System.Diagnostics;

#if __ANDROID__
using Android.Content;
using Xamarin.Social.Services;
using Xamarin.Social;
using Android.App;
#endif

namespace InCityApp
{
    /// <summary>
    /// The App shared interface
    /// </summary>
    public static class InCityApplication
    {
        public enum ScreenOrientationMode
        { 
            Portrait,
            Landscape
        }

        public static App App;

        public static int ScreenWidth;
        public static int ScreenHeight;
        public static int RealScreenWidth;
        public static int RealScreenHeight;
        public static int ScreenLayoutHeight;
        public static bool InLoginPage = true;
        public static bool InRegistrationPage;
        public static ScreenOrientationMode ScreenOrientation;

        public static Action OnConnectionNotAvailable;
        public static Action OnConnectionAvailable;

        public static bool OpenAnnotation = false;
        public static string AnnotationIdToOpen = "";

        //public static INavigation Navigation;

        public enum DataProviderType
        { 
            DB,
            WS
        }

        private static DataProviderType _currentDataProviderType = DataProviderType.DB;

        public static DataProviderType CurrentDataProviderType
        {
            get { return _currentDataProviderType; }
        }

        static Assembly _reflectionAssembly;
        internal static readonly MethodInfo GetDependency;

        static InCityApplication()
        {
            GetDependency = typeof(DependencyService)
                .GetRuntimeMethods()
                .Single((method) =>
                    method.Name.Equals("Get"));
        }

        public static void Init(Assembly assembly)
        {
            System.Threading.Interlocked.CompareExchange(ref _reflectionAssembly, assembly, null);
        }

        public static Stream LoadResource(String name)
        {
            return _reflectionAssembly.GetManifestResourceStream(name);
        }

        public static void SetApp(App app)
        { 
        
        }

        /*public static void SetRootPage(Page rootPage)
        {
            Navigation = rootPage.Navigation;
        }*/


        public static string GetSOVersionInfo()
        {
            var sdkNum = string.Empty;
            var sdkName = string.Empty;
            var releaseNum = string.Empty;
            var incrementalNum = string.Empty;

            #if __ANDROID__
                try { sdkNum = Android.OS.Build.VERSION.Sdk; }
                catch { }

                try { sdkName = Android.OS.Build.VERSION.SdkInt.ToString(); }
                catch { }

                try { releaseNum = Android.OS.Build.VERSION.Release; }
                catch { }
            
                try { incrementalNum = Android.OS.Build.VERSION.Incremental; }
                catch { }

                return string.Format("Android: [{0} ({1}) V {2}]", sdkName, sdkNum, releaseNum);
            #else
                #if __IOS__
                        return "";
                #endif
            #endif


        }


        public static string GetAppVersionInfo()
        {
            #if __ANDROID__
                Context context = Forms.Context;
                return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
            #else
                #if __IOS__
                    return NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"];
                #endif
            #endif
        }

        //public static string GetDeviceId()
        //{
        //    try
        //    {
        //        return DependencyService.Get<IAppIdGenerator>().GetDeviceUniqueId();
        //    }
        //    catch (Exception exc)
        //    {
        //        Debug.Print(exc.Message);
        //        return string.Empty;
        //    }
        //}

    }
}