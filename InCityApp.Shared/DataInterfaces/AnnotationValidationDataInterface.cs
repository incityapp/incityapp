using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Json;

using InCityApp.Entities;
using InCityApp.DataProviders;
using Xamarin.Forms;

namespace InCityApp.DataInterfaces
{
    public class AnnotationValidationDataInterface : BaseDataInterface
    {
        public async Task<AnnotationValidation> GetByUserId(int annotationId, string userId)
        {
            AnnotationValidation _annotationValidation = new AnnotationValidation();

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("annotationId", annotationId.ToString());
            string parameters = data.GetFormattedString();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "AnnotationValidations/getByUserAndAnnotationId", data),
                                                (string requestFailedMessage) =>
                                                {
                                                    Debug.Print("AnnotationValidations/getByUserAndAnnotationId >> " + requestFailedMessage);
                                                },
                                                null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("AnnotationValidations/getByUserAndAnnotationId >> " + json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
						return _annotationValidation;
                    }
                    if (json["result"].Count > 0)
                    {
                        int aId = 0;
                        int.TryParse(json["result"][0]["annotationId"], out aId);
                        _annotationValidation.AnnotationId = aId;
                        _annotationValidation.UserId = json["result"][0]["userId"];
                        DateTime date;
                        DateTime.TryParse(json["result"][0]["date"], out date);
                        _annotationValidation.Date = date.ToLocalTime().ToString();
                        int guessVote = 0;
                        int.TryParse(json["result"][0]["guessVote"], out guessVote);
                        _annotationValidation.GuessVote = guessVote;
                    }
                }
                catch
                {
                    Debug.Print("AnnotationValidations/getByUserAndAnnotationId >> Cannot access returned json object values.");
                    throw new InvalidOperationException();
                }
            }
            else
            {
                Debug.Print("AnnotationValidations/getByUserAndAnnotationId >> null json.");
            }
            

            return _annotationValidation;
        }

        public async Task<int> Insert(AnnotationValidation annotationValidation)
        {
            int quizScore = -1;

            PostData data = new PostData();
            data.AddParameter("operationType", "ADD");
            data.AddParameter("userId", annotationValidation.UserId);
            data.AddParameter("annotationId", annotationValidation.AnnotationId.ToString());;
            data.AddParameter("date", annotationValidation.Date);
            data.AddParameter("guessVote", annotationValidation.GuessVote.ToString());

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "AnnotationValidations", data),
                                                (string requestFailedMessage) =>
                                                {
                                                    Debug.Print("AnnotationValidations >> " + requestFailedMessage);
                                                },
                                                null);
            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print(json["description"].ToString());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                    });
                    return quizScore;
                }

                if (json["result"].Count > 0)
                {
                    try
                    {
                        int.TryParse(json["result"]["quizScore"].ToString(), out quizScore);
                    }
                    catch (Exception exc)
                    { 
                        
                    }
                }

                return quizScore;
            }
            else
            {
                Debug.Print("AnnotationValidations >> null json.");
                return quizScore;
            }
            
        }
    }
}
