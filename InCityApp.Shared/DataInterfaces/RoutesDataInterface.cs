
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Json;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;

using InCityApp.Entities;
using InCityApp.DataProviders;
using Xamarin.Forms;

namespace InCityApp.DataInterfaces
{
    public class RoutesDataInterface : BaseDataInterface
    {
        public async Task<List<RoutePosition>> GetInDateRange(string userId, DateTime fromDate, DateTime toDate)
        {
			List<RoutePosition> routePositions = new List<RoutePosition>();

			GetData data = new GetData();
			data.AddParameter("userId", userId);
            data.AddParameter("fromDate", fromDate.ToString("yyyy-MM-dd HH:mm:ss"));
            data.AddParameter("toDate", toDate.ToString("yyyy-MM-dd HH:mm:ss"));

			string parameters = data.GetFormattedString();

			JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "Locations/getAllByUserIdAndInterval", data),
													(string requestFailedMessage) =>
													{
														Debug.Print("Locations/getAllByUserIdAndInterval >> " + requestFailedMessage);
													},
													null);

			if (json != null)
			{
				try
				{
					if (!json["code"].ToString().Equals("-1"))
					{
						Debug.Print("Locations/getAllByUserIdAndInterval >> " + json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
						return null;
					}

					if (json["result"] == null)
					{
						Debug.Print("Locations/getAllByUserIdAndInterval >> null result. ID : " + userId);
						return routePositions;
					}
                    if (json["result"].Count > 0)
                    {
                        for (int i = 0, iLen = json["result"].Count; i < iLen; i++)
                        {
                            RoutePosition routePos = new RoutePosition();

                            double latitude = 0;
                            double.TryParse(json["result"][i]["latitude"], NumberStyles.Any, CultureInfo.InvariantCulture, out latitude);
                            routePos.Latitude = latitude;

                            double longitude = 0;
                            double.TryParse(json["result"][i]["longitude"], NumberStyles.Any, CultureInfo.InvariantCulture, out longitude);
                            routePos.Longitude = longitude;

                            DateTime date;
                            DateTime.TryParse(json["result"][i]["date"], out date);
                            routePos.Timestamp = date.ToLocalTime().ToString();

                            routePositions.Add(routePos);
                        }
                    }
				}
				catch
				{
					Debug.Print("Locations/getAllByUserIdAndInterval >> Cannot access returned json object values.");
					throw new InvalidOperationException();
				}
			}
			else
			{
				Debug.Print("Locations/getAllByUserIdAndInterval >> null json. ID : " + userId);
			}

            return routePositions;
        }

        public async Task<ActivityStats> GetDistinctActivities(string userId, DateTime fromDate, DateTime toDate)
        {
            ActivityStats stats = null;

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("fromDate", fromDate.ToString("yyyy-MM-dd HH:mm:ss"));
            data.AddParameter("toDate", toDate.ToString("yyyy-MM-dd HH:mm:ss"));

            string parameters = data.GetFormattedString();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "Locations/getDistinctActivitiesByUser", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("Locations/getDistinctActivities >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("Locations/getDistinctActivities >> " + json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return null;
                    }

                    if (json["result"] == null)
                    {
                        Debug.Print("Locations/getDistinctActivities >> null result. ID : " + userId);
                        return null;
                    }
                    if (json["result"].Count >= 4)
                    {
                        stats = new ActivityStats();
                        for (int i = 0, iLen = json["result"].Count; i < iLen; i++)
                        {
                            int type = 0;
                            int.TryParse(json["result"][i]["Type"], NumberStyles.Any, CultureInfo.InvariantCulture, out type);

                            double distanceSum = 0;
                            double.TryParse(json["result"][i]["DistanceSum"], NumberStyles.Any, CultureInfo.InvariantCulture, out distanceSum);
                            stats.DistanceSum[Enum.GetName(typeof(UserLocator.ActivityType), (UserLocator.ActivityType)type)] = distanceSum;

                            double timeSum = 0;
                            double.TryParse(json["result"][i]["TimeSum"], NumberStyles.Any, CultureInfo.InvariantCulture, out timeSum);
                            stats.TimeSum[Enum.GetName(typeof(UserLocator.ActivityType), (UserLocator.ActivityType)type)] = timeSum;
                        }
                    }
                }
                catch
                {
                    Debug.Print("Locations/getDistinctActivities >> Cannot access returned json object values.");
                    throw new InvalidOperationException();
                }
            }
            else
            {
                Debug.Print("Locations/getDistinctActivities >> null json. ID : " + userId);
            }

            return stats;
        }

        public async Task<bool> Insert(string userId, RoutePosition routePosition)
        {
            PostData data = new PostData();
            data.AddParameter("operationType", "ADD");
            data.AddParameter("id", userId);
            data.AddParameter("lat", routePosition.Latitude.ToString(CultureInfo.InvariantCulture));
            data.AddParameter("lon", routePosition.Longitude.ToString(CultureInfo.InvariantCulture));
            data.AddParameter("date", routePosition.Timestamp);
            data.AddParameter("type", ((int)routePosition.Activity).ToString());
            data.AddParameter("distance", routePosition.DistanceFromLastEvent.ToString(CultureInfo.InvariantCulture));
            data.AddParameter("timePassed", routePosition.MinutesFromLastEvent.ToString());
            data.AddParameter("accuracy", routePosition.ActivityAccuracy.ToString());

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "Locations", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("Locations >> " + requestFailedMessage);
                                                    },
                                                    null);
            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print("Locations >> " + json["description"].ToString());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                    });
                    return false;
                }

                return true;
            }
            else
            {
                Debug.Print("Locations >> null json.");
                return false;
            }
              
        }
    }
}
