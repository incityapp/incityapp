
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Json;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Globalization;

using InCityApp.Entities;
using InCityApp.DataProviders;
using Xamarin.Forms;

namespace InCityApp.DataInterfaces
{
    public class StepsDataInterface : BaseDataInterface
    {
        public async Task<List<StepSession>> GetByDateTime(string userId, DateTime date)
        {
            List<StepSession> stepSessions = null;

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("date", date.ToString("yyyy-MM-dd HH:mm:ss"));

            string parameters = data.GetFormattedString();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "StepSessions/getAllByUserIdAndData", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("StepSessions/getAllByUserIdAndData >> " +  requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print(json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return null;
                    }

                    if (json["result"].Count > 0)
                    {
                        for (int i = 0, iLen = json["result"].Count; i < iLen; i++)
                        {
                            StepSession stepSession = new StepSession();
                            int id = 0;
                            int.TryParse(json["result"][i]["id"], out id);
                            stepSession.Id = id;
                            int steps = 0;
                            int.TryParse(json["result"][i]["steps"], out steps);
                            stepSession.Steps = steps;
                            stepSession.Date = DateTime.Parse(json["result"][i]["date"], CultureInfo.CurrentCulture).ToLocalTime();
    
                            stepSessions.Add(stepSession);
                        }
                    }
                    else
                    {
                        Debug.Print("StepSessions/getAllByUserIdAndData >> No step sessions retrieved.");
                        return null;
                    }
                }
                catch(Exception exc)
                {
                    Debug.Print("StepSessions/getAllByUserIdAndData >> Cannot access returned json object values : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("StepSessions/getAllByUserIdAndData >> null json. ID : " + userId);
            }
            

            return stepSessions;
        }

        public async Task<bool> Insert(string userId, StepSession stepSession)
        {
            PostData data = new PostData();
            data.AddParameter("operationType", "ADD");
            data.AddParameter("id", userId);
            data.AddParameter("steps", stepSession.Steps.ToString());
            data.AddParameter("date", stepSession.Date.ToString("yyyy-MM-dd HH:mm:ss"));
            string x = data.GetFormattedString();
            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "StepSessions", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("StepSessions ADD >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print(json["description"].ToString());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                    });
                    return false;
                }

                return true;
            }
            else
            {
                Debug.Print("StepSessions ADD >> null json.");
                return false;
            }
            
        }

        public async Task<bool> Update(string userId, StepSession stepSession)
        {
            PostData data = new PostData();
            data.AddParameter("operationType", "UPDATE");
            data.AddParameter("id", userId);
            data.AddParameter("steps", stepSession.Steps.ToString());
            data.AddParameter("date", stepSession.Date.ToString("yyyy-MM-dd HH:mm:ss"));
            string x = data.GetFormattedString();
            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "StepSessions", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("StepSessions UPDATE >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print(json["description"].ToString());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                    });
                    return false;
                }

                return true;
            }
            else
            {
                Debug.Print("StepSessions UPDATE >> null json.");
                return false;
            }
        }
    }
}
