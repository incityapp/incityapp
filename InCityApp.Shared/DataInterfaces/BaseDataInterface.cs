﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Json;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using Polly;

using InCityApp.DataProviders;

namespace InCityApp.DataInterfaces
{
    public class BaseDataInterface
    {
        private  RemoteStorageDataProvider _remoteStorageWS;

        public RemoteStorageDataProvider RemoteStorageWS
        {
            get { return _remoteStorageWS; }
        }

        public BaseDataInterface()
        {
            _remoteStorageWS = new RemoteStorageDataProvider();
        }

        protected async Task<JsonValue> SafeWSRequest(WSDataProvider wsDataProvider, Task<JsonValue> requestTask,  Action<string> onRequestFailedAction, Action<string> onConnectionNotAvailableAction, bool autoRetry = true )
        {
            wsDataProvider.OnRequestFailed = onRequestFailedAction;

            JsonValue json = null;
            try
            {
                if (autoRetry)
                {
                    json = await Policy.Handle<WebException>()
                                       .WaitAndRetryAsync(new[] { TimeSpan.FromSeconds(DataProviderSettings.RetryTime[0]), 
                                                              TimeSpan.FromSeconds(DataProviderSettings.RetryTime[1]), 
                                                              TimeSpan.FromSeconds(DataProviderSettings.RetryTime[2]) })
                                       .ExecuteAsync<JsonValue>(() => requestTask);
                }
                else
                {
                    json = await requestTask;
                }

                return json;
            }
            catch (Exception exc)
            {
                Debug.Print(exc.Message);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MessagingCenter.Send<BaseDataInterface>(this, "OnConnectionLost");
                });

                if (onConnectionNotAvailableAction != null)
                    onConnectionNotAvailableAction(exc.Message);

                return null;
            }
        }
    }
}
