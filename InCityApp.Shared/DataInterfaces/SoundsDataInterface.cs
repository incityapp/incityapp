
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Json;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;

using Newtonsoft.Json;

using InCityApp.Entities;
using InCityApp.DataProviders;
using System.Collections.Concurrent;
using Xamarin.Forms;

namespace InCityApp.DataInterfaces
{
    public class SoundsDataInterface : BaseDataInterface
    {

        public async Task<SoundStats> GetStats(string userId, DateTime date)
        {
            SoundStats stats = new SoundStats();

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("date", date.ToString("yyyy-MM-dd"));

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "NoiseAtPositions/getNoiseStats", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("NoiseAtPositions/getNoiseStats >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("NoiseAtPositions/getNoiseStats >> " + json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return null;
                    }

                    if (json["result"].Count > 0)
                    {
                        double max, avg, min;
                        double.TryParse(json["result"][0]["MaxNoise"], NumberStyles.Any, CultureInfo.InvariantCulture, out max);
                        stats.Max = max;
                        double.TryParse(json["result"][0]["AvgNoise"], NumberStyles.Any, CultureInfo.InvariantCulture, out avg);
                        stats.Avarage = avg;
                        double.TryParse(json["result"][0]["MinNoise"], NumberStyles.Any, CultureInfo.InvariantCulture, out min);
                        stats.Min = min;

                        return stats;
                    }
                    else
                    {
                        Debug.Print("NoiseAtPositions/getNoiseStats >> MaxNoise not returned.");
                        return null;
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("NoiseAtPositions/getNoiseStats >> Cannot access returned json object values : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("NoiseAtPositions/getNoiseStats >> null json. ID : " + userId);
            }

            return null;
        }

        public async Task<double> GetMaxVolumeByDate(string userId, DateTime date)
        {
            double maxVolume;

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("date", date.ToString("yyyy-MM-dd"));

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "NoiseAtPositions/getMaxByUserIdAndDate", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("NoiseAtPositions/getMaxByUserIdAndDate >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print(json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return -1;
                    }

                    if (json["result"].Count > 0)
                    {
                        double.TryParse(json["result"][0]["MaxNoise"], NumberStyles.Any, CultureInfo.InvariantCulture, out maxVolume);
                        return maxVolume;
                    }
                    else
                    {
                        Debug.Print("NoiseAtPositions/getMaxByUserIdAndDate >> MaxNoise not returned.");
                        return -1;
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("NoiseAtPositions/getMaxByUserIdAndDate >> Cannot access returned json object values : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("NoiseAtPositions/getMaxByUserIdAndDate >> null json. ID : " + userId);
            }

            return -1;
        }

        public async Task<double> GetAvarageVolumeByDate(string userId, DateTime date)
        {
            double avgVolume;

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("date", date.ToString("yyyy-MM-dd"));

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "NoiseAtPositions/getAvgByUserIdAndDate", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("NoiseAtPositions/getAvgByUserIdAndDate >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print(json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return -1;
                    }

                    if (json["result"].Count > 0)
                    {
                        double.TryParse(json["result"][0]["AvgNoise"], NumberStyles.Any, CultureInfo.InvariantCulture, out avgVolume);
                        return avgVolume;
                    }
                    else
                    {
                        Debug.Print("NoiseAtPositions/getAvgByUserIdAndDate >> AvgNoise not returned.");
                        return -1;
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("NoiseAtPositions/getAvgByUserIdAndDate >> Cannot access returned json object values : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("NoiseAtPositions/getAvgByUserIdAndDate >> null json. ID : " + userId);
            }

            return -1;
        }

        public async Task<double> GetMinVolumeByDate(string userId, DateTime date)
        {
            double minVolume;

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("date", date.ToString("yyyy-MM-dd"));

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "NoiseAtPositions/getMinByUserIdAndDate", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print(json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return -1;
                    }

                    if (json["result"].Count > 0)
                    {
                        double.TryParse(json["result"][0]["MinNoise"], NumberStyles.Any, CultureInfo.InvariantCulture, out minVolume);
                        return minVolume;
                    }
                    else
                    {
                        Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> MinVolume not returned.");
                        return -1;
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> Cannot access returned json object values : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> null json. ID : " + userId);
            }

            return -1;
        }

        public async Task<List<SoundAtPosition>>  GetAllByData(string userId, DateTime date)
        {
            List<SoundAtPosition> soundsAtPosition = new List<SoundAtPosition>();

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("date", date.ToString("yyyy-MM-dd"));

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "NoiseAtPositions/getAllByUserIdAndDate", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> " + json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        throw new InvalidOperationException("NoiseAtPositions/getMinByUserIdAndDate >> " + json["description"].ToString());
                    }

                    if (json["result"].Count > 0)
                    {
                        for (int i = 0, iLen = json["result"].Count; i < iLen; i++)
                        {
                            SoundAtPosition sound = new SoundAtPosition();
                            double latitude = 0;
                            double.TryParse(json["result"][i]["latitude"], NumberStyles.Any, CultureInfo.InvariantCulture, out latitude);
                            sound.Latitude = latitude;
                            double longitude = 0;
                            double.TryParse(json["result"][i]["longitude"], NumberStyles.Any, CultureInfo.InvariantCulture, out longitude);
                            sound.Longitude = longitude;
                            DateTime timestamp;
                            DateTime.TryParse(json["result"][i]["date"], out timestamp);
                            sound.Timestamp = timestamp.ToLocalTime().ToString();
                            float volume = 0;
                            float.TryParse(json["result"][i]["noise"], NumberStyles.Any, CultureInfo.InvariantCulture, out volume);
                            sound.SoundDecibelValue = volume;

                            soundsAtPosition.Add(sound);
                        }
                    }
                    else
                    {
                        Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> MinVolume not returned.");
                        return soundsAtPosition;
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> Cannot access returned json object values : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("NoiseAtPositions/getMinByUserIdAndDate >> null json. ID : " + userId);
            }

            return soundsAtPosition;
        }

        public async Task<List<SoundAtPosition>> GetLastFiveMinutes(string userId)
        {
            List<SoundAtPosition> soundsAtPosition = new List<SoundAtPosition>();

            GetData data = new GetData();
            data.AddParameter("userId", userId);
           // data.AddParameter("date", date.ToString("yyyy-MM-dd"));

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "NoiseAtPositions/getLast5MinutesByUserId", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("NoiseAtPositions/getLast5MinutesByUserId >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("NoiseAtPositions/getLast5MinutesByUserId >> " + json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        throw new InvalidOperationException("NoiseAtPositions/getMinByUserIdAndDate >> " + json["description"].ToString());
                    }

                    if (json["result"].Count > 0)
                    {
                        for (int i = 0, iLen = json["result"].Count; i < iLen; i++)
                        {
                            SoundAtPosition sound = new SoundAtPosition();
                            double latitude = 0;
                            double.TryParse(json["result"][i]["latitude"], NumberStyles.Any, CultureInfo.InvariantCulture, out latitude);
                            sound.Latitude = latitude;
                            double longitude = 0;
                            double.TryParse(json["result"][i]["longitude"], NumberStyles.Any, CultureInfo.InvariantCulture, out longitude);
                            sound.Longitude = longitude;
                            DateTime timestamp;
                            DateTime.TryParse(json["result"][i]["date"], out timestamp);
                            sound.Timestamp = timestamp.ToLocalTime().ToString();
                            float volume = 0;
                            float.TryParse(json["result"][i]["noise"], NumberStyles.Any, CultureInfo.InvariantCulture, out volume);
                            sound.SoundDecibelValue = volume;

                            soundsAtPosition.Add(sound);
                        }
                    }
                    else
                    {
                        Debug.Print("NoiseAtPositions/getLast5MinutesByUserId >> MinVolume not returned.");
                        return soundsAtPosition;
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("NoiseAtPositions/getLast5MinutesByUserId >> Cannot access returned json object values : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("NoiseAtPositions/getLast5MinutesByUserId >> null json. ID : " + userId);
            }

            return soundsAtPosition;
        }

        public async Task<bool> Insert(SoundAtPosition[] decibelSamples)
        {
            PostData data = new PostData();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Culture = CultureInfo.InvariantCulture;
            settings.DateFormatString = string.Format("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;

            string serializedSamples = JsonConvert.SerializeObject(decibelSamples, settings).ToString();

            data.AddParameter("operationType", "ADD");
            data.AddParameter("sounds", serializedSamples);

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "NoiseAtPositions", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("NoiseAtPositions >> " + requestFailedMessage);
                                                    },
                                                    null);
            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print(json["description"].ToString());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                    });
                    return false;
                }

                return true;
            }
            else
            {
                Debug.Print("NoiseAtPositions >> null json.");
                return false;
            }
              
        }
    }
}
