
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Json;
using System.Net;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Globalization;

using Polly;

using Newtonsoft.Json;

using InCityApp.Entities;
using InCityApp.DataProviders;
using InCityApp.Helpers;
using Xamarin.Forms;

namespace InCityApp.DataInterfaces
{
    public class AnnotationsDataInterface : BaseDataInterface
    {
        public async Task<List<Annotation>> GetAllNearTo(string userId, MapLocation locatable, double maxDistance)
        {
            List<Annotation> annotations = new List<Annotation>();

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("latitude", locatable.Latitude.ToString());
            data.AddParameter("longitude", locatable.Longitude.ToString());
            data.AddParameter("radius", maxDistance.ToString());            

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "Annotations/getAllInRadiusUserCheck", data),
                                                (string requestFailedMessage) =>
                                                {
                                                    Debug.Print("Annotations/getAllInRadiusUserCheck >> " + requestFailedMessage);
                                                },
                                                null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("Annotations/getAllInRadiusUserCheck >> " + json["description"].ToString());

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
						return annotations;
                    }

                    if (json["result"].Count > 0)
                    {
                        for (int i = 0, iLen = json["result"].Count; i < iLen; i++)
                        {
                            // parse the annotation
                            Annotation annotation = GetAnnotationFromResult(json, i);
                            annotations.Add(annotation);
                        }
                    }
                }
                catch(Exception exc)
                {
                    Debug.Print("getAllInRadiusUserCheck >> Cannot access returned json object values : " + exc.Message);
                    throw new InvalidOperationException();
                }
            }
            else
            {
                Debug.Print("getAllInRadiusUserCheck >> null json.");
            }
            

            return annotations;
        }

        public async Task<Annotation> IsThereAnnotationInRange(string userId, MapLocation locatable, double maxDistance)
        {
            Annotation annotation = new Annotation();

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("latitude", locatable.Latitude.ToString());
            data.AddParameter("longitude", locatable.Longitude.ToString());
            data.AddParameter("radius", maxDistance.ToString());

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "Annotations/isThereAnnotationInRadius", data),
                                                (string requestFailedMessage) =>
                                                {
                                                    Debug.Print("Annotations/isThereAnnotationInRadius >> " + requestFailedMessage);
                                                },
                                                null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("Annotations/isThereAnnotationInRadius >> " + json["description"].ToString());

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return annotation;
                    }

                    if (json["result"].Count > 0)
                    {
                        annotation = GetAnnotationFromResult(json, 0);
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("isThereAnnotationInRadius >> Cannot access returned json object values : " + exc.Message);
                    throw new InvalidOperationException();
                }
            }
            else
            {
                Debug.Print("isThereAnnotationInRadius >> null json.");
            }

            return annotation;
        }

        public async Task<Annotation> GetById(string annotationId)
        {
            Annotation annotation = new Annotation();

            GetData data = new GetData();
            data.AddParameter("id", annotationId);

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "Annotations/getById", data),
                                                (string requestFailedMessage) =>
                                                {
                                                    Debug.Print("Annotations/getById >> " + requestFailedMessage);
                                                },
                                                null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("Annotations/getById >> " + json["description"].ToString());

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return annotation;
                    }

                    if (json["result"].Count > 0)
                    {
                        annotation = GetAnnotationFromResult(json, 0);
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("getById >> Cannot access returned json object values : " + exc.Message);
                    throw new InvalidOperationException();
                }
            }
            else
            {
                Debug.Print("getById >> null json.");
            }

            return annotation;
        }


        public async Task<Annotation> Insert(Annotation annotation)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Culture = CultureInfo.InvariantCulture;
            settings.DateFormatString = string.Format("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;

            annotation.Tags.RemoveAll(x => x.Vote <= 0);
            Tag[] tags = annotation.Tags.ToArray();

            string serializedTags = JsonConvert.SerializeObject(tags, settings).ToString();

            JsonValue json = null;
            if (annotation.PhotoImageBytes != null)
            {
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add("operationType", "ADD");
                data.Add("userId", annotation.UserId);
                data.Add("tags", serializedTags);
                data.Add("title", annotation.Title);
                data.Add("vote", annotation.Vote.ToString());
                data.Add("latitude", annotation.Latitude.ToString(CultureInfo.InvariantCulture));
                data.Add("longitude", annotation.Longitude.ToString(CultureInfo.InvariantCulture));
                data.Add("date", DateTime.Parse(annotation.Date, CultureInfo.CurrentCulture).ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"));
                data.Add("x", (int)annotation.PhotoHotSpotPoint.X == 0 ? "0" : ((int)annotation.PhotoHotSpotPoint.X).ToString());
                data.Add("y", (int)annotation.PhotoHotSpotPoint.Y == 0 ? "0" : ((int)annotation.PhotoHotSpotPoint.Y).ToString());
                data.Add("wPhoto", (int)annotation.PhotoSize.X);
                data.Add("hPhoto", (int)annotation.PhotoSize.Y);
                data.Add("capturedPicture", new PostFileParameter(annotation.PhotoImageBytes, "image.jpg", "image/jpeg"));
                json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONMultiformPostRequest(RemoteStorageWS.BaseEndPoint + "Annotations", data),
                                            (string requestFailedMessage) =>
                                            {
                                                Debug.Print("Annotations >> " + requestFailedMessage);
                                            },
                                            null);
            }
            else
            {
                PostData data = new PostData();
                data.AddParameter("operationType", "ADD");
                data.AddParameter("userId", annotation.UserId);
                data.AddParameter("tags", serializedTags);
                data.AddParameter("title", annotation.Title);
                data.AddParameter("vote", annotation.Vote.ToString());
                data.AddParameter("latitude", annotation.Latitude.ToString(CultureInfo.InvariantCulture));
                data.AddParameter("longitude", annotation.Longitude.ToString(CultureInfo.InvariantCulture));
                data.AddParameter("date", DateTime.Parse(annotation.Date, CultureInfo.CurrentCulture).ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"));
                data.AddParameter("x", (int)annotation.PhotoHotSpotPoint.X == 0 ? "0" : ((int)annotation.PhotoHotSpotPoint.X).ToString());
                data.AddParameter("y", (int)annotation.PhotoHotSpotPoint.Y == 0 ? "0" : ((int)annotation.PhotoHotSpotPoint.Y).ToString());
                data.AddParameter("wPhoto", "0");
                data.AddParameter("hPhoto", "0");

                json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "Annotations", data),
                                           (string requestFailedMessage) =>
                                           {
                                               Debug.Print("Annotations >> " + requestFailedMessage);
                                           },
                                           null);
            }


            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print(json["description"].ToString());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                    });
  
                    return null;
                }

                if (annotation.PhotoImageBytes != null)
                {
                    if (json["result"].ToString().Equals("\"\""))
                    {
                        if (annotation.PhotoImageBytes != null)
                        {
                            Debug.Print("Annotations >> Error during image upload, no URL returned from server.");
                            return null;
                        }
                        else
                            return null;
                    }
                    else
                    {
                        annotation.PhotoURL = json["result"]["photoUrl"].ToString();
                        int explorationPoints;
                        int.TryParse(json["result"]["exploration"], out explorationPoints);
                        annotation.ExplorationPoints = explorationPoints;
                        return annotation;
                    }
                }
                else
                    return annotation;
            }
            else
            {
                Debug.Print("Annotations >> null json.");
                return null;
            }
        }

        private  Annotation GetAnnotationFromResult(JsonValue json, int index)
        {
            Annotation annotation = new Annotation();

            annotation.Id = json["result"][index]["id"];
            annotation.UserId = json["result"][index]["userId"];
            annotation.Title = json["result"][index]["title"];
            int vote = 0;
            int.TryParse(json["result"][index]["vote"], out vote);
            annotation.Vote = vote;
            DateTime date;
            DateTime.TryParse(json["result"][index]["date"], out date);
            annotation.Date = date.ToLocalTime().ToString();
            double latitude = 0;
            double.TryParse(json["result"][index]["latitude"], NumberStyles.Any, CultureInfo.InvariantCulture, out latitude);
            annotation.Latitude = latitude;
            double longitude = 0;
            double.TryParse(json["result"][index]["longitude"], NumberStyles.Any, CultureInfo.InvariantCulture, out longitude);
            annotation.Longitude = longitude;
            int xPhoto = 0;
            int.TryParse(json["result"][index]["xPhoto"], out xPhoto);
            int yPhoto = 0;
            int.TryParse(json["result"][index]["yPhoto"], out yPhoto);
            annotation.PhotoHotSpotPoint = new Vec2(xPhoto, yPhoto);
            int wPhoto = 0;
            int.TryParse(json["result"][index]["wPhoto"], out wPhoto);
            int hPhoto = 0;
            int.TryParse(json["result"][index]["hPhoto"], out hPhoto);
            annotation.PhotoSize = new Vec2(wPhoto, hPhoto);
            int votedFromCurrentUser;
            int.TryParse(json["result"][index]["flagVoted"], out votedFromCurrentUser);
            annotation.VotedFromCurrentUser = votedFromCurrentUser == 0 ? false : true;
            int createdFromCurrentUser;
            int.TryParse(json["result"][index]["flagSameUser"], out createdFromCurrentUser);
            annotation.CreatedFromCurrentUser = createdFromCurrentUser == 0 ? false : true;
            annotation.PhotoURL = json["result"][index]["photoURL"].ToString();
            annotation.PhotoURL = annotation.PhotoURL.Replace("\"", "");
            //annotation.PhotoURL = annotation.PhotoURL.Substring(2, annotation.PhotoURL.Length - 2);
            annotation.InUserArea = true;
            int firstInArea;
            int.TryParse(json["result"][index]["exploration"], out firstInArea);
            annotation.FirstInArea = firstInArea == 0 ? false : true;

            annotation.Tags = new List<Tag>();
            // parse the tags
            if (json["result"][index]["tags"].Count > 0)
            {
                List<Tag> tags = new List<Tag>();
                for (int j = 0, jLen = json["result"][index]["tags"].Count; j < jLen; j++)
                {
                    Tag tag = new Tag();
                    tag.Id = json["result"][index]["tags"][j]["id"];
                    tag.UserId = json["result"][index]["tags"][j]["userId"];
                    tag.Name = json["result"][index]["tags"][j]["name"];
                    vote = 0;
                    int.TryParse(json["result"][index]["tags"][j]["vote"], out vote);
                    tag.Vote = vote;

                    annotation.Tags.Add(tag);
                }
            }

            return annotation;
        }

    }
}
