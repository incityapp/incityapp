
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using InCityApp.Entities;
using InCityApp.DataProviders;

namespace InCityApp.DataInterfaces
{
    public class TagVoteInstanceDataInterface
    {
        private SQLiteDataProvider _sqlite;

        public TagVoteInstanceDataInterface()
        {
            _sqlite = new SQLiteDataProvider();
        }

        public List<TagVote> GetAllById(Tag tag)
        {
            List<TagVote> tagVotes = new List<TagVote>();

            // TODO > remove this if-else once the WS is ready
            if (InCityApplication.CurrentDataProviderType == InCityApplication.DataProviderType.DB)
            {
                tagVotes = _sqlite.DB.Table<TagVote>().Where(x => x.TagId == tag.Id && x.Vote >= 0).ToList();
            }
            else
            {

            }

            return tagVotes;
        }

        public TagVote GetLastById(int tagId)
        {
            List<TagVote> tagVotes = new List<TagVote>();

            // TODO > remove this if-else once the WS is ready
            if (InCityApplication.CurrentDataProviderType == InCityApplication.DataProviderType.DB)
            {
                tagVotes = _sqlite.DB.Table<TagVote>().Where(x => x.TagId == tagId).OrderBy(x => x.Date).ToList();
            }
            else
            {

            }

            if (tagVotes != null && tagVotes.Count > 0)
                return tagVotes.Last();
            else
                return null;
        }

        public int Insert(TagVote tagVoteInstance)
        {
            // TODO > remove this if-else once the WS is ready
            if (InCityApplication.CurrentDataProviderType == InCityApplication.DataProviderType.DB)
            {
                return _sqlite.DB.Insert(tagVoteInstance, tagVoteInstance.GetType());
            }
            else
            {
                return 0;
            }
        }
    }
}
