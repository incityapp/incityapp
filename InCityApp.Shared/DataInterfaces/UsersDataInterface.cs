
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Json;
using System.Diagnostics;

using Polly;

using InCityApp.Entities;
using InCityApp.DataProviders;
using InCityApp.DataInterfaces;
using InCityApp.Helpers;
using Xamarin.Forms;

namespace InCityApp.DataInterfaces
{
    public class UsersDataInterface : BaseDataInterface
    {
        private XTribeDataProvider _xtribeWS;

        public UsersDataInterface()
        {
            _xtribeWS = new XTribeDataProvider();
        }

        public async Task<User.AuthorizationData> Login(string deviceId, string userName, string password)
        {
            PostData data = new PostData();
            data.AddParameter("operationType", "LOGIN");
            data.AddParameter("user", userName);
            data.AddParameter("password", password);
            data.AddParameter("deviceId", deviceId);

            User.AuthorizationData authData = new User.AuthorizationData();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "UserTokens", data),
                                                 (string requestFailedMessage) =>
                                                 {
                                                     authData.ErrorMessage = requestFailedMessage;
                                                     Debug.Print("UserTokens >> " + requestFailedMessage);
                                                 },
                                                 (string connectionNotAvailableMessage) =>
                                                 {
                                                     authData.ErrorMessage = connectionNotAvailableMessage;
                                                     authData.State = User.AuthorizationState.CannotConnect;
                                                 }, false);

            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print("UserTokens >> " + json["description"].ToString());

                    authData.ErrorMessage = json["description"].ToString();
                    authData.State = User.AuthorizationState.Error;
                    return authData;
                }

                try
                {
                    authData.State = User.AuthorizationState.OK;
                    authData.UserId = json["result"]["userId"].ToString().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries)[0]; ;
                    Settings.UserId = authData.UserId;
                    Settings.UserToken = json["result"]["token"].ToString().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries)[0]; ;
                    Settings.UserName = userName;
                }
                catch (Exception exc)
                {
                    Debug.Print("UserTokens >> Error retrieving data : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("UserTokens >> null json.");
                authData.State = User.AuthorizationState.CannotConnect;
            }

            return authData;
        }

        public async Task<bool> IsUserLogged(string deviceId, string userId, string token)
        {
            GetData data = new GetData();
            data.AddParameter("userId", userId);
            data.AddParameter("device", deviceId);
            data.AddParameter("token", token);

            User.AuthorizationData authData = new User.AuthorizationData();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequest(RemoteStorageWS.BaseEndPoint + "UserTokens/check", data),
                                                 (string requestFailedMessage) =>
                                                 {
                                                     authData.ErrorMessage = requestFailedMessage;
                                                     Debug.Print("UserTokens/check >> " + requestFailedMessage);
                                                 },
                                                 (string connectionNotAvailableMessage) =>
                                                 {
                                                     authData.ErrorMessage = connectionNotAvailableMessage;
                                                     authData.State = User.AuthorizationState.CannotConnect;
                                                 });

            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print("UserTokens/check >> " + json["description"].ToString());
                    return false;
                }

                try
                {
                    int alreadyLogged = 0;
                    int.TryParse(json["result"]["Login"], out alreadyLogged);

                    return alreadyLogged == 0 ? false : true;
                }
                catch (Exception exc)
                {
                    Debug.Print("UserTokens/check >> Error retrieving data : " + exc.Message);
                }
            }
            else
            {
                Debug.Print("UserTokens/check >> null json.");
            }

            return true;
        }

        public async Task<bool> Logout(string deviceId, string userId, string token)
        {
            PostData data = new PostData();
            data.AddParameter("operationType", "LOGOUT");
            data.AddParameter("user", userId);
            data.AddParameter("device", deviceId);
            data.AddParameter("token", token);

            User.AuthorizationData authData = new User.AuthorizationData();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "UserTokens", data),
                                                 (string requestFailedMessage) =>
                                                 {
                                                     authData.ErrorMessage = requestFailedMessage;
                                                     Debug.Print("UserTokens(Logout) >> " + requestFailedMessage);
                                                 },
                                                 (string connectionNotAvailableMessage) =>
                                                 {
                                                     authData.ErrorMessage = connectionNotAvailableMessage;
                                                     authData.State = User.AuthorizationState.CannotConnect;
                                                 });

            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print("UserTokens(Logout) >> " + json["description"].ToString());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                    });
                    return false;
                }

                Settings.UserId = "";
                Settings.UserName = "";
                Settings.UserToken = "";

                return true;
            }
            else
            {
                Debug.Print("UserTokens(Logout) >> null json.");
            }

            return false;
        }

        public async Task<User.AuthorizationData> Register(string userName, string mail, string confirmMail, string password, bool agreement)
        {
            PostData data = new PostData();
            data.AddParameter("operationType", "ADD");
            data.AddParameter("name", userName);
            data.AddParameter("mail", mail);
            data.AddParameter("conf_mail", confirmMail);
            data.AddParameter("password", password);
            data.AddParameter("agreement", Convert.ToInt32(agreement).ToString());

            User.AuthorizationData regData = new User.AuthorizationData();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "Users", data),
                                                 (string requestFailedMessage) =>
                                                 {
                                                     regData.ErrorMessage = requestFailedMessage;
                                                     Debug.Print("Users (Register) >> " + requestFailedMessage);
                                                 },
                                                 (string connectionNotAvailableMessage) =>
                                                 {
                                                     regData.ErrorMessage = connectionNotAvailableMessage;
                                                     regData.State = User.AuthorizationState.CannotConnect;
                                                 }, false);

            if (json != null)
            {
                if (!json["code"].ToString().Equals("-1"))
                {
                    Debug.Print("Users (Register) >> " + json["description"].ToString());

                    if (json["description"].Count > 0)
                    {
                        regData.State = User.AuthorizationState.Error;
                        for (int i = 0, iLen = json["description"].Count; i < iLen; i++)
                            regData.ErrorMessage += json["description"][i].ToString() + '\n';
                    }

                    regData.State = User.AuthorizationState.Error;
                }
                else
                {
                    regData.State = User.AuthorizationState.OK;
                }
            }
            else
            {
                Debug.Print("Users (Register) >>  null json.");
                regData.State = User.AuthorizationState.CannotConnect;
            }

            return regData;
        }

        public async Task<int> GetUserScore(string userId)
        {
            int score = -1;

            GetData data = new GetData();
            data.AddParameter("userId", userId);
            string parameters = data.GetFormattedString();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "Users/getUserScore", data),
                                                    (string requestFailedMessage) =>
                                                    {
                                                        Debug.Print("Users/getUserScore >> " + requestFailedMessage);
                                                    },
                                                    null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("Users/getUserScore >> " + json["description"].ToString());
                        return score;
                    }

                    if (json["result"].Count > 0)
                    {
                        string value = json["result"][0]["Score"].ToString();
                        value = value.Replace("\"", "");
                        int.TryParse(value, out score);
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("Users/getUserScore >> Cannot access returned json object values : " + exc.Message);
                    throw new InvalidOperationException();
                }
            }
            else
            {
                Debug.Print("Users/getUserScore >> null json.");
                throw new NullReferenceException();
            }

            return score;
        }

        //public async Task<User.AuthorizationData> Authenticate(string userName, string password)
        //{
        //    PostData data = new PostData();
        //    data.AddParameter("username", userName);
        //    data.AddParameter("password", password);

        //    User.AuthorizationData authData = new User.AuthorizationData();

        //    //authData.GUID = "caa9af3349245";
        //    //Settings.UserToken = "xxx";
        //    //authData.State = User.AuthorizationState.OK;

        //    //return authData;

        //    JsonValue json = await SafeWSRequest(_xtribeWS, _xtribeWS.AsyncJSONPostRequest(_xtribeWS.BaseEndPoint + "user/login", data),
        //                                         (string requestFailedMessage) =>
        //                                         {
        //                                             authData.ErrorMessage = requestFailedMessage;
        //                                             Debug.Print("user/login >> " + requestFailedMessage);
        //                                         },
        //                                         (string connectionNotAvailableMessage) =>
        //                                         {
        //                                             authData.ErrorMessage = connectionNotAvailableMessage;
        //                                             authData.State = User.AuthorizationState.CannotConnect;
        //                                         });

        //    if (json != null)
        //    {
        //        authData.GUID = json["guid"].ToString().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries)[0];
        //        string userToken = json["token"].ToString().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries)[0];
        //        Settings.UserToken = userToken;
        //        authData.State = User.AuthorizationState.OK;

        //        return authData;
        //    }
        //    else
        //    {
        //        authData.State = User.AuthorizationState.Error;
        //        return authData;
        //    }
        //}

        //public async Task<User.AuthorizationData> Register(string userName, string mail, string confirmMail, string password, bool agreement)
        //{
        //    PostData data = new PostData();
        //    data.AddParameter("api-key", "1Xtribe.WebServ1");
        //    data.AddParameter("name", userName);
        //    data.AddParameter("mail", mail);
        //    data.AddParameter("conf_mail", confirmMail);
        //    data.AddParameter("pass", password);
        //    data.AddParameter("I_agree", Convert.ToInt32(agreement).ToString());

        //    User.AuthorizationData regData = new User.AuthorizationData();

        //    JsonValue json = await SafeWSRequest(_xtribeWS, _xtribeWS.AsyncJSONPostRequest(_xtribeWS.AlternativeEndPoint + "user", data),
        //                                         (string requestFailedMessage) =>
        //                                         {
        //                                             regData.ErrorMessage = requestFailedMessage;
        //                                             Debug.Print("user >> " + requestFailedMessage);
        //                                         },
        //                                         (string connectionNotAvailableMessage) =>
        //                                         {
        //                                             regData.ErrorMessage = connectionNotAvailableMessage;
        //                                             regData.State = User.AuthorizationState.CannotConnect;
        //                                         });

        //    if (json != null)
        //    {
        //        regData.GUID = json["guid"].ToString().Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries)[0];
        //        regData.State = User.AuthorizationState.OK;
        //        return regData; 
        //    }

        //    Debug.Print("user >> null json.");
        //    regData.State = User.AuthorizationState.Error;
        //    return regData;
        //}

        //public async Task<User.AuthorizationData> CheckIfAuthenticated(string userToken)
        //{
        //    User.AuthorizationData authData = new User.AuthorizationData();

        //    HttpWebRequest request = _xtribeWS.MakeHttpPostRequest(_xtribeWS.BaseEndPoint + "system/connect");
        //    request.Headers.Add("X-CSRF-Token", userToken);

        //    JsonValue json = await SafeWSRequest(_xtribeWS, _xtribeWS.AsyncJSONPostRequest(request),
        //                                         (string requestFailedMessage) =>
        //                                         {
        //                                             authData.ErrorMessage = requestFailedMessage;
        //                                             Debug.Print("system/connect >> " + requestFailedMessage);
        //                                         },
        //                                         (string connectionNotAvailableMessage) =>
        //                                         {
        //                                             authData.ErrorMessage = connectionNotAvailableMessage;
        //                                             authData.State = User.AuthorizationState.CannotConnect;
        //                                         });

        //    if (json != null)
        //    {
        //        authData.GUID = json["guid"];
        //        authData.AlreadyLoggedUser = await GetById(authData.GUID);
        //        if (authData.AlreadyLoggedUser == null)
        //            authData.State = User.AuthorizationState.Error;
        //        else
        //        {
        //            authData.State = User.AuthorizationState.OK;
        //        }

        //        return authData;
        //    }
        //    else
        //    {
        //        authData.State = User.AuthorizationState.Error;
        //        return authData;
        //    }
        //}

        //public async Task<User> GetById(string Id)
        //{
        //    User user = null;

        //    GetData data = new GetData();
        //    data.AddParameter("userId", Id);
        //    string parameters = data.GetFormattedString();

        //    JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "Users/getById", data),
        //                                            (string requestFailedMessage) =>
        //                                            {
        //                                                user = null;
        //                                                Debug.Print("Users/getById >> " + requestFailedMessage);
        //                                            },
        //                                            (string connectionNotAvailableMessage) =>
        //                                            {
        //                                                user = null;
        //                                            });

        //    if (json != null)
        //    {
        //        try
        //        {
        //            if (!json["code"].ToString().Equals("-1"))
        //            {
        //                Debug.Print("Users/getById >> " + json["description"].ToString());
                        
        //                Device.BeginInvokeOnMainThread(() =>
        //                {
        //                    MessagingCenter.Send<BaseDataInterface>(this, "OnConnectionLost");
        //                });
        //                return null;
        //            }

        //            if (json["result"].Count > 0)
        //            {
        //                user = new User();
        //                user.Id = json["result"][0]["id"];
        //                user.Username = json["result"][0]["userName"];
        //                user.Token = json["result"][0]["token"];
        //                return user;
        //            }
        //        }
        //        catch
        //        {
        //            Debug.Print("Users/getById >> Cannot access returned json object values.");
        //            throw new InvalidOperationException();
        //        }
        //    }
        //    else
        //    {
        //        Debug.Print("Users/getById >> null json.");
        //        throw new NullReferenceException();
        //    }
           
        //    return null;
        //}

        //public async Task<bool> Insert(User user)
        //{
        //    PostData data = new PostData();
        //    data.AddParameter("operationType", "ADD");
        //    data.AddParameter("id", user.Id);
        //    data.AddParameter("userName", user.Username);
        //    data.AddParameter("token", user.Token);
        //    string x = data.GetFormattedString();
        //    JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONPostRequest(RemoteStorageWS.BaseEndPoint + "Users", data),
        //                                        (string requestFailedMessage) =>
        //                                        {
        //                                            Debug.Print("Users Insert >> " + requestFailedMessage);
        //                                        },
        //                                        null);

        //    if (json != null)
        //    {
        //        if (!json["code"].ToString().Equals("-1"))
        //        {
        //            Debug.Print(json["description"].ToString());
        //            Device.BeginInvokeOnMainThread(() =>
        //            {
        //                MessagingCenter.Send<BaseDataInterface>(this, "OnConnectionLost");
        //            });
        //            return false;
        //        }

        //        return true;
        //    }
        //    else
        //    {
        //        Debug.Print("Users Insert >> null json.");
        //        return false;
        //    }
        //}
    }
}
