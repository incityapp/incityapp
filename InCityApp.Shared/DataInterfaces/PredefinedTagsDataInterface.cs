
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Json;
using System.Threading.Tasks;
using System.Diagnostics;

using InCityApp.Entities;
using InCityApp.DataProviders;
using Xamarin.Forms;

namespace InCityApp.DataInterfaces
{
    public class PredefinedTagsDataInterface : BaseDataInterface
    {
        public async Task<List<PredefinedTag>> GetAllByUser(User user)
        {
            List<PredefinedTag> predefinedTags = new List<PredefinedTag>();

            GetData data = new GetData();
            data.AddParameter("userId", user.Id);
            string parameters = data.GetFormattedString();

            JsonValue json = await SafeWSRequest(RemoteStorageWS, RemoteStorageWS.AsyncJSONGetRequestArray(RemoteStorageWS.BaseEndPoint + "Tags/getAllByUser", data),
                                                (string requestFailedMessage) =>
                                                {
                                                    Debug.Print("Tags/getAllByUser >> " + requestFailedMessage);
                                                    throw new InvalidOperationException();
                                                },
                                                null);

            if (json != null)
            {
                try
                {
                    if (!json["code"].ToString().Equals("-1"))
                    {
                        Debug.Print("Tags/getAllByUser >> " + json["description"].ToString());
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<BaseDataInterface, string>(this, "OnServerError", json["description"].ToString());
                        });
                        return null;
                    }
                    if (json["result"].Count > 0)
                    {
                        for (int i = 0, iLen = json["result"].Count; i < iLen; i++)
                        {
                            PredefinedTag predefinedTag = new PredefinedTag();
                            predefinedTag.Id = json["result"][i]["id"];
                            predefinedTag.UserId = json["result"][i]["userId"];
                            predefinedTag.Name = json["result"][i]["name"];
                            predefinedTags.Add(predefinedTag);
                        }
                    }
                }
                catch
                {
                    Debug.Print("Tags/getAllByUser >> Cannot access returned json object values.");
                    throw new InvalidOperationException();
                }
            }
            else
            {
                Debug.Print("Tags/getAllByUser >> null json. ID : " + user.Id);
                //throw new NullReferenceException();
            }
            

            return predefinedTags;
        }
    }
}
