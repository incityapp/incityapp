
using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace InCityApp.Entities
{
    public class StepSession
    {
		public int Id { get; set; }

		public Int64 Steps { get; set; }

		public DateTime Date { get; set; }
    }
}
