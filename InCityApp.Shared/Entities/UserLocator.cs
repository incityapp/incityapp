using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Xamarin.Forms;
using InCityApp.Models;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using InCityApp.DataModels;
using InCityApp.DependencyServiceInterfaces;
using Xamarin.Geolocation;
using System.IO;
using InCityApp.Helpers;

namespace InCityApp.Entities
{
    /// <summary>
    /// User device locator.
    /// Singleton shared class used to keep track of device current position and movement informations,
    /// through the specific platform API provided at target-OS-application level
    /// </summary>
    public class UserLocator
    {
        /// <summary>
        /// Device location is changed significatively (based on _locationChangedThreshold).
        /// Event routed from the specific OS implementation.
        /// </summary>
        public System.Action<MapLocation, double> OnLocationChanged;
        public System.Action<ILocatable> OnAccurateLocationChanged;
        public Action OnInitialLocationAcquired;

        /// <summary>
        /// Device location is changed (a minimal movement is enough).
        /// Event routed from the specific OS implementation.
        /// </summary>
        //public System.Action<ILocatable> OnAccurateLocationChanged;

        /// <summary>
        /// User activity is changed (walk, run, cycle...).
        /// Event routed from the specific OS implementation.
        /// </summary>
        public System.Action<string> OnUserActivityChanged;

        public enum ActivityType
        {
            Unknown,
            Walking,
            Running,
            OnBicycle,
            InVehicle,
            Still,
            EndEnum
        }

        // TODO > REMEMBER TO set this to 5 meters before the production deploy
        // TODO > read this from a configuration file/DB
        static private float _locationDisplacementThreshold = 2.0f; // in meters
        static private int _locationUpdateTime = 20000; // in milliseconds


        static private float _annotationToVoteTimeThreshold = 15; //  minutes
        static private DateTime _lastNotificationTime = new DateTime();
        static private float _annotationToVoteDistanceThreshold = 200f; // in meters
        static private bool _canNotifyNewAnnotationToVote = true;
        static private MapLocation _lastPlaceNotificationSent;

        static private bool _firstLocationEvent = true;
        static private bool _firstValidLocationEvent = true;        

        static private int _positionRecordingCallTime = 30000; // in milliseconds

        static private object _lock = new object();

        static private readonly UserLocator _instance = new UserLocator();
        static private bool _isLocationServiceEnabled;
        static private bool _isLocationServiceAvailable;
        static private MapLocation _currentLocation;
        static private MapLocation _oldLocation;

        static private bool _useDebugActivity = false;
        static private ActivityType _debugActivity;

        static private ActivityType _currentActivity;
        static private int _currentActivityAccuracy;
        
        static private ActivityType _lastActivity;
        static private int _lastActivityAccuracy;

        static private string _currentActivityString;

        // TODO > read this from a configuration file/DB
        static private double _userAreaRadius = 200;

        static private ConcurrentBag<RoutePosition> _trackedPositions = new ConcurrentBag<RoutePosition>();
        static private RoutePosition _lastRoutePosition;
        static private DateTime _lastRouteEventTime = new DateTime();
        static private int _trackedPositionsCurrentIndex = 0;

        static private ConcurrentQueue<RoutePosition> _currentRoute = new ConcurrentQueue<RoutePosition>();

        static private KalmanLatLong kalamanFilter = new KalmanLatLong(3.0f);

        public static UserLocator Instance
        {
            get
            {
                return _instance;
            }
        }

        public static ConcurrentQueue<RoutePosition> currentRoute
        {
            get { return UserLocator._currentRoute; }
        }

        public static float LocationDisplacementThreshold
        {
            get { return UserLocator._locationDisplacementThreshold; }
        }

        public static int LocationUpdateTime
        {
            get { return UserLocator._locationUpdateTime; }
        }

        public bool isLocationServiceAvailable
        {
            get { return _isLocationServiceAvailable; }
            set { _isLocationServiceAvailable = value;  }
        }

        public bool isLocationServiceEnabled
        {
            get { return _isLocationServiceEnabled; }
            set { _isLocationServiceEnabled = value; }
        }

        public MapLocation OldLocation
        {
            get { return _oldLocation; }
        }

        public MapLocation CurrentLocation
        {
            get { return _currentLocation; }
            set 
            {
                bool fireChange = false;
                bool isDifferentFromOld = false;
                double moveDistance = 0;

                lock (_lock)
                {
                    if (_currentLocation == null)
                    {
                        kalamanFilter.SetState(value.Latitude, value.Longitude, (float)value.Accuracy, DateTime.Now.Ticks );
                        fireChange = true;
                        isDifferentFromOld = true;

                        _oldLocation = new MapLocation(value.Latitude, value.Longitude, value.Speed, value.Accuracy);
                        _currentLocation = new MapLocation(value.Latitude, value.Longitude, value.Speed, value.Accuracy);
                    }
                    else
                    {
                        kalamanFilter.Process(value.Latitude, value.Longitude, (float)value.Accuracy, DateTime.Now.Ticks);

                        if ((value.Speed > 0 || _currentLocation.Speed > 0) && ( value.Latitude != _currentLocation.Latitude || value.Longitude != _currentLocation.Longitude ))
                            isDifferentFromOld = true;

                        _oldLocation.SetLatLongSpeedAccuracy(_currentLocation.Latitude, _currentLocation.Longitude, _currentLocation.Speed, _currentLocation.Accuracy);

                        _currentLocation.SetLatLongSpeedAccuracy(kalamanFilter.get_lat(), kalamanFilter.get_lng(), value.Speed, value.Accuracy); 
                    }

                    LogLocation("Accepted", _currentLocation, value);

                    if (_firstLocationEvent)
                    {
                        _firstLocationEvent = false;
                        if (OnInitialLocationAcquired != null)
                            OnInitialLocationAcquired();
                    }


                    //if (_currentLocation.Accuracy < 40)
                    //{
                    //    if (_currentActivity == ActivityType.Running && _currentLocation.Speed < 1.6f)
                    //    {
                    //        _currentActivity = ActivityType.Walking;
                    //    }
                    //}
                    //else
                    //{
                    //    UserLocator.Instance.LogLocation("Discarded", _currentLocation);
                    //    return;
                    //}

                    if (!fireChange) //_oldLocation != null
                    {
                        if (isDifferentFromOld)
                        {
                            if (_firstValidLocationEvent)
                            {
                                _firstValidLocationEvent = false;

                                _lastPlaceNotificationSent = _currentLocation;
                            }

                            // throw the event only if the position is really changed by atleast '_locationChangedThreshold' meters
                            moveDistance = _currentLocation.DistanceFrom(_oldLocation);
                            if (moveDistance >= _locationDisplacementThreshold)
                            {
                                RoutePosition routePosition = new RoutePosition();
                                double time = 0;
                                if (_lastRouteEventTime.Year != 1)
                                {
                                    time = DateTime.Now.Subtract(_lastRouteEventTime).TotalMinutes;
                                }

                                if (_lastActivity == _currentActivity)
                                {
                                    routePosition = new RoutePosition(_currentLocation, time, moveDistance / 1000, _currentActivity, _currentActivityAccuracy);
                                    _trackedPositions.Add(routePosition);
                                }
                                else
                                {
                                    routePosition = new RoutePosition(_currentLocation, time, moveDistance / 1000, _lastActivity, _lastActivityAccuracy);
                                    _trackedPositions.Add(routePosition);
                                    routePosition = new RoutePosition(_currentLocation, 0, 0, _currentActivity, _currentActivityAccuracy);
                                    _trackedPositions.Add(routePosition);
                                }                               

                                _lastRoutePosition = routePosition;
                                _lastRouteEventTime = DateTime.Now;
                                _lastActivity = _currentActivity;
                                _lastActivityAccuracy = _currentActivityAccuracy;

                                _currentRoute.Enqueue(routePosition);

                                fireChange = true;
                            }
                        }
                    }

                }

                if (isDifferentFromOld)
                {
                    if (OnAccurateLocationChanged != null)
                        OnAccurateLocationChanged(_currentLocation);
                }

                if (fireChange)
                {
                    if (OnLocationChanged != null)
                        OnLocationChanged(_currentLocation, moveDistance);
                }

                CheckForNewNotification();
            }
        }

        public ActivityType CurrentActivity
        {
            get { return _currentActivity; }
        }

        public void SetCurrentActivity(ActivityType natAct, int accuracy)
        {
            //this line can be commented to disable activity type improvements
           ActivityType type = filterActivityType(natAct, accuracy < 0);

           LogActivityChange(Enum.GetName(typeof(ActivityType), _currentActivity), Enum.GetName(typeof(ActivityType), natAct), Enum.GetName(typeof(ActivityType), type), accuracy);

            //if _useDebugActivity is true, ignore update and use a default value for the activity
            /*if (_useDebugActivity)
                _currentActivity = _debugActivity;
            else*/
                _currentActivity = type;

            _currentActivityAccuracy = accuracy;
            if (OnUserActivityChanged != null)
                OnUserActivityChanged("Activity Detected : " + Enum.GetName(typeof(ActivityType), _currentActivity) + "| Accuracy : " + accuracy.ToString());
        }


        #region Activity Guess Filter

        private const int END_ACTIVITY_THR = 2;
        private const double STILL_THR = 0.001;
        private const double VEHICLE_SPEED_THR = 4;
        private int zeroSpeedGuessCount = 0;
        private bool waitUserToStart = true;

        /// <summary>
        /// Generate a more accurate activity detection using speed and user inputs 
        /// </summary>
        private ActivityType filterActivityType(ActivityType typeGuess, bool fromUser)
        {
            //wait user to start the selected activity
            //waitUserToStart = waitUserToStart && (CurrentLocation.Speed < STILL_THR);
         
            /*if(fromUser)
            //activity specified by user, not a guess.
            {
                waitUserToStart = true;
                return typeGuess;
            }*/

            return removeFlickeringFromActivity(typeGuess);


            //update the numper of updates for which the user speed has been 0

            /*if ( CurrentLocation != null)
                zeroSpeedGuessCount = (CurrentLocation.Speed < STILL_THR && !waitUserToStart) ? zeroSpeedGuessCount + 1 : 0;

            if (
                (zeroSpeedGuessCount >= END_ACTIVITY_THR) || //wait END_ACTIVITY_THR times before considering an activity finished 
                (zeroSpeedGuessCount > 0 && _currentActivity == ActivityType.Unknown) //guess 'still' if speed is almost 0 and the activity is unknown
            )
            //the user is still, end the current activity
            {
                return ActivityType.Still;
            }*/

            /*if(_currentActivity == ActivityType.Still)
            //no activity has been specified, we have to guess.
            {
                if(CurrentLocation.Speed > VEHICLE_SPEED_THR)
                {
                    return ActivityType.InVehicle;
                }
                else if(typeGuess == ActivityType.OnBicycle || typeGuess == ActivityType.Running || typeGuess == ActivityType.Walking)
                {
                    return ActivityType.Walking;
                }
            }*/

            //if we aren't sure: keep the previous 
            //return _currentActivity;
        }

        //Activity flickering removal
        private const int HISTORY_SIZE = 3;
        private ActivityType[] activityHistory;
        private int ahIndex;
        private ActivityType removeFlickeringFromActivity(ActivityType activity)
        {
            if (activity == ActivityType.Unknown) //Don't store this type of activity
                return activity;

            if (CurrentLocation == null || CurrentLocation.Speed <= 0.001)
            {
                return ActivityType.Still;
            }

            if (activityHistory == null)
            //if history is not available, create it.
            {
                activityHistory = new ActivityType[HISTORY_SIZE];
                ahIndex = 0;

                for (int i = 0; i < HISTORY_SIZE; i++)
                {
                    activityHistory[i] = activity;
                }
            }
            else
            //update it with the new activity otherwise
            {
                activityHistory[ahIndex] = activity;
                ahIndex = (ahIndex + 1) % HISTORY_SIZE;
            }

            //keep the most frequent activity in history as current activity.
            //this can remove wrong activities during a transition into a new one
            //removing transition up to (HISTORY_SIZE / 2) in length
            ActivityType mostCommon = (from item in activityHistory
                                       group item by item into g
                                       orderby g.Count() descending
                                       select g.Key).First();

            return mostCommon;
        }

        #endregion

        public double UserAreaRadius
        {
            get { return _userAreaRadius; }
            set { _userAreaRadius = value; }
        }

        public ConcurrentBag<RoutePosition> TrackedPositions
        {
            get { return _trackedPositions; }
        }

        static UserLocator ()
        {
 
        }

        UserLocator() 
        {
            if (_instance == null)
            {
                StartPositionRecording();
            }
        }

        public void SetDebugActivity(ActivityType activity)
        {
            _useDebugActivity = false;// true;
            _debugActivity = activity;
        }

        public void CheckForNewNotification()
        {
            if (_canNotifyNewAnnotationToVote)
            {
                _canNotifyNewAnnotationToVote = false;
                NotifyAnnotationToVote();
            }
            else
            {
                if (_currentLocation != null && _lastPlaceNotificationSent != null)
                {
                    double distanceFromLastNotificationPlace = _currentLocation.DistanceFrom(_lastPlaceNotificationSent);

                    if (distanceFromLastNotificationPlace >= _annotationToVoteDistanceThreshold)
                    {
                        NotifyAnnotationToVote();
                    }
                }
            }
        }

        /*public async Task DrawLastRoute()
        {
            List<RoutePosition> lastRoute = await RoutesModel.GetInDateRange(DateTime.Now.ToUniversalTime().AddDays(-1), DateTime.Now.ToUniversalTime());
            Device.BeginInvokeOnMainThread(() =>
            {
                if (lastRoute != null && lastRoute.Count > 0)
                    DependencyService.Get<IMapCustomRenderer>().DrawRoute(lastRoute);
            });
        }*/

        public void DrawRouteBetween(MapLocation start, MapLocation end)
        {
            //Device.BeginInvokeOnMainThread(() =>
            //{
            //    List<RoutePosition> route = new List<RoutePosition>();
            //    route.Add(new RoutePosition(end, 0, 0, 0, 0));
            //    route.Add(new RoutePosition(new MapLocation(47, 8), 0, 0, 0, 0));
            //    DependencyService.Get<IMapCustomRenderer>().DrawRoute(route);
            //});
        }

        public ActivityStats GetActivitiesFromMemory()
        {
            ActivityStats stats = new ActivityStats();

            if (_trackedPositions.Count > 0)
            {
                RoutePosition[] routePositions = _trackedPositions.ToArray();
                for (int i = 0; i < routePositions.Length; i++) 
                {
                    stats.DistanceSum[Enum.GetName(typeof(UserLocator.ActivityType), (UserLocator.ActivityType)routePositions[i].Activity)] += routePositions[i].DistanceFromLastEvent;
                    stats.TimeSum[Enum.GetName(typeof(UserLocator.ActivityType), (UserLocator.ActivityType)routePositions[i].Activity)] += routePositions[i].MinutesFromLastEvent;
                }
            }

            return stats;
        }

        public void LogLocation(string tag, MapLocation location, MapLocation realLoc)
        {
#if DEBUG
                string path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                string filename = Path.Combine(path, "LogInCity.txt");

                try
                {
                    using (var streamWriter = new StreamWriter(filename, true))
                    {
                        string info = string.Format("{0} >> Location Accuracy : {1} | Speed : {2} | Lat : {3} | Lon : {4} | Timestamp : {5} | Activity : {6} | Activity Accuracy : {7} | RLat : {8} | RLon : {9} ",
                            /* 0 */                 tag,
                            /* 1 */                 location.Accuracy,
                            /* 2 */                 location.Speed.ToString("0.000"),
                            /* 3 */                 location.Latitude.ToString("0.000000000000"),
                            /* 4 */                 location.Longitude.ToString("0.000000000000"),
                            /* 5 */                 DateTime.Now.ToString(),
                            /* 6 */                 Enum.GetName(typeof(UserLocator.ActivityType), (UserLocator.ActivityType)_currentActivity),
                            /* 7 */                 _currentActivityAccuracy,
                            /* 8 */                 realLoc.Latitude.ToString("0.000000000000"),
                            /* 9 */                 realLoc.Longitude.ToString("0.000000000000")
                        );

                        streamWriter.WriteLine(info);
                    }
                }
                catch (Exception exc)
                { 
                
                }
#endif

        }

        public void LogActivityChange(string actCurrent, string actNative, string actFiltered, int accuracy)
        {
#if DEBUG
                string path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                string filename = Path.Combine(path, "LogInCity.txt");

                try
                {
                    using (var streamWriter = new StreamWriter(filename, true))
                    {
                        string info = string.Format("Activity changed: {0} - {1} >> {2} wa: {3} - sp: {4}",
                            actCurrent,
                            actNative,
                            actFiltered,
                            accuracy,
                            CurrentLocation.Speed
                        );

                        streamWriter.WriteLine(info);
                    }
                }
                catch (Exception exc)
                {

                }

#endif
        }

        private void StartPositionRecording()
        {
            // gets last route from yesterday
            //List<RoutePosition> lastRoute = await RouteModel.GetInDateRange(DateTime.Now.AddDays(-1), DateTime.Now);
            //DependencyService.Get<IMapCustomRenderer>().DrawRoute(lastRoute);

            //// generates LocationChanged events to draw the last route
            //MapLocation location = new MapLocation();
            //foreach (RoutePosition routePos in lastRoute)
            //{
            //    location.Latitude = routePos.Latitude;
            //    location.Longitude = routePos.Longitude;
            //    if (OnLocationChanged != null)
            //        OnLocationChanged(location);
            //}


            

            // record the tracked position from another thread each N milliseconds (30 seconds by default)
            Timer timer = new Timer(RecordPositions, null, 0, _positionRecordingCallTime);
        }

        private async void RecordPositions(object threadState)
        {
            if (_trackedPositions.Count > 0)
            {
                RoutePosition[] routePositions = _trackedPositions.ToArray();
                for (int i = _trackedPositionsCurrentIndex; i < routePositions.Length; i++)
                {
                    if (await RoutesModel.Insert(routePositions[i]) == true)
                        _trackedPositionsCurrentIndex++;
                }
            }
        }

        private void NotifyAnnotationToVote()
        {
            TimeSpan span = DateTime.Now.Subtract(_lastNotificationTime);
            if (span.Minutes > _annotationToVoteTimeThreshold)
            {
                _lastNotificationTime = DateTime.Now;
                _lastPlaceNotificationSent = _currentLocation;

                Task.Run(async () =>
                {
                    try
                    {
                        Annotation annotationToVoteInUserArea = await AnnotationsModel.IsThereAnnotationInUserArea();
                        if (annotationToVoteInUserArea != null)
                            DependencyService.Get<ILocalNotifications>().NotifyAnnotation(annotationToVoteInUserArea.Id.ToString(), "InCity", "C'e una nuova annotazione da votare in questa zona.");
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message);
                    }
                });
            }
        }
    }
}

