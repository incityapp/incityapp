using System;
using InCityApp.Services;

namespace InCityApp.Entities
{
    public class Address : ILocatable
	{
        public const int EQUATOR_RADIUS = 6378137;

        protected bool _inUserArea = false;

        public string Description { get; set; }
        public string Street { get; set; }
        public string Unit { get; set; }
        public string City { get; set; }
        [Display("Postal Code")]
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        #region ILocatable implementation

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool InUserArea
        {
            get
            {
                return _inUserArea;
            }
            set
            {
                _inUserArea = value;
            }
        }

        public double DistanceBetween(ILocatable a, ILocatable b)
        {
            double dLat = ConvertToRadians(b.Latitude - a.Latitude);
            double dLng = ConvertToRadians(b.Longitude - a.Longitude);

            double sindLat = Math.Sin(dLat / 2);
            double sindLng = Math.Sin(dLng / 2);

            double ang = Math.Pow(sindLat, 2) + Math.Pow(sindLng, 2) * Math.Cos(ConvertToRadians(a.Latitude)) * Math.Cos(ConvertToRadians(b.Latitude));

            double c = 2 * Math.Atan2(Math.Sqrt(ang), Math.Sqrt(1 - ang));

            return EQUATOR_RADIUS * c;
        }

        public double DistanceFrom(ILocatable other)
        {
            return DistanceBetween(this, other);
        }

        #endregion

        public override string ToString ()
        {
            return string.Format ("{0} {1} {2} {3} {4}", Street, !string.IsNullOrWhiteSpace(Unit) ? Unit + "," : string.Empty, !string.IsNullOrWhiteSpace(City) ? City + "," : string.Empty, State, PostalCode);
        }

        private double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
	}
}

