using System;
using System.Collections;
using System.Collections.Generic;
using InCityApp.Services;

namespace InCityApp.Entities
{
	public interface ILocation
	{
        Address Address { get; set; }
	}
}

