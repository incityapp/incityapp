using Newtonsoft.Json;
using System;

namespace InCityApp.Entities
{
    public interface ILocatable
    {
        double Latitude { get; }
        double Longitude { get; }

        double DistanceBetween(ILocatable a, ILocatable b);
        
        double DistanceFrom(ILocatable other);

        [JsonIgnore]
        bool InUserArea { get; set; }
    }
}

