using System.Collections.Generic;
using InCityApp.Services;
using System.ComponentModel;
using System;

namespace InCityApp.Entities
{
    public class MapLocation : ILocation, ILocatable, INotifyPropertyChanged
    {
        public const int EQUATOR_RADIUS = 6378137;
        protected bool _inUserArea = false;

        public MapLocation () { }

        public MapLocation(double latitude, double longitude) 
        {
            SetLatLong(latitude, longitude);
        }

        public MapLocation(double lat, double lon, double spd, double acc)
        {
            latitude = lat;
            longitude = lon;
            speed = spd;
            accuracy = acc;
        }

        public void SetLatLongSpeedAccuracy(double lat, double lon, double spd, double acc)
        {
            latitude = lat;
            longitude = lon;
            speed = spd;
            accuracy = acc;
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged (string propertyName)
        {
            var evt = PropertyChanged;
            if (evt == null) return;
            evt(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region ILocatable implementation

        private double latitude;
        private double longitude;

        public double Latitude { get { return latitude;} }
        public double Longitude { get { return longitude; } }

        public void SetLatLong( double lat,  double lon )
        {
            latitude = lat;
            longitude = lon;
        }

        #endregion

        #region ILocation implementation

        public Address Address { get; set; }

        public bool InUserArea
        {
            get 
            {
                return _inUserArea;
            }
            set
            {
                _inUserArea = value;
            }
        }

        public double DistanceBetween(ILocatable a, ILocatable b)
        {
            double earthRadius = EQUATOR_RADIUS; //6371

            double dLat = ConvertToRadians(b.Latitude - a.Latitude);
            double dLng = ConvertToRadians(b.Longitude - a.Longitude);

            double sindLat = Math.Sin(dLat / 2);
            double sindLng = Math.Sin(dLng / 2);

            double ang = Math.Pow(sindLat, 2) + Math.Pow(sindLng, 2) * Math.Cos(ConvertToRadians(a.Latitude)) * Math.Cos(ConvertToRadians(b.Latitude));

            double c = 2 * Math.Atan2(Math.Sqrt(ang), Math.Sqrt(1 - ang));

            return earthRadius * c;
        }

        public double DistanceFrom(ILocatable other)
        {
            return DistanceBetween(this, other);
        }

        #endregion

        private double speed;
        private double accuracy;

        public double Speed { get { return speed; } }
        public double Accuracy { get { return accuracy; } }

        public override string ToString ()
        {
            return string.Format ("<{0},{1}>",  Latitude, Longitude);
        }

        private double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
    }
}

