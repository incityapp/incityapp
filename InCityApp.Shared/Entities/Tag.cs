
using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace InCityApp.Entities
{
    public class Tag
    {
        public int Id { get; set; }

        [JsonIgnore]
        public int AnnotationId { get; set; }

        public string UserId { get; set; }

        public string Name { get; set; }

        public int Vote {get; set; }

        [JsonIgnore]
        public int Tagged { get; set; }

        [JsonIgnore]
        public string Date { get; set; }

        [JsonIgnore]
        public bool Changed { get; set; }

        public Tag Clone()
        {
            Tag tag = new Tag();
            tag.AnnotationId = AnnotationId;
            tag.UserId = UserId;
            tag.Name = Name;
            tag.Tagged = Tagged;
            return tag;
        }
    }
}
