﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InCityApp.Entities
{
    public class SoundStats
    {
        public double Max { get; set; }
        
        public double Avarage { get; set; }

        public double Min { get; set; }

        public SoundStats()
        {
            
        }
    }
}
