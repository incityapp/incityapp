
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Xamarin.Forms;

using Newtonsoft.Json;

using InCityApp.DataModels;
using Polly;
using System.Net;
using InCityApp.DataProviders;
using System.Diagnostics;

namespace InCityApp.Entities
{
    public class Annotation : ILocatable, IAnnotation
    {
        public const int EQUATOR_RADIUS = 6378137;

        public int Id { get; set; }

        public string UserId { get; set; }

        public List<Tag> Tags { get; set; }

        public string Title { get; set; }

        [JsonIgnore]
        public string Description { get; set; }

        public int Vote { get; set; }

        [JsonIgnore]
        public bool VotedFromCurrentUser { get; set; }

        [JsonIgnore]
        public bool CreatedFromCurrentUser { get; set; }

        [JsonIgnore]
        public string PhotoURL { get; set; }

        /// <summary>
        /// Filled with the local file name when a photo is selected, 
        /// </summary>
        [JsonIgnore]
        public string PhotoLocalName { get; set; }

        [JsonIgnore]
        public byte [] PhotoImageBytes { get; set; }

        [JsonIgnore]
        public Vec2 PhotoHotSpotPoint { get; set; }

        public Vec2 PhotoSize { get; set; }

        [JsonIgnore]
        public bool FirstInArea { get; set; }

        [JsonIgnore]
        public int ExplorationPoints { get; set; }

        #region ILocatable implementation

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Date { get; set; }

        [JsonIgnore]
        public bool InUserArea { get; set; }

        public double DistanceBetween(ILocatable a, ILocatable b)
        {
            double earthRadius = EQUATOR_RADIUS; //6371

            double dLat = ConvertToRadians(b.Latitude - a.Latitude);
            double dLng = ConvertToRadians(b.Longitude - a.Longitude);

            double sindLat = Math.Sin(dLat / 2);
            double sindLng = Math.Sin(dLng / 2);

            double ang = Math.Pow(sindLat, 2) + Math.Pow(sindLng, 2) * Math.Cos(ConvertToRadians(a.Latitude)) * Math.Cos(ConvertToRadians(b.Latitude));

            double c = 2 * Math.Atan2(Math.Sqrt(ang), Math.Sqrt(1 - ang));

            return earthRadius * c;
        }

        public double DistanceFrom(ILocatable other)
        {
            return DistanceBetween(this, other);
        }

        #endregion
    
        public override string ToString ()
        {
            return string.Format ("<{1},{2}>", Latitude, Longitude);
        }

        private double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }

        public Annotation () : base () {}

        public Annotation (ILocatable location)
        {
            Latitude = location.Latitude;
            Longitude = location.Longitude;
            InUserArea = location.InUserArea;
            Tags = new List<Tag>();
        }

        public Annotation(double latitude, double longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
            Tags = new List<Tag>();
        }

        public async Task LoadTags()
        {
            try
            {
                //List<PredefinedTag> predefinedTags = await TagsModel.GetPredefinedTagsName();
                List<PredefinedTag> predefinedTags = await Policy.Handle<InvalidOperationException>()
                                       .WaitAndRetryAsync(new[] { TimeSpan.FromSeconds(DataProviderSettings.RetryTime[0]), 
                                                                  TimeSpan.FromSeconds(DataProviderSettings.RetryTime[1]), 
                                                                  TimeSpan.FromSeconds(DataProviderSettings.RetryTime[2]) })
                                       .ExecuteAsync<List<PredefinedTag>>(() => TagsModel.GetPredefinedTagsName());

                foreach (PredefinedTag tag in predefinedTags)
                    Tags.Add(new Tag() { Name = tag.Name, Id = tag.Id, UserId = tag.UserId, Vote = -1, Tagged = 0 });
            }
            catch (Exception exc)
            {
                Debug.Print("LoadTags >> " + exc.Message);
                throw exc;
            }
        }

        public bool HasTag(string tagName, out Tag tag)
        {
            tag = null;
            if (Tags == null || Tags.Count == 0)
                return false;
            
            tag = Tags.Find(x => x.Name == tagName);
            return tag != null;
        }

        public bool HasTag(string tagName)
        {
            Tag dummy;
            return HasTag(tagName, out dummy);
        }
    }
}
