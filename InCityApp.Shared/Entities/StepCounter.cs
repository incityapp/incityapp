using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace InCityApp.Entities
{
    /// <summary>
    /// Step Counter.
    /// Singleton shared class used to keep track StepCounter Service informations, 
    /// it basically route the events from the service and expose some stored informations.
    /// </summary>
    public class StepCounter
    {
        /// <summary>
        /// Volume of recorded audio is changed.
        /// Event routed from the specific OS application.
        /// </summary>
        public EventHandler<long> OnStepsChanged;

        static readonly StepCounter _instance = new StepCounter(); 

        private long _stepsCount = 0;

        public static StepCounter Instance
        {
            get
            {
                return _instance;
            }
        }

        public long stepsCount { get { return _stepsCount;} }

        public long StepCount
        {
            get { return _stepsCount; }
            set {
                Device.BeginInvokeOnMainThread(() =>
                {
                    _stepsCount = value;
                    if (OnStepsChanged != null)
                        OnStepsChanged(this, _stepsCount);
                });
            }
        }

        static StepCounter ()
        {
 
        }

        StepCounter() 
        {
                    
        }
    }
}

