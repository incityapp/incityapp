
using System;
using System.Collections.Generic;
using System.Text;

using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace InCityApp.Entities
{
    [Table ("TagVotes")]
    public class TagVote
    {
        public int TagId { get; set; }

        public int Vote {get; set; }

        //public int VotesAverage { get; set; }

        public string Date { get; set; }

        public TagVote() { }

        public TagVote(Tag tag)
        {
            TagId = tag.Id;
            Vote = tag.Vote;
            //VotesAverage = tag.VotesAverage;
            Date = tag.Date;
        }
    }
}
