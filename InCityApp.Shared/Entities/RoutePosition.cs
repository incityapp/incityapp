
using System;
using System.Collections.Generic;
using System.Text;

#if __ANDROID__
using Android.Gms.Maps.Model;
#endif

namespace InCityApp.Entities
{
    public class RoutePosition
    {
        public double Latitude { get; set; }
        
        public double Longitude { get; set; }

        public string Timestamp { get; set; }

        public double MinutesFromLastEvent { get; set; }

        public UserLocator.ActivityType Activity { get; set; }

        public double ActivityAccuracy { get; set; }

        public double DistanceFromLastEvent { get; set; } // in KM

        public RoutePosition() 
        {
            Timestamp = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"); 
        }

        public RoutePosition(double latitude, double longitude) : base ()
        {
            Latitude = latitude;
            Longitude = longitude;
            Timestamp = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"); 
        }

        public RoutePosition(MapLocation mapLocation, double minutesFromLastEvent, double distanceFromLastEvent, UserLocator.ActivityType activity, int accuracy) : base ()
        {
            Latitude = mapLocation.Latitude;
            Longitude = mapLocation.Longitude;
            Timestamp = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss");
            MinutesFromLastEvent = minutesFromLastEvent;
            DistanceFromLastEvent = distanceFromLastEvent;
            Activity = activity;
            ActivityAccuracy = accuracy;
        }

        public MapLocation ToMapLocation()
        {
            return new MapLocation(Latitude, Longitude);
        }

#if __ANDROID__
        public LatLng ToLatLng()
        {
            return new LatLng(Latitude, Longitude);
        }
#endif
    }
}
