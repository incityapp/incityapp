using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;

using Xamarin.Forms;
using InCityApp.Models;
using InCityApp.DataModels;
using System.Threading.Tasks;
using Syncfusion.SfChart.XForms;
using InCityApp.Shared.ViewModels;


namespace InCityApp.Entities
{
    /// <summary>
    /// Audio Recorder.
    /// Singleton shared class used to keep track of audio informations
    /// through the specific platform API provided at target-OS-application level (ie: an Activity inside an Android project)
    /// </summary>
    public class AudioRecorder
    {
        // 1 second
        public const int RECORD_SAMPLE_TIME = 5000; // in milliseconds
        // 5 minutes
        public const int RECORD_SESSION_TIME = 300000; // in milliseconds

        /// <summary>
        /// Volume of recorded audio is changed.
        /// Event routed from the specific OS application.
        /// </summary>
        public EventHandler<double> OnFiveSecondsVolume;
        public static EventHandler<double> OnOneSecondVolume;

        private static List<ChartDataPoint> _avaragedChartDataPoints = new List<ChartDataPoint>();

        public static object Locker = new object();

        public ConcurrentStack<SoundAtPosition> FiveMinutesSamples = new ConcurrentStack<SoundAtPosition>();
        public ConcurrentQueue<SoundAtPosition> Last30Seconds = new ConcurrentQueue<SoundAtPosition>();
        //private static bool _savingSamples;

        static readonly AudioRecorder _instance = new AudioRecorder();

        private double _currentOneSecondVolume;
        private double _currentFiveSecondsVolume;
        private double _oldFiveSecondsVolume;

        private static NoisePollutionViewModel noiseModel;

        public static List<ChartDataPoint> AvaragedChartDataPoints
        {
            get { /*lock (Locker)*/ { return _avaragedChartDataPoints; } }
            set { /*lock (Locker)*/ { _avaragedChartDataPoints = value; } }
        }

        public static AudioRecorder Instance
        {
            get
            {
                return _instance;
            }
        }

        static AudioRecorder ()
        {
 
        }

        AudioRecorder() 
        {
                    
        }

        public double CurrentVolume
        {
            get { return _currentOneSecondVolume; }
            set
            {
                _currentOneSecondVolume = value;

                if (OnOneSecondVolume != null)
                    OnOneSecondVolume(this, _currentOneSecondVolume);
                else
                {
                    if (noiseModel == null)
                        noiseModel = new NoisePollutionViewModel();

                    noiseModel.ComputeAvaragedData(_currentOneSecondVolume);
                }
            }
        }

        public void AddSoundSample(double value)
        {
            if (UsersModel.CurrentUser == null)
                return;

            SoundAtPosition[] tmpArray = null;

            lock (Locker)
            {
                _oldFiveSecondsVolume = _currentFiveSecondsVolume;
                _currentFiveSecondsVolume = value;
                
                int recordSampleRate = RECORD_SAMPLE_TIME / 1000; // milliseconds to seconds
                int recordSessionTime = (RECORD_SESSION_TIME / 1000) / 60; // milliseconds to minutes

                if (UserLocator.Instance.CurrentLocation != null)
                {
                    SoundAtPosition soundAtPosition = new SoundAtPosition();
                    soundAtPosition.UserId = UsersModel.CurrentUser.Id;
                    soundAtPosition.Latitude = UserLocator.Instance.CurrentLocation.Latitude;
                    soundAtPosition.Longitude = UserLocator.Instance.CurrentLocation.Longitude;
                    soundAtPosition.SoundDecibelValue = (float)_currentFiveSecondsVolume;

                    FiveMinutesSamples.Push(soundAtPosition);
                }
                else
                {
                    Console.WriteLine("InCityApp.Entities.AudioRecorder - ERROR! : Missing user location!");
                }

                if (FiveMinutesSamples.Count >= (60 / recordSampleRate) * recordSessionTime)
                {
                    tmpArray = FiveMinutesSamples.ToArray();
                    FiveMinutesSamples.Clear();
                }
            }

            if (tmpArray != null && tmpArray.Length > 0)
            {
                Task.Run(async () =>
                {
                    await SoundsModel.Insert(tmpArray);
                });
            }

            if (OnFiveSecondsVolume != null)
                OnFiveSecondsVolume(this, _currentFiveSecondsVolume);
        }
    }
}

