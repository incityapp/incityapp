
using System;
using System.Collections.Generic;
using System.Text;

using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using InCityApp.DataModels;

namespace InCityApp.Entities
{
    [Table ("AnnotationApprovation")]
    public class AnnotationApprovation
    {
        public int UserId { get; set; }

        public int AnnotationId { get; set; }

        public int Approvation {get; set; }

        public string Date { get; set; }

        public AnnotationApprovation() { }

        public AnnotationApprovation(Annotation annotation)
        {
            UserId = UsersModel.CurrentUser.Id;
            AnnotationId = annotation.Id;
            Approvation = 0;
        }
    }
}
