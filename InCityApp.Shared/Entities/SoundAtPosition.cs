
using System;
using System.Collections.Generic;
using System.Text;

#if __ANDROID__
using Android.Gms.Maps.Model;
#endif

namespace InCityApp.Entities
{
    public class SoundAtPosition
    {
        public string UserId { get; set; }

        public double Latitude { get; set; }
        
        public double Longitude { get; set; }

        public string Timestamp { get; set; }

        public float SoundDecibelValue { get; set; }

        public SoundAtPosition() 
        {
            Timestamp = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"); 
        }

        public SoundAtPosition(double latitude, double longitude) : base ()
        {
            Latitude = latitude;
            Longitude = longitude;
            Timestamp = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"); 
        }

        public SoundAtPosition(MapLocation mapLocation)
            : base()
        {
            Latitude = mapLocation.Latitude;
            Longitude = mapLocation.Longitude;
            Timestamp = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss"); 
        }

        public MapLocation ToMapLocation()
        {
            return new MapLocation(Latitude, Longitude);
        }

#if __ANDROID__
        public LatLng ToLatLng()
        {
            return new LatLng(Latitude, Longitude);
        }
#endif
    }
}
