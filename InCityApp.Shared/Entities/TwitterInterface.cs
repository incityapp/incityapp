using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;

using Xamarin.Forms;

using InCityApp.Models;

using Xamarin.Social.Services;
using Xamarin.Social;

#if __ANDROID__
using Android.App;
using Android.Content;
using Xamarin.Auth;
#endif

namespace InCityApp.Entities
{
    /// <summary>
    /// Twitter native interface.
    /// Singleton shared class used to send command to native Twitter implementations and to receive feedbacks.
    /// </summary>
    public class TwitterInterface
    {
        /// <summary>
        /// Should be received from the native renderer as a command to start the "post" call.
        /// </summary>
        public event Action<string> OnTwitMessage;
        /// <summary>
        /// Should be received from the shared library to know when an attempt to "post" was succesfully performed.
        /// </summary>
        public static event Action OnTwitPublished;
        /// <summary>
        /// Should be received from the shared library to know when an attempt to "post" has failed.
        /// </summary>
        public event Action OnTwitFailed;

        static private readonly TwitterInterface _instance = new TwitterInterface();

        public static TwitterInterface Instance
        {
            get
            {
                return _instance;
            }
        }

        static TwitterInterface ()
        {
            
        }

        TwitterInterface() 
        {

        }

        public void TwitMessage(string message)
        {
#if __ANDROID__
            AndroidTwitMessage(message);
#endif

            if (OnTwitMessage != null)
                OnTwitMessage(message);
        }

#if __ANDROID__
        private static async void AndroidTwitMessage(string message)
        {
            try
            {
                Activity activity = Forms.Context as Activity;

                TwitterService twitter = new TwitterService()
                {
                    ConsumerKey = "F2w7cCyxnPMphtedeZbvgAANU",
                    ConsumerSecret = "ogHd4wdGsAsjXA3syodlhCZVZoH0b4aB82qA0I24TtHyhY5kI8",
                    CallbackUrl = new System.Uri("https://www.incityapp.eu/index.php"),
                };

                var item = new Item { Text = message };
                item.Links.Add(new System.Uri("http://incityapp.eu"));

                IEnumerable<Account> accounts = await twitter.GetAccountsAsync(activity);
                Account account = accounts.FirstOrDefault();

                Intent uiIntent = null;

                if (account == null)
                {
                    uiIntent = twitter.GetAuthenticateUI(activity, async uiAccount =>
                    {
                        await twitter.ShareItemAsync(item, account, CancellationToken.None).ContinueWith(shareTask =>
                        {
                            if (shareTask.IsFaulted)
                            {
                                if (OnTwitPublished != null)
                                    OnTwitPublished();
                            }
                            else { }

                        }, TaskScheduler.FromCurrentSynchronizationContext());
                    });
                }
                else
                {
                    await twitter.ShareItemAsync(item, account).ContinueWith(shareTask =>
                    {
                        if (shareTask.IsFaulted)
                        {
                            if (OnTwitPublished != null)
                                OnTwitPublished();
                        }
                        else { }

                    }, TaskScheduler.FromCurrentSynchronizationContext());
                }

                if (uiIntent != null)
                    activity.StartActivity(uiIntent);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
#endif 

    }
}

