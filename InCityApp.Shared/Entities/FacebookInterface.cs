﻿using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;

using Xamarin.Forms;

using InCityApp.Models;
using Xamarin.Social.Services;
using Xamarin.Social;
using Xamarin.Auth;

#if __ANDROID__
using Android.App;
using Android.Content;
#endif

namespace InCityApp.Entities
{
    /// <summary>
    /// Facebook native interface.
    /// Singleton shared class used to send command to native Facebook implementations and to receive feedbacks.
    /// </summary>
    public class FacebookInterface
    {
        /// <summary>
        /// Should be received from the native renderer as a command to start the "post" call.
        /// </summary>
        public event Action<string> OnPostMessage;
        /// <summary>
        /// Should be received from the shared library to know when an attempt to "post" was succesfully performed.
        /// </summary>
        public static event Action OnPostPublished;
        /// <summary>
        /// Should be received from the shared library to know when an attempt to "post" has failed.
        /// </summary>
        public event Action OnPostFailed;

        public List<Account> Accounts;
        public Account ActiveAccount;

        static private readonly FacebookInterface _instance = new FacebookInterface();

        public static FacebookInterface Instance
        {
            get
            {
                return _instance;
            }
        }

        static FacebookInterface()
        {

        }

        FacebookInterface()
        {

        }

        public void PostMessage(string message)
        {
#if __ANDROID__
            AndroidPostMessage(message);
#endif
            if (OnPostMessage != null)
                OnPostMessage(message);
        }

#if __ANDROID__
        private static async void AndroidPostMessage(string message)
        {
            try
            {
                Activity activity = Forms.Context as Activity;

                FacebookService facebook = new FacebookService()
                {
                    ClientId = "",
                    Scope = "publish_actions",
                    RedirectUrl = new System.Uri("https://www.incityapp.eu/index.php"),
                };

                var item = new Item { Text = message };
                item.Links.Add(new System.Uri("https://incityapp.eu"));

                IEnumerable<Account> accounts = await facebook.GetAccountsAsync(activity);
                Account account = accounts.FirstOrDefault();

                Intent uiIntent = null;

                if (account == null)
                {
                    uiIntent = facebook.GetAuthenticateUI(activity, async uiAccount =>
                    {
                        await facebook.ShareItemAsync(item, account).ContinueWith(shareTask =>
                        {
                            if (shareTask.IsFaulted)
                            {
                                if (OnPostPublished != null)
                                    OnPostPublished();
                            }
                            else { }

                        }, TaskScheduler.FromCurrentSynchronizationContext());
                    });
                }
                else
                {
                    await facebook.ShareItemAsync(item, account).ContinueWith(shareTask =>
                    {
                        if (shareTask.IsFaulted)
                        {
                            if (OnPostPublished != null)
                                OnPostPublished();
                        }
                        else { }

                    }, TaskScheduler.FromCurrentSynchronizationContext());
                }

                if (uiIntent != null)
                    activity.StartActivity(uiIntent);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
#endif

    }
}