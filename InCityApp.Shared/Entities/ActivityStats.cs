﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InCityApp.Entities
{
    public class ActivityStats
    {
        private Dictionary<string, double> _distanceSum = new Dictionary<string, double>();
        private Dictionary<string, double> _timeSum = new Dictionary<string, double>();

        public Dictionary<string, double> DistanceSum
        {
            get { return _distanceSum; }
            set { _distanceSum = value; }
        }

        public Dictionary<string, double> TimeSum
        {
            get { return _timeSum; }
            set { _timeSum = value; }
        }

        public ActivityStats()
        {
            for (int i = 0; i < (int)UserLocator.ActivityType.EndEnum; i++)
            {
                _distanceSum.Add(Enum.GetName(typeof(UserLocator.ActivityType), (UserLocator.ActivityType)i), 0);
                _timeSum.Add(Enum.GetName(typeof(UserLocator.ActivityType), (UserLocator.ActivityType)i), 0);
            }
        }
    }
}
