using System;

namespace InCityApp.Entities
{
    public class User 
    {
        public enum AuthorizationState
        {
            OK,
            Error,
            CannotConnect
        }

        public class AuthorizationData
        {
            public string UserId { get; set; }
            
            public AuthorizationState State { get; set; }
            
            public string ErrorMessage { get; set; }
        }

        public User():base()
        {
        }

        public string Id {
            get;  set;
        }

        public string Username {
            get; set;
        }

        public string EMail {
            get; set;
        }

        public string Token { get; set; }
    }
}

