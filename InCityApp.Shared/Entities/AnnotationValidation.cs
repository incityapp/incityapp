
using System;
using System.Collections.Generic;
using System.Text;

using InCityApp.DataModels;

namespace InCityApp.Entities
{
    public class AnnotationValidation
    {
        public string UserId { get; set; }

        public int AnnotationId { get; set; }

        public int GuessVote { get; set; }

        public string Date { get; set; }

        public AnnotationValidation() { }

        public AnnotationValidation(Annotation annotation, int guessVote)
        {
            UserId = UsersModel.CurrentUser.Id;
            AnnotationId = annotation.Id;
            GuessVote = guessVote;
            Date = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
