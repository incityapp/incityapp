using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace InCityApp.Entities
{
    public abstract class OptionItem
    {
        public virtual string Title { get { return string.Empty; } } // var n = GetType().Name; return n.Substring(0, n.Length - 10); 
        public virtual int Count { get; set; }
        public virtual bool Selected { get; set; }
        public virtual string Icon { get { return 
                Title.ToLower().TrimEnd('s') + ".png" ; } }
        public ImageSource IconSource { get { return ImageSource.FromFile(Icon); } }
    }

    public class VoteAnnotationsOptionItem : OptionItem
    {
        public override string Title { get { return "Mappa"; } }
        public override string Icon { get { return "opportunity.png"; } }
    }

    public class NoisePollutionOptionItem : OptionItem
    {
        public override string Title { get { return "Rilevamento Rumore"; } }
        public override string Icon { get { return "opportunity.png"; } }
    }
}

