﻿using System;

namespace InCityApp
{
	public class MetersCounter
	{
		public enum eMovementType
		{
			Foot,
			Bike,
			Other,

			None
		}

		float[] _metersRegistered = new float[(int)eMovementType.None];
		DateTime _date;

		public MetersCounter(DateTime date)
		{
			Reset (date);
		}

		public void Reset(DateTime date)
		{
			_date = date;
			for (int i = 0; i < (int)eMovementType.None; i++)
				_metersRegistered [i] = 0;
		}

		public float GetMeters(eMovementType type)
		{
			if (((int)type) < 0 || type >= eMovementType.None)
				throw new InvalidOperationException ("type " + ((int)type).ToString () + " invalid!");
			return _metersRegistered [(int)type];
		}

		public void AddMeters(eMovementType type, float meters)
		{
			if (((int)type) < 0 || type >= eMovementType.None)
				throw new InvalidOperationException ("type " + ((int)type).ToString () + " invalid!");
			if(meters < 0)
				throw new InvalidOperationException ("meters " + meters.ToString () + " invalid!");

			_metersRegistered [(int)type] += meters;
		}

	}
}

