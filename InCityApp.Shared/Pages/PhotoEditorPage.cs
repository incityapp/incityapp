﻿using InCityApp.Pages;
using InCityApp.Shared.ViewModels;
using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Shared.Pages
{
    public class PhotoEditorPage : BasePage
    {
        public Action<int, int, int, int> OnHotPointSelected;

        private AbsoluteLayout _layout;
        private Label lblDescription;
        private StackLayout buttonsLayout;
        private Button doneBtn;
        private Button cancelBtn;
        //Button removeHotSpotBtn;

        private EditableImage _editableImage;
        private int _imageWidth, _imageHeight;
        private int _imageX, _imageY;
        private Image _hotSpotIcon;

        private double topMargin;

        // marker image resource name 
        // (it's took from the target OS resources, so it's must have the same name on each platform)
        private const string _hotspotMarkerImage = "hotspot.png";

        // marke image size
        // remember to change this properly if you change the marker image resource 
        private const int _hotspotMarkerWidth = 64;
        private const int _hotspotMarkerHeight = 64;

        public enum EditMode
        { 
            None,
            SetHotSpot,
            //etc..
        }

        public PhotoEditorPage(ImageSource imageSource, int width, int height, bool editable = true)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = new PhotoEditorViewModel(Navigation);
            BackgroundColor = Color.White;

            _imageWidth = width;
            _imageHeight = height;

            _layout = new AbsoluteLayout()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
            };

            lblDescription = new Label()
            {
                TextColor = Color.FromHex("#333333"),
                XAlign = TextAlignment.Center,
                YAlign = TextAlignment.Center,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold
            };

            _layout.Children.Add( lblDescription );

            if (editable)
                lblDescription.Text = "Per favore indica la zona di interesse in questa immagine";
            else
                lblDescription.Text = "EVIDENZA";

            _editableImage = new EditableImage()
            {
                Source = imageSource,
                ImageWidth = width,
                ImageHeight = height
            };

            _layout.Children.Add(_editableImage);

            _editableImage.OnClick += HandleEditableImageOnClick;

            if (editable)
                _editableImage.CurrentEditMode = EditableImage.EditMode.HotSpot;

            buttonsLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            if (editable)
            {
                doneBtn = new Button() { Text = "CONFERMA" };
                doneBtn.Clicked += btnDone_Clicked;
                buttonsLayout.Children.Add(doneBtn);
            }

            cancelBtn = new Button();
            cancelBtn.Clicked += cancelBtn_Clicked;

            if (editable)
                cancelBtn.Text = "ANNULLA";
            else
                cancelBtn.Text = "CHIUDI";

            buttonsLayout.Children.Add(cancelBtn);

                /*removeHotSpotBtn = new Button() { Text = "Annulla Hotspot", BackgroundColor = Color.FromHex("52B296") };
                removeHotSpotBtn.Clicked += removeHotSpotBtn_Clicked;*/

                _layout.Children.Add(buttonsLayout);
           

            _hotSpotIcon = new Image() { Source = ImageSource.FromFile(_hotspotMarkerImage), Aspect = Aspect.AspectFit };

            this.SizeChanged += OnSizeChangedEvent;

            Content =  _layout;
        }

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentPage page = (ContentPage)sender;

            double wndWidth = page.Width;
            double wndHeight = page.Height;

            double screenRatio = (page.Width / 1080);

            double spacing = 10;

            topMargin = 120 * screenRatio;
   
            lblDescription.HeightRequest = topMargin;
            lblDescription.WidthRequest = page.Width;
            lblDescription.FontSize = 45 * screenRatio;

            AbsoluteLayout.SetLayoutBounds(lblDescription, new Rectangle(0f, 0f, lblDescription.Width, lblDescription.Height));

            buttonsLayout.Spacing = 10 * screenRatio;

            cancelBtn.WidthRequest = (page.Width - (3 * buttonsLayout.Spacing)) / 2;

            if (doneBtn != null)
            {
                doneBtn.WidthRequest = cancelBtn.WidthRequest;
                InCityApp.Helpers.LayoutFormatter.LayoutForDefaultButton(doneBtn, screenRatio);
            }

            InCityApp.Helpers.LayoutFormatter.LayoutForDefaultButton(cancelBtn, screenRatio);

            AbsoluteLayout.SetLayoutBounds(buttonsLayout, new Rectangle((page.Width - (doneBtn != null ? 2 : 1) * cancelBtn.WidthRequest) * 0.5, page.Height - topMargin - spacing, page.Width, topMargin));

            AbsoluteLayout.SetLayoutBounds(_editableImage, new Rectangle(0f, topMargin, page.Width, page.Height -  2*spacing - 2 * topMargin));

            _hotSpotIcon.HeightRequest = _hotspotMarkerWidth * screenRatio;
            _hotSpotIcon.WidthRequest = _hotspotMarkerHeight * screenRatio;
        }

        public void SetHotspot(int x, int y)
        {
            double xRatio = _editableImage.ContainerWidth / _imageWidth;

            double realImageHeight = _imageHeight * xRatio;
            double realScrenHeight = InCityApplication.RealScreenHeight;
            double yOffset = (realScrenHeight - realImageHeight) * 0.5;

            double nx = x * xRatio;
            double ny = (y * xRatio) + yOffset;

            _hotSpotIcon.AnchorX = 0;
            _hotSpotIcon.AnchorY = 0;

            _layout.LowerChild(_editableImage);

            _layout.Children.Add(_hotSpotIcon);
            AbsoluteLayout.SetLayoutBounds(_hotSpotIcon, new Rectangle((nx - _hotSpotIcon.Width * 0.5), (ny - _hotSpotIcon.Height * 0.5), _hotSpotIcon.Width, _hotSpotIcon.Height));
        }


        void HandleEditableImageOnClick(int x, int y, int imageX, int imageY)
        {
            _editableImage.CurrentEditMode = EditableImage.EditMode.HotSpot;

            _imageX = imageX;
            _imageY = imageY;

            _hotSpotIcon.AnchorX = 0;
            _hotSpotIcon.AnchorY = 0;

            _layout.LowerChild(_editableImage);

            _layout.Children.Add(_hotSpotIcon);
            AbsoluteLayout.SetLayoutBounds(_hotSpotIcon, new Rectangle((x - _hotSpotIcon.Width * 0.5), (y - _hotSpotIcon.Height * 0.5), _hotSpotIcon.Width, _hotSpotIcon.Height));
        }

        async void btnDone_Clicked(object sender, EventArgs e)
        {
            if (!_layout.Children.Contains(_hotSpotIcon))
            {
                await DisplayAlert("Selezione zona obbligatoria", "Non hai selezionato una zona di interesse", "Ok");
                return;
            }

            await Navigation.PopAsync(false);
            if (OnHotPointSelected != null)
                OnHotPointSelected(_imageX, _imageY, _imageWidth, _imageHeight);
        }

        async void cancelBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync(false);
        }

        void removeHotSpotBtn_Clicked(object sender, EventArgs e)
        {
            if (_hotSpotIcon != null)
            {
                _layout.Children.Remove(_hotSpotIcon);
            }

            _editableImage.CurrentEditMode = EditableImage.EditMode.HotSpot;
        }

    }
}
