﻿using InCityApp.Entities;
using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Pages
{
    public class AnnotationExplorationAward : ContentPage
	{
        public event Action OnClosed;

        private StackLayout globalContainer;
        StackLayout buttonsStack;
        private AbsoluteLayout cockadeLayout;
        private Image cockadeBg;
        private FixedLabel titleLbl;
        private FixedLabel annotationNameLbl;
        private FixedLabel pointsLbl;
        private Button goBtn;

        private Annotation currentAnnotation;
        int guessVote;
        int points = 0;

        public AnnotationExplorationAward(Annotation ann)
		{
            NavigationPage.SetHasNavigationBar(this, false);

            currentAnnotation = ann;

            this.SizeChanged += OnSizeChangedEvent;

            pointsLbl = new FixedLabel
            {
                Text = String.Format("+ {0} pt", currentAnnotation.ExplorationPoints),
                TextColor = Color.White,
                XAlign = TextAlignment.Center,
                FontSize = 90,
                FontAttributes = FontAttributes.Bold
            };

            titleLbl = new FixedLabel
            {
                Text = "Complimenti è la prima annotazione in questa zona!",
                TextColor = Color.FromHex("#57ba9d"),
                XAlign = TextAlignment.Center,
                FontSize = 55
            };

            annotationNameLbl = new FixedLabel
            {
                Text = currentAnnotation.Title.ToUpper(),
                TextColor = Color.Black,
                XAlign = TextAlignment.Center,
                FontSize = 55
            };
             
            cockadeBg = new Image
            {
                Source = ImageSource.FromFile("AnnotationQuizResultCockade")
            };

            goBtn = new Button
            {
                Text = "OK",
                TextColor = Color.White,
                BackgroundColor = Color.FromHex("#57ba9d"),
                FontSize = 32,
                VerticalOptions = LayoutOptions.End,
                FontAttributes = FontAttributes.Bold
            };

            goBtn.Clicked += goButtonClicked;

            buttonsStack = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.End,

                Children = {
                    goBtn
				}
            };

            cockadeLayout = new AbsoluteLayout
            {
                Children =
                {
                    cockadeBg,
                    pointsLbl,
                }
            };

            globalContainer = new StackLayout
            {
                Padding = 0,
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White,

				Children =
                {
                    titleLbl,
                    annotationNameLbl,
					cockadeLayout,
                    buttonsStack
				}
			};

            Content = globalContainer;
		}

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentPage page = (ContentPage)sender;

            double wndWidth = page.Width;
            double wndHeight = page.Height;

            double screenRatio = (page.Width / 1080);

            double space = 50 * screenRatio;
            globalContainer.Padding = new Thickness(0, space, 0, space);
            globalContainer.Spacing = space;

            annotationNameLbl.FontSize = 55 * screenRatio;

            cockadeBg.WidthRequest = 756 * screenRatio;
            cockadeBg.HeightRequest = 547 * screenRatio;

            AbsoluteLayout.SetLayoutBounds(cockadeBg, new Rectangle((page.Width - cockadeBg.Width) * 0.5, 0f, cockadeBg.Width, cockadeBg.Height));

            titleLbl.FontSize = 55 * screenRatio;
            AbsoluteLayout.SetLayoutBounds(titleLbl, new Rectangle((page.Width - titleLbl.Width) * 0.5, cockadeBg.Y + (cockadeBg.Height - titleLbl.Height) * 0.5, titleLbl.Width, titleLbl.Height));

            pointsLbl.FontSize = 55 * screenRatio;
            AbsoluteLayout.SetLayoutBounds(pointsLbl, new Rectangle((page.Width - pointsLbl.Width) * 0.5, cockadeBg.Y + (cockadeBg.Height - pointsLbl.Height) * 0.5, pointsLbl.Width, pointsLbl.Height));
 
            goBtn.WidthRequest = 490 * screenRatio;
            goBtn.HeightRequest = 120 * screenRatio;
            goBtn.FontSize = 32 * screenRatio;

            //AbsoluteLayout.SetLayoutBounds(buttonsStack, new Rectangle((page.Width - buttonsStack.Width) * 0.5, page.Height - 40 * screenRatio, buttonsStack.Width, buttonsStack.Height));
 
        }

        void goButtonClicked(object sender, EventArgs e)
        {
            if (OnClosed != null)
                OnClosed();
        }
	}
}
