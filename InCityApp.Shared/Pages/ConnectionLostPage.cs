﻿
using InCityApp.DataModels;
using InCityApp.DataProviders;
using InCityApp.DependencyServiceInterfaces;
using InCityApp.Helpers;
using InCityApp.Views;
using Polly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace InCityApp.Pages
{
    public class ConnectionLostPage : ContentPage
    {
        static bool _lastCheckWasReachable = true;
        static bool _reachable = true;
        static bool _currentPage;


        public ConnectionLostPage(string message = "Provare nuovamente appena la connessione internet è disponibile.")
        {
            _reachable = false;
            _currentPage = true;

            FixedLabel lblDescription = new FixedLabel()
            {
                BackgroundColor = Color.FromRgb(220, 220, 220),
                Text = message,
                XAlign = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.CenterAndExpand, 
                VerticalOptions = LayoutOptions.Fill, 
            };

            FixedButton btnRetry = new FixedButton() 
            {
                Text = "OK",
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.FromHex("52B296"),
            };
            btnRetry.Clicked += async (s, e) =>
            {

                    btnRetry.IsEnabled = false;
                    bool isOurWSReachable = await Connectivity.Instance.IsOurWSConnected();
                    btnRetry.IsEnabled = true;

                    if (isOurWSReachable)
                    {
                        _lastCheckWasReachable = _reachable;
                        _reachable = true;
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<ConnectionLostPage>(this, "OnConnectionAvailable");
                        });

                        _currentPage = false;
                        await Navigation.PopModalAsync(true);
                    }
                    else
                    {
                        _lastCheckWasReachable = _reachable;
                        _reachable = false;
                        lblDescription.Text = "Server non raggiungibile. Riprovare in seguito.";
                    }
            };

            StackLayout innerStack = new StackLayout
            {
                BackgroundColor = Color.FromRgb(220, 220, 220),
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                WidthRequest = 400,
                Children = 
                {
                    new BoxView() { HeightRequest = 20 },
                    new Label() 
                    { 
                        BackgroundColor = Color.FromRgb(220, 220, 220),
                        TextColor = Color.Black,
                        Text = " Connettività assente",
                        FontSize = 22,
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.CenterAndExpand, 
                        FontAttributes = FontAttributes.Bold,
                    },
                    new BoxView() { HeightRequest = 5 },
                    new StackLayout()
                    {
                        Padding = new Thickness (10, 0),
                        Children = 
                        {
                            lblDescription,
                        }
                    },
                    new BoxView() { HeightRequest = 5 },
                    btnRetry,
                    new BoxView() { HeightRequest = 5 },
                }
            };

            StackLayout stack = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                WidthRequest = InCityApplication.ScreenWidth,
                HeightRequest = InCityApplication.ScreenHeight,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent,
                Children = 
                {
                    new BoxView() { HeightRequest = (InCityApplication.RealScreenHeight / 3)},
                    innerStack,
                }
            };

            Content = stack;
        }
    }
}
