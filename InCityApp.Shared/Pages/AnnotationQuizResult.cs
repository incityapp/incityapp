﻿using InCityApp.Entities;
using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Pages
{
    public class AnnotationQuizResult : ContentPage
	{
        private StackLayout globalContainer;
        private AbsoluteLayout cockadeLayout;
        private Image cockadeBg;
        private Label annotationNameLbl;
        private Label pointsLbl;
        private Button goBtn;
        private Label guessVoteTitle;
        private StackLayout guessVoteStack;
        private FiveStarsRatingView guessRating;
        private Label realVoteTitle;
        private StackLayout realVoteStack;
        private FiveStarsRatingView realRating;

        private Annotation currentAnnotation;
        int guessVote;
        int points = 0;

        public AnnotationQuizResult( Annotation ann, int guessVt, int pt )
		{
            NavigationPage.SetHasNavigationBar(this, false);

            currentAnnotation = ann;
            guessVote = guessVt;
            points = pt;

            this.SizeChanged += OnSizeChangedEvent;

            pointsLbl = new Label
            {
                Text = String.Format("+ {0} pt", points),
                TextColor = Color.White,
                XAlign = TextAlignment.Center,
                FontSize = 90,
                FontAttributes = FontAttributes.Bold
            };

            annotationNameLbl = new Label
            {
                Text = currentAnnotation.Title.ToUpper(),
                TextColor = Color.Black,
                XAlign = TextAlignment.Center,
                FontSize = 55
            };
             
            cockadeBg = new Image
            {
                Source = ImageSource.FromFile("AnnotationQuizResultCockade")
            };

            guessVoteTitle = new Label
            {
                Text = "Questa è la tua valutazione:",
                TextColor = Color.Black,
                XAlign = TextAlignment.Center,
                FontSize = 40,
                FontAttributes = FontAttributes.Bold
            };

            guessVoteStack = new StackLayout() { Orientation = StackOrientation.Vertical, HorizontalOptions = LayoutOptions.Center };

            guessRating = new FiveStarsRatingView(guessVote, false)
            {
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                IsVisible = false,
            };

            guessVoteStack.Children.Add(guessRating);

            guessRating.IsVisible = true;
            guessRating.SetGuessMode(false, true);

            realVoteTitle = new Label
            {
                Text = "Questa è la valutazione reale:",
                TextColor = Color.Black,
                XAlign = TextAlignment.Center,
                FontSize = 40,
                FontAttributes = FontAttributes.Bold
            };

            realVoteStack = new StackLayout() { Orientation = StackOrientation.Vertical, HorizontalOptions = LayoutOptions.Center };

            realRating = new FiveStarsRatingView(ann.Vote, false)
            {
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                IsVisible = false,
            };

            realVoteStack.Children.Add(realRating);

            realRating.IsVisible = true;
            guessRating.SetGuessMode(false, true);

            cockadeLayout = new AbsoluteLayout
            {
                Children =
                {
                    cockadeBg,
                    pointsLbl
                }
            };

            goBtn = new Button
            {
                Text = "VAI ALL'ANNOTAZIONE",
                TextColor = Color.White,
                BackgroundColor = Color.FromHex("#57ba9d"),
                FontSize = 32,
                FontAttributes = FontAttributes.Bold
            };

            goBtn.Clicked += goButtonClicked;

            StackLayout buttonsStack = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.Center,

                Children = {
                    goBtn
				}
            };

            CustomNavigationView cstnavVW = new CustomNavigationView("Risultato");

            globalContainer = new StackLayout
            {
                Padding = 0,
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White,

				Children =
                {
                    cstnavVW,
                    annotationNameLbl,
					cockadeLayout,
                    guessVoteTitle,
                    guessVoteStack,
                    realVoteTitle,
                    realVoteStack,
                    buttonsStack
				}
			};

            Content = globalContainer;
		}

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentPage page = (ContentPage)sender;

            double wndWidth = page.Width;
            double wndHeight = page.Height;

            double screenRatio = (page.Width / 1080);

            double space = 50 * screenRatio;
            globalContainer.Padding = new Thickness(0, space, 0, space);
            globalContainer.Spacing = space;

            annotationNameLbl.FontSize = 55 * screenRatio;

            cockadeBg.WidthRequest = 756 * screenRatio;
            cockadeBg.HeightRequest = 547 * screenRatio;

            AbsoluteLayout.SetLayoutBounds(cockadeBg, new Rectangle((page.Width - cockadeBg.Width) * 0.5, 0f, cockadeBg.Width, cockadeBg.Height));

            pointsLbl.FontSize *= screenRatio;
            AbsoluteLayout.SetLayoutBounds(pointsLbl, new Rectangle((page.Width - pointsLbl.Width) * 0.5, cockadeBg.Y + (cockadeBg.Height - pointsLbl.Height) * 0.5, pointsLbl.Width, pointsLbl.Height));

            guessVoteTitle.FontSize = 40 * screenRatio;
            realVoteTitle.FontSize = 40 * screenRatio;

            goBtn.WidthRequest = 490 * screenRatio;
            goBtn.HeightRequest = 120 * screenRatio;
            goBtn.FontSize = 32 * screenRatio;
        }

        void goButtonClicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new AnnotationDialog(currentAnnotation, false), false);
        }
	}
}
