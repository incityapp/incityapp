
using InCityApp.Helpers;
using InCityApp.Pages;
using InCityApp.Shared.ViewModels;
using InCityApp.Shared.Views;
using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Xamarin.Forms;
using XLabs;
using XLabs.Forms.Controls;

namespace InCityApp.Shared.Pages
{
    public class RegistrationPage : BasePage
    {
        public System.Action OnRegistrationCompleted;

        private Button _btnRegister;
        private Button _btnBack;
        private ScrollView disclaimerScrollView;
        private Image logo;
        private LongLabel disclaimer;


        public RegistrationPage()
        {
            InCityApplication.InRegistrationPage = true;

            BindingContext = new RegistrationViewModel(this);

            BackgroundColor = Color.White;

            var layout = new StackLayout { Padding = 10 };

            StackLayout logoLayout = new StackLayout() { Orientation = StackOrientation.Horizontal, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand };

            logo = new Image
            {
                Source = (FileImageSource)FileImageSource.FromFile("inCityLogo"),
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent
            };
            logoLayout.Children.Add(logo);

            layout.Children.Add(logoLayout);

            var username = new Entry { Placeholder = "Nome Utente" };
            username.SetBinding(Entry.TextProperty, RegistrationViewModel.UsernamePropertyName);
            layout.Children.Add(username);

            var mail = new Entry { Placeholder = "E-Mail" };
            mail.SetBinding(Entry.TextProperty, RegistrationViewModel.MailPropertyName);
            layout.Children.Add(mail);

            var confirmMail = new Entry { Placeholder = "Conferma E-Mail" };
            confirmMail.SetBinding(Entry.TextProperty, RegistrationViewModel.ConfirmMailPropertyName);
            layout.Children.Add(confirmMail);

            var password = new Entry { Placeholder = "Password", IsPassword = true };
            password.SetBinding(Entry.TextProperty, RegistrationViewModel.PasswordPropertyName);
            layout.Children.Add(password);

            var passwordCof = new Entry { Placeholder = "Conferma Password", IsPassword = true };
            passwordCof.SetBinding(Entry.TextProperty, RegistrationViewModel.ConfPasswordPropertyName);
            layout.Children.Add(passwordCof);

            var errorMessage = new Label()
            {
                XAlign = TextAlignment.Center,
                TextColor = Color.Red,
            };

            errorMessage.SetBinding(Label.TextProperty, RegistrationViewModel.LogMessagePropertyName);
            layout.Children.Add(errorMessage);

            Label disclaimerTitle = new Label() { HorizontalOptions = LayoutOptions.Center };
            disclaimerTitle.Text = "- Disclaimer -";
            layout.Children.Add(disclaimerTitle);

            disclaimer = new LongLabel()
            {
                TextColor = Color.FromHex("52B296"),
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = InCityApplication.ScreenWidth / 2,
                XAlign = TextAlignment.Center, // Center the text in the box.
                YAlign = TextAlignment.Center, // Center the text in the box.
            };
            disclaimer.SetBinding(Label.TextProperty, RegistrationViewModel.DisclaimerPropertyName);

            Stream stream = ResourceLoader.GetEmbeddedResourceStream(Assembly.GetAssembly(typeof(ResourceLoader)), "disclaimerXTribe.txt");
            string disclaimerText = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                disclaimerText = reader.ReadToEnd();
            }

            disclaimer.Text = disclaimerText;

            disclaimerScrollView = new ScrollView()
            {
                Orientation = ScrollOrientation.Vertical,
                HeightRequest = 200
            };

            Frame tapulloframe = new Frame()
            {
                Padding = new Thickness(0, 0)
            };

            tapulloframe.Content = disclaimerScrollView;

            layout.Children.Add(tapulloframe);

            CheckBox agreement = new CheckBox()
            {
                CheckedText = "Accetto",
                UncheckedText = "Non Accetto",
                TextColor = Color.FromHex("52B296")
            };

            agreement.SetBinding(CheckBox.CheckedProperty, RegistrationViewModel.AgreementPropertyName);
            agreement.CheckedChanged += agreement_Changed;
            layout.Children.Add(agreement);

            var btnLayout = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.CenterAndExpand, BackgroundColor = Color.White };

            _btnRegister = new Button
            { 
                Text = "INVIA REGISTRAZIONE", 
            };

            btnLayout.Children.Add(_btnRegister);

            _btnRegister.SetBinding(FixedButton.CommandProperty, RegistrationViewModel.RegisterCommandPropertyName);

            _btnBack = new Button { Text = "ANNULLA" };
            btnLayout.Children.Add(_btnBack);
            _btnBack.IsEnabled = true;

            layout.Children.Add(btnLayout);
            _btnBack.Clicked += cancelButtonClicked;

            PrepareLayout(layout);

            this.SizeChanged += OnSizeChangedEvent;
        }

        protected override void OnAppearing()
        {
            _btnRegister.IsEnabled = false;

            InCityApp.Helpers.LayoutFormatter.LayoutChangeDefaultButtonForState(_btnRegister);
        }

        void agreement_Changed(object sender, EventArgs<bool> e)
        {
            if (e.Value)
            {
                _btnRegister.IsEnabled = true;
                InCityApp.Helpers.LayoutFormatter.LayoutChangeDefaultButtonForState(_btnRegister);
            }
            else
            {
                _btnRegister.IsEnabled = false;
                InCityApp.Helpers.LayoutFormatter.LayoutChangeDefaultButtonForState(_btnRegister);
            }
        }

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentPage page = (ContentPage)sender;

            double wndWidth = page.Width;
            double wndHeight = page.Height;

            double screenRatio = (page.Width / 1080);

            disclaimerScrollView.WidthRequest = page.Width;
            disclaimerScrollView.HeightRequest = 200 * screenRatio;

            disclaimerScrollView.Content = disclaimer;

            _btnBack.WidthRequest = (page.Width * 0.9) * 0.5;
            _btnRegister.WidthRequest = (page.Width * 0.9) * 0.5;

            logo.WidthRequest = 256 * screenRatio;
            logo.HeightRequest = 256 * screenRatio;

            InCityApp.Helpers.LayoutFormatter.LayoutForDefaultButton(_btnRegister, screenRatio);
            InCityApp.Helpers.LayoutFormatter.LayoutForDefaultButton(_btnBack, screenRatio);
        }

        private void cancelButtonClicked(object sender, EventArgs e)
        {
            InCityApplication.InRegistrationPage = false;
            InCityApplication.InLoginPage = true;
            InCityApplication.App.SetLoginAsRootPage();
        }
    }
}
