
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

using InCityApp.Entities;
using InCityApp.Shared.Views;
using InCityApp.Shared.ViewModels;
using InCityApp.Views;

using XLabs.Forms.Charting.Controls;
using XLabs.Platform.Device;
using InCityApp.Pages;
using Syncfusion.SfChart.XForms;
using System.Collections.ObjectModel;
using InCityApp.UserControls;
using InCityApp.Models;

namespace InCityApp.Shared.Pages
{
    public class ActivitiesReportPage : BasePage
    {
        private StackLayout _stack;
        private FixedLabel _lblSteps;
        private FixedLabel _lblKmDistance;
        private FixedLabel _lblWalkingTotal;
        private FixedLabel _lblRunningTotal;
        private FixedLabel _lblOnBicycleTotal;
        private FixedLabel _lblInVehicleTotal;

        private SfChart _chart;
        private ObservableCollection<ChartDataPoint> _chartSamples = new ObservableCollection<ChartDataPoint>();
        private ObservableCollection<ChartDataPoint> _runningSamples = new ObservableCollection<ChartDataPoint>();
        private ObservableCollection<ChartDataPoint> _onBicycleSamples = new ObservableCollection<ChartDataPoint>();
        private ObservableCollection<ChartDataPoint> _inVehicleSamples = new ObservableCollection<ChartDataPoint>();

        public ActivitiesReportPage()
        {
            _stack = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            CustomNavigationView cstnavVW = new CustomNavigationView("Report Attivit� Ultima Giornata");
            _stack.Children.Add(cstnavVW);

            Content = _stack;

            StepCounter.Instance.OnStepsChanged += OnNewStep;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Task.Run(async () => { await UpdateData(); });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void OnNewStep(object sender, long steps)
        {
            Device.BeginInvokeOnMainThread(() =>
                  {
                      if (_lblSteps != null)
                          _lblSteps.Text = steps.ToString();
                  });
        }

        private async Task UpdateData()
        {
            try
            {
                //ActivityStats stats = await RoutesModel.GetDistinctActivities(DateTime.Now.ToUniversalTime().AddDays(-1), DateTime.Now.ToUniversalTime());
                ActivityStats stats = UserLocator.Instance.GetActivitiesFromMemory();

                Device.BeginInvokeOnMainThread(() =>
                {
                    if (stats == null)
                    {
                        stats = new ActivityStats();
                    }

                    _lblSteps = new FixedLabel() { FontSize = 20, FontAttributes = FontAttributes.Bold, XAlign = TextAlignment.Center, Text = "0" };

                    _lblSteps.Text = StepCounter.Instance.stepsCount.ToString();

                    _lblKmDistance = new FixedLabel() { FontSize = 20, FontAttributes = FontAttributes.Bold, XAlign = TextAlignment.Center, Text = "-" };
                    _lblWalkingTotal = new FixedLabel() { FontSize = 16, FontAttributes = FontAttributes.Bold, XAlign = TextAlignment.Center, Text = "-" };
                    _lblRunningTotal = new FixedLabel() { FontSize = 16, FontAttributes = FontAttributes.Bold, XAlign = TextAlignment.Center, Text = "-" };
                    _lblOnBicycleTotal = new FixedLabel() { FontSize = 16, FontAttributes = FontAttributes.Bold, XAlign = TextAlignment.Center, Text = "-" };
                    _lblInVehicleTotal = new FixedLabel() { FontSize = 16, FontAttributes = FontAttributes.Bold, XAlign = TextAlignment.Center, Text = "-" };

                    _chart = new Syncfusion.SfChart.XForms.SfChart()
                    {
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        HeightRequest = InCityApplication.RealScreenHeight / 2,
                    };

                    _chart.Title = new ChartTitle() { Text = "Percentuali Tipologia Movimento" };
                    _chart.Legend = new ChartLegend()
                    {
                        IsVisible = true,
                        IsIconVisible = true,
                    };

                    double walkingTime = stats.TimeSum[Enum.GetName(typeof(UserLocator.ActivityType), UserLocator.ActivityType.Walking)];
                    double runningTime = stats.TimeSum[Enum.GetName(typeof(UserLocator.ActivityType), UserLocator.ActivityType.Running)];
                    double onBicycleTime = stats.TimeSum[Enum.GetName(typeof(UserLocator.ActivityType), UserLocator.ActivityType.OnBicycle)];
                    double inVehicleTime = stats.TimeSum[Enum.GetName(typeof(UserLocator.ActivityType), UserLocator.ActivityType.InVehicle)];

                    double walkingDistance = stats.DistanceSum[Enum.GetName(typeof(UserLocator.ActivityType), UserLocator.ActivityType.Walking)];
                    double runningDistance = stats.DistanceSum[Enum.GetName(typeof(UserLocator.ActivityType), UserLocator.ActivityType.Running)];
                    double bicycleDistance = stats.DistanceSum[Enum.GetName(typeof(UserLocator.ActivityType), UserLocator.ActivityType.OnBicycle)];
                    double vehicleDistance = stats.DistanceSum[Enum.GetName(typeof(UserLocator.ActivityType), UserLocator.ActivityType.InVehicle)];

                    _lblWalkingTotal.Text = walkingDistance.ToString("0.00") + " Km";// +" in " + walkingTime.ToString("0.00" + " h");// +" in " + TimeSpan.FromMinutes(walkingTime).ToString();
                    _lblRunningTotal.Text = runningDistance.ToString("0.00") + " Km";// + " in " + runningTime.ToString("0.00" + " h");// +" in " + TimeSpan.FromMinutes(runningTime).ToString();
                    _lblOnBicycleTotal.Text = bicycleDistance.ToString("0.00") + " Km";// + " in " + onBicycleTime.ToString("0.00" + " h");// +" in " + TimeSpan.FromMinutes(onBicycleTime).ToString();
                    _lblInVehicleTotal.Text = vehicleDistance.ToString("0.00") + " Km";// + " in " + inVehicleTime.ToString("0.00" + " h");// +" in " + TimeSpan.FromMinutes(inVehicleTime).ToString();

                    double totalTime = walkingTime + runningTime + onBicycleTime + inVehicleTime;
                    double kmTotalDistance = walkingDistance + runningDistance + bicycleDistance + vehicleDistance;

                    _lblKmDistance.Text = kmTotalDistance.ToString("0.00") + " Km";// + " in " + totalTime.ToString("0.00" + " h"); ;

                    _chartSamples.Add(new ChartDataPoint("Camminando", walkingDistance * 100 / kmTotalDistance));
                    _chartSamples.Add(new ChartDataPoint("Correndo", runningDistance * 100 / kmTotalDistance));
                    _chartSamples.Add(new ChartDataPoint("In Bicicletta", bicycleDistance * 100 / kmTotalDistance));
                    _chartSamples.Add(new ChartDataPoint("In Auto", vehicleDistance * 100 / kmTotalDistance));

                    ChartDataMarker dataMarker = new ChartDataMarker()
                    {
                        MarkerType = DataMarkerType.Ellipse,
                        LabelContent = LabelContent.Percentage,
                        ShowLabel = true,
                        LabelStyle = new DataMarkerLabelStyle() { Font = Font.SystemFontOfSize(20) },
                    };

                    ChartColorModel colorModel = new ChartColorModel();
                    colorModel.Palette = ChartColorPalette.Custom;
                    colorModel.CustomBrushes = new List<Color>() 
                    { 
                        Color.FromHex("F19EE6"), 
                        Color.FromHex("5285C7"), 
                        Color.FromHex("BF9EF7"), 
                        Color.FromHex("C1D397") 
                    };
 
                    PieSeries pie = new PieSeries()
                    {
                        ColorModel = colorModel,
                        DataMarker = dataMarker,
                        ItemsSource = _chartSamples,
                       
                        DataMarkerPosition = CircularSeriesDataMarkerPosition.OutsideExtended,
                    };                    
                    _chart.Series.Add(pie);

                    _stack.Children.Add(_chart);

                    StackLayout statsStack = new StackLayout()
                    {
                        Orientation = StackOrientation.Vertical,
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.Start,
                        HeightRequest = InCityApplication.RealScreenHeight / 2,
                    };


                    StackLayout stepsRow = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.Center,
                    };
                    FixedLabel lblStepsDesc = new FixedLabel()
                    {
                        Text = " PASSI SESSIONE CORRENTE : ",
                        FontSize = 20,
                        FontAttributes = FontAttributes.Bold,
                        XAlign = TextAlignment.Center,
                    };
                    stepsRow.Children.Add(lblStepsDesc);
                    stepsRow.Children.Add(_lblSteps);
                    statsStack.Children.Add(stepsRow);

                    StackLayout kmDistanceRow = new StackLayout() 
                    { 
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center, 
                        VerticalOptions = LayoutOptions.Center, 
                    };
                    FixedLabel lblKmDistanceDesc = new FixedLabel()
                    {
                        Text = " TOTALE : ",
                        FontSize = 20,
                        FontAttributes = FontAttributes.Bold,
                        XAlign = TextAlignment.Center,
                    };
                    kmDistanceRow.Children.Add(lblKmDistanceDesc);
                    kmDistanceRow.Children.Add(_lblKmDistance);
                    statsStack.Children.Add(kmDistanceRow);

                    StackLayout walkingTotalRow = new StackLayout() 
                    { 
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center, 
                        VerticalOptions = LayoutOptions.Center, 
                    };
                    FixedLabel lblWalkingTotalDesc = new FixedLabel()
                    {
                        Text = " Camminando : ",
                        FontSize = 16,
                        XAlign = TextAlignment.Center,
                    };
                    walkingTotalRow.Children.Add(lblWalkingTotalDesc);
                    walkingTotalRow.Children.Add(_lblWalkingTotal);
                    statsStack.Children.Add(walkingTotalRow);

                    StackLayout runningTotalRow = new StackLayout() 
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center, 
                        VerticalOptions = LayoutOptions.Center, 
                    };
                    FixedLabel lblRunningTotalDesc = new FixedLabel()
                    {
                        Text = " Correndo : ",
                        FontSize = 16,
                        XAlign = TextAlignment.Center,
                    };
                    runningTotalRow.Children.Add(lblRunningTotalDesc);
                    runningTotalRow.Children.Add(_lblRunningTotal);
                    statsStack.Children.Add(runningTotalRow);

                    StackLayout onBicycleTotalRow = new StackLayout() 
                    { 
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center, 
                        VerticalOptions = LayoutOptions.Center, 
                    };
                    FixedLabel lblInBicycleTotalDesc = new FixedLabel()
                    {
                        Text = " In Bicicletta : ",
                        FontSize = 16,
                        XAlign = TextAlignment.Center,
                    };
                    onBicycleTotalRow.Children.Add(lblInBicycleTotalDesc);
                    onBicycleTotalRow.Children.Add(_lblOnBicycleTotal);
                    statsStack.Children.Add(onBicycleTotalRow);

                    StackLayout inVehicleTotalRow = new StackLayout() 
                    { 
                        Orientation = StackOrientation.Horizontal, 
                        HorizontalOptions = LayoutOptions.Center, 
                        VerticalOptions = LayoutOptions.Center, 
                    };
                    FixedLabel lblInVehicleTotalDesc = new FixedLabel()
                    {
                        Text = " In Auto : ",
                        FontSize = 16,
                        XAlign = TextAlignment.Center,
                    };
                    inVehicleTotalRow.Children.Add(lblInVehicleTotalDesc);
                    inVehicleTotalRow.Children.Add(_lblInVehicleTotal);
                    statsStack.Children.Add(inVehicleTotalRow);

                    _stack.Children.Add(statsStack);
                });
            }
            catch(Exception exc)
            {
                Debug.Print("ActivitiesReportPage >> " + exc.Message);
            }
        }
    }

}
