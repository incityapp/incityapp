using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using InCityApp.Shared.Pages;
using InCityApp.Entities;
using InCityApp.Services;
using InCityApp.DataModels;
using InCityApp.Helpers;


namespace InCityApp.Shared.Pages
{
    public class RootPage : MasterDetailPage
    {
        MenuPage optionsPage;
        OptionItem previousItem;

        public RootPage ()
        {
            optionsPage = new MenuPage { Icon = "settings.png", Title = "menu" };
            
            optionsPage.Menu.ItemSelected += (sender, e) => NavigateTo(e.SelectedItem as OptionItem);

            Master = optionsPage;

            OptionItem initialOptionItem = optionsPage.Menu.ItemsSource.Cast<OptionItem>().First();
            initialOptionItem.Selected = true;
            NavigateTo(initialOptionItem);

            if (UsersModel.CurrentUser == null)
            {
                InCityApplication.InLoginPage = false;
                ShowLoginDialog();
            }
        }

        async void ShowLoginDialog()
        {
            User alreadyLoggedUser = await UsersModel.GetCurrentUser();
            if (alreadyLoggedUser != null)
            {
                NavigateToFirstPage();
            }

            var page = new LoginPage();
            page.ViewModel.OnLoginCompleted += () =>
            {
                NavigateToFirstPage();             
                // Retrieve and draw the last route if exists
                UserLocator.Instance.DrawLastRoute();
            };

            await Navigation.PushModalAsync(page);
        }

        void NavigateToFirstPage()
        {
            OptionItem initialOptionItem = optionsPage.Menu.ItemsSource.Cast<OptionItem>().First();
            initialOptionItem.Selected = true;
            var displayPage = PageForOption(initialOptionItem);

            Detail = new NavigationPage(displayPage)
            {
                BarBackgroundColor = Color.White,
                BarTextColor = Color.Black,
                //BackgroundColor = Color.FromHex("52B296")
            };
        }

        void NavigateTo(OptionItem option)
        {
            if (previousItem != null)
                previousItem.Selected = false;
                    
            if (previousItem != null && previousItem.Title == option.Title)
                return;

            option.Selected = true;
            previousItem = option;

            var displayPage = PageForOption(option);

#if WINDOWS_PHONE
            Detail = new ContentPage();//work around to clear current page.
#endif
            Detail = new NavigationPage(displayPage)
            {
                BarBackgroundColor = Color.White,
                BarTextColor = Color.Black
                //BarBackgroundColor = Color.FromHex("52B296")
            };


            IsPresented = false;
        }

        Page PageForOption (OptionItem option)
        {
            // TODO: Refactor this to the Builder pattern (see ICellFactory).

            if (option is VoteAnnotationsOptionItem)
                return new MapPage();
            if (option is NoisePollutionOptionItem)
                return new NoisePollutionPage();


            //if (option.Title == NoiseOptionItem.Name)
                

            //if (option.Title == "Contacts")
            //    return new MasterPage<Contact>(option);
            //if (option.Title == "Leads")
            //    return new MasterPage<Lead>(option);
            //if (option.Title == "Accounts")
            //{
            //    var page = new MasterPage<Account>(option);
            //    var cell = page.List.Cell;
            //    cell.SetBinding(TextCell.TextProperty, "Company");
            //    return page;
            //}
            //if (option.Title == "Opportunities")
            //{
            //    var page = new MasterPage<Opportunity>(option);
            //    var cell = page.List.Cell;
            //    cell.SetBinding(TextCell.TextProperty, "Company");
            //    cell.SetBinding(TextCell.DetailProperty, new Binding("EstimatedAmount", stringFormat: "{0:C}"));
            //    return page;
            //}
            throw new NotImplementedException("Unknown menu option: " + option.Title);
        }
    }
}

