
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

using InCityApp.Entities;
using InCityApp.Shared.Views;
using InCityApp.Shared.ViewModels;
using InCityApp.Views;

using XLabs.Forms.Charting.Controls;
using XLabs.Platform.Device;
using InCityApp.Pages;
using Syncfusion.SfChart.XForms;
using System.Collections.ObjectModel;
using InCityApp.UserControls;
using InCityApp.Models;
using InCityApp.DataModels;

namespace InCityApp.Shared.Pages
{
    public class NoisePollutionPage : BasePage
    {
        private static StackLayout _stack;
        private static StackLayout _statsStack;
        private static StackLayout _pickerRow;
        private static bool _chartReady = false;
        private static bool _statsReady = false;
        private static FixedLabel _lblMaxNoise;
        private static FixedLabel _lblAvgNoise;
        private static FixedLabel _lblMinNoise;
        private static Picker _picker;
        private static FixedLabel _pickerDesc;
        private readonly Object _lock = new Object();

        private static SfChart _chart;

        private static Dictionary<string, int> _timePickerValues = new Dictionary<string, int>
        {
            { "30 secondi", 30 }, 
            { "1 minuto", 60 },
            { "2 minuti", 120 }, 
            { "3 minuti", 180 }
        };

        private static string _timePickerSelected = "30 secondi";

        private static bool _updatingStats;

        private NoisePollutionViewModel ViewModel
        {
            get { return BindingContext as NoisePollutionViewModel; }
        }

        public NoisePollutionPage()
        {
            NoisePollutionViewModel viewModel = new NoisePollutionViewModel();
            BindingContext = viewModel;

            _stack = new StackLayout() { Orientation = StackOrientation.Vertical, HorizontalOptions = LayoutOptions.Fill };

            CustomNavigationView cstnavVW = new CustomNavigationView( "Report Rumore" );
            _stack.Children.Add(cstnavVW);

            _pickerRow = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center,
            };

            _pickerDesc = new FixedLabel()
            {
                Text = " Finestra Temporale : ",
                FontSize = 18,
                FontAttributes = FontAttributes.Bold,
                YAlign = TextAlignment.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            _picker = new Picker
            {
                Title = "30 Secondi",
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            foreach (string desc in _timePickerValues.Keys)
                _picker.Items.Add(desc);

            _picker.SelectedIndexChanged += (sender, args) =>
            {
                if (_picker.SelectedIndex != -1)
                {
                    _timePickerSelected = _picker.Items[_picker.SelectedIndex];
                    NoisePollutionViewModel.MaxSamplesPerSeries = _timePickerValues[_timePickerSelected];
                    viewModel.ReInit();
                }
            };
            _pickerRow.Children.Add(_pickerDesc);
            _pickerRow.Children.Add(_picker);

            _stack.Children.Add(_pickerRow);

            //_chart = new Syncfusion.SfChart.XForms.SfChart()
            //{
            //    HorizontalOptions = LayoutOptions.Start,
            //    HeightRequest = InCityApplication.RealScreenHeight / 2,
            //};

            //_stack.Children.Add(_chart);

            Content = _stack;

            if (viewModel.OnInitialSeriesPrepared != null)
                viewModel.OnInitialSeriesPrepared -= OnInitialData;

            viewModel.OnInitialSeriesPrepared += OnInitialData;

            viewModel.Init();
        }

        private void UpdateChart(List<ChartDataPoint> dataPoints)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    if (_chart != null)
                    {
                        _stack.Children.Remove(_chart);
                        _chart = null;
                    }

                    _chart = new Syncfusion.SfChart.XForms.SfChart()
                    {
                        HorizontalOptions = LayoutOptions.Start,
                        HeightRequest = InCityApplication.RealScreenHeight / 2,
                    };
                        
                    _chart.Title = new ChartTitle() { Text = "Rumore rilevato" };

                    CategoryAxis primaryAxis = new CategoryAxis();
                    primaryAxis.Interval = 10;
                    primaryAxis.Title = new ChartAxisTitle() { Text = string.Format("(Un campione al secondo in {0})", _timePickerSelected), };
                    _chart.PrimaryAxis = primaryAxis;

                    NumericalAxis secondaryAxis = new NumericalAxis();
                    secondaryAxis.Title = new ChartAxisTitle() { Text = "dB" };

                    _chart.SecondaryAxis = secondaryAxis;

                    ChartDataMarker dataMarker = new ChartDataMarker()
                    {
                        MarkerColor = Color.FromHex("52B296"),
                        MarkerType = DataMarkerType.Diamond,
                        MarkerWidth = 10,
                        MarkerHeight = 10,
                        ShowMarker = true,
                        ShowLabel = false,
                    };

                    _chart.Series.Add(new LineSeries()
                    {
                        Color = Color.FromHex("52B296"),
                        DataMarker = dataMarker,
                        ItemsSource = dataPoints
                    });

                    _stack.Children.Add(_chart);

                    _chartReady = true;

                    //----------------------------------------------------
                    // STATS
                    //----------------------------------------------------

                    if (_statsStack != null)
                    {
                        _stack.Children.Remove(_statsStack);
                        _statsStack = null;
                    }

                    _statsStack = new StackLayout()
                    {
                        Orientation = StackOrientation.Vertical,
                        HorizontalOptions = LayoutOptions.Center,
                        HeightRequest = InCityApplication.RealScreenHeight / 2,
                    };

                    if (!_statsReady)
                    {
                        _lblMaxNoise = new FixedLabel() { FontSize = 20, XAlign = TextAlignment.Center };
                        _lblAvgNoise = new FixedLabel() { FontSize = 20, XAlign = TextAlignment.Center };
                        _lblMinNoise = new FixedLabel() { FontSize = 20, XAlign = TextAlignment.Center };

                        NoisePollutionViewModel.MaxSamplesPerSeries = 30;
                    }

                    StackLayout maxNoiseRow = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                    };
                    FixedLabel lblMaxNoiseDesc = new FixedLabel()
                    {
                        Text = " Rumore Massimo : ",
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.Center,
                        FontSize = 20,
                    };

                    maxNoiseRow.Children.Add(lblMaxNoiseDesc);
                    maxNoiseRow.Children.Add(_lblMaxNoise);
                    _statsStack.Children.Add(maxNoiseRow);

                    StackLayout avgNoiseRow = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                    };
                    FixedLabel lblAvgNoiseDesc = new FixedLabel()
                    {
                        Text = " Rumore Medio : ",
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.Center,
                        FontSize = 20,
                    };

                    avgNoiseRow.Children.Add(lblAvgNoiseDesc);
                    avgNoiseRow.Children.Add(_lblAvgNoise);
                    _statsStack.Children.Add(avgNoiseRow);

                    StackLayout minNoiseRow = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                    };
                    FixedLabel lblMinNoiseDesc = new FixedLabel()
                    {
                        Text = " Rumore Minimo : ",
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.Center,
                        FontSize = 20,
                    };

                    minNoiseRow.Children.Add(lblMinNoiseDesc);
                    minNoiseRow.Children.Add(_lblMinNoise);
                    _statsStack.Children.Add(minNoiseRow);

                    _stack.Children.Add(_statsStack);

                    _statsReady = true;

                    _lblMaxNoise.Text = ViewModel.DataPoints.MaxOrDefault(x => x.YValue, 0).ToString("0.00") + " dB";
                    _lblAvgNoise.Text = ViewModel.DataPoints.AvarageOrDefault(x => x.YValue, 0).ToString("0.00") + " dB";
                    _lblMinNoise.Text = ViewModel.DataPoints.MinOrDefault(x => x.YValue, 0).ToString("0.00") + " dB";
                }
                catch (Exception exc)
                {
                    Debug.Print("NoisePollutionPage Update UI Error >> " + exc.Message);
                }

            });
        }


        private void OnDataModelChanged(object sender, List<ChartDataPoint> dataPoints)
        {
            lock (_lock)
            {
                if (!_chartReady)
                    return;
           
                UpdateChart(dataPoints);
            }
        }

        private void OnInitialData(object sender, List<ChartDataPoint> dataPoints)
        {
            lock (_lock)
            {
                if (dataPoints != null)
                {
                    int timeInterval = _timePickerValues[_timePickerSelected];
                    if (dataPoints.Count > timeInterval)
                        UpdateChart(dataPoints.GetRange(dataPoints.Count - timeInterval, timeInterval));
                    else
                        UpdateChart(dataPoints);
                }
                else
                    UpdateChart(new List<ChartDataPoint>());

                if (ViewModel.OnSeriesChanged != null)
                    ViewModel.OnSeriesChanged -= OnDataModelChanged;

                if (ViewModel.OnSeriesChanged == null)
                    ViewModel.OnSeriesChanged += OnDataModelChanged;
            }
        }
    }

}
