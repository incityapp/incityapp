
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

using InCityApp.Entities;
using InCityApp.Shared.Views;
using InCityApp.Shared.ViewModels;
using InCityApp.Views;

using XLabs.Forms.Charting.Controls;
using XLabs.Platform.Device;
using InCityApp.Pages;
using System.Collections.ObjectModel;
using InCityApp.UserControls;
using InCityApp.Models;
using InCityApp.Helpers;
using InCityApp.DataModels;
using InCityApp.DependencyServiceInterfaces;


namespace InCityApp.Shared.Pages
{
    public class SettingsPage : BasePage
    {
        private FixedLabel _pointsDesc;

        public SettingsPage()
        {
            StackLayout stack = new StackLayout() { Orientation = StackOrientation.Vertical, HorizontalOptions = LayoutOptions.Fill, VerticalOptions = LayoutOptions.Fill };

            CustomNavigationView cstnavVW = new CustomNavigationView("Impostazioni");
            stack.Children.Add(cstnavVW);

            stack.Children.Add(new BoxView() { VerticalOptions = LayoutOptions.Start, Color = Color.Transparent, HeightRequest = 30 });

            StackLayout userNameRow = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Center };
            FixedLabel userNameDesc = new FixedLabel() { Text = "Nome Utente : ", FontSize = 18, FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.Center };
            userNameRow.Children.Add(userNameDesc);
            FixedLabel userName = new FixedLabel() { Text = UsersModel.CurrentUser.Username, TextColor = Color.FromHex("57ba9d"), FontSize = 18, FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.Center };
            userNameRow.Children.Add(userName);
            stack.Children.Add(userNameRow);

            _pointsDesc = new FixedLabel() { Text = "Totale Punteggio : ", FontSize = 18, FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.Center };
            stack.Children.Add(_pointsDesc);

            stack.Children.Add(new BoxView() { VerticalOptions = LayoutOptions.Start, Color = Color.Transparent, HeightRequest = 30 });

            FixedButton btnLogout = new FixedButton() { Text = "ESCI", TextColor = Color.White, BackgroundColor = Color.FromHex("57ba9d"), HorizontalOptions = LayoutOptions.CenterAndExpand };
            btnLogout.Clicked += btnLogout_Clicked;
            stack.Children.Add(btnLogout);

            stack.Children.Add(new BoxView() { VerticalOptions = LayoutOptions.Start, Color = Color.Transparent, HeightRequest = 30 });

            StackLayout positionTrackingLayout = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Center };
            FixedLabel positionTrackingDesc = new FixedLabel() { Text = "Tracciamento Posizione" };
            Xamarin.Forms.Switch positionTrackingSwitch = new Xamarin.Forms.Switch() { IsToggled = Settings.PositionTrackingEnabled, HorizontalOptions = LayoutOptions.End };
            positionTrackingSwitch.Toggled += positionTrackingSwitch_Toggled;
            positionTrackingLayout.Children.Add(positionTrackingDesc);
            positionTrackingLayout.Children.Add(positionTrackingSwitch);
            stack.Children.Add(positionTrackingLayout);

            StackLayout audioRecordingLayout = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Center };
            FixedLabel audioRecordingDesc = new FixedLabel() { Text = "Registrazione Dati Audio" };
            Xamarin.Forms.Switch audioRecordingSwitch = new Xamarin.Forms.Switch() { IsToggled = Settings.AudioRecordingEnabled, HorizontalOptions = LayoutOptions.End };
            audioRecordingSwitch.Toggled += audioRecordingSwitch_Toggled;
            audioRecordingLayout.Children.Add(audioRecordingDesc);
            audioRecordingLayout.Children.Add(audioRecordingSwitch);
            stack.Children.Add(audioRecordingLayout);

            stack.Children.Add(new BoxView() { VerticalOptions = LayoutOptions.Start, Color = Color.Transparent, HeightRequest = 30 });

            FixedButton btnIncity = new FixedButton() { Text = "http://www.incityapp.eu/", TextColor = Color.FromHex("57ba9d"), BackgroundColor = Color.Transparent, HorizontalOptions = LayoutOptions.CenterAndExpand };
            btnIncity.Clicked += btnInCity_Clicked;
            stack.Children.Add(btnIncity);

            stack.Children.Add(new BoxView() { VerticalOptions = LayoutOptions.Start, Color = Color.Transparent, HeightRequest = 30 });

            string AppInfo;
            AppInfo = string.Format("Versione: {0}", InCityApplication.GetAppVersionInfo());

            FixedLabel AppInfoLbl = new FixedLabel() { Text = AppInfo, VerticalOptions = LayoutOptions.End, HorizontalOptions = LayoutOptions.Center };
            stack.Children.Add(AppInfoLbl);

            string SOInfo;
            SOInfo = string.Format("Versione Sistema Operativo {0}", InCityApplication.GetSOVersionInfo()) ;

            FixedLabel SOInfoLbl = new FixedLabel() { Text = SOInfo, VerticalOptions = LayoutOptions.End, HorizontalOptions = LayoutOptions.Center };
            stack.Children.Add(SOInfoLbl);

            _pointsDesc.Text = "Totale Punteggio : -";

            PrepareLayout(stack);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            UpdateScore();
        }

        async void btnLogout_Clicked(object sender, EventArgs e)
        {
            if (IsLoading)
                return;

            IsLoading = true;

            await UsersModel.Logout();

            IsLoading = false;
            InCityApplication.App.SetLoginAsRootPage();
        }

        void btnInCity_Clicked(object sender, EventArgs e)
        {
            var url = "http://www.incityapp.eu/";
            Device.OpenUri(new Uri(url));
        }

        private void UpdateScore()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    int points = await UsersModel.GetUserScore();
                    _pointsDesc.Text = "Punteggio : " + points;
                }
                catch (Exception exc)
                { 
                
                }
            });
        }

        void positionTrackingSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            Settings.PositionTrackingEnabled = e.Value;
        }

        void audioRecordingSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            Settings.AudioRecordingEnabled = e.Value;
        }
    }

}
