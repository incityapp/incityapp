
using Xamarin.Forms;
using InCityApp.Shared.Pages;
using InCityApp.Shared.ViewModels;
using InCityApp.Entities;

namespace InCityApp.Shared.Pages
{
    public class MasterPage : ContentPage 
    {
        public MasterPage(OptionItem menuItem, ContentPage page)
        {
            //BackgroundColor = Color.FromHex("52B296");

            this.SetValue(Page.TitleProperty, menuItem.Title);
            this.SetValue(Page.IconProperty, menuItem.Icon);


            //this.Children.Add(page);
        }
    }
}
