﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace InCityApp.Pages
{
	public class SplashScreen : ContentPage
	{
        private Image _image;

		public SplashScreen ()
		{
            this.SizeChanged += OnSizeChangedEvent;

            _image = new Image()
            { 
                Source = (FileImageSource)FileImageSource.FromFile("incity_splash.png"), 
                HorizontalOptions = LayoutOptions.Center, 
                VerticalOptions = LayoutOptions.Center
            };

            AbsoluteLayout absLayout = new AbsoluteLayout
            {
                Children =
                {
                    _image
                }
            };

			Content = absLayout;
		}

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentPage page = (ContentPage)sender;

            double wndWidth = page.Width;
            double wndHeight = page.Height;

            double screenRatio = (page.Width / 1080);

            _image.WidthRequest = 1080 * screenRatio;
            _image.HeightRequest = 1920 * screenRatio;

            AbsoluteLayout.SetLayoutBounds(_image, new Rectangle((page.Width - _image.Width) * 0.5, (page.Height - _image.Height) * 0.5, _image.WidthRequest, _image.Height));
        }
	}
}
