
using InCityApp.Pages;
using InCityApp.Shared.ViewModels;
using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Shared.Pages
{
    public class LoginPage : BasePage
    {
        FixedButton _btnLogin;
        FixedButton _btnRegister;

        public LoginViewModel ViewModel
        {
            get { return BindingContext as LoginViewModel; }
        }

        public LoginPage()
        {
            this.SizeChanged += OnSizeChangedEvent;

            // we use this flag in the native project to know if we are in the login page,
            // to disable the hardware back button on android
            InCityApplication.InLoginPage = true;

            BindingContext = new LoginViewModel(this);

            BackgroundColor = Color.White;

            var layout = new StackLayout () { Padding = 10, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };

            StackLayout logoLayout = new StackLayout() 
            { Orientation = StackOrientation.Horizontal, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand };

            Image logo = new Image
            {
                Source = (FileImageSource)FileImageSource.FromFile("inCityLogo.png"),
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent
            };
            logoLayout.Children.Add(logo);

            layout.Children.Add(logoLayout);

            var subtitle = new Label()
            {
                TextColor = Color.Red,
                VerticalOptions = LayoutOptions.Start,
                XAlign = TextAlignment.Center, 
                YAlign = TextAlignment.Center, 
            };

            subtitle.SetBinding(Label.TextProperty, LoginViewModel.SubtitlePropertyName);
            layout.Children.Add(subtitle);

            var username = new Entry { Placeholder = "Nome Utente" };
            username.SetBinding(Entry.TextProperty, LoginViewModel.UsernamePropertyName);
            layout.Children.Add(username);

            var password = new Entry { Placeholder = "Password", IsPassword = true };
            password.SetBinding(Entry.TextProperty, LoginViewModel.PasswordPropertyName);
            layout.Children.Add(password);

            StackLayout confirmationButtons = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Center };

            _btnLogin = new FixedButton { Text = "ACCEDI" };
            _btnLogin.SetBinding(FixedButton.CommandProperty, LoginViewModel.LoginCommandPropertyName);
            confirmationButtons.Children.Add(_btnLogin);

            _btnRegister = new FixedButton { Text = "REGISTRAZIONE" };
            _btnRegister.SetBinding(FixedButton.CommandProperty, LoginViewModel.RegistrationCommandPropertyName);
            confirmationButtons.Children.Add(_btnRegister);

            layout.Children.Add(confirmationButtons);

            PrepareLayout(layout);
        }

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentPage page = (ContentPage)sender;

            double wndWidth = page.Width;
            double wndHeight = page.Height;

            double screenRatio = (page.Width / 1080);

            _btnLogin.WidthRequest = (page.Width * 0.8) * 0.5;
            _btnRegister.WidthRequest = (page.Width * 0.8) * 0.5;

            InCityApp.Helpers.LayoutFormatter.LayoutForDefaultButton(_btnLogin, screenRatio);
            InCityApp.Helpers.LayoutFormatter.LayoutForDefaultButton(_btnRegister, screenRatio);
        }
    }
}
