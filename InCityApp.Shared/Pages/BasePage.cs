﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;

using InCityApp.Views;
using InCityApp.Shared.Pages;
using InCityApp.DataInterfaces;

namespace InCityApp.Pages
{
    public class BasePage : ContentPage
    {
        private CustomPopupLayout _popupLayout = new CustomPopupLayout();
        private int _popupWidth, _popupHeight;
        private static bool _popupOpen;
        private readonly Object _lock = new Object();
        private ActivityIndicator _indicator;
        private bool _isLoading;

        public View BaseContent { get; set; }

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                if (_isLoading == true)
                    ShowLoadingIndicator();
                else
                    RemoveLoadingIndicator();
            }
        }

        public BasePage()
        {
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<BaseDataInterface>(this, "OnConnectionLost", async (sender) =>
            {
                if (InCityApplication.App.status == GroundStatus.Background)
                    return;

                if (!_popupOpen)
                {
                    _popupOpen = true;

                    //await DisplayAlert("ATTENZIONE", "Manca la connessione a Internet", "OK");
                    _popupOpen = false;
                }
            });

            MessagingCenter.Subscribe<BaseDataInterface, string>(this, "OnServeError", async (sender, errorMessage) =>
            {
                if (InCityApplication.App.status == GroundStatus.Background)
                    return;

                if (!_popupOpen)
                {
                    _popupOpen = true;

                    await DisplayAlert("ERRORE", errorMessage, "OK");

                    _popupOpen = false;
                }
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<BaseDataInterface, string>(this, "OnConnectionLost");
            MessagingCenter.Unsubscribe<BaseDataInterface, string>(this, "OnServeError");
        }

        public void PrepareLayout(View content)
        {
            // make the popupLayout the top-layout
            _popupLayout.Content = content;
            _popupWidth = InCityApplication.ScreenWidth;
            _popupHeight = InCityApplication.ScreenHeight;
            Content = _popupLayout;
        }

        protected ActivityIndicator CreateLoadingIndicator()
        {
            var loadingIndicator = new ActivityIndicator
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.Center,
                Scale = 1,
                Color = Color.FromHex("52B296"),
            };

            return loadingIndicator;
        }

        public void ShowLoadingIndicator(string message = "Aggiornamento dati...")
        { 
            _indicator = CreateLoadingIndicator();
            _indicator.IsVisible = true;
            _indicator.IsRunning = true;
            FixedLabel lblMessage = new FixedLabel()
            {
                Text = message,
                FontSize = 18,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.FromHex("52B296"),
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };
            OpenLoadingPopup(lblMessage, _indicator);
        }

        public void ShowRegistrationSuccessPopup()
        {
            _indicator = CreateLoadingIndicator();
            _indicator.IsVisible = true;
            _indicator.IsRunning = true;
            FixedLabel title = new FixedLabel()
            {
                Text = "Grazie per esserti registrato, una mail di verifica è stata inviata al tuo indirizzo.\nE' necessario confermare la registrazione per accedere.",
                FontSize = 18,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.FromHex("52B296"),
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };
            FixedButton backToLogin = new FixedButton() { Text = "Ok", TextColor = Color.White, BackgroundColor = Color.FromHex("52B296") };
            backToLogin.Clicked += (s, e) =>
            {
                CloseModalPopup();
                InCityApplication.App.SetLoginAsRootPage();
            };
            OpenLoadingPopup(title, backToLogin);
        }

        public void ShowValutationErrorPopup()
        {
            _indicator = CreateLoadingIndicator();
            _indicator.IsVisible = true;
            _indicator.IsRunning = true;
            FixedLabel title = new FixedLabel()
            {
                Text = "Si è verificato un errore durante la registrazione della valutazione, riprova più tardi.",
                FontSize = 18,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.FromHex("52B296"),
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };
            FixedButton backToLogin = new FixedButton() { Text = "Ok", TextColor = Color.White, BackgroundColor = Color.FromHex("52B296") };
            backToLogin.Clicked += (s, e) =>
            {
                CloseModalPopup();
                InCityApplication.App.SetLoginAsRootPage();
            };
            OpenLoadingPopup(title, backToLogin);
        }

        public void RemoveLoadingIndicator()
        {
            _indicator.IsVisible = false;
            _indicator.IsRunning = false;
            CloseModalPopup();
        }

        public void OpenLoadingPopup(FixedLabel title, View content, string id = "")
        {
            lock (_lock)
            {
                //if (_popupOpen)
                //    return;

                //_popupOpen = true;

                StackLayout innerStack = new StackLayout
                {
                    Padding = new Thickness(10, 0, 10, 0),
                    BackgroundColor = Color.FromHex("EDF6F3"),
                    Orientation = StackOrientation.Vertical,
                    HorizontalOptions = LayoutOptions.Fill,
                    WidthRequest = InCityApplication.RealScreenWidth * 0.5,
                    HeightRequest = 150,
                    Spacing = 0,
                    Children = 
                    {
                        title,
                        content
                    }
                };

                StackLayout stack = new StackLayout
                {
                    Orientation = StackOrientation.Vertical,
                    WidthRequest = InCityApplication.ScreenWidth,
                    HeightRequest = InCityApplication.ScreenHeight,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    BackgroundColor = Color.FromRgba(0.5, 0.5, 0.5, 0.5),
                    Spacing = 0,
                    Children = 
                    {
                        new BoxView() { HeightRequest = (InCityApplication.ScreenHeight / 2) -  innerStack.HeightRequest },
                        innerStack,
                        new BoxView() { Color = Color.FromHex("52B296"), BackgroundColor = Color.FromHex("52B296"), HeightRequest = 3 }
                    }
                };

                _popupLayout.ShowPopup(stack);
            }
        }

        public void OpenModalPopup(string title, View content, string id = "")
        {
            lock (_lock)
            {
                //if (_popupOpen)
                //    return;

                //_popupOpen = true;

                StackLayout innerStack = new StackLayout
                {
                    BackgroundColor = Color.White,
                    Orientation = StackOrientation.Vertical,
                    HorizontalOptions = LayoutOptions.Fill,
                    WidthRequest = InCityApplication.RealScreenWidth / 2,
                    HeightRequest = 150,
                    Children = 
                    {
                        new Label() 
                        { 
                            Text = title,
                            HorizontalOptions = LayoutOptions.CenterAndExpand, 
                            VerticalOptions = LayoutOptions.CenterAndExpand, 
                            FontAttributes = FontAttributes.Bold,
                        },
                        content
                    }
                };

                StackLayout stack = new StackLayout
                {
                    Orientation = StackOrientation.Vertical,
                    WidthRequest = InCityApplication.ScreenWidth,
                    HeightRequest = InCityApplication.ScreenHeight,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    BackgroundColor =  Color.FromRgba(0.5, 0.5, 0.5, 0.5),
                    Children = 
                    {
                        new BoxView() { HeightRequest = (InCityApplication.ScreenHeight / 2) -  innerStack.HeightRequest },
                        innerStack
                    }
                };

                _popupLayout.ShowPopup(stack);
            }
        }

        public void CloseModalPopup()
        {
           // if (_popupLayout.IsPopupActive)
                _popupLayout.DismissPopup();
        }

        async void HandleOnConnectionNotAvailable()
        {
           // lock (_lock)
            {
                //Device.BeginInvokeOnMainThread(() =>
                //{
                    //if (!_popupOpen)
                    //{
                    //    _popupOpen = true;

                    //   await DisplayAlert("Seleziona una foto...", "Connessione internet assente, provare nuovamente appena la connessione internet è disponibile.", "Continua");

                    //   _popupOpen = false;

                            //if (_connectionLostPage == null)
                            //{
                            //    _connectionLostPage = new ConnectionLostPage("Connessione internet assente, provare nuovamente appena la connessione internet è disponibile.");
                            //    _connectionLostPage.OnClosed += connectionLostPage_OnClosed;

                            //    Navigation.PushModalAsync(_connectionLostPage);
                            //}

                    //}
                //});
            }
            //Device.BeginInvokeOnMainThread(() =>
            //{
            //    OnConnectionNotAvailable();

            //    FixedButton okButton = new FixedButton() { Text = "Ok", WidthRequest = 160, BackgroundColor = Color.FromHex("52B296") };
            //    okButton.Clicked += ConnectionLostPopup_OkButton_Clicked;
            //    StackLayout popupContent = new StackLayout()
            //    {
            //        Orientation = StackOrientation.Vertical,
            //        Spacing = 10,
            //        Children = 
            //    { 
            //        new Label() 
            //        {
            //            Text = "Connessione internet assente, provare nuovamente appena la connessione internet è disponibile.", 
            //            HorizontalOptions = LayoutOptions.Center, 
            //            TextColor = Color.FromHex("52B296")  
            //        },
            //        okButton
            //    }
            //    };
            //    //if (_popupLayout.IsPopupActive)
            //    //    _popupLayout.DismissPopup();

            //    OpenModalPopup("Problema di connessione", popupContent);
            //});
        }


        public virtual void OnConnectionNotAvailable() { }
    }
}
