﻿using InCityApp.DataModels;
using InCityApp.Entities;
using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Pages
{
    public class AnnotationQuizPage : ContentPage
    {
        public EventHandler OnAnnotationModified;

        private FiveStarsRatingView guessVote;
        private Image topImage;
        private Image effectbarBg;
        private Label annotationNameLbl;
        private Label descriptionLbl;
        private Label tagTitle;
        private Button cancelBtn;
        private Button confermBtn;
        private Label guessVoteTitle;
        private StackLayout guessVoteStack;
        private AbsoluteLayout topABLayout;
        private StackLayout buttonsStack;
        private StackLayout tagsCloudStack;

        private Annotation currentAnnotation;
        private int guessVoteNb = 0;

        public AnnotationQuizPage( Annotation ann )
        {
            NavigationPage.SetHasNavigationBar(this, false);

            BackgroundColor = Color.White;

            currentAnnotation = ann;

            this.SizeChanged += OnSizeChangedEvent;

            topImage = new Image
            {
                Source = ImageSource.FromFile("schermata_valutazione"),
            };

            annotationNameLbl = new Label
            {
                Text = currentAnnotation.Title,
                TextColor = Color.Black,
                FontAttributes = FontAttributes.Bold
            };

            descriptionLbl = new Label
            {
                Text = "Prova ad indovinare il voto che è stato dato a questa annotazione aiutandoti con i suoi tag. Più ti avvicinerai alla valutazione, più punti otterrai.",
                TextColor = Color.Black,
                XAlign = TextAlignment.Center
            };

            tagTitle = new Label
            {
                Text = ( currentAnnotation.Tags.Count > 0 ? "TAG PER QUESTA ANNOTAZIONE" : "NON CI SONO TAG ASSOCIATI"),
                TextColor = Color.FromHex("#57ba9d"),
                XAlign = TextAlignment.Center
            };

            topABLayout = new AbsoluteLayout
            {
                Children =
                {
                    topImage,
                    annotationNameLbl,
                    descriptionLbl
                }
            };

            effectbarBg = new Image
            {
                Source = ImageSource.FromFile("AnnotationQuizEffectBar")
            };

            confermBtn = new Button
            {
                Text = "CONFERMA",
            };
            confermBtn.IsEnabled = false;
            InCityApp.Helpers.LayoutFormatter.LayoutChangeDefaultButtonForState(confermBtn);

            cancelBtn = new Button
            {
                Text = "ANNULLA",
            };
            cancelBtn.IsEnabled = true;
            InCityApp.Helpers.LayoutFormatter.LayoutChangeDefaultButtonForState(confermBtn);

            confermBtn.Clicked += confirmButtonClicked;
            cancelBtn.Clicked += cancelButtonClicked;

            guessVoteTitle = new Label
            {
                Text = "Fai la tua valutazione:",
                TextColor = Color.Black,
                FontAttributes = FontAttributes.Bold,
                XAlign = TextAlignment.Center
            };

            guessVoteStack = new StackLayout() { Orientation = StackOrientation.Horizontal };

            guessVote = new FiveStarsRatingView(0, false)
            {
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                IsVisible = false,
            };

            guessVoteStack.Children.Add(guessVote);

            guessVote.IsVisible = true;
            guessVote.SetGuessMode(true, false);

            guessVote.OnVoted = (object sender, int vote) =>
            {
                guessVoteNb = vote;
                confermBtn.IsEnabled = true;
                InCityApp.Helpers.LayoutFormatter.LayoutChangeDefaultButtonForState(confermBtn);
            };

            AbsoluteLayout middleABLayout = new AbsoluteLayout
            {
                Padding = new Thickness( 0f, 50f, 0f, 0f ),
                Children =
                     {
                         effectbarBg,
                         guessVoteTitle,
                         guessVoteStack
                     }
            };

            tagsCloudStack = new StackLayout//new Grid
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.White

                /*VerticalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions = 
                {
                    new RowDefinition { Height = GridLength.Auto },
                },

                ColumnDefinitions = 
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto }
                },*/
            };


            Label tmpTagVw;
            foreach (Tag tag in currentAnnotation.Tags)
            {
                tmpTagVw = new Label
                {
                    Text = String.Format(" {0} ", tag.Name),
                    TextColor = Color.FromHex("#666666"),
                    BackgroundColor = Color.FromHex("#e7f5f1"),
                    XAlign = TextAlignment.Center,
                    FontSize = 16
                };

                tagsCloudStack.Children.Add(tmpTagVw);
            }

            buttonsStack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,

                Children = {
                    confermBtn,
                    cancelBtn
				}
            };

            //Alternativa
            /*StackLayout bottomStack = new StackLayout
            {

                Spacing = 0,
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center, 

                Children = {
                       tagTitle,
                       tagsCloudStack,
                       buttonsStack
				}
            };*/

            ScrollView tagsCloudScroller = new ScrollView
            {
                Orientation = ScrollOrientation.Vertical,
                Content = tagsCloudStack
            };

            CustomNavigationView cstnavVW = new CustomNavigationView("Indovina la valutazione");

            Content = new StackLayout
            {

                Spacing = 0,
                Orientation = StackOrientation.Vertical,

                Children = {
                       cstnavVW,
                       topABLayout,
                       middleABLayout,
                       tagTitle,
                       tagsCloudScroller,
                       buttonsStack
				}
            };
        }

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentPage page = (ContentPage)sender;

            double wndWidth = page.Width;
            double wndHeight = page.Height;

            double screenRatio = (page.Width / 1080);

            topImage.WidthRequest = 1080 * screenRatio;
            topImage.HeightRequest = 700 * screenRatio;

            AbsoluteLayout.SetLayoutBounds(topImage, new Rectangle((page.Width - topImage.Width) * 0.5, 0f, topImage.Width, topImage.Height));

            effectbarBg.WidthRequest = 1080 * screenRatio;
            effectbarBg.HeightRequest = 254 * screenRatio;

            AbsoluteLayout.SetLayoutBounds(effectbarBg, new Rectangle((page.Width - effectbarBg.Width) * 0.5, 0f, effectbarBg.Width, effectbarBg.Height));

            descriptionLbl.FontSize = 35 * screenRatio;
            annotationNameLbl.FontSize = 40 * screenRatio;
            tagTitle.FontSize = 35 * screenRatio;

            descriptionLbl.WidthRequest = page.Width;

            AbsoluteLayout.SetLayoutBounds(annotationNameLbl, new Rectangle(562f * screenRatio, 434f * screenRatio, annotationNameLbl.Width, annotationNameLbl.Height));
            AbsoluteLayout.SetLayoutBounds(descriptionLbl, new Rectangle((page.Width - descriptionLbl.Width) * 0.5, topImage.Height - (108 * screenRatio), page.Width, descriptionLbl.Height));

            confermBtn.WidthRequest = (page.Width * 0.7) * 0.5;
            cancelBtn.WidthRequest = confermBtn.WidthRequest;

            InCityApp.Helpers.LayoutFormatter.LayoutForDefaultButton(confermBtn, screenRatio);
            InCityApp.Helpers.LayoutFormatter.LayoutForDefaultButton(cancelBtn, screenRatio);

            tagsCloudStack.Padding = new Thickness(50 * screenRatio);

            guessVoteTitle.FontSize = 40 * screenRatio;
            AbsoluteLayout.SetLayoutBounds(guessVoteTitle, new Rectangle(0f, (25 * screenRatio), page.Width, guessVoteTitle.Height));
            AbsoluteLayout.SetLayoutBounds(guessVoteStack, new Rectangle((page.Width - guessVoteStack.Width) * 0.5, guessVoteTitle.Height + (60 * screenRatio), page.Width, guessVoteStack.Height));
        }

        private void cancelButtonClicked(object sender, EventArgs e)
        {
            this.Navigation.PopAsync(false);
        }

        private async void confirmButtonClicked(object sender, EventArgs e)
        {
            IsBusy = true;
            int ret = await AnnotationsModel.InsertValidation(currentAnnotation, guessVoteNb);

            if (ret < 0)
            {
                await DisplayAlert("ERRORE", "Errore durante l'invio. Riprova più tardi", "OK");
                await this.Navigation.PopAsync(false);
            }
            else
            {
                AnnotationQuizResult tmpAnn = new AnnotationQuizResult(currentAnnotation, guessVoteNb, ret);
                await this.Navigation.PushAsync(tmpAnn, false);
            }


            IsBusy = false;

            if (OnAnnotationModified != null)
                OnAnnotationModified(this, new EventArgs());

        }
    }
}

