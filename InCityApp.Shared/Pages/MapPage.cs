
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

using InCityApp.Entities;
using InCityApp.Shared.Views;
using InCityApp.Shared.ViewModels;
using InCityApp.Views;
using InCityApp.Pages;
using InCityApp.DependencyServiceInterfaces;
using InCityApp.DataInterfaces;
using InCityApp.DataModels;
using System.IO;

#if __ANDROID__
//using InCityApp_Android.Services;
#endif

namespace InCityApp.Shared.Pages
{
    public class MapPage : BasePage
    {
        private static bool DEBUG_MODE = false;
        
        AbsoluteLayout _bottomLayout;
        StackLayout _stack;
        StackLayout _legendYourNoteRow;
        StackLayout _legendNoteToVoteRow;
        StackLayout _legendVotedNoteRow;
        private CustomMapView _map;
        FixedLabel _legendTitle;
        FixedLabel _lblYourNoteDesc;
        Image _imgYourNote;
        FixedLabel _lblNoteToVoteDesc;
        Image _imgNoteToVote;
        FixedLabel _lblVotedNoteDesc;
        Image _imgVotedNote;
        FixedLabel _debugLabel;
        FixedButton _debugSignalWalkingBtn;
        FixedButton _debugSignalRunBtn;
        FixedButton _debugSignalOnBicycleBtn;
        FixedButton _debugSignalInVehicleBtn;

        //private static bool _firstRouteDrawn;

        FixedLabel _lblHint;
        StackLayout _legendStack;

        private MapViewModel ViewModel
        {
            get { return BindingContext as MapViewModel; }
        }

        public MapPage()
        {
            BindingContext = new MapViewModel();

            _stack = new StackLayout { Spacing = 0 };

            CustomNavigationView cstnavVW = new CustomNavigationView();
            _stack.Children.Add(cstnavVW);

            _lblHint = new FixedLabel() 
            { 
                Text = "Tieni premuto per 2 secondi sulla mappa per aggiungere un' annotazione.", 
                HeightRequest = 40,
                BackgroundColor = Color.FromHex("EDF6F3"),
                TextColor = Color.FromHex("#333333"),
                FontSize = 16,
                XAlign = TextAlignment.Center,
                YAlign = TextAlignment.Center,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold, 
            };
            _stack.Children.Add(_lblHint);

            _map = MakeMap();

            _map.VerticalOptions = LayoutOptions.FillAndExpand;
            //_stack.Children.Add(_map);

            _bottomLayout = new AbsoluteLayout()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
            };

            _legendStack = new StackLayout() 
            { 
                Orientation = StackOrientation.Vertical,
                BackgroundColor = Color.White,
            };

            _legendTitle = new FixedLabel() { HorizontalOptions = LayoutOptions.Center, FontAttributes = FontAttributes.Bold, Text = "Legenda", YAlign = TextAlignment.Center };
            _legendStack.Children.Add(_legendTitle);

            _lblYourNoteDesc = new FixedLabel() { Text = "Le tue annotazioni", YAlign = TextAlignment.Center };
            _imgYourNote = new Image () { Source = ImageSource.FromFile("blueMapPin.png") };
            _legendYourNoteRow = new StackLayout() 
            { 
                Orientation = StackOrientation.Horizontal, 
                Padding = new Thickness(10, 0, 0, 0),
                Children = 
                {
                   _imgYourNote,
                   _lblYourNoteDesc
                }
            };
             
            _lblNoteToVoteDesc = new FixedLabel () { Text = "Annotazioni da votare", YAlign = TextAlignment.Center };
            _imgNoteToVote = new Image() { Source = ImageSource.FromFile("purpleMapPin.png") };
            _legendNoteToVoteRow = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(10, 0, 0, 0),
                Children = 
                {
                    _imgNoteToVote,
                    _lblNoteToVoteDesc
                }
            };

            _lblVotedNoteDesc = new FixedLabel() { Text = "Annotazioni votate", YAlign = TextAlignment.Center };
            _imgVotedNote = new Image() { Source = ImageSource.FromFile("votedMapPin.png") };
            _legendVotedNoteRow = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(10, 0, 0, 0),
                Children = 
                {
                   _imgVotedNote,
                   _lblVotedNoteDesc
                }
            };

            _legendStack.Children.Add(_legendYourNoteRow);
            _legendStack.Children.Add(_legendNoteToVoteRow);
            _legendStack.Children.Add(_legendVotedNoteRow);

            _debugLabel = new FixedLabel();

            _debugLabel.IsVisible = DEBUG_MODE;

            _debugSignalWalkingBtn = new FixedButton() { Text = "Walk" };
            _debugSignalWalkingBtn.Clicked += _debugSignalWalkingBtn_Clicked;
            _debugSignalRunBtn = new FixedButton() { Text = "Run" };
            _debugSignalRunBtn.Clicked += _debugSignalRunBtn_Clicked;
            _debugSignalOnBicycleBtn = new FixedButton() { Text = "Bicycle" };
            _debugSignalOnBicycleBtn.Clicked += _debugSignalOnBicycleBtn_Clicked;
            _debugSignalInVehicleBtn = new FixedButton() { Text = "Vehicle" };
            _debugSignalInVehicleBtn.Clicked += _debugSignalInVehicleBtn_Clicked;

            _debugSignalWalkingBtn.IsVisible = DEBUG_MODE;
            _debugSignalRunBtn.IsVisible = DEBUG_MODE;
            _debugSignalOnBicycleBtn.IsVisible = DEBUG_MODE;
            _debugSignalInVehicleBtn.IsVisible = DEBUG_MODE;

            _bottomLayout.Children.Add(_map);
            _bottomLayout.Children.Add(_legendStack);
            _bottomLayout.Children.Add(_debugLabel);
            _bottomLayout.Children.Add(_debugSignalWalkingBtn);
            _bottomLayout.Children.Add(_debugSignalRunBtn);
            _bottomLayout.Children.Add(_debugSignalOnBicycleBtn);
            _bottomLayout.Children.Add(_debugSignalInVehicleBtn);

            _stack.Children.Add(_bottomLayout);

            PrepareLayout(_stack);

            this.SizeChanged += OnSizeChangedEvent;

            if (InCityApplication.OpenAnnotation && !string.IsNullOrWhiteSpace(InCityApplication.AnnotationIdToOpen))
            {
                Task.Run(async () =>
                {
                    _map.SelectedPinAnnotation = await AnnotationsModel.GetAnnotationById(InCityApplication.AnnotationIdToOpen);

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await _map.ShowAnnotationDialog(false);
                    });
                });
            }

            /*MessagingCenter.Subscribe<ConnectionLostPage>(this, "OnConnectionAvailable", (sender) =>
            {
                LoadPins();
            });*/
        }

        void _debugSignalInVehicleBtn_Clicked(object sender, EventArgs e)
        {
            //UserLocator.Instance.SetDebugActivity(UserLocator.ActivityType.InVehicle);

#if DEBUG
                string path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                string filename = Path.Combine(path, "LogInCity.txt");

                try
                {
                    using (var streamWriter = new StreamWriter(filename, true))
                    {
                        string info = "I'm into a vehicle!";
                        streamWriter.WriteLine(info);
                    }
                }
                catch (Exception exc)
                {

                }
#endif
        }

        void _debugSignalOnBicycleBtn_Clicked(object sender, EventArgs e)
        {
            //UserLocator.Instance.SetDebugActivity(UserLocator.ActivityType.OnBicycle);
            //UserLocator.Instance.SetCurrentActivity(UserLocator.ActivityType.OnBicycle, -1);
#if DEBUG
                string path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                string filename = Path.Combine(path, "LogInCity.txt");

                try
                {
                    using (var streamWriter = new StreamWriter(filename, true))
                    {
                        string info = "I'm on bicycle!";
                        streamWriter.WriteLine(info);
                    }
                }
                catch (Exception exc)
                {

                }
#endif
        }

        void _debugSignalRunBtn_Clicked(object sender, EventArgs e)
        {
            //UserLocator.Instance.SetDebugActivity(UserLocator.ActivityType.Running);
            //UserLocator.Instance.SetCurrentActivity(UserLocator.ActivityType.Running, -1);
#if DEBUG
            string path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            string filename = Path.Combine(path, "LogInCity.txt");

            try
            {
                using (var streamWriter = new StreamWriter(filename, true))
                {
                    string info = "I'm running!";
                    streamWriter.WriteLine(info);
                }
            }
            catch (Exception exc)
            {

            }
#endif

        }

        void _debugSignalWalkingBtn_Clicked(object sender, EventArgs e)
        {
            //UserLocator.Instance.SetDebugActivity(UserLocator.ActivityType.Walking);

#if DEBUG
            string path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            string filename = Path.Combine(path, "LogInCity.txt");

            try
            {
                using (var streamWriter = new StreamWriter(filename, true))
                {
                    string info = "I'm walking!";
                    streamWriter.WriteLine(info);
                }
            }
            catch (Exception exc)
            {

            }
#endif

        }

        void OnSizeChangedEvent(object sender, EventArgs e)
        {
            ContentPage page = (ContentPage)sender;
            double wndWidth = page.Width;

            double screenRatio = (page.Width / 1080);

            _map.WidthRequest = page.Width;
            _map.HeightRequest = _bottomLayout.Height;

            AbsoluteLayout.SetLayoutBounds(_legendStack, new Rectangle(0f, 0f, _map.WidthRequest, _map.HeightRequest));

            _legendStack.WidthRequest = 288 * screenRatio;
            _legendStack.HeightRequest = 230 * screenRatio;

            _legendTitle.FontSize = 
            _lblYourNoteDesc.FontSize = 
            _lblNoteToVoteDesc.FontSize =
            _debugLabel.FontSize =
            _lblVotedNoteDesc.FontSize = 20 * screenRatio;

            AbsoluteLayout.SetLayoutBounds(_legendStack, new Rectangle(40f * screenRatio, _bottomLayout.Height - _legendStack.HeightRequest - (80f * screenRatio), _legendStack.WidthRequest, _legendStack.HeightRequest));

            _legendYourNoteRow.Padding = new Thickness(10 * screenRatio, 0, 0, 0);
            _legendNoteToVoteRow.Padding = new Thickness(10 * screenRatio, 0, 0, 0);
            _legendVotedNoteRow.Padding = new Thickness(10 * screenRatio, 0, 0, 0); 

            _imgYourNote.WidthRequest = 32 * screenRatio;
            _imgYourNote.HeightRequest = 44 * screenRatio;

            _imgNoteToVote.WidthRequest = 32 * screenRatio;
            _imgNoteToVote.HeightRequest = 44 * screenRatio;

            _imgVotedNote.WidthRequest = 32 * screenRatio;
            _imgVotedNote.HeightRequest = 44 * screenRatio;


            AbsoluteLayout.SetLayoutBounds(_debugLabel, new Rectangle(0f, 0f, _debugLabel.WidthRequest, _debugLabel.HeightRequest));
            AbsoluteLayout.SetLayoutBounds(_debugSignalWalkingBtn, new Rectangle(0f, _debugLabel.Height + (10 * screenRatio), _debugSignalWalkingBtn.WidthRequest, _debugSignalWalkingBtn.HeightRequest));
            AbsoluteLayout.SetLayoutBounds(_debugSignalRunBtn, new Rectangle(_debugSignalWalkingBtn.Width + (10 * screenRatio), _debugLabel.Height + (10 * screenRatio), _debugSignalRunBtn.WidthRequest, _debugSignalRunBtn.HeightRequest));
            AbsoluteLayout.SetLayoutBounds(_debugSignalOnBicycleBtn, new Rectangle(_debugSignalWalkingBtn.Width + (10 * screenRatio) + _debugSignalRunBtn.Width + (10 * screenRatio), _debugLabel.Height + (10 * screenRatio), _debugSignalOnBicycleBtn.WidthRequest, _debugSignalOnBicycleBtn.HeightRequest));
            AbsoluteLayout.SetLayoutBounds(_debugSignalInVehicleBtn, new Rectangle(_debugSignalWalkingBtn.Width + (10 * screenRatio) + _debugSignalRunBtn.Width + (10 * screenRatio) + _debugSignalOnBicycleBtn.Width + (10 * screenRatio), _debugLabel.Height + (10 * screenRatio), _debugSignalInVehicleBtn.WidthRequest, _debugSignalInVehicleBtn.HeightRequest));

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (!UserLocator.Instance.isLocationServiceAvailable)
            {
                DisplayAlert("ATTENZIONE", "Questa applicazione ha bisogno della tua posizione per funzionare ma questo dispositivo sembra non supportare tale funzionalitą.", "OK");
            }
            else
            {
                if (!UserLocator.Instance.isLocationServiceEnabled)
                {
                    DisplayAlert("ATTENZIONE", "Questa applicazione ha bisogno della tua posizione per funzionare. Abilita il rilevamento della tua posizione nelle impostazioni di sistema.", "OK");
                }
            }

            LoadPins();

            UserLocator.Instance.OnLocationChanged += HandleOnUserLocationChanged;
            UserLocator.Instance.OnInitialLocationAcquired += HandleOnInitialLocationAcquired;

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            UserLocator.Instance.OnLocationChanged -= HandleOnUserLocationChanged;
            UserLocator.Instance.OnInitialLocationAcquired -= HandleOnInitialLocationAcquired;
        }

        public CustomMapView MakeMap()
        { 
            if (UserLocator.Instance == null || UserLocator.Instance.CurrentLocation == null)
                return new CustomMapView(this, MapSpan.FromCenterAndRadius(new Position (0, 0), Distance.FromMiles(0.3))); // TODO > retrieve last knowed user location

            Position pos = new Position (UserLocator.Instance.CurrentLocation.Latitude, UserLocator.Instance.CurrentLocation.Longitude);
            
            CustomMapView map = new CustomMapView(this, MapSpan.FromCenterAndRadius(pos, Distance.FromMiles(0.3)));
            map.OnAnnotationCreated = HandleOnUpdate;
            map.OnAnnotationDismissed = HandleOnUpdate;
            map.OnAnnotationModified = HandleOnUpdate;
            map.OnCreateAnnotationAborted = HandleOnUpdate; 

            return map;
        }

        public void LoadPins()
        {
            if (UserLocator.Instance.isLocationServiceAvailable && UserLocator.Instance.isLocationServiceEnabled)
            {
                Task.Run(async () =>
                {
                    await ViewModel.GetPins();
                    _map.UpdatePins(ViewModel.Models as IEnumerable<Annotation>);
                });
            }
        }

        private void HandleOnInitialLocationAcquired()
        {
            LoadPins();

            Position pos = new Position(UserLocator.Instance.CurrentLocation.Latitude, UserLocator.Instance.CurrentLocation.Longitude);
            _map.MoveToRegion(MapSpan.FromCenterAndRadius(pos, Distance.FromMiles(0.3)));
        }

        private void HandleOnUpdate(object sender, EventArgs e)
        {
            if (ViewModel == null || ViewModel.Models == null) 
                return;

            LoadPins();
       
            _map.Focus(); 
        }

        private void AddAnnotation()
        {
            _map.CurrentState = CustomMapView.State.InsertingAnnotation;
        }

        private void HandleOnUserLocationChanged(MapLocation location, double distance) //meters
        {
            if (_map == null)
                return;

            if (ViewModel == null || ViewModel.Models == null)
                return;

            LoadPins();

            Device.BeginInvokeOnMainThread(() =>
            { 
                _debugLabel.Text = "Last location speed : " + location.Speed + " | Last location accuracy : " + location.Accuracy;
            });
            /*if (_firstRouteDrawn == false)
            {
                await UserLocator.Instance.DrawLastRoute();
                _firstRouteDrawn = true;
            }*/

            if (UserLocator.Instance.OldLocation != null)
            {
                if (distance > 30)
                {
                    Debug.WriteLine("Location Changed : Centering map view");
                    MapSpan mapSpan = MapSpan.FromCenterAndRadius(new Position(location.Latitude, location.Longitude), Distance.FromMiles(0.3));
                    _map.MoveToRegion(mapSpan);
                }
            }
            else
            {
                Debug.WriteLine("Location Changed : Centering map view");
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(new Position(location.Latitude, location.Longitude), Distance.FromMiles(0.3));
                _map.MoveToRegion(mapSpan);
            }

        }

        /*private void OnUserActivityChanged(string activityId)
        {
            if (_map == null)
                return;
        }*/

        //private void ResetSteps()
        //{
        //    //_annotationsToolbar.Steps = "0";
        //    DependencyService.Get<IStepCounterService>().ResetCounter();
        //}


    }
}
