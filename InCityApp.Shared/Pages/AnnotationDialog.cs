﻿

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using XLabs.Platform.Services.Media;
using XLabs.Forms.Controls;

using InCityApp.Shared.Pages;
using InCityApp.DataModels;
using InCityApp.Entities;
using InCityApp.Shared.Views;
using InCityApp.Shared.ViewModels;
using InCityApp.Views;
using InCityApp.Helpers;
using InCityApp.DependencyServiceInterfaces;
using System.Diagnostics;
using Android.App;


namespace InCityApp.Pages
{
    public class AnnotationDialog : BasePage
    {
        private static string Default_Value_Title_label = "Inserisci Titolo";

        public EventHandler<Annotation> OnAnnotationCreated;
        public EventHandler OnAnnotationModified;
        public EventHandler OnAnnotationDismissed;
        public EventHandler OnCreateAnnotationAborted;

        private bool _creatingNewAnnotation;
        private Annotation _annotation;
        private AnnotationValidation _validation;
        private List<Tag> _newTags = new List<Tag>();

        //private ScrollView _scrollView;
        private StackLayout _stack;
        private StackLayout _header;
        private StackLayout _voteStack;
        private StackLayout _confirmationButtons;
        private Entry _titleEntry;
        //private FixedButton _btnValidationAgree;
        //private FixedButton _btnValidationDisagree;
        private FiveStarsRatingView _generalVote;
        private FixedLabel _voteHintDesc;
        private BoxView _voteHintFooterLine;
        private FixedLabel _guessDescription;
        private FixedLabel _guessSubDescription;
        private FiveStarsRatingView _guessVote;
        private Image _fbImage;
        private Image _twitterImage;
        private ImageButton _btnSelectPhoto;
        private ImageButton _btnViewPhoto;

        private FixedButton _btnOk;
        private FixedButton _btnCancel;
        private TagListView _tagListView;
        private StackLayout _addOrCancelRowLayout;
        private FixedButton _btnAddCustomTag, _btnConfirmCustomTag, _btnCancelInsertingCustomTag;
        private Entry _txtCustomTagName;
        private ImageButton _guessPointsResult;

        private readonly TaskScheduler _scheduler = TaskScheduler.FromCurrentSynchronizationContext();
        private MediaPicker _mediaPicker;
        private MediaFile _acquiredMediaFile;
        private string _currentPhotoFileName;
        private bool _postOnFacebook;
        private bool _postOnTwitter;
        private bool _feedbackChanged;

        public AnnotationDialogViewModel ViewModel
        {
            get { return BindingContext as AnnotationDialogViewModel; }
        }

        public bool IsLoading 
        {
            get { return ViewModel.IsLoading; }
            set { ViewModel.IsLoading = value; }
        }

        private bool IsPortrait() { return this.Width < this.Height; }

        public AnnotationDialog(Annotation annotation, bool createNew)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = new AnnotationDialogViewModel(this, annotation, createNew);
            ViewModel.PageRef = this;

            _annotation = annotation;
            _creatingNewAnnotation = createNew;

            _stack = new StackLayout() { Orientation = StackOrientation.Vertical, BackgroundColor = Color.White, Padding = new Thickness( 0, 0, 0, 10 ) };

            CustomNavigationView cstnavVW = new CustomNavigationView((_creatingNewAnnotation ? "Crea Nuova Annotazione" : _annotation.Title));
            _stack.Children.Add(cstnavVW);

            // Header ------------------------------------------------------------------------------
            if (_creatingNewAnnotation)
            {
                _header = new StackLayout() { Orientation = StackOrientation.Horizontal, HeightRequest = 50 };

                FixedLabel preTitle = new FixedLabel()
                {
                    Text = "Nome : ",
                    HorizontalOptions = LayoutOptions.Start,
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 20,
                    YAlign = TextAlignment.Center,
                };
                _header.Children.Add(preTitle);

                _annotation.Date = DateTime.Now.ToString();

                _titleEntry = new Entry()
                {
                    HorizontalOptions = LayoutOptions.Start,
                    Placeholder = Default_Value_Title_label
                };
                _titleEntry.Focused += titleEntry_Focused;
                _header.Children.Add(_titleEntry);

                _stack.Children.Add(_header);
            }

            // ----------------------------------------------------------------------------------------

            // Vote -----------------------------------------------------------------------------------
            _voteStack = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Fill };

            _voteStack.BackgroundColor = Color.FromHex("e6e6e6");

            FixedLabel voteDescription = new FixedLabel()
            {
                XAlign = TextAlignment.Center,
                Text = "VALUTAZIONE",
                FontAttributes = FontAttributes.Bold,
                FontSize = 12,
                WidthRequest = 120,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            };
            _voteStack.Children.Add(voteDescription);

            _generalVote = new FiveStarsRatingView(_annotation.Vote, _creatingNewAnnotation) 
            { 
                BackgroundColor = Color.FromHex("e6e6e6"),
                HorizontalOptions = LayoutOptions.End, 
                VerticalOptions = LayoutOptions.Center 
            };
            _generalVote.OnVoted = (object sender, int vote) =>
            {
                _annotation.Vote = vote;
                _btnOk.IsEnabled = true;
                _btnOk.BackgroundColor = Color.FromHex("52B296");
            };
            if (!_creatingNewAnnotation)
                _generalVote.IsEnabled = false;

            _voteStack.Children.Add(_generalVote);

            _stack.Children.Add(_voteStack);

            StackLayout hintStack = new StackLayout { Spacing = 0 };

            _voteHintDesc = new FixedLabel()
            {
                Text = "Prova ad indovinare la valutazione data a questa annotazione, aiutandoti con i tag associati.\nPiu ti avvicinerai al valore reale, piu punti ti verranno assegnati",
                HeightRequest = 40,
                BackgroundColor = Color.FromHex("EDF6F3"),
                FontSize = 16,
                XAlign = TextAlignment.Center,
                YAlign = TextAlignment.Center,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold,
                IsVisible = false,
            };
            hintStack.Children.Add(_voteHintDesc);

            _voteHintFooterLine = new BoxView() { Color = Color.FromHex("52B296"), BackgroundColor = Color.FromHex("52B296"), HeightRequest = 3, IsVisible = false, };
            hintStack.Children.Add(_voteHintFooterLine);

            _stack.Children.Add(hintStack);

            StackLayout guessVoteStack = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Fill };

            guessVoteStack.BackgroundColor = Color.FromHex("e6e6e6");

            _guessDescription = new FixedLabel()
            {
                XAlign = TextAlignment.Center,
                Text = "Secondo te :",
                FontAttributes = FontAttributes.Bold,
                FontSize = 12,
                WidthRequest = 120,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                IsVisible = false,
            };
            guessVoteStack.Children.Add(_guessDescription);

            _guessVote = new FiveStarsRatingView(0, _creatingNewAnnotation)
            {
                BackgroundColor = Color.FromHex("e6e6e6"),
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                IsVisible = false,
            };
            guessVoteStack.Children.Add(_guessVote);

            _stack.Children.Add(guessVoteStack);

            _guessSubDescription = new FixedLabel()
            {
                XAlign = TextAlignment.Center,
                Text = "Questi sono i tag assegnati all'annotazione:",
                FontAttributes = FontAttributes.Bold,
                FontSize = 12,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                IsVisible = false,
            };

            _stack.Children.Add(_guessSubDescription);

            _tagListView = new TagListView(_annotation, _creatingNewAnnotation);

            //_tagListView.OnNewTagValidating += HandleOnNewTagValidating;
            //_tagListView.OnNewTagValidated += HandleOnNewTagValidated;
            if (!_creatingNewAnnotation)
                _tagListView.IsEnabled = false;

            _stack.Children.Add(_tagListView);

            if (_creatingNewAnnotation)
            {
                // --- Add and Cancel buttons
                _addOrCancelRowLayout = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal
                };
                _btnAddCustomTag = new FixedButton()
                {
                    Text = "Aggiungi Tag",
                    HorizontalOptions = LayoutOptions.Start,
                    BackgroundColor = Color.FromHex("52B296"),
                    TextColor = Color.White,
                };
                _btnAddCustomTag.Clicked += btnAddCustomTag_Clicked;
                _addOrCancelRowLayout.Children.Add(_btnAddCustomTag);

                _btnCancelInsertingCustomTag = new FixedButton()
                {
                    Text = "Annulla",
                    IsEnabled = false,
                    IsVisible = false,
                    HorizontalOptions = LayoutOptions.End,
                    BackgroundColor = Color.FromHex("52B296"),
                    TextColor = Color.White,
                };
                _btnCancelInsertingCustomTag.Clicked += btnCancelInsertingCustomTag_Clicked;
                _addOrCancelRowLayout.Children.Add(_btnCancelInsertingCustomTag);

                _stack.Children.Add(_addOrCancelRowLayout);
                // ---

                // tag entry
                _txtCustomTagName = new Entry()
                {
                    IsEnabled = false,
                    IsVisible = false,
                    HorizontalOptions = LayoutOptions.Fill
                };
                _txtCustomTagName.TextChanged += txtCustomTagName_TextChanged;
                _txtCustomTagName.Focused += txtCustomTagName_Focused;
                _stack.Children.Add(_txtCustomTagName);
                // confirm button
                _btnConfirmCustomTag = new FixedButton()
                {
                    Text = "Ok",
                    IsEnabled = false,
                    IsVisible = false,
                    HorizontalOptions = LayoutOptions.Fill,
                    BackgroundColor = Color.FromHex("52B296"),
                    TextColor = Color.White,
                };
                _btnConfirmCustomTag.Clicked += btnConfirmCustomTag_Clicked;
                _stack.Children.Add(_btnConfirmCustomTag);
            }

            BoxView footerInterspace = new BoxView() { VerticalOptions = LayoutOptions.FillAndExpand };
            _stack.Children.Add(footerInterspace);

            // Footer ----------------------------------------------------------------------------------
            StackLayout footer = new StackLayout() { Orientation = StackOrientation.Vertical, VerticalOptions = LayoutOptions.End };

            StackLayout footerRow = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Center };
            if (_creatingNewAnnotation)
            {
#if __ANDROID__

                CheckBox btnPublishOnFacebook = new CheckBox() { HorizontalOptions = LayoutOptions.Start };
                btnPublishOnFacebook.CheckedChanged += btnPublishOnFacebook_Toggled;
                _fbImage = new Image() { Source = ImageSource.FromFile("fbOff.png") };
                footerRow.Children.Add(_fbImage);
                footerRow.Children.Add(btnPublishOnFacebook);
                //FacebookInterface.Instance.OnPostPublished += HandleOnPostPublished;

                CheckBox btnPublishOnTwitter = new CheckBox() { HorizontalOptions = LayoutOptions.Start };
                btnPublishOnTwitter.CheckedChanged += btnPublishOnTwitter_Toggled;
                _twitterImage = new Image() { Source = ImageSource.FromFile("twitterOff.png") };
                footerRow.Children.Add(_twitterImage);
                footerRow.Children.Add(btnPublishOnTwitter);
#endif
                if (string.IsNullOrEmpty(_annotation.PhotoURL))
                {
                    _btnSelectPhoto = new ImageButton()
                    {
                        // Text = "Seleziona una foto...",
                        Image = (FileImageSource)FileImageSource.FromFile("photoOn.png"),
                        HorizontalOptions = LayoutOptions.End,
                        VerticalOptions = LayoutOptions.End,
                        BackgroundColor = Color.Transparent,
                    };
                    _btnSelectPhoto.Clicked += async (sender, e) => { await btnSelectPhoto_Clicked(sender, e); };
                    footerRow.Children.Add(_btnSelectPhoto);

                    BoxView interspaceBox = new BoxView() { HeightRequest = 20 };
                    footerRow.Children.Add(interspaceBox);
                }

                footer.Children.Add(footerRow);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(_annotation.PhotoURL))
                {
                    _btnViewPhoto = new ImageButton()
                    {
                        Image = (FileImageSource)FileImageSource.FromFile("photoOn.png"),
                        HorizontalOptions = LayoutOptions.End,
                        VerticalOptions = LayoutOptions.End,
                        BackgroundColor = Color.Transparent,
                    };
                    _btnViewPhoto.Clicked += async (sender, e) => { await btnViewPhoto_Clicked(sender, e); };
                    footerRow.Children.Add(_btnViewPhoto);
                    footer.Children.Add(footerRow);
                }
            }

            _confirmationButtons = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.End };

            string okButtonText = "INVIA DATI";
            string cancelButtonText = "ANNULLA";
            if (!_creatingNewAnnotation)
            {
                cancelButtonText = "CHIUDI";
            }

            string currentUserId = UsersModel.CurrentUser.Id;
            if (_creatingNewAnnotation)
            {
                _btnOk = new FixedButton()
                {
                    Text = okButtonText,
                    TextColor = Color.White,
                    FontAttributes = FontAttributes.Bold,
                    WidthRequest = 120,
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.End,
                    BackgroundColor = Color.FromHex("909090"),
                    IsEnabled = false
                };
                _btnOk.Clicked += btnOk_Clicked;
                _confirmationButtons.Children.Add(_btnOk);
            }

            _btnCancel = new FixedButton() 
            { 
                Text = cancelButtonText,
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold,
                WidthRequest = 120, 
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.End,
                BackgroundColor = Color.FromHex("52B296") 
            };

            _btnCancel.Clicked += btnCancel_Clicked;
            _confirmationButtons.Children.Add(_btnCancel);

            _guessPointsResult = new ImageButton()
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = false,
                IsVisible = false,
                Image = (FileImageSource)FileImageSource.FromFile("starBigRed.png"),
            };
            footer.Children.Add(_guessPointsResult);

            footer.Children.Add(_confirmationButtons);

            _stack.Children.Add(footer);

            //_scrollView.Content = _stack;

            PrepareLayout(_stack);


            this.SizeChanged += AnnotationDialog_SizeChanged;
        }

        void titleEntry_Focused(object sender, FocusEventArgs e)
        {
            /*if (_titleEntry.Text == Default_Value_Title_label)
                _titleEntry.Text = string.Empty;*/
        }


        void btnPublishOnFacebook_Toggled(object sender, XLabs.EventArgs<bool> e)
        {
            _postOnFacebook = e.Value;
            if (_postOnFacebook)
                _fbImage.Source = ImageSource.FromFile("fbOn.png");
            else
                _fbImage.Source = ImageSource.FromFile("fbOff.png");
        }

        void btnPublishOnTwitter_Toggled(object sender, XLabs.EventArgs<bool> e)
        {
            _postOnTwitter = e.Value;
            if (_postOnTwitter)
                _twitterImage.Source = ImageSource.FromFile("twitterOn.png");
            else
                _twitterImage.Source = ImageSource.FromFile("twitterOff.png");
        }

        //void btnPublishOnFacebook_CheckedChanged(object sender, XLabs.EventArgs<bool> e)
        //{
        //    _postOnFacebook = e.Value;
        //    if (_postOnFacebook)
        //        _fbImage.Source = ImageSource.FromFile("fbOn.png");
        //    else
        //        _fbImage.Source = ImageSource.FromFile("fbOff.png");
        //}

        void HandleOnPostPublished()
        {
            
        }

        //void btnValidationDisagree_Clicked(object sender, EventArgs e)
        //{
        //    _btnOk.IsEnabled = true;
        //    _btnOk.BackgroundColor = Color.FromHex("52B296");

        //    _subHeader.Children.Remove(_btnValidationAgree);
        //    _subHeader.Children.Remove(_btnValidationDisagree);
        //    _subHeader.Children.Add(new Label() { Text = "Il tuo feedback : Non sono d'accordo", HorizontalOptions = LayoutOptions.Start, XAlign = TextAlignment.Start });

        //    _validation.Feedback = 0;
        //    _feedbackChanged = true;
        //}

        //void btnValidationAgree_Clicked(object sender, EventArgs e)
        //{
        //    _btnOk.IsEnabled = true;
        //    _btnOk.BackgroundColor = Color.FromHex("52B296");

        //    _subHeader.Children.Remove(_btnValidationAgree);
        //    _subHeader.Children.Remove(_btnValidationDisagree);
        //    _subHeader.Children.Add(new Label() { Text = "Il tuo feedback : Sono d'accordo", HorizontalOptions = LayoutOptions.Start, XAlign = TextAlignment.Start });

        //    _validation.Feedback = 1;
        //    _feedbackChanged = true;
        //}

        private async Task btnSelectPhoto_Clicked(object sender, EventArgs e)
        {
            //bool fromGallery = await DisplayAlert("Seleziona una foto...", "", "Dalla Galleria", "Scatta una foto");
            //if (fromGallery)
            //{
            //    ImageSource imageSource = await SelectPicture();
            //    if (imageSource != null)
            //    {
            //        PhotoEditorPage photoEditorPage = new PhotoEditorPage(imageSource, _acquiredMediaFile.Exif.Width, _acquiredMediaFile.Exif.Height);
            //        photoEditorPage.OnHotPointSelected += HandlePhotoOnHotPointSelected;
            //        await Navigation.PushModalAsync(photoEditorPage);
            //    }
            //}
            //else
            //{
                ImageSource imageSource = await TakePicture();
                if (imageSource != null)
                {
                    PhotoEditorPage photoEditorPage = new PhotoEditorPage(imageSource, _acquiredMediaFile.Exif.Width, _acquiredMediaFile.Exif.Height);
                    photoEditorPage.OnHotPointSelected += HandlePhotoOnHotPointSelected;
                    await Navigation.PushAsync(photoEditorPage, false);
                }
            //}
        }

        private async Task btnViewPhoto_Clicked(object sender, EventArgs e)
        {
            ShowLoadingIndicator("Caricamento Immagine...");

            byte[] bytes;
            bytes = await AnnotationsModel.GetPhoto(_annotation);
          
            ImageSource imageSource = null;
            MemoryStream ms = new MemoryStream(bytes);
            imageSource = ImageSource.FromStream(() => ms);

            PhotoEditorPage photoEditorPage = new PhotoEditorPage(imageSource, (int)_annotation.PhotoSize.X, (int)_annotation.PhotoSize.Y, false);
            
            RemoveLoadingIndicator();

            await Navigation.PushAsync(photoEditorPage, false);
            photoEditorPage.SetHotspot((int)_annotation.PhotoHotSpotPoint.X, (int)_annotation.PhotoHotSpotPoint.Y);
        }

        private async Task<ImageSource> TakePicture()
        {
            if (_mediaPicker == null)
                _mediaPicker = new MediaPicker();

            //_imgPhotoIcon.Source = null;

            return await _mediaPicker.TakePhotoAsync(new CameraMediaStorageOptions { DefaultCamera = CameraDevice.Front, MaxPixelDimension = 400 }).ContinueWith(t =>
            {
                if (t.IsFaulted == false && t.IsCanceled == false)
                {
                    _acquiredMediaFile = t.Result;

                    try
                    {
                    if (_acquiredMediaFile != null)
                    {
                        _currentPhotoFileName = _acquiredMediaFile.Exif.FileName;
                        ImageSource imageSource = null;
                        Byte[] imageAsBytes;

                        using (MemoryStream ms = new MemoryStream())
                        {
                            _acquiredMediaFile.Source.CopyTo(ms);
                            imageAsBytes = ms.ToArray();
                        }

                        // Workaround : on android the texture size seems to be limited, and camera produce photo with a larger resolution of what is supported.
                        // This is strange, anyway seems that people encoutering the same error use to resize their acquired images. We'll do the same.
                        int scaledWidth, scaledHeight;

                        Console.WriteLine("SIZE BEFORE RESIZE : " + _acquiredMediaFile.Exif.Width.ToString() + "x" + _acquiredMediaFile.Exif.Height.ToString());

                        if (_acquiredMediaFile.Exif.Width > _acquiredMediaFile.Exif.Height)
                        {
                            _annotation.PhotoImageBytes = new MemoryStream(ImageHelper.RotateImage(imageAsBytes, _acquiredMediaFile.Exif.Width, _acquiredMediaFile.Exif.Height, 90)).ToArray();

                            _annotation.PhotoImageBytes = new MemoryStream(ImageHelper.ResizeImage(_annotation.PhotoImageBytes, _acquiredMediaFile.Exif.Height, _acquiredMediaFile.Exif.Width, out scaledWidth, out scaledHeight)).ToArray();
                        }
                        else
                            _annotation.PhotoImageBytes = new MemoryStream(ImageHelper.ResizeImage(imageAsBytes, _acquiredMediaFile.Exif.Width, _acquiredMediaFile.Exif.Height, out scaledWidth, out scaledHeight)).ToArray();

                        _annotation.PhotoLocalName = _acquiredMediaFile.Exif.FileName;
                        _annotation.PhotoSize = new Vec2(scaledWidth, scaledHeight);

                        //if (scaledWidth > scaledHeight)
                        //    _annotation.PhotoSize = new Vec2(scaledHeight, scaledWidth);    
                        //else
                            
                        MemoryStream mm = new MemoryStream(_annotation.PhotoImageBytes);

                        imageSource = ImageSource.FromStream(() => mm);

                        //_imgPhotoThumbnail.Source = icon image_selected

                        return imageSource;
                    }
                    }
                    catch (Exception e)
                    {

                    }
                    return null;
                }

                return null;
            }, _scheduler);
        }

        private async Task<ImageSource> SelectPicture()
        {
            if (_mediaPicker == null)
                _mediaPicker = new MediaPicker();

            //_imgPhotoIcon.Source = null;

            try
            {
                _acquiredMediaFile = await _mediaPicker.SelectPhotoAsync(new CameraMediaStorageOptions());
                if (_acquiredMediaFile == null)
                    return null;

                _annotation.PhotoLocalName = _acquiredMediaFile.Exif.FileName;
                
                using (MemoryStream ms = new MemoryStream())
                {
                    _acquiredMediaFile.Source.CopyTo(ms);
                    _annotation.PhotoImageBytes = ms.ToArray();
                }

                _currentPhotoFileName = _acquiredMediaFile.Exif.FileName;
                //_imgPhotoIcon.Source = ImageSource.FromStream(() => mediaFile.Source);
                return ImageSource.FromStream(() => _acquiredMediaFile.Source);
            }
            catch (System.Exception ex)
            {
                // TODO > handle this case
            }

            return null;
        }

        private void HandlePhotoOnHotPointSelected(int x, int y, int imageWidth, int imageHeight)
        {
            _annotation.PhotoHotSpotPoint = new Vec2(x, y);
            //_annotation.PhotoSize = new Vec2(imageWidth, imageHeight);

            _btnSelectPhoto.Image = (FileImageSource)FileImageSource.FromFile("photoOn.png");
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            btnCancel_Clicked(this, new EventArgs());

            return true;
        }

        void AnnotationDialog_SizeChanged(object sender, EventArgs e)
        {
            //if (IsPortrait())
            //{
            //    _popUpLayout.WidthRequest = InCityApplication.ScreenWidth;
            //    _popUpLayout.HeightRequest = InCityApplication.ScreenHeight;
            //}
            //else
            //{
            //    _popUpLayout.WidthRequest = InCityApplication.ScreenHeight;
            //    _popUpLayout.HeightRequest = InCityApplication.ScreenWidth;
            //}

            //if (_popUpLayout.IsPopupActive)
            //{
            //    _popUpLayout.DismissPopup();
            //    OpenVotePopUp(_lastPopupHandler, _lastPopupTitle);
            //}
        }

        /// <summary>
        /// Called when the validation of a new tag is required.
        /// </summary>
        /// <param name="tagName"></param>
        /// <returns>True if tag doesn't exists </returns>
        private bool ValidateNewTag(string tagName)
        {
           Tag tag = _annotation.Tags.Find(x => x.Name == tagName);
           if (tag == null)
               return true;
           else
               return false;
        }

        private async void btnOk_Clicked(object sender, EventArgs e)
        {
            try
            {
                _annotation.Title = _titleEntry.Text;

                if (string.IsNullOrWhiteSpace(_annotation.Title) || _annotation.Title == Default_Value_Title_label)
                    _annotation.Title = DateTime.Now.ToString("yyyy-MM-dd hh:mm");
                //_annotation.Description = 

                IsLoading = true;

                if (_creatingNewAnnotation)
                {
                    _annotation.Title = _annotation.Title;
                    Annotation insertedAnnotation = await AnnotationsModel.Insert(_annotation);
                    if (insertedAnnotation == null)
                    {
                        IsLoading = false;

                        await DisplayAlert("ERRORE", "Errore durante il salvataggio dell'annotazione sul server.", "OK");
                        return;
                    }

                    _creatingNewAnnotation = false;

                    if (_postOnFacebook)
                        FacebookInterface.Instance.PostMessage("Ho annotato un luogo attraverso InCity!");

                    if (_postOnTwitter)
                        TwitterInterface.Instance.TwitMessage("Ho annotato un luogo attraverso InCity!");                        

                    if (OnAnnotationCreated != null)
                        OnAnnotationCreated(this, insertedAnnotation);

                    IsLoading = false;

                    await Navigation.PopToRootAsync(true);
                }
                else
                {
                    if (_feedbackChanged)
                    {
                        _feedbackChanged = false;
                        await AnnotationsModel.InsertValidation(_annotation, _validation.GuessVote);
                    }

                    if (OnAnnotationModified != null)
                        OnAnnotationModified(this, new EventArgs());

                    IsLoading = false;
                    await Navigation.PopToRootAsync(true);
                }
            }
            catch (Exception exc)
            {
                Debug.Print("AnnotationdDialog_Ok >> " + exc.Message);
                IsLoading = false;
                DisplayAlert("ERRORE", "Errore nel salvataggio dell'annotazione. Riprovare.", "OK");
            }
        }

        private async void btnShowAnnotation_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync().ContinueWith(async (t) =>
            {
                await Navigation.PushAsync(this, false);
            });
        }

        private void btnCancel_Clicked(object sender, EventArgs e)
        {
            if (_creatingNewAnnotation)
            {
                if (OnCreateAnnotationAborted != null)
                    OnCreateAnnotationAborted(this, new EventArgs());

                _creatingNewAnnotation = false;
            }
            
            if (OnAnnotationDismissed != null)
                OnAnnotationDismissed(this, new EventArgs());

            Navigation.PopToRootAsync(true);
        }

        void btnCancelInsertingCustomTag_Clicked(object sender, EventArgs e)
        {
            _txtCustomTagName.IsEnabled = false;
            _txtCustomTagName.IsVisible = false;
            _txtCustomTagName.Text = string.Empty;

            _btnConfirmCustomTag.IsEnabled = false;
            _btnConfirmCustomTag.IsVisible = false;

            _btnCancelInsertingCustomTag.IsEnabled = false;
            _btnCancelInsertingCustomTag.IsVisible = false;

            _btnAddCustomTag.IsEnabled = true;
            _btnAddCustomTag.BackgroundColor = Color.FromHex("52B296");
        }

        void btnAddCustomTag_Clicked(object sender, EventArgs e)
        {
            _txtCustomTagName.IsEnabled = true;
            _txtCustomTagName.IsVisible = true;
            _txtCustomTagName.Text = "Inserisci il nome del nuovo tag...";

            _btnConfirmCustomTag.IsEnabled = false;
            _btnConfirmCustomTag.BackgroundColor = Color.Gray;
            _btnConfirmCustomTag.IsVisible = true;

            _btnCancelInsertingCustomTag.IsEnabled = true;
            _btnCancelInsertingCustomTag.IsVisible = true;

            _btnAddCustomTag.IsEnabled = false;
            _btnAddCustomTag.BackgroundColor = Color.Gray;
           // _scrollView.ScrollToAsync(0, _btnConfirmCustomTag.Y, true);
        }

        void txtCustomTagName_TextChanged(object sender, TextChangedEventArgs e)
        {
            _txtCustomTagName.TextColor = Color.Black;
            if (string.IsNullOrWhiteSpace(e.NewTextValue))
            {
                _btnConfirmCustomTag.IsEnabled = false;
                _btnConfirmCustomTag.BackgroundColor = Color.Gray;
            }
            else
            {
                _btnConfirmCustomTag.IsEnabled = true;
                _btnConfirmCustomTag.BackgroundColor = Color.FromHex("52B296");
            }
        }

        void txtCustomTagName_Focused(object sender, FocusEventArgs e)
        {
            if (_txtCustomTagName.Text == "Inserisci il nome del nuovo tag...")
                _txtCustomTagName.Text = string.Empty;
        }

        void btnConfirmCustomTag_Clicked(object sender, EventArgs e)
        {
            bool isValid = ValidateNewTag(_txtCustomTagName.Text);

            if (isValid)
            {
                Tag tag = new Tag() { Id = -1, UserId = UsersModel.CurrentUser.Id, AnnotationId = _annotation.Id, Name = _txtCustomTagName.Text, Tagged = 1, Vote = 5 };
                int rowLayoutIndex = _stack.Children.IndexOf(_addOrCancelRowLayout);
                int customTagNameIndex = _stack.Children.IndexOf(_txtCustomTagName);
                int confirmCustomTagIndex = _stack.Children.IndexOf(_btnConfirmCustomTag);

                _stack.Children.Remove(_addOrCancelRowLayout);
                _stack.Children.Remove(_txtCustomTagName);
                _stack.Children.Remove(_btnConfirmCustomTag);

                _tagListView.AddRow(new TagListRow(tag, _creatingNewAnnotation) );

                _stack.Children.Insert(rowLayoutIndex, _addOrCancelRowLayout);
                _stack.Children.Insert(customTagNameIndex, _txtCustomTagName);
                _stack.Children.Insert(confirmCustomTagIndex, _btnConfirmCustomTag);

                _txtCustomTagName.IsEnabled = false;
                _txtCustomTagName.IsVisible = false;
                _txtCustomTagName.Text = string.Empty;

                _btnConfirmCustomTag.IsEnabled = false;
                _btnConfirmCustomTag.IsVisible = false;

                _btnCancelInsertingCustomTag.IsEnabled = false;
                _btnCancelInsertingCustomTag.IsVisible = false;

                _btnAddCustomTag.IsEnabled = true;
                _btnAddCustomTag.BackgroundColor = Color.FromHex("52B296");

                _annotation.Tags.Add(tag);
            }
            else
            {
                _txtCustomTagName.TextColor = Color.Red;
            }
        }
    }
} // end namespace
