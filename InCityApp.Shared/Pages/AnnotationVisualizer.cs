﻿using InCityApp.Entities;
using InCityApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Pages
{
	public class AnnotationVisualizer : ContentPage
	{
        private StackLayout _stack;
        private Annotation _annotation;

        public AnnotationVisualizer(Annotation annotation)
		{
            _annotation = annotation;

            _stack = new StackLayout() { Orientation = StackOrientation.Vertical, BackgroundColor = Color.White };

            CustomNavigationView cstnavVW = new CustomNavigationView(_annotation.Title);
            _stack.Children.Add(cstnavVW);
		}
	}
}
