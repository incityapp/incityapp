using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using InCityApp.Entities;

namespace InCityApp.DependencyServiceInterfaces
{
    public interface IMapCustomRenderer
    {
        void DrawRoute(List<RoutePosition> routePositions);

        void ForcePinsUpdate();

    }
}