﻿using InCityApp.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace InCityApp.DependencyServiceInterfaces
{
    public interface ILocalNotifications
    {
        void Notify(string title, string text, bool emitSound = true, bool emitVibration = true);
        void NotifyAnnotation(string annotationId, string title, string text, bool emitSound = true, bool emitVibration = true);
    }
}
