﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InCityApp.DependencyServiceInterfaces
{
    public interface IFacebookActivity
    {
        void PostMessage(string message);
    }
}
