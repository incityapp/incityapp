﻿using System;
using System.Collections.Generic;
using System.Text;

using InCityApp.Views;

namespace InCityApp.DependencyServiceInterfaces
{
    public interface IEditableImage
    {
        void SetEditMode(EditableImage.EditMode editMode);
    }
}
