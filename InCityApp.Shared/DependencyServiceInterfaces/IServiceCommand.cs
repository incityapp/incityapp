﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InCityApp.DependencyServiceInterfaces
{
    public interface IServiceCommand
    {
        void StartAudioService();

        void StopAudioService();

        void StartLocationService();

        void StopLocationService();
    }
}
