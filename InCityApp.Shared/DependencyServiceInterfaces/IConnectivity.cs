﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InCityApp.DependencyServiceInterfaces
{
    public interface IConnectivity
    {
        bool DetectNetwork();
    }
}
