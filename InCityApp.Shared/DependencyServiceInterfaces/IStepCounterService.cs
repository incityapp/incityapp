using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InCityApp.DependencyServiceInterfaces
{
    public interface IStepCounterService
    {
        void ResetCounter();

    }
}