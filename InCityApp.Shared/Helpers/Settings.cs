﻿using System;
using System.Collections.Generic;
using System.Text;

using Refractored.Xam.Settings;
using Refractored.Xam.Settings.Abstractions;
using Xamarin.Forms;
using InCityApp.DependencyServiceInterfaces;

namespace InCityApp.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants
        
        private const string UserTokenKey = "usertoken_key";
        private static readonly string UserTokenDefault = string.Empty;

        private const string UserIdKey = "userId_key";
        private static readonly string UserIdDefault = string.Empty;
        
        private const string UserNameKey = "userName_key";
        private static readonly string UserNameDefault = string.Empty;

        private const string PositionTrackingEnabledKey = "positionTrackingEnabled_key";
        private static readonly bool PositionTrackingEnabledDefault = true;

        private const string AudioRecordingEnabledKey = "audioRecordingEnabled_key";
        private static readonly bool AudioRecordingEnabledDefault = true;

        #endregion

        public static string UserToken
        {
            get { return AppSettings.GetValueOrDefault(UserTokenKey, UserTokenDefault); }
            set { AppSettings.AddOrUpdateValue(UserTokenKey, value); }
        }

        public static string UserId
        {
            get { return AppSettings.GetValueOrDefault(UserIdKey, UserIdDefault); }
            set { AppSettings.AddOrUpdateValue(UserIdKey, value); }
        }

        public static string UserName
        {
            get { return AppSettings.GetValueOrDefault(UserNameKey, UserNameDefault); }
            set { AppSettings.AddOrUpdateValue(UserNameKey, value); }
        }

        public static bool PositionTrackingEnabled
        {
            get { return AppSettings.GetValueOrDefault(PositionTrackingEnabledKey, PositionTrackingEnabledDefault); }
            set 
            { 
                AppSettings.AddOrUpdateValue(PositionTrackingEnabledKey, value);
                if (value == true)
                    DependencyService.Get<IServiceCommand>().StartLocationService();
                else
                    DependencyService.Get<IServiceCommand>().StopLocationService();    
            }
        }

        public static bool AudioRecordingEnabled
        {
            get { return AppSettings.GetValueOrDefault(AudioRecordingEnabledKey, AudioRecordingEnabledDefault); }
            set 
            { 
                AppSettings.AddOrUpdateValue(AudioRecordingEnabledKey, value);
                if (value == true)
                    DependencyService.Get<IServiceCommand>().StartAudioService();
                else
                    DependencyService.Get<IServiceCommand>().StopAudioService();    
            }
        }
    }
}
