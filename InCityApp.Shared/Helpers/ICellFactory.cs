using System;
using Xamarin.Forms;
using System.Reflection;
using InCityApp.Entities;

namespace InCityApp
{
	public interface ICellFactory
	{
        Cell CellForProperty (PropertyInfo info, ILocation context, Page parent = null);
    }
}

