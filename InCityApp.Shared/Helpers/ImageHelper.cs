
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
#if __ANDROID__
using Android.Graphics;
using System.Threading.Tasks;
using Xamarin.Forms;
#elif __IOS__
using UIKit;
using CoreGraphics;
#endif


namespace InCityApp.Helpers
{
    public static class ImageHelper
    {
        private static Dictionary<byte[], Func<byte[], Size>> imageFormatDecoders = new Dictionary<byte[], Func<byte[], Size>>()
        {
            { new byte[]{ 0xff, 0xd8 }, DecodeJfif },
        };

        static ImageHelper()
        {
        }
        public static short ReadLittleEndianInt16(this byte[] buffer, int startAt)
        {
            byte[] bytes = new byte[sizeof(short)];
            for (int i = 0; i < sizeof(short); i += 1)
            {
                bytes[sizeof(short) - 1 - i] = buffer[startAt + i];
            }
            return BitConverter.ToInt16(bytes, 0);
        }

        public static Size DecodeJfif(byte[] image)
        {
            int i = 0;
            while (image[i] == 0xff)
            {
                i++;
                byte marker = image[i];
                short chunkLength = ReadLittleEndianInt16(image, i);
                i++;
                if (marker == 0xc0)
                {
                    i++;
                    int height = ReadLittleEndianInt16(image, i);
                    i++;
                    int width = ReadLittleEndianInt16(image, i);

                    return new Size(width, height);
                }

                if (chunkLength < 0)
                {
                    ushort uchunkLength = (ushort)chunkLength;
                    i += uchunkLength - 2;
                }
                else
                {
                    i += chunkLength - 2;
                }
            }

            throw new ArgumentException();
        }

        public static Size GetDimensions(byte[] image)
        {
            int maxMagicBytesLength = imageFormatDecoders.Keys.OrderByDescending(x => x.Length).First().Length;

            byte[] magicBytes = new byte[maxMagicBytesLength];

            for (int i = 0; i < maxMagicBytesLength; i += 1)
            {
                magicBytes[i] = image[i];

                foreach (var kvPair in imageFormatDecoders)
                {
                    if (magicBytes[0] == kvPair.Key[0] && magicBytes[1] == kvPair.Key[1])
                    {
                        return kvPair.Value(image);
                    }
                }
            }

            throw new ArgumentException();
        }



        public static byte[] ResizeImage(byte[] imageData, float width, float height, out int scaledWidth, out int scaledHeight)
        {

#if __ANDROID__
            return ResizeImageAndroid(imageData, width, height, out scaledWidth, out scaledHeight);
#endif
#if __IOS__
			return ResizeImageIOS ( imageData, width, height );
#endif
#if WINDOWS_PHONE
			return ResizeImageWinPhone ( imageData, width, height );
#endif
        }

        public static byte[] RotateImage(byte[] imageData, float width, float height, float rotationAngle)
        {
#if __ANDROID__
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
            Matrix m = new Matrix();
            m.PostRotate(rotationAngle);
            Bitmap rotatedImage = Bitmap.CreateBitmap(originalImage, 0, 0, (int)width, (int)height, m, false);

            byte[] bytes;
            using (MemoryStream ms = new MemoryStream())
            {
                rotatedImage.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);
                originalImage.Recycle();
                rotatedImage.Recycle();
                originalImage.Dispose();
                rotatedImage.Dispose();
                bytes = ms.ToArray();

                return bytes;
            }
#endif
            return null;
        }

        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = stream.Position;
            stream.Position = 0;

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                stream.Position = originalPosition;
            }
        }

#if __ANDROID__

        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            float height = options.OutHeight;
            float width = options.OutWidth;
            double inSampleSize = 1D;

            if (height > reqHeight || width > reqWidth)
            {
                int halfHeight = (int)(height / 2);
                int halfWidth = (int)(width / 2);

                // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
                while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
                {
                    inSampleSize *= 2;
                }
            }

            return (int)inSampleSize;
        }

        public static byte[] ResizeImageAndroid(byte[] imageData, float width, float height, out int newWidth, out int newHeight)
        {
            BitmapFactory.Options options = new BitmapFactory.Options();
            // Calculate inSampleSize
            options.InSampleSize = CalculateInSampleSize(options, (int)width, (int)height);
            // Decode bitmap with inSampleSize set
            options.InJustDecodeBounds = false;
            
            // Load the bitmap
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);
            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)468, (int)648, false);
            newWidth = resizedImage.Width;
            newHeight = resizedImage.Height;

            byte[] bytes;
            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 80, ms);
                originalImage.Recycle();
                resizedImage.Recycle();
                originalImage.Dispose();
                resizedImage.Dispose();
                bytes = ms.ToArray();

                return bytes;
            }
        }

#endif


#if __IOS__
		public static byte[] ResizeImageIOS (byte[] imageData, float width, float height)
		{
			UIImage originalImage = ImageFromByteArray (imageData);

			//create a 24bit RGB image
			using (CGBitmapContext context = new CGBitmapContext (IntPtr.Zero,
				(int)width, (int)height, 8,
				(int)(4 * width), CGColorSpace.CreateDeviceRGB (),
				CGImageAlphaInfo.PremultipliedFirst)) {

				CGRect imageRect = new CGRect (0, 0, width, height);

				// draw the image
				context.DrawImage ((CGRect)imageRect, (CGImage)originalImage.CGImage);

				UIKit.UIImage resizedImage = UIKit.UIImage.FromImage (context.ToImage ());

				// save the image as a jpeg
				return resizedImage.AsJPEG ().ToArray ();
			}
		}

		public static UIKit.UIImage ImageFromByteArray(byte[] data)
		{
			if (data == null) {
				return null;
			}

			UIKit.UIImage image;
			try {
				image = new UIKit.UIImage(Foundation.NSData.FromArray(data));
			} catch (Exception e) {
				Console.WriteLine ("Image load failed: " + e.Message);
				return null;
			}
			return image;
		}
#endif



#if WINDOWS_PHONE

        public static byte[] ResizeImageWinPhone (byte[] imageData, float width, float height)
        {
            byte[] resizedData;

            using (MemoryStream streamIn = new MemoryStream (imageData))
            {
                WriteableBitmap bitmap = PictureDecoder.DecodeJpeg (streamIn, (int)width, (int)height);

                using (MemoryStream streamOut = new MemoryStream ())
                {
                    bitmap.SaveJpeg(streamOut, (int)width, (int)height, 0, 100);
                    resizedData = streamOut.ToArray();
                }
            }
            return resizedData;
        }
        
#endif

    }
}
