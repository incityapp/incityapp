﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InCityApp.Helpers
{
    static class DistanceCalculator
    {
        const double earthRadiusKm = 6371.0; 
        const double radiansToDegrees =180f/Math.PI;
        const double degreesToRadians =Math.PI/180f;
        const double earthRadiusMl = 3960.0; // in miles

        public static double MilesToLatitudeDegrees(double miles)
        {
            return (miles / earthRadiusMl) * radiansToDegrees;
        }

        public static double MilesToLongitudeDegrees(double miles, double atLatitude)
        {
            double radiusAtLatitude = earthRadiusMl * Math.Cos(atLatitude * degreesToRadians);
            return (miles / radiusAtLatitude) * radiansToDegrees;
        }

        public static double KmsToLatitudeDegrees(double kms)
        {
            return (kms/earthRadiusKm) * radiansToDegrees;
        }
        
        public static double KmsToLongitudeDegrees(double kms, double latitude)
        {
            double radiusAtLat = earthRadiusKm *Math.Cos(latitude * degreesToRadians);
            return (kms/ radiusAtLat) *radiansToDegrees;
        }
    }
}
