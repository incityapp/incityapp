﻿#if __IOS__
using Foundation;
#endif

using System;
using System.Collections.Generic;
using System.Text;

namespace InCityApp.Helpers
{
	public static class DateConverter
	{
#if __IOS__
		public static DateTime NSDateToDateTime(NSDate date)
		{
			return (new DateTime(2001, 1, 1, 0, 0, 0)).AddSeconds(date.SecondsSinceReferenceDate);
		}

		public static NSDate DateTimeToNSDate(DateTime date)
		{
			return NSDate.FromTimeIntervalSinceReferenceDate((date - (new DateTime(2001, 1, 1, 0, 0, 0))).TotalSeconds);
		}
#endif
	}
}
