
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using InCityApp.DataProviders;

#if __ANDROID__
using Android.Graphics;
#elif __IOS__
using UIKit;
using CoreGraphics;
#endif


namespace InCityApp.Helpers
{
    public class Connectivity
    {
        static private readonly Connectivity _instance = new Connectivity();

        public static Connectivity Instance
        {
            get
            {
                return _instance;
            }
        }

        static Connectivity()
        {
        }

        Connectivity()
        {
        }

        public async Task<bool> IsOurWSConnected()
        {
            RemoteStorageDataProvider remoteWS = new RemoteStorageDataProvider();
            return await remoteWS.TestConnection();
        }
    }
}

