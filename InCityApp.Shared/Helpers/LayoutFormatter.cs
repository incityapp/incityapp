﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace InCityApp.Helpers
{
    class LayoutFormatter
    {
        public static void LayoutForDefaultButton(Button btn, double screenRatio, int fontSize = 35)
        {
            btn.FontSize = fontSize;
            btn.FontSize *=  screenRatio;
            btn.HeightRequest = 120 * screenRatio;

            btn.TextColor = Color.White;

            LayoutChangeDefaultButtonForState(btn);
        }

        public static void LayoutChangeDefaultButtonForState(Button btn)
        {
            if (btn.IsEnabled)
                btn.BackgroundColor = Color.FromHex("#57ba9d");
            else
                btn.BackgroundColor = Color.FromHex("#cccccc");
        }
    }


}
