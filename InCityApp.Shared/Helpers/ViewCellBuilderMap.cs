using System;
using InCityApp.Entities;
using System.Collections.Generic;
using System.Reflection;
using Xamarin.Forms;
using InCityApp.Services;
using InCityApp.Helpers;

[assembly:Dependency(typeof(ViewCellBuilderMap))]

namespace InCityApp.Helpers
{
    public class ViewCellBuilderMap : Dictionary<Type,Func<PropertyInfo, ILocation, Page, Cell>>
    {
        public ViewCellBuilderMap()
        {
            Add(typeof(bool), ViewCellFactory.BoolCell);
            Add(typeof(int),  ViewCellFactory.IntCell);
            Add(typeof(decimal), ViewCellFactory.DecimalCell);
            Add(typeof(Address), ViewCellFactory.AddressCell);
            Add(typeof(string), ViewCellFactory.StringCell);
        }
    }
}

