using System;
using System.Collections.Generic;
using System.Reflection;
using InCityApp.Entities;
using InCityApp.Services;
using Xamarin.Forms;
using InCityApp.Helpers;

[assembly:Dependency(typeof(EditCellBuilderMap))]

namespace InCityApp.Helpers
{
    public class EditCellBuilderMap : Dictionary<Type,Func<PropertyInfo, ILocation, Page, Cell>>
    {
        public EditCellBuilderMap()
        {
            Add(typeof(bool), EditCellFactory.BoolCell);
            Add(typeof(int),  EditCellFactory.IntCell);
            Add(typeof(decimal), EditCellFactory.DecimalCell);
            Add(typeof(Address), EditCellFactory.AddressCell);
            Add(typeof(string), EditCellFactory.StringCell);
        }
    }
}

