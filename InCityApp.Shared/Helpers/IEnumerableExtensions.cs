﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

public static class IEnumerableExtensions
{
    public static double MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector, double defaultValue)
    {
        if (source.Any<TSource>())
            return source.Min<TSource>(selector);

        return defaultValue;
    }

    public static double AvarageOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector, double defaultValue)
    {
        if (source.Any<TSource>())
            return source.Average<TSource>(selector);

        return defaultValue;
    }

    public static double MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector, double defaultValue)
    {
        if (source.Any<TSource>())
            return source.Max<TSource>(selector);

        return defaultValue;
    }
} 
