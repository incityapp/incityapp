﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Xamarin.Forms;

using InCityApp.Shared.Pages;
using InCityApp.Helpers;
using InCityApp.DataModels;
using System.Threading.Tasks;
using InCityApp.Entities;

#if __IOS__
using InCityApp_iOS;
#endif

namespace InCityApp
{
    public enum GroundStatus { Background, Foreground };

    public class App : Xamarin.Forms.Application
    {
        public GroundStatus status = GroundStatus.Foreground;

        public App()
        {
            InCityApplication.App = this;

            MainPage = new Page();

            string tmpTk = Settings.UserToken;

            if (!string.IsNullOrEmpty(tmpTk))
            {
                UsersModel.OnUserVerified = HandleOnUserVerified;
                UsersModel.VerifyAccessToken(Settings.UserId);
            }
            else
            {
                // the user never pass the login or registration phase
                if (InCityApplication.InLoginPage || InCityApplication.InRegistrationPage)
                    SetLoginAsRootPage();
                else
                {
                    // this must never happen!
                    SetMapAsRootPage();
                }
            }
        }

        protected override void OnSleep()
        {
            status = GroundStatus.Background;
        }
        protected override void OnResume()
        {
            status = GroundStatus.Foreground;
        }

        public void HandleOnUserVerified(bool alreadyLoggedIn)
        {
            if (alreadyLoggedIn)
                SetMapAsRootPage();
            else
                SetLoginAsRootPage();
        }

        public void SetLoginAsRootPage()
        {
            LoginPage page = new LoginPage();
            MainPage = page;
        }

        public void SetRegistrationAsRootPage()
        {
            RegistrationPage page = new RegistrationPage();
            MainPage = page;
        }

        public void SetMapAsRootPage()
        {
            MapPage mapPage = new MapPage();
            NavigationPage nav = new NavigationPage(mapPage);
            nav.BarBackgroundColor = Color.White;
            nav.BarTextColor = Color.FromHex("#57ba9d");

            NavigationPage.SetHasNavigationBar(mapPage, false);

            MainPage = nav;
        }

        public void SetNoiseAsRootPage()
        {
            NoisePollutionPage newPage = new NoisePollutionPage();
            NavigationPage.SetHasNavigationBar(newPage, false);

            MainPage = newPage;
        }

        public void SetActivitiesAsRootPage()
        {
            ActivitiesReportPage newPage = new ActivitiesReportPage();
            NavigationPage.SetHasNavigationBar(newPage, false);

            MainPage = newPage;
        }

        public void SetSettingsAsRootPage()
        {
            SettingsPage newPage = new SettingsPage();
            NavigationPage.SetHasNavigationBar(newPage, false);

            MainPage = newPage;
        }

        public void SetCustomPage(ContentPage newPage)
        {
            MainPage = newPage;
        }
    }
}
