using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using InCityApp;
using InCityApp.Entities;

using Xamarin.Social.Services;
using Xamarin.Social;
using Xamarin.Auth;
using InCityApp.DependencyServiceInterfaces;

namespace InCityApp_Android.Activities
{
    [Activity(Label = "")]
    public class TwitterActivity : Activity//, ITwitterActivity
    {
        private static TwitterService _twitter;
        //private string _messageToTwit;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);


        }

        #region ITwitterActivity Methods Implementation

        public async void Twit(string message)
        {
            var accounts = AccountStore.Create(this.BaseContext).FindAccountsForService("InCityApp-Twitter");
            if (accounts == null || accounts.Count() == 0)
            {
                OAuth1Authenticator auth = new OAuth1Authenticator(consumerKey: "F2w7cCyxnPMphtedeZbvgAANU",
                                                                    consumerSecret: "ogHd4wdGsAsjXA3syodlhCZVZoH0b4aB82qA0I24TtHyhY5kI8",
                                                                    requestTokenUrl: new Uri("https://api.twitter.com/oauth/request_token"),
                                                                    authorizeUrl: new Uri("https://api.twitter.com/oauth/authorize"),
                                                                    accessTokenUrl: new Uri("https://api.twitter.com/oauth/access_token"),
                                                                    callbackUrl: new Uri("http://www.incityapp.eu"));

                auth.Completed += async (s, e) =>
                {
                    if (e.IsAuthenticated)
                    {
                        try
                        {
                            AccountStore.Create(this.BaseContext).Save(e.Account, "InCityApp-Twitter");

                            await SendTweet(message, e.Account);
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(string.Format("Twitter Activity >> Error sharing item for account '{0}' : ", e.Account.Username) + exc.Message);
                        }
                    }
                    else
                    {
                        // The user cancelled
                    }
                };
            }
            else
            {
                Account account = null;
                try
                {
                    account = accounts.FirstOrDefault();
                    if (account == null)
                        throw new ArgumentNullException();

                    await SendTweet(message, account);
                }
                catch (Exception exc)
                {
                    Console.WriteLine(string.Format("Twitter Activity >> Error sharing item for account '{0}' : ", account.Username) + exc.Message);
                }
            }
        }

        private async Task SendTweet(string message, Account account)
        {
            _twitter = new TwitterService
            {
                ConsumerKey = Resources.GetText(Resource.String.twitter_consumer_key),
                ConsumerSecret = Resources.GetText(Resource.String.twitter_consumer_secret)
            };
            var item = new Item { Text = message };
            item.Links.Add(new Uri("http://www.incityapp.eu"));

            await _twitter.ShareItemAsync(item, account).ContinueWith(x =>
            {

            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        #endregion
    }
}