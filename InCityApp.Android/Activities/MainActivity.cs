﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Xamarin.Social.Services;
using Xamarin.Social;
using Xamarin.Auth;

using Android.App;
using Android.OS;
using Android.Graphics.Drawables;
using Android.Content.PM;
using Android.Locations;
using Android.Util;
using Android.Widget;
using Android.Views;
using Android.Content;
using Android.Net;

using InCityApp;
using InCityApp.Entities;
using InCityApp.Shared.Pages;
using InCityApp_Android.Services;

using InCityApp.DependencyServiceInterfaces;
using InCityApp_Android.Activities;
using Android.Provider;
using Xamarin.Geolocation;

[assembly: Xamarin.Forms.Dependency(typeof(InCityApp_Android.MainActivity))]
namespace InCityApp_Android
{
    [Activity(
        Label = "INCITY", 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, 
        ScreenOrientation = ScreenOrientation.Portrait, 
        LaunchMode = Android.Content.PM.LaunchMode.SingleTop)]
    public class MainActivity : Xamarin.Forms.Platform.Android.FormsApplicationActivity,
                                IConnectivity, IServiceCommand, IUniqueIdGenerator
    {
        private static readonly string _logTag = "InCity.Android";

        private static TwitterService _twitter;

        private static Geolocator geolocator;

        private static Activity _thisActivity;

        public MainActivity() : base() { }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            _thisActivity = this;

            Forms.Init(this, bundle);
            FormsMaps.Init(this, bundle);

            geolocator = new Geolocator(this) { DesiredAccuracy = 1 };
            SetUpGeoLocation();

            /*ServiceInterface<LocationService>.Instance.ServiceConnected += (object sender, ServiceConnectedEventArgs e) =>
            {
                Log.Debug(_logTag, "Location Service Connected.");
                // notifies us of location changes from the system
                ServiceInterface<LocationService>.Instance.Service.LocationChanged += LocationService_OnLocationChanged;
                // notifies us of user changes to the location provider (ie the user disables or enables GPS)
                ServiceInterface<LocationService>.Instance.Service.ProviderDisabled += LocationService_OnProviderDisabled;
                ServiceInterface<LocationService>.Instance.Service.ProviderEnabled += LocationService_OnProviderEnabled;
                // notifies us of the changing status of a provider (ie GPS no longer available)
                ServiceInterface<LocationService>.Instance.Service.StatusChanged += LocationService_OnStatusChanged;
            };*/

            ServiceInterface<AudioService>.Instance.ServiceConnected += (object sender, ServiceConnectedEventArgs e) =>
            {
                Log.Debug(_logTag, "Audio Service Connected.");
                ServiceInterface<AudioService>.Instance.Service.OnOneSecondRecorded += AudioService_OnOneSecondVolume;
                ServiceInterface<AudioService>.Instance.Service.OnFiveSecondsRecorded += AudioService_OnFiveSecondsRecorded;
            };

            ServiceInterface<StepCounterService>.Instance.ServiceConnected += (object sender, ServiceConnectedEventArgs e) =>
            {
                Log.Debug(_logTag, "StepCounter Service Connected.");
                // notifies us if the user stepped
                ServiceInterface<StepCounterService>.Instance.Service.OnNewStep += StepCounterService_OnNewStep;
                // notifies us it the user reset the step session
                StepCounterService.OnNewStepSession += StepCounterService_OnNewStepSession;
                // notifies us if something is gone wrong with the sensor and we are currently simulating steps
                ServiceInterface<StepCounterService>.Instance.Service.OnBeginSimulatingSteps += StepCounterService_OnBeginSimulatingSteps;
                // notifies us if StepCounter sensor is back and operative
                ServiceInterface<StepCounterService>.Instance.Service.OnEndSimulatingSteps += StepCounterService_OnEndSimulatingSteps;
            };

            ServiceInterface<ActivityRecognitionService>.Instance.ServiceConnected += (object sender, ServiceConnectedEventArgs e) =>
            {
                Log.Debug(_logTag, "Activity Recognition Service Connected.");
                // notifies us if user activity is changed
                ServiceInterface<ActivityRecognitionService>.Instance.Service.OnActivityChanged += ActivityRecognitionService_OnActivityChanged;
            };

            InCityApplication.ScreenWidth = Resources.DisplayMetrics.WidthPixels;
            InCityApplication.ScreenHeight = Resources.DisplayMetrics.HeightPixels;

            InCityApplication.RealScreenWidth = (int)(Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density);
            InCityApplication.RealScreenHeight = (int)(Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density);

            LoadApplication(new App());

            if (Intent != null)
            {
                if (Intent.Extras != null && Intent.Extras.ContainsKey("annotationId"))
                {
                    string annotationId = Intent.Extras.GetString("annotationId");
                    if (!string.IsNullOrWhiteSpace(annotationId) && InCityApplication.OpenAnnotation == false)
                    {
                        InCityApplication.AnnotationIdToOpen = annotationId;
                        InCityApplication.OpenAnnotation = true;
                        // useless, cannot get it to work
                        //Intent.Extras.Remove("annotationId");
                    }
                    else
                    {
                        InCityApplication.AnnotationIdToOpen = string.Empty;
                        InCityApplication.OpenAnnotation = false;
                        //Intent.Extras.Remove("annotationId");
                    }
                }
            }
        }

        protected override void OnResume()
        {
            base.OnResume();

            SetUpGeoLocation();


        }

        private void SetUpGeoLocation()
        {
            UserLocator.Instance.isLocationServiceAvailable = geolocator.IsGeolocationAvailable;
            UserLocator.Instance.isLocationServiceEnabled = geolocator.IsGeolocationEnabled;

            Task.Run(async () =>
            {
                if (geolocator.IsGeolocationAvailable && geolocator.IsGeolocationEnabled)
                {
                    await geolocator.GetPositionAsync(30000).ContinueWith((t) =>
                    {
                        if (t.IsFaulted)
                        {
                            Log.Debug(_logTag, "First position exception: " + ((GeolocationException)t.Exception.InnerException).Error.ToString());
                        }
                        else if (t.IsCanceled)
                        {
                            Log.Debug(_logTag, "First position canceled");
                        }
                        else
                        {
                            SetCurrentPosition(t.Result);
                        }
                    });
                }
            });

            if (geolocator.IsGeolocationAvailable && geolocator.IsGeolocationEnabled)
            {
                geolocator.PositionChanged += geolocator_PositionChanged;
                StartLocationService();
            }
        }

        private void geolocator_PositionChanged(object sender, PositionEventArgs e)
        {
            SetCurrentPosition(e.Position);
        }

        private void SetCurrentPosition(Position pos)
        {
            if (UserLocator.Instance.CurrentLocation == null)
            {
                Log.Error(_logTag, "Unable to determine your location.");
            }
            else
            {
                Log.Debug(_logTag, string.Format("Coords : {0} Accuracy : {1} Speed : {2} ", UserLocator.Instance.CurrentLocation.ToString(), pos.Accuracy, pos.Speed));
            }

            //if (UserLocator.Instance.OldLocation != null)
            //{
            //    if (pos.Accuracy < 40 && pos.Speed > 0)
            //    {
            //        UserLocator.Instance.CurrentLocation = new MapLocation(pos.Latitude, pos.Longitude, pos.Speed, pos.Accuracy);
            //    }
            //    else
            //        UserLocator.Instance.LogLocation("Discarded", new MapLocation(pos.Latitude, pos.Longitude, pos.Speed, pos.Accuracy));
            //}
            //else
            //{
            //    UserLocator.Instance.CurrentLocation = new MapLocation(pos.Latitude, pos.Longitude, pos.Speed, pos.Accuracy);
            //}
            UserLocator.Instance.CurrentLocation = new MapLocation(pos.Latitude, pos.Longitude, pos.Speed, pos.Accuracy);
            
        }

        #region LocationService Methods

        ///<summary>
        /// Updates UI with location data
        /// </summary>
        /*public void LocationService_OnLocationChanged(object sender, LocationChangedEventArgs e)
        {
            Log.Debug(_logTag, "Foreground updating");
        }

        public void LocationService_OnProviderDisabled(object sender, ProviderDisabledEventArgs e)
        {
            UserLocator.Instance.isLocationServiceAvailable = false;
            Log.Debug(_logTag, "Location provider disabled event raised");
        }

        public void LocationService_OnProviderEnabled(object sender, ProviderEnabledEventArgs e)
        {
            UserLocator.Instance.isLocationServiceAvailable = true;
            Log.Debug(_logTag, "Location provider enabled event raised");
        }

        public void LocationService_OnStatusChanged(object sender, StatusChangedEventArgs e)
        {
            Log.Debug(_logTag, "Location status changed, event raised");
        }*/

        #endregion

        #region AudioService Methods

        void AudioService_OnOneSecondVolume(object sender, double e)
        {
            AudioRecorder.Instance.CurrentVolume = e;
        }

        void AudioService_OnFiveSecondsRecorded(object sender, double e)
        {
            AudioRecorder.Instance.AddSoundSample(e);
        }

        #endregion

        #region StepCounterService Methods

        void StepCounterService_OnNewStep(object sender, long e)
        {
            StepCounter.Instance.StepCount += e;
        }

        void StepCounterService_OnNewStepSession(object sender, EventArgs e)
        {
            StepCounter.Instance.StepCount = 0;
        }

        void StepCounterService_OnBeginSimulatingSteps(object sender, EventArgs e)
        {

        }

        void StepCounterService_OnEndSimulatingSteps(object sender, EventArgs e)
        {
           
        }

        #endregion

        #region ActivityRecognitionService Methods

        void ActivityRecognitionService_OnActivityChanged(int activity, int accuracy)
        {
            // update on the UI thread
            RunOnUiThread(() =>
            {
                if (UserLocator.Instance != null)
                    UserLocator.Instance.SetCurrentActivity((UserLocator.ActivityType)activity, accuracy);
            });
        }

        #endregion

        #region IConnectivity Implementation Methods
        public bool DetectNetwork()
        {
            var connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);

            var activeConnection = connectivityManager.ActiveNetworkInfo;

            if ((activeConnection != null) && activeConnection.IsConnected)
                return true;
            else
                return false;

            //var mobile = connectivityManager.GetNetworkInfo(ConnectivityType.Mobile).GetState();
            //if (mobile == NetworkInfo.State.Connected)
            //{

            //}

            //var wifiState = connectivityManager.GetNetworkInfo(ConnectivityType.Wifi).GetState();
            //if (wifiState == NetworkInfo.State.Connected)
            //{

            //}
        }

        #endregion

        #region IServiceCommand Implementation Methods

        public void StartAudioService()
        {
            ServiceInterface<AudioService>.Instance.Start();
        }

        public void StopAudioService()
        {
            //Android.App.Application.Context.StopService(new Intent(Android.App.Application.Context, typeof(AudioService)));
            ServiceInterface<AudioService>.Instance.Service.OnDestroy();
            ServiceInterface<AudioService>.Instance.Stop();
        }

        public void StartLocationService()
        {
            //ServiceInterface<LocationService>.Instance.Start();

            if(!geolocator.IsListening)
                geolocator.StartListening(UserLocator.LocationUpdateTime, UserLocator.LocationDisplacementThreshold);
        }

        public void StopLocationService()
        {
            //ServiceInterface<LocationService>.Instance.Service.OnDestroy();
            //ServiceInterface<LocationService>.Instance.Stop();
            if (geolocator.IsListening)
                geolocator.StopListening();
        }

        #endregion

        #region IAppIdGenerator Implementation Methods

        public string GetDeviceUniqueId() 
        {
            var serial = "";
            try
            {
                // Android 2.3 and up (API 10)
                serial = Build.Serial;
            }
            catch (Exception)
            {
                // ignored
            }

            var androidId = "";
            try
            {
                // Not 100% reliable on 2.2 (API 8)
                androidId = Settings.Secure.GetString(this.ContentResolver, Settings.Secure.AndroidId);
            }
            catch (Exception)
            {
                // ignored
            }

            return serial + androidId;
        }

        #endregion 



    }
}


