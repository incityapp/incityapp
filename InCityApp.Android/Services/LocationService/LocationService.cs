using System;

using Android.App;
using Android.Util;
using Android.Content;
using Android.OS;
using Android.Gms.Common;
using Android.Gms.Location;

using InCityApp.Entities;
using Android.Locations;
using Android.Gms.Common.Apis;


namespace InCityApp_Android.Services
{
    [Service(Name = "incityApp.locationservice",
             Label = "INCITY LocationService",
             Exported = true)]
    public class LocationService : Service,
                                   Android.Gms.Location.ILocationListener,
                                   IGoogleApiClientConnectionCallbacks,
                                   IGoogleApiClientOnConnectionFailedListener
	{
		public event EventHandler<LocationChangedEventArgs> LocationChanged = delegate { };
		public event EventHandler<ProviderDisabledEventArgs> ProviderDisabled = delegate { };
		public event EventHandler<ProviderEnabledEventArgs> ProviderEnabled = delegate { };
		public event EventHandler<StatusChangedEventArgs> StatusChanged = delegate { };

		public LocationService() 
		{
		}

        protected LocationRequest _locationRequest;
        protected IGoogleApiClient _googleApiClient;

		readonly string _logTag = "LocationService";

		public override void OnCreate ()
		{
			base.OnCreate ();
            Log.Debug(_logTag, "LocationService >> Initializing...");
		}

		public override StartCommandResult OnStartCommand (Intent intent, StartCommandFlags flags, int startId)
		{
			Log.Debug (_logTag, "LocationService >> Started");
			return StartCommandResult.Sticky;
		}

		// This gets called once, the first time any client bind to the Service
		// and returns an instance of the LocationServiceBinder. All future clients will
		// reuse the same instance of the binder
		public override IBinder OnBind (Intent intent)
		{
            Log.Debug(_logTag, "LocationService >> Client bound");

            IBinder binder = new ServiceBinder<LocationService>(this);

            try
            {
                Log.Debug(_logTag, "LocationService >> Connecting to Google Api Client...");

                _googleApiClient = new GoogleApiClientBuilder(this).AddConnectionCallbacks(this).AddOnConnectionFailedListener(this).AddApi(LocationServices.Api).Build();
                _googleApiClient.Connect();

                Log.Debug(_logTag, "LocationService >> Connected to Google Api Client.");
            }
            catch (Exception exc)
            {
                Log.Debug(_logTag, "LocationService >> Cannot open a connection to Google Api Client : " + exc.Message);
            }

            return binder;
		}

        public override void OnRebind(Intent intent)
        {
            base.OnRebind(intent);
            Log.Debug(_logTag, "LocationService >> Client rebinding...");

            try
            {
                Log.Debug(_logTag, "LocationService >> Connecting to Google Api Client...");

                _googleApiClient = new GoogleApiClientBuilder(this).AddConnectionCallbacks(this).AddOnConnectionFailedListener(this).AddApi(LocationServices.Api).Build();
                _googleApiClient.Connect();

                Log.Debug(_logTag, "LocationService >> Connected to Google Api Client.");
            }
            catch (Exception exc)
            {
                Log.Debug(_logTag, "LocationService >> Cannot open a connection to Google Api Client : " + exc.Message);
            }

        }

        public override bool OnUnbind(Intent intent)
        {
            base.OnUnbind(intent);
            return true;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            _googleApiClient.Disconnect();
            Log.Debug(_logTag, "LocationService >> Terminated");
        }

        #region IGooglePlayServicesClientConnectionCallbacks implementation

        public void OnConnected(Bundle connectionHint)
        {
            Log.Debug(_logTag, "LocationService >> Connected to Google Api Client.");
            _locationRequest = LocationRequest.Create();
            _locationRequest.SetPriority(LocationRequest.PriorityBalancedPowerAccuracy); 

            _locationRequest.SetFastestInterval(UserLocator.LocationUpdateTime);
            _locationRequest.SetInterval(UserLocator.LocationUpdateTime);
            _locationRequest.SetSmallestDisplacement(UserLocator.LocationDisplacementThreshold);

            LocationServices.FusedLocationApi.RequestLocationUpdates(_googleApiClient, _locationRequest, this);
            
            Log.Debug(_logTag, "LocationService >> Sending location updates");
        }

        public void OnConnectionSuspended(int cause)
        {
            Log.Debug(_logTag, "LocationService >> Connection to Google Api Client suspended.");
        }

        #endregion

        #region IGooglePlayServicesClientOnConnectionFailedListener implementation

        public void OnConnectionFailed(ConnectionResult result)
        {
            Log.Debug(_logTag, "LocationService >> Failed to connect to Google Api Client. Error Code : " + result.ErrorCode);
        }

        #endregion

		#region ILocationListener implementation

        void Android.Gms.Location.ILocationListener.OnLocationChanged(Location location)
        {
            Log.Debug(_logTag, "OnLocationChanged");
            
            // we route the event through the UserLocator, it will provide an high level interface to the user location data.
            // the shared part of the app code will then use the UserLocator that will be bound to OS dependment event for location data.
            UserLocator.Instance.CurrentLocation = new MapLocation(location.Latitude, location.Longitude);
            if (UserLocator.Instance.CurrentLocation == null)
            {
                Log.Error(_logTag, "Unable to determine your location.");
            }
            else
                Log.Debug(_logTag, string.Format("Coords : {0} ", UserLocator.Instance.CurrentLocation.ToString()));

            LocationChanged(this, new LocationChangedEventArgs(location));

            // This should be updating every time we request new location updates
            // both when teh app is in the background, and in the foreground
            Log.Debug(_logTag, String.Format("Latitude is {0}", location.Latitude));
            Log.Debug(_logTag, String.Format("Longitude is {0}", location.Longitude));
            Log.Debug(_logTag, String.Format("Altitude is {0}", location.Altitude));
            Log.Debug(_logTag, String.Format("Speed is {0}", location.Speed));
            Log.Debug(_logTag, String.Format("Accuracy is {0}", location.Accuracy));
            Log.Debug(_logTag, String.Format("Bearing is {0}", location.Bearing));
        }

        #endregion
    }
}

