using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace InCityApp_Android.Services
{
    public class LocationServiceInterface
    {
		public event EventHandler<ServiceConnectedEventArgs> LocationServiceConnected = delegate {};
		
		protected readonly string _logTag = "LocationServiceInterface";
        private static LocationServiceInterface _instance;
        
        protected ServiceConnection<LocationService> _locationServiceConnection;
		
        public static LocationServiceInterface Instance
		{
			get { return _instance; }
		} 
		
		public LocationService LocationService
		{
			get {
				if (_locationServiceConnection.Binder == null)
					throw new Exception ("Service not bound yet!");
				
				return _locationServiceConnection.Binder.Service;
			}
		}

        static LocationServiceInterface()
		{
            _instance = new LocationServiceInterface();
		}

		protected LocationServiceInterface () 
		{
			// starting a service is blocking, so it's better to do it on a background thread
            new Task(() =>
            {
                Android.App.Application.Context.StartService(new Intent(Android.App.Application.Context, typeof(LocationService)));

                _locationServiceConnection = new ServiceConnection<LocationService>(null);

                _locationServiceConnection.ServiceConnected += (object sender, ServiceConnectedEventArgs e) =>
                {

                    Log.Debug(_logTag, "Service Connected");
                    // we will use this event to notify MainActivity when to start updating the UI
                    LocationServiceConnected(this, e);
                };

                // find the Intent for LocationService
                Intent locationServiceIntent = new Intent(Android.App.Application.Context, typeof(LocationService));
                Log.Debug(_logTag, "Calling service binding");

                // bind the service
                Android.App.Application.Context.BindService(locationServiceIntent, _locationServiceConnection, Bind.AutoCreate);

            }).Start();
		}

    }
}