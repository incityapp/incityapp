using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace InCityApp_Android.Services
{
    public class ServiceInterface<T>
    {
		public event EventHandler<ServiceConnectedEventArgs> ServiceConnected = delegate {};
		
		protected readonly string _logTag = "ServiceInterface";
        private static ServiceInterface<T> _instance;
        
        protected ServiceConnection<T> _serviceConnection;
		
        public static ServiceInterface<T> Instance
		{
			get { return _instance; }
		} 
		
		public T Service
		{
			get {
				if (_serviceConnection.Binder == null)
					throw new Exception ("Service not bound yet!");
				
				return _serviceConnection.Binder.Service;
			}
		}

        static ServiceInterface()
		{
            _instance = new ServiceInterface<T>();
		}

        protected ServiceInterface() 
		{
            _logTag += ":" + typeof(T).ToString();

            Start();
		}

        public void Start()
        {
            // starting a service is blocking, so it's better to do it on a background thread
            new Task(() =>
            {
                Android.App.Application.Context.StartService(new Intent(Android.App.Application.Context, typeof(T)));

                _serviceConnection = new ServiceConnection<T>(null);

                _serviceConnection.ServiceConnected += (object sender, ServiceConnectedEventArgs e) =>
                {
                    Log.Debug(_logTag, "Service Connected");
                    // we will use this event to notify MainActivity 
                    ServiceConnected(this, e);
                };

                // find the Intent for LocationService
                Intent serviceIntent = new Intent(Android.App.Application.Context, typeof(T));
                Log.Debug(_logTag, "Calling service binding");

                // bind the service
                Android.App.Application.Context.BindService(serviceIntent, _serviceConnection, Bind.AutoCreate);

            }).Start();
        }

        public void Stop()
        {
            _serviceConnection.OnServiceDisconnected(new ComponentName("", ""));
            Android.App.Application.Context.UnbindService(_serviceConnection);
        }
    }
}