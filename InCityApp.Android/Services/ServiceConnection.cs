using System;

using Android.Content;
using Android.OS;
using Android.Util;

namespace InCityApp_Android.Services
{
	public class ServiceConnection<T> : Java.Lang.Object, IServiceConnection
	{
		public event EventHandler<ServiceConnectedEventArgs> ServiceConnected = delegate {};
        public event EventHandler ServiceDisconnected = delegate { };

		public ServiceBinder<T> Binder
		{
			get { return _binder; }
			set { _binder = value; }
		}
		protected ServiceBinder<T> _binder;

		public ServiceConnection (ServiceBinder<T> binder)
		{
			if (_binder != null) 
				_binder = binder;
		}
        
		// This gets called when a client tries to bind to the Service with an Intent and an 
		// instance of the ServiceConnection. The system will locate a binder associated with the 
		// running Service 
		public void OnServiceConnected (ComponentName name, IBinder service)
		{
			// cast the binder located by the OS as our local binder subclass
            ServiceBinder<T> serviceBinder = service as ServiceBinder<T>;
			if (serviceBinder != null) 
            {
				_binder = serviceBinder;
				_binder.IsBound = true;
                Log.Debug(string.Format(" {0} Connection", typeof(T).Name), string.Format("{0} >> Connecting...", typeof(T).Name));
				// raise the service connected event
                if (ServiceConnected != null)
				    ServiceConnected(this, new ServiceConnectedEventArgs () { Binder = service } );
			}
		}
			
		// This will be called when the Service unbinds, or when the app crashes ;)
		public void OnServiceDisconnected (ComponentName name)
		{
			this._binder.IsBound = false;
            Log.Debug(string.Format("{0} Connection", typeof(T).Name), string.Format("{0} >> Disconnected.", typeof(T).Name));

            if (ServiceDisconnected != null)
                ServiceDisconnected(this, new EventArgs());
		}
	}
}

