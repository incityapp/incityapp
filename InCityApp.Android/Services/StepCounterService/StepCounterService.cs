using System;
using System.ComponentModel;

using Android.App;
using Android.Hardware;
using Android.Content;
using Android.Support.V4.App;
using Android.Util;
using InCityApp_Android.Helpers;
using Android.OS;
using InCityApp.Models;
using InCityApp.Entities;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;
using InCityApp.DependencyServiceInterfaces;

[assembly: Xamarin.Forms.Dependency(typeof(InCityApp_Android.Services.StepCounterService))]
namespace InCityApp_Android.Services
{
    [Service(Name = "incityapp.stepcounterservice",
            Label = "INCITY StepCounterService",
            Exported = true)]
    [IntentFilter(new String[] { "com.InCityApp.StepCounterService" })]
    public class StepCounterService : Service, ISensorEventListener, INotifyPropertyChanged, IStepCounterService
    {
        public event EventHandler<Int64> OnNewStep;
        public static event EventHandler OnNewStepSession;
        public event EventHandler OnBeginSimulatingSteps;
        public event EventHandler OnEndSimulatingSteps;

        private readonly Object _lock = new Object();
        private bool _isRunning;
        private static Int64 _currentStepSession = 0;
        private Int64 _oldStepSession = 0;
        private Int64 _stepSessionCounter = 0;
        private string _logTag = "StepCounterService";
        private Int64 _newSteps = 0;
        private Int64 _lastSteps = 0;
        private static bool _firstStepSensorScan = true;
        private static bool _insertStepSession;
        private static bool _firstStepSession = true;

        public bool WarningState
        {
            get;
            set;
        }

        public Int64 CurrentStepSession
        {
            get 
            {
                lock (_lock)
                {
                    return _currentStepSession;
                }
            }
            set
            {
                lock (_lock)
                {
                    if (_currentStepSession == value)
                        return;


                    _currentStepSession = value;
                }
            }
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Log.Debug(_logTag, "Start command result called, incoming startup");

            var alarmManager = ((AlarmManager)ApplicationContext.GetSystemService(Context.AlarmService));
            var intent2 = new Intent(this, typeof(StepCounterService));
            intent2.PutExtra("warning", WarningState);
            var stepIntent = PendingIntent.GetService(ApplicationContext, 10, intent2, PendingIntentFlags.UpdateCurrent);

            // Workaround as on Android 4.4.2 START_STICKY has currently no effect 
            // restart service after a while
            // TODO > check if this is solved for KitKat and Jelly Bean, it seems to have been solved in Android >= 5.0.0
            // https://code.google.com/p/android/issues/detail?id=63793
            // https://code.google.com/p/android/issues/detail?id=53313#c7
            alarmManager.Set(AlarmType.RtcWakeup, Java.Lang.JavaSystem.CurrentTimeMillis() + StepCounterSettings.ServiceRestartTime, stepIntent);

            var warning = false;
            if (intent != null)
                warning = intent.GetBooleanExtra("warning", false);
            
            Startup();

            return StartCommandResult.Sticky;
        }

        public override void OnTaskRemoved(Intent rootIntent)
        {            
            UnregisterListeners();
            Log.Debug(_logTag, "Task Removed, going down");

            var intent = new Intent(this, typeof(StepCounterService));
            intent.PutExtra("warning", WarningState);
            
            // restart service in a short time
            ((AlarmManager)GetSystemService(Context.AlarmService)).Set(AlarmType.Rtc, Java.Lang.JavaSystem.CurrentTimeMillis() + StepCounterSettings.OnTaskRemovedRestartTime, PendingIntent.GetService(this, 11, intent, 0));

            base.OnTaskRemoved(rootIntent);
        }

        public void ResetCounter()
        {
            lock (_lock)
            {
                if (_firstStepSession)
                    _insertStepSession = true;
                else
                    _insertStepSession = false;

                SaveSteps(null, null);

                _stepSessionCounter = 0;
                _currentStepSession = 0;

                _firstStepSession = true;
                if (OnNewStepSession != null)
                    OnNewStepSession(this, new EventArgs());
            }
        }

        private async Task Startup(bool warning = false)
        {
            // stepcounter sensor is available from KitKat
            if (!StepCounterUtils.IsKitKatWithStepCounter(PackageManager))
            {
                Log.Warn(_logTag, "StepCounter sensor not available, tryin to simulating steps with LocationService...");

                if (UserLocator.Instance == null)
                {
                    Log.Error(_logTag, "LocationService not available. Cannot simulate sensor. Stopping service...");
                    StopSelf();
                    return;
                }

                UserLocator.Instance.OnAccurateLocationChanged += OnDeviceLocationChanged;
                Log.Info(_logTag, "Simulating steps with LocationService.");
            }
            else
                Log.Info(_logTag, "StepCounter Sensor available.");

            await CheckStepSession(true);

            if (!_isRunning)
            {
                RegisterListeners(warning ? SensorType.StepDetector : SensorType.StepCounter);
                WarningState = warning;
            }

            _isRunning = true;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            
            CheckStepSession();

            UnregisterListeners();
            _isRunning = false;

        }

        void RegisterListeners(SensorType sensorType)
        {
            SensorManager sensorManager = (SensorManager)GetSystemService(Context.SensorService);
            Sensor sensor = sensorManager.GetDefaultSensor(sensorType);
            // TODO > check which SensorDelay is better to use
            sensorManager.RegisterListener(this, sensor, SensorDelay.Ui);
            Log.Debug(_logTag, "Sensor listener registered of type: " + sensorType);
        }

        void UnregisterListeners()
        {
            if (!_isRunning)
                return;

            try
            {
                var sensorManager = (SensorManager)GetSystemService(Context.SensorService);
                sensorManager.UnregisterListener(this);
                Log.Debug(_logTag, "Sensor listener unregistered.");
                _isRunning = false;
            }
            catch (Exception ex)
            {
                Log.Debug(_logTag, "Unable to unregister: " + ex);
            }
        }

        public override Android.OS.IBinder OnBind(Android.Content.Intent intent)
        {
            IBinder binder = new ServiceBinder<StepCounterService>(this);
            return binder;
        }

        public void OnAccuracyChanged(Sensor sensor, SensorStatus accuracy)
        {
            //...
        }

        public async void OnSensorChanged(SensorEvent e)
        {
            Int64 count;
            switch (e.Sensor.Type)
            {
                case SensorType.StepCounter:
                    count = (Int64)e.Values[0];
                    // As reported from the Xamarin lead developer :
                    // In some instances if things are running too long (about 4 days)
                    // the value flips and gets crazy and this will be -1
                    // so switch to step detector instead, but put up warning sign.
                    if (count < 0)
                    {
                        UnregisterListeners();
                        RegisterListeners(SensorType.StepDetector);
                        _isRunning = true;

                        Log.Debug(_logTag, "Something has gone wrong with the step counter, simulating steps.");

                        count = _stepSessionCounter + 3;
                        lock (_lock)
                        {
                            _currentStepSession = count;
                        }

                        WarningState = true;
                        if (OnBeginSimulatingSteps != null)
                            OnBeginSimulatingSteps(this, new EventArgs());
                    }
                    else
                    {
                        if (WarningState)
                        {
                            Log.Debug(_logTag, "Stop simulating steps, StepCounter sensor ready.");
                            if (OnEndSimulatingSteps != null)
                                OnEndSimulatingSteps(this, new EventArgs());
                        }

                        WarningState = false;
                    }

                    if (_firstStepSensorScan || _insertStepSession)
                    {
                        _stepSessionCounter = count;
                       
                        
                        if (OnNewStep != null)
                            OnNewStep(this, 1);
                        
                        lock (_lock)
                        {
                            _currentStepSession = 1;
                            _firstStepSensorScan = false;
                        }
                    }
                    else
                    {
                        long steps = count - _stepSessionCounter;
                        _stepSessionCounter = count;
                        lock (_lock)
                        {
                            _currentStepSession += steps;
                        }

                        if (OnNewStep != null)
                            OnNewStep(this, steps);
                    }
                    break;
                case SensorType.StepDetector:
                    if (_insertStepSession)
                        _stepSessionCounter = 0;
                    _stepSessionCounter += 1;
                    lock (_lock)
                    {
                        _currentStepSession += 1;
                    }
                    if (OnNewStep != null)
                        OnNewStep(this, 1);
                    break;
            }
        }

        private void PopUpNotification(int id, string title, string message)
        {
            Notification.Builder mBuilder =
                new Notification.Builder(this)
                    //.SetSmallIcon(Resource.Drawable.) TODO > prepare an icon
                    .SetContentTitle(title)
                    .SetContentText(message)
                    .SetAutoCancel(true);
            // Creates an explicit intent for an Activity into the app
            Intent resultIntent = new Intent(this, typeof(MainActivity));
            resultIntent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
            // The stack builder object will contain an artificial back stack for the
            // started Activity.
            // This ensures that navigating backward from the Activity leads out of
            // the application to the Home screen.
            var stackBuilder = Android.App.TaskStackBuilder.Create(this);
            // Adds the back stack for the Intent (but not the Intent itself)
            //stackBuilder.AddParentStack();
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.AddNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);
            mBuilder.SetContentIntent(resultPendingIntent);

            NotificationManager mNotificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            // the id allows us to update the notification later on
            mNotificationManager.Notify(id, mBuilder.Build());
        }

        private async Task CheckStepSession(bool startup = false)
        {
            if (startup)
            {
                System.Timers.Timer timer = new System.Timers.Timer();
                timer.Interval = StepCounterSettings.SaveStepsTime;
                timer.Elapsed += SaveSteps;
                timer.Enabled = true;
                return;
            }
        }

        private async void SaveSteps(object sender, System.Timers.ElapsedEventArgs e)
        {
            StepSession dayEntry = new StepSession();
            lock (_lock)
            {
                if (_currentStepSession == 0)
                    return;

                dayEntry.Date = DateTime.Now.ToUniversalTime();
                dayEntry.Steps = CurrentStepSession;
            }

            try
            {
                if (_firstStepSession)
                {
                    await StepsModel.Save(dayEntry, true);
                    lock (_lock)
                    {
                        _firstStepSession = false;
                    }
                }
                else
                    await StepsModel.Save(dayEntry, _insertStepSession);

                lock (_lock)
                {
                    _insertStepSession = false;
                }
            }
            catch (Exception exc)
            {
                Log.Error(_logTag, "Something horrible has gone wrong attempting to save database entry, it is lost forever : " + exc.Message);
            }
        }

        private void OnDeviceLocationChanged(ILocatable location)
        {
            lock (_lock)
            {
                try
                {
                    Log.Debug(_logTag, "OnDeviceLocationChanged");
                    ILocatable oldLocation = UserLocator.Instance.OldLocation;
                    if (oldLocation == null)
                    {
                        Log.Debug(_logTag, "Old Location is invalid, use the current one.");
                        oldLocation = new MapLocation(location.Latitude, location.Longitude);
                    }

                    double distance = oldLocation.DistanceFrom(location);
                    Log.Debug(_logTag, "Old Location distance from current location (in meters): " + distance);
                    if (distance >= 1)
                    {
                        long count = _stepSessionCounter;

                        if (distance >= 1 && distance <= 2)
                            count += 3;
                        if (distance >= 2)
                            count += (long)(distance / 2 * 3);

                        if (_firstStepSession)
                        {
                            count = 0;
                            _stepSessionCounter = 0;
                        }

                        long newSteps = count - _stepSessionCounter;

                        newSteps = newSteps == 0 ? 1 : newSteps;
                        Log.Debug(_logTag, "Steps : " + newSteps);

                        if (OnNewStep != null)
                            OnNewStep(this, newSteps);

                        _currentStepSession += newSteps;
                        _stepSessionCounter = count;
                    }
                }
                catch (Exception exc)
                {
                    Log.Debug(_logTag, "OnDeviceLocationChanged Exception : " + exc.Message);
                }
            }
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged == null)
                return;

            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}