using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using InCityApp_Android.Helpers;

namespace InCityApp_Android.Services
{
    /// <summary>
    /// Step Counter Settings.
    /// </summary>
    public static class StepCounterSettings
    {
        #region Setting Constants

        //public static readonly int DailyStepGoalDefault = 10000;
        private const string _weightKey = "Weight";
        private static readonly int _weightDefault = 0;
        private const string _cadenceKey = "Cadence3";
        private static readonly string _cadenceDefault = "3";        
        private const string _enhancedKey = "Enhanced";
        private static readonly bool _enhancedDefault = false;
        private const string _progressNotificationsKey = "ProgressNotifications";
        private static readonly bool _progressNotificationsDefault = true;
        private const string _accumulativeNotificationsKey = "AccumulativeNotifications";
        private static readonly bool _accumulativeNotificationsDefault = true;
        private const string _currentDayKey = "CurrentDay";
        private static readonly DateTime _currentDayDefault = DateTime.Today;
        private const string _currentStepsKey = "CurrentSteps";
        private static readonly Int64 _currentDayStepsDefault = 0;
        private const string _stepsBeforeTodayKey = "StepsBeforeToday";
        private static readonly Int64 _stepsBeforeTodayDefault = 0;
        private const string _totalStepsKey = "TotalSteps";
        private static readonly Int64 _totalStepsDefault = 0;
        private const string _goalTodayMessageKey = "GoalTodayMessage";
        private static readonly string _goalTodayMessageDefault = string.Empty;
        private const string _goalTodayDayKey = "GoalTodayDay";
        private static readonly DateTime _goalTodayDayDefault = DateTime.Today.AddDays(-1);
        private const string _nextGoalKey = "NextGoal";
        private static readonly Int64 _nextGoalDefault = 100000;
        private const string _highScoreKey = "HighScore";
        private static readonly Int64 _highScoreDefault = 0;
        private const string _highScoreDayKey = "HighScoreDay";
        private static readonly DateTime _highScoreDayDefault = DateTime.Today;
        private const string _firstDayOfUseKey = "FirstDayOfUse";
        private static readonly DateTime _firstDayOfUseDefault = DateTime.Today;
        private const string _serviceRestartTimeKey = "ServiceRestartTime";
        private static readonly long _serviceRestartTimeDefault = 1000 * 60 * 60; // 1 hour in milliseconds
        private const string _onTaskRemovedRestartTimeKey = "OnTaskRemovedRestartTime";
        private static readonly long _onTaskRemovedRestartTimeDefault = 500; // milliseconds
        private static readonly long _saveStepsTime = 1000 * 60 * 1; // 15 minutes
        #endregion

        /// <summary>
        /// Gets or sets the next goal. for total
        /// </summary>
        /// <value>The next goal.</value>
        public static Int64 NextGoal
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_nextGoalKey, _nextGoalDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_nextGoalKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show progress notifications.
        /// </summary>
        /// <value><c>true</c> if progress notifications; otherwise, <c>false</c>.</value>
        public static bool ProgressNotifications
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_progressNotificationsKey, _progressNotificationsDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_progressNotificationsKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show accumulative notifications.
        /// </summary>
        /// <value><c>true</c> if accumulative notifications; otherwise, <c>false</c>.</value>
        public static bool AccumulativeNotifications
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_accumulativeNotificationsKey, _accumulativeNotificationsDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_accumulativeNotificationsKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the high score day.
        /// </summary>
        /// <value>The high score day.</value>
        public static DateTime FirstDayOfUse
        {
            get
            {
                var firstDay = AppPrefs.Instance.GetValueOrDefault(_firstDayOfUseKey, (long)-1);
                if (firstDay == -1)
                {
                    FirstDayOfUse = DateTime.Today;
                    CurrentDay = DateTime.Today;
                    return DateTime.Today;
                }
                else
                    return new DateTime(firstDay);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_firstDayOfUseKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Ensure that high score is not today
        /// </summary>
        /// <value><c>true</c> if today is high score; otherwise, <c>false</c>.</value>
        public static bool TodayIsHighScore
        {
            get
            {
                //if first day then always return false;
                if (FirstDayOfUse.DayOfYear == HighScoreDay.DayOfYear && FirstDayOfUse.Year == HighScoreDay.Year)
                    return false;

                //else is same day.
                return DateTime.Today.DayOfYear == HighScoreDay.DayOfYear && DateTime.Today.Year == HighScoreDay.Year;
            }
        }

        /// <summary>
        /// Gets or sets the goal message to display to user
        /// </summary>
        /// <value>The goal today message.</value>
        public static string GoalTodayMessage
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_goalTodayMessageKey, _goalTodayMessageDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_goalTodayMessageKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the high score.
        /// </summary>
        /// <value>The high score.</value>
        public static Int64 HighScore
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_highScoreKey, _highScoreDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_highScoreKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the goal today day.
        /// Only display messages if it is currenlty the same day.
        /// </summary>
        /// <value>The goal today day.</value>
        public static DateTime GoalTodayDay
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_goalTodayDayKey, _goalTodayDayDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_goalTodayDayKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the high score day.
        /// </summary>
        /// <value>The high score day.</value>
        public static DateTime HighScoreDay
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_highScoreDayKey, _highScoreDayDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_highScoreDayKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the day we are currently tracking
        /// </summary>
        /// <value>The current day.</value>
        public static DateTime CurrentDay
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_currentDayKey, _currentDayDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_currentDayKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the steps before today.
        /// </summary>
        /// <value>The steps before today.</value>
        public static Int64 StepsBeforeToday
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_stepsBeforeTodayKey, _stepsBeforeTodayDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_stepsBeforeTodayKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the cadence. (pace of walking)
        /// </summary>
        /// <value>The cadence.</value>
        public static string Cadence
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_cadenceKey, _cadenceDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_cadenceKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the current steps for this session, from the last saved session.
        /// </summary>
        /// <value>The current day steps.</value>
        public static Int64 CurrentStepSession
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_currentStepsKey, _currentDayStepsDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_currentStepsKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the total steps since the beginning of tracking
        /// </summary>
        /// <value>The total steps.</value>
        public static Int64 TotalSteps
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_totalStepsKey, _totalStepsDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_totalStepsKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the weight. (used for calulations)
        /// </summary>
        /// <value>The weight.</value>
        public static int Weight
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_weightKey, _weightDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_weightKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we want to use enhanced tracking
        /// </summary>
        /// <value><c>true</c> if enhanced; otherwise, <c>false</c>.</value>
        public static bool Enhanced
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_enhancedKey, _enhancedDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_enhancedKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets a value indicating whether to use kilometeres.
        /// </summary>
        /// <value><c>true</c> if use kilometeres; otherwise, <c>false</c>.</value>
        public static bool UseKilometeres
        {
            get
            {
                return CultureInfo.CurrentCulture.Name != "en-US";
            }
        }

        /// <summary>
        /// Gets or sets the scheduled time to restart the service
        /// </summary>
        public static long ServiceRestartTime
        {
            get
            {
                return AppPrefs.Instance.GetValueOrDefault(_serviceRestartTimeKey, _serviceRestartTimeDefault);
            }
            set
            {
                if (AppPrefs.Instance.AddOrUpdateValue(_serviceRestartTimeKey, value))
                    AppPrefs.Instance.Save();
            }
        }

        /// <summary>
        /// Gets or sets the scheduled time to restart the service when the task is forcely removed
        /// </summary>
        public static long OnTaskRemovedRestartTime
        {
            get
            {
                return _onTaskRemovedRestartTimeDefault;
            }

        }

        public static long SaveStepsTime
        {
            get { return _saveStepsTime; }
        }

    }
}