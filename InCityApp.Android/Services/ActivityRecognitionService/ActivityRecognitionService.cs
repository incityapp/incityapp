using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Locations;

using Android.Gms.Location;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Preferences;
using InCityApp.Entities;

namespace InCityApp_Android.Services
{
    [Service(Name = "incityapp.activityrecognitionservice",
             Label = "INCITY Activity Recognition Service",
             Exported = true)]
    public class ActivityRecognitionService : Service, IGoogleApiClientConnectionCallbacks, IGoogleApiClientOnConnectionFailedListener
    {
        public event Action<int, int> OnActivityChanged;

        // Confidence values stoled from the web        
        private const int ExtremeConfidence = 85;
        private const int StrongConfidence = 75;
        private const int NormalConfidence = 50;
        private const int WeakConfidence = 25;

        private const int TrackingTime = 10000; // In milliseconds

        private static PendingIntent _callbackIntent;
        private static IGoogleApiClient _googleClient;
        private static int _lastActivityType;

        private ServiceHandler _serviceHandler;
        private Looper _serviceLooper;

        public override void OnCreate()
        {
            base.OnCreate();

            var thread = new HandlerThread("IntentService[ActivityRecognitionService]");
            thread.Start();
            _serviceLooper = thread.Looper;
            _serviceHandler = new ServiceHandler(this, _serviceLooper);
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            var msg = _serviceHandler.ObtainMessage();
            msg.Arg1 = startId;
            msg.Obj = intent;
            _serviceHandler.SendMessage(msg);

            return StartCommandResult.NotSticky;
        }

        public override void OnDestroy()
        {
            _serviceLooper.Quit();
        }

        public override IBinder OnBind(Intent intent)
        {
            Log.Debug("ActivityRecognitionService", "ActivityRecognitionService >> Client bound");

            IBinder binder = new ServiceBinder<ActivityRecognitionService>(this);

            try
            {
                Log.Debug("ActivityRecognitionService", "ActivityRecognitionService >> Connecting to Google Api Client...");

                _googleClient = new GoogleApiClientBuilder(this).AddConnectionCallbacks(this).AddOnConnectionFailedListener(this).AddApi(ActivityRecognition.Api).Build();
                _googleClient.Connect();

                Log.Debug("ActivityRecognitionService", "ActivityRecognitionService >> Connected to Google Api Client.");
            }
            catch (Exception exc)
            {
                Log.Debug("ActivityRecognitionService", "ActivityRecognitionService >> Cannot open a connection to Google Api Client : " + exc.Message);
            }

            return binder;
        }

        public void OnConnected(Bundle connectionHint)
        {
            if (_callbackIntent == null)
            {
                var intent = new Intent(this, typeof(ActivityRecognitionService));
                _callbackIntent = PendingIntent.GetService(this, 0, intent, PendingIntentFlags.UpdateCurrent);

                ActivityRecognition.ActivityRecognitionApi.RequestActivityUpdates(_googleClient, TrackingTime, _callbackIntent);
            }
        }

        void HandleActivityRecognition(Intent intent)
        {
            var result = ActivityRecognitionResult.ExtractResult(intent);
            if (result == null)
                return;

            var activity = result.MostProbableActivity;

            if (activity.Type == DetectedActivity.Tilting)
                return;

            int activityType = (int)activity.Type;

            // The service mistakenly thinks fast moving is a vehicle activity 
            //if (activity.Type == DetectedActivity.InVehicle && activity.Confidence >= ExtremeConfidence)
            //    activityType = (int)DetectedActivity.InVehicle;
            //else if (activity.Type == DetectedActivity.InVehicle && activity.Confidence < ExtremeConfidence)
            //    activityType = (int)DetectedActivity.OnBicycle;
            //else if (activity.Type == DetectedActivity.InVehicle && activity.Confidence < NormalConfidence)
            //    activityType = (int)DetectedActivity.Running;
            //else if (activity.Type == DetectedActivity.OnBicycle && (activity.Confidence < NormalConfidence))
            //    activityType = (int)DetectedActivity.Running;
            //else if ((_lastActivityType == (int)DetectedActivity.Walking || _lastActivityType == (int)DetectedActivity.Running) && activity.Confidence < WeakConfidence)
            //    activityType = _lastActivityType;
            //else if (activity.Type == DetectedActivity.Unknown && (activity.Confidence <= NormalConfidence))
            //    activityType = _lastActivityType;

            switch (activityType)
            {
                case DetectedActivity.Walking:
                    if (OnActivityChanged != null)
                        OnActivityChanged(0, activity.Confidence);
                    break;
                case DetectedActivity.OnFoot:
                    if (OnActivityChanged != null)
                        OnActivityChanged(1, activity.Confidence);
                    break;
                case DetectedActivity.Running:
                    if (OnActivityChanged != null)
                        OnActivityChanged(1, activity.Confidence);
                    break;
                case DetectedActivity.OnBicycle:
                    if (OnActivityChanged != null)
                        OnActivityChanged(2, activity.Confidence);
                    break;
                case DetectedActivity.InVehicle:
                    if (OnActivityChanged != null)
                        OnActivityChanged(3, activity.Confidence);
                    break;
                case DetectedActivity.Still:
                    if (OnActivityChanged != null)
                        OnActivityChanged(4, activity.Confidence);
                    break;
            }

            _lastActivityType = activityType;
            Log.Debug("ActivityRecognitionService", "ActivityRecognitionService >> Activity ({0} w/ {1}%)", activity.Type, activity.Confidence);
        }

        IGoogleApiClient CreateGoogleClient()
        {
            return new GoogleApiClientBuilder(this, this, this).AddApi(ActivityRecognition.Api).Build();
        }

        public void OnConnectionFailed(ConnectionResult connectionResult)
        {
            Log.Error("ActivityRecognitionService", "ActivityRecognitionService >> Connection error, {0}", connectionResult.ErrorCode);
        }

        public void OnConnectionSuspended(int cause)
        {
            Log.Warn("ActivityRecognitionService", "ActivityRecognitionService >> Connection suspended for reason {0}", cause);
        }

        class ServiceHandler : Handler
        {
            ActivityRecognitionService svc;

            public ServiceHandler(ActivityRecognitionService svc, Looper looper)
                : base(looper)
            {
                this.svc = svc;
            }

            public override void HandleMessage(Message msg)
            {
                svc.HandleActivityRecognition((Intent)msg.Obj);
            }
        }
    }
}