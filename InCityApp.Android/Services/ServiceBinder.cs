using System;
using Android.OS;

namespace InCityApp_Android.Services
{
	public class ServiceBinder<T> : Binder
	{
        protected T _service;

		public T Service
		{
			get { return _service; }
		} 

		public bool IsBound { get; set; }

		public ServiceBinder (T service)
		{
			_service = service;
		}
	}
}

