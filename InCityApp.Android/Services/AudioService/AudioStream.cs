using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using System.Threading.Tasks;
using System.Threading;
using InCityApp.Entities;
using Android.Util;

namespace InCityApp_Android.Services
{
    public class AudioStream : IAudioStream
    {
        public event EventHandler<EventArgs<byte[]>> OnBroadcast;
        //public event EventHandler<EventArgs<bool>> OnActiveChanged;
        //public event EventHandler<EventArgs<Exception>> OnException;

        public static AudioSource DefaultDevice = AudioSource.Mic;
        public static int MinSampleRate = 8000;

        private AudioRecord _audioSource;
        private int _bufferSize;

        public int SampleRate
        {
            get
            {
                return _audioSource.SampleRate;
            }
        }

        public int BitsPerSample
        {
            get
            {
                return (_audioSource.AudioFormat == Android.Media.Encoding.Pcm16bit) ? 16 : 8;
            }
        }
  
        public int ChannelCount
        {
            get
            {
                return _audioSource.ChannelCount;
            }
        }

        /// <summary>
        /// Gets the average data transfer rate
        /// </summary>
        /// <value>The average data transfer rate in bytes per second.</value>
        public int AverageBytesPerSecond
        {
            get
            {
                return SampleRate * this.BitsPerSample / 8 * this.ChannelCount;
            }
        }

        public bool Active
        {
            get
            {
                if (_audioSource == null)
                    return false;

                return (_audioSource.RecordingState == RecordState.Recording);
            }
        }

        public bool Start(int sampleRate, int bufferSize)
        {
            Android.OS.Process.SetThreadPriority(Android.OS.ThreadPriority.UrgentAudio);

            if (Active)
                return Active;

            InitializeAudioSource(sampleRate, bufferSize);

            _audioSource.StartRecording();

            ReadRecordedStream();

            return Active;
        }

        public void Stop()
        {
            try
            {
                if (_audioSource.State != State.Uninitialized)
                {
                    _audioSource.Stop();
                    _audioSource.Release();
                }
            }
            catch (Java.Lang.IllegalStateException e)
            {
                Log.Debug("InCityApp_Android.Services.AudioStream", "Exception: " + e.ToString());
            }
        }

        /// <summary>
        /// Initializes the audio source with a given sample rate and the max size for the buffer in bytes.
        /// </summary>
        /// <param name="sampleRate">Sample rate.</param>
        /// <param name="bufferSize">Buffer size in bytes.</param>
        private void InitializeAudioSource(int sampleRate, int bufferSize)
        {
            _bufferSize = bufferSize;
            _audioSource = new AudioRecord( AudioStream.DefaultDevice,
                                            sampleRate,
                                            ChannelIn.Mono,
                                            Android.Media.Encoding.Pcm16bit,
                                            _bufferSize);
        }

        private void ReadRecordedStream()
        {
            var buffer = new byte[this._bufferSize];
            int nBytes = 0;
            try
            {
                _audioSource.ReadAsync(buffer, 0, _bufferSize).ContinueWith((Task<int> bytesRead) =>
                {
                    nBytes = bytesRead.Result;
                    // TODO > handle this case in the proper way
                    // something gone wrong! 
                    if (nBytes < 0)
                        Console.WriteLine("AudioStream >> Error reading buffer. Bytes read = " + nBytes.ToString());

                    if (OnBroadcast != null)
                        OnBroadcast(this, new EventArgs<byte[]>(buffer));

                    if (Active)
                    {
                        Thread.Sleep(1000);
                        ReadRecordedStream();
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("AudioStream >> Error reading buffer : ", e.Message);
            }
        }

        /// <summary>
        /// Reads what's recorded and broadcast the buffer.
        /// </summary>
        //private async void ReadRecordedStream()
        //{
        //    var buffer = new byte[this._bufferSize];
        //    int nBytes = 0;
        //    try
        //    {
        //        await _audioSource.ReadAsync(buffer, 0, _bufferSize).ContinueWith((Task<int> bytesRead) =>
        //        {
        //            nBytes = bytesRead.Result;
        //            // TODO > handle this case in the proper way
        //            // something gone wrong! 
        //            if (nBytes < 0) 
        //                Console.WriteLine("AudioStream >> Error reading buffer. Bytes read = " + nBytes.ToString());
                    
        //            if (OnBroadcast != null)
        //                OnBroadcast(this, new EventArgs<byte[]>(buffer));

        //            if (Active)
        //                ReadRecordedStream();
        //        });
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("AudioStream >> Error reading buffer : ", e.Message);
        //    }
        //}
    }
}