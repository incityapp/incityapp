﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Util;
using Android.Content;
using Android.Media;
using Android.Net;
using Android.Net.Wifi;
using Android.OS;
using Android.Graphics;
using System.Threading.Tasks;

namespace InCityApp_Android.Services
{
    [Service(Name = "incityapp.audioservice",
            Label = "INCITY AudioService",
            Exported = true)]
    [IntentFilter(new[] { ActionRecord, ActionStop })]
    public class AudioService : Service, AudioManager.IOnAudioFocusChangeListener
    {
        public event EventHandler<double> OnOneSecondRecorded;
        public event EventHandler<double> OnFiveSecondsRecorded;

        //Actions
        public const string ActionRecord = "com.xamarin.action.RECORD";
        public const string ActionStop = "com.xamarin.action.STOP";

        private AudioStream _audioStream;
        private AudioManager _audioManager;
        private WifiManager _wifiManager;
        private WifiManager.WifiLock _wifiLock;
        private bool _recording;
        private double _lastRecorderDBVolume = 0;

        readonly string _logTag = "AudioService";
        readonly int _bufferSize = 8000;

        static int _eventsCounter = 0;
        
        // LOOKUPTABLE  
		// conversion table between normalizes values of samples and decibel
		private float [,] lookUpTable = new float[,]
		{
			{0      , 0},
			{0.003f , 50.0f},
			{0.0046f, 55.0f},
			{0.009f , 60.0f},
			{0.016f , 65.0f},
			{0.031f , 70.0f},
			{0.052f , 75.0f},
			{0.085f , 80.0f},
			{0.15f  , 85.0f},
			{0.25f  , 90.0f},
			{0.5f   , 95.0f},
			{0.8f   , 100.0f},
			{0.9f   , 110.0f},
			{1.0f   , 120.0f}
		}; 	

        #region Public Methods

        /// <summary>
        /// On create detect some of our managers
        /// </summary>
        public override void OnCreate()
        {
            base.OnCreate();
            //Find our audio and notificaton managers
            _audioManager = (AudioManager)GetSystemService(AudioService);
            _wifiManager = (WifiManager)GetSystemService(WifiService);
        }
      
        // This gets called once, the first time any client bind to the Service
        // and returns an instance of the ServiceBinder. All future clients will
        // reuse the same instance of the binder
        public override IBinder OnBind(Intent intent)
        {
            Log.Debug(_logTag, "AudioService >> Client bound");

            IBinder binder = new ServiceBinder<AudioService>(this);
            Record();
            return binder;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            try
            {
                if (intent != null)
                {
                    switch (intent.Action)
                    {
                        case ActionRecord: Record(); break;
                        // case ActionStop: Stop(); break;
                    }
                }
            }
            catch (Exception exc)
            {
                Log.Debug(_logTag, "AudioService >> Error starting service : " + exc.Message);
            }
            // set sticky as we are a long running operation
            return StartCommandResult.Sticky;
        }

        public override void OnRebind(Intent intent)
        {
            base.OnRebind(intent);
            Record();
        }

        public override bool OnUnbind(Intent intent)
        {
            base.OnUnbind(intent);
            return true;
        }

        public override void OnDestroy()
        {
            if (_audioStream != null)
                Stop();

            base.OnDestroy();
        }

        /// <summary>
        /// For a good user experience we should account for when audio focus has changed.
        /// There is only 1 audio output there may be several media services trying to use it so
        /// we should act correctly based on this.  "duck" to be quiet and when we gain go full.
        /// All applications are encouraged to follow this, but are not enforced.
        /// </summary>
        /// <param name="focusChange"></param>
        public void OnAudioFocusChange(AudioFocus focusChange)
        {
            switch (focusChange)
            {
                case AudioFocus.Gain:
                    break;
                case AudioFocus.Loss:
                    break;
                case AudioFocus.LossTransient:
                    break;
                case AudioFocus.LossTransientCanDuck:
                    break;
            }
        }

        #endregion

        #region Private & Protected Methods

        private void Record()
        {
            if (_audioStream == null)
                _audioStream = new AudioStream();

            AquireWifiLock();

            _audioStream.OnBroadcast += AudioStream_OnBroadcast;
            _audioStream.Start(AudioStream.MinSampleRate, _bufferSize);
        }

        private void AudioStream_OnBroadcast(object sender, EventArgs<byte[]> e)
        {
            double db = 0;

            try
            {
                db = MeasureDecibels(e.Value, _bufferSize / 20, 16, 1, new int[] { 0 });

                if (OnOneSecondRecorded != null)
                    OnOneSecondRecorded(this, db);

                if (_eventsCounter >= 5)
                {
                    _eventsCounter = 0;

                    if (OnFiveSecondsRecorded != null)
                        OnFiveSecondsRecorded(this, db);
                }

                _eventsCounter++;
            }
            catch (Exception exc)
            {
                Console.WriteLine("AudioService >> Error measuring decibels : " + exc.Message);
            }
        }

        private void Stop()
        {
            if (_audioStream != null)
                _audioStream.Stop();
            ReleaseWifiLock();
        }

        /// <summary>
        /// Lock the wifi so we can still stream under lock screen
        /// </summary>
        private void AquireWifiLock()
        {
            if (_wifiLock == null)
                _wifiLock = _wifiManager.CreateWifiLock(WifiMode.Full, "incityapp_wifi_lock");
            
            if (_wifiLock != null)
                _wifiLock.Acquire();
        }

        /// <summary>
        /// This will release the wifi lock if it is no longer needed
        /// </summary>
        private void ReleaseWifiLock()
        {
            if (_wifiLock == null)
                return;

            _wifiLock.Release();
            _wifiLock = null;
        }
        
        // INTERPOLATE
		// Dato un valore X, restituisce il corrispondente in DB
		public float Interpolate(float x)
		{
			// se minore di 0, restituisce 0
			if ( x <= lookUpTable[0, 0])
			{
				return lookUpTable[0, 1];
			}
			
			// scorre la tabella
			for (int i = 0; i < lookUpTable.Length - 1; i++)
			{
				// ricava i valori di indice i e i+1
				float x0 = lookUpTable[i, 0];
				float y0 = lookUpTable[i, 1];
				float x1 = lookUpTable[i + 1, 0];
				float y1 = lookUpTable[i + 1, 1];
				
				// controlla la X, se minore del valore di indice i+1, ho trovato la cella corrispondente
				if ( x <= x1 )
				{
					// calcolo il valore in DB corrispondente, mediante interpolazione
					float y = ((y1 - y0) / (x1 - x0)) * (x - x0) + y0;
					return y;
				}
			}
			
			// se nn ho trovato nulla, restituisco il massimo valore in DB
			return lookUpTable[lookUpTable.Length - 1, 1];
		}	

        private double MeasureDecibels(byte[] samples, int length, int bitsPerSample, int numChannels, params int[] channelsToMeasure)
        {
            if (samples == null || length == 0 || samples.Length == 0)
            {
                throw new ArgumentException("Missing samples to measure.");
            }
            // check bits are 8 or 16.
            if (bitsPerSample != 8 && bitsPerSample != 16)
            {
                throw new ArgumentException("Only 8 and 16 bit samples allowed.");
            }
            // check channels are valid
            if (channelsToMeasure == null || channelsToMeasure.Length == 0)
            {
                throw new ArgumentException("Must have target channels.");
            }
            // check each channel is in proper range.
            foreach (int channel in channelsToMeasure)
            {
                if (channel < 0 || channel >= numChannels)
                {
                    throw new ArgumentException("Invalid channel requested.");
                }
            }

            // ensure we have only full blocks. A half a block isn't considered valid.
            int sampleSizeInBytes = bitsPerSample / 8;
            int blockSizeInBytes = sampleSizeInBytes * numChannels;
            if (length % blockSizeInBytes != 0)
            {
                throw new ArgumentException("Non-integral number of bytes passed for given audio format.");
            }

            double sum = 0;
            for (var i = 0; i < length; i = i + blockSizeInBytes)
            {
                double sumOfChannels = 0;
                for (int j = 0; j < channelsToMeasure.Length; j++)
                {
                    int channelOffset = channelsToMeasure[j] * sampleSizeInBytes;
                    int channelIndex = i + channelOffset;
                    if (bitsPerSample == 8)
                    {
                        sumOfChannels = (127 - samples[channelIndex]) / byte.MaxValue;
                    }
                    else
                    {
                        double sampleValue = BitConverter.ToInt16(samples, channelIndex);
                        sumOfChannels += (sampleValue / short.MaxValue);
                    }
                }
                double averageOfChannels = sumOfChannels / channelsToMeasure.Length;
                sum += (averageOfChannels * averageOfChannels);
            }

            int numberSamples = length / blockSizeInBytes;
            double rootMeanSquared = Math.Sqrt(sum / numberSamples);
            
            /*if (rootMeanSquared == 0)
            {
                return 0;
            }
            else
            {
                double logvalue = Math.Log10(rootMeanSquared);
                double decibel = 20 * logvalue;
                return decibel;
            }*/
            
            return Interpolate((float)rootMeanSquared);
        }

        #endregion
    }
    
} // end namespace