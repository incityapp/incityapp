using System;
using Android.OS;

namespace InCityApp_Android.Services
{
	public class ServiceConnectedEventArgs : EventArgs
	{
		public IBinder Binder { get; set; }
	}
}