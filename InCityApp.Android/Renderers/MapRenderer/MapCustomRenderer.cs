﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Maps.Android;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Util;

using Java.Net;
using Java.IO;
using Java.Lang;

using Org.Json;

using InCityApp.Shared.Pages;
using InCityApp.Views;
using InCityApp.Entities;
using InCityApp.DataModels;
using InCityApp.DependencyServiceInterfaces;
using InCityApp;
using InCityApp.DataInterfaces;
using InCityApp.Pages;
using System.Collections.Concurrent;

[assembly: ExportRenderer(typeof(InCityApp.Views.CustomMapView), typeof(InCityApp_Android.Renderers.CustomMapRenderer_Android))]
namespace InCityApp_Android.Renderers
{
    /// <summary>
    /// Render the MapPage using platform-specific Android.Views controls
    /// </summary>
    public class CustomMapRenderer_Android : MapRenderer, IOnMapReadyCallback
    {
        public class CustomInfoWindowAdapter : Java.Lang.Object, GoogleMap.IInfoWindowAdapter
        {
            private LayoutInflater _layoutInflater = null;

            public CustomInfoWindowAdapter(Context context)
            {
                _layoutInflater = LayoutInflater.From(context);
            }

            //public CustomInfoWindowAdapter(IntPtr javaReference, JniHandleOwnership jniHandleOwnership)
            //    : base(javaReference, jniHandleOwnership)
            //{
            //}

            public Android.Views.View GetInfoWindow(Marker marker)
            {
                return null;
            }

            public Android.Views.View GetInfoContents(Marker marker)
            {
                var customPopup = _layoutInflater.Inflate(Resource.Layout.MarkerModal, null);

                var titleTextView = customPopup.FindViewById<TextView>(Resource.Id.InfoWindowTitle);
                if (titleTextView != null)
                {
                    titleTextView.Gravity = GravityFlags.CenterHorizontal;
                    titleTextView.Text = "Apri \n\n" + marker.Title;
                }

                var snippetTextView = customPopup.FindViewById<TextView>(Resource.Id.InfoWindowDescription);
                if (snippetTextView != null)
                {
                    snippetTextView.Text = marker.Snippet;
                }

                return customPopup;
            }
        }

        static object _lock = new object();
        private static bool _annotationDialogIsOpening;
        private static GoogleMap _map;
        private static List<RoutePosition> _route = new List<RoutePosition>();

        //private static bool _online = true;

        private MapView _nativeMapControl_Android;
        private CustomMapView _sharedMapView;

        private Marker _currentAnnotationMarker = null;

        //public CustomMapRenderer_Android(IntPtr javaReference, JniHandleOwnership jniHandleOwnership)
        //{
        //}

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                _sharedMapView = (CustomMapView)Element;
                _sharedMapView.OnAnnotationCreated += HandleOnAnnotationCreated;
                _sharedMapView.OnAnnotationModified += HandleOnAnnotationModified;
                _sharedMapView.OnAnnotationDismissed += HandleOnAnnotationDismissed;
                _sharedMapView.OnCreateAnnotationAborted += HandleOnCreateAnnotationAborted;
                _sharedMapView.OnUpdateRender += HandleOnUpdated;

                _nativeMapControl_Android = (MapView)Control;
                _nativeMapControl_Android.GetMapAsync(this);
            }

            /*MessagingCenter.Subscribe<BaseDataInterface>(this, "OnConnectionLost", (sender) =>
            {
                lock (_lock)
                {
                    _online = false;
                }
            });

            MessagingCenter.Subscribe<ConnectionLostPage>(this, "OnConnectionAvailable", (sender) =>
            {
                lock (_lock)
                {
                    _online = true;
                }
            });*/
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            if (_nativeMapControl_Android != null && googleMap != null)
            {
                _map = googleMap;
                _map.SetIndoorEnabled(true);
                _map.SetInfoWindowAdapter(new CustomInfoWindowAdapter(this.Context));
                _map.InfoWindowClick += HandleInfoWindowClick;
                _map.MapLongClick += HandleMapLongClick;
                _map.MarkerClick += HandleMarkerClick;

                _map.MyLocationEnabled = true;
            }
        }

        private void Update()
        {
            _map.Clear();

            UpdatePins();
            
            UpdateUserArea();

            UpdateRoute();
        }

        private void UpdateUserArea()
        {
            if (UserLocator.Instance.CurrentLocation == null)
                return;

            var androidMapView = (MapView)Control;

            CircleOptions circleOptions = new CircleOptions();
            circleOptions.InvokeStrokeWidth(2f);
            circleOptions.InvokeStrokeColor(Color.FromHex("52B296").ToAndroid().ToArgb()); 
            circleOptions.InvokeCenter(new LatLng(UserLocator.Instance.CurrentLocation.Latitude, UserLocator.Instance.CurrentLocation.Longitude));
            circleOptions.InvokeRadius(200);
            _map.AddCircle(circleOptions);
        }

        private void UpdatePins()
        {
            if (_sharedMapView == null || _nativeMapControl_Android == null)
                return;

            BitmapDescriptor pinIcon = null;

            foreach (var item in _sharedMapView.Items)
            {
                if (item.InUserArea)
                {
                    if (item.CreatedFromCurrentUser)
                    {
                        pinIcon = BitmapDescriptorFactory.FromResource(GetMyPinIcon());
                    }
                    else
                    {
                        if (item.VotedFromCurrentUser)
                            pinIcon = BitmapDescriptorFactory.FromResource(GetVotedPinIcon());
                        else
                            pinIcon = BitmapDescriptorFactory.FromResource(GetPinToVoteIcon());
                    }
                }

                var markerWithIcon = new MarkerOptions();
                markerWithIcon.SetPosition(new LatLng(item.Latitude, item.Longitude));
                markerWithIcon.SetTitle(item.Title);
                markerWithIcon.InvokeIcon(pinIcon);

                _map.AddMarker(markerWithIcon);
            }
        }

        private void UpdateRoute()
        {
            DrawRoute(UserLocator.currentRoute);
        }

        public void DrawRoute(ConcurrentQueue<RoutePosition> route)
        {
            try
            {
                (this.Context as Activity).RunOnUiThread(() =>
                {
                    if (_map == null)
                        return;

                    if (route == null)
                        return;

                    if (route.Count <= 1)
                        return;

                    PolylineOptions lineOptions = new PolylineOptions();
                    PolylineOptions lineOptionsWalking = new PolylineOptions();
                    PolylineOptions lineOptionsRunning = new PolylineOptions();
                    PolylineOptions lineOptionsOnBicycle = new PolylineOptions();
                    PolylineOptions lineOptionsInVehicle = new PolylineOptions();

                    lineOptions.InvokeColor(Color.FromHex("666666").ToAndroid());
                    lineOptions.InvokeWidth(4);
                    lineOptions.Visible(true);

                    lineOptionsWalking.InvokeColor(Color.FromHex("F19EE6").ToAndroid());
                    lineOptionsWalking.InvokeWidth(4);
                    lineOptionsWalking.Visible(true);

                    lineOptionsRunning.InvokeColor(Color.FromHex("5285C7").ToAndroid());
                    lineOptionsRunning.InvokeWidth(4);
                    lineOptionsRunning.Visible(true);

                    lineOptionsOnBicycle.InvokeColor(Color.FromHex("BF9EF7").ToAndroid());
                    lineOptionsOnBicycle.InvokeWidth(4);
                    lineOptionsOnBicycle.Visible(true);

                    lineOptionsInVehicle.InvokeColor(Color.FromHex("C1D397").ToAndroid());
                    lineOptionsInVehicle.InvokeWidth(4);
                    lineOptionsInVehicle.Visible(true);

                    // Traversing through all the routes
                    for (int i = 0; i < route.Count-1; i++)
                    {
                        //lineOptions.Add(route.ElementAt(i).ToLatLng());

                         //draw with a color relatives to activity type
                        switch (route.ElementAt(i).Activity)
                        {
                            case UserLocator.ActivityType.Walking:
                                {
                                    lineOptionsWalking.Add(route.ElementAt(i).ToLatLng());
                                    lineOptionsWalking.Add(route.ElementAt(i+1).ToLatLng());

                                    lineOptionsWalking.InvokeColor(Color.FromHex("F19EE6").ToAndroid());
                                    lineOptionsWalking.InvokeWidth(4);
                                    lineOptionsWalking.Visible(true);
                                    _map.AddPolyline(lineOptionsWalking);

                                    lineOptionsWalking = new PolylineOptions();
                                    lineOptionsWalking.InvokeWidth(4);
                                    lineOptionsWalking.Visible(true);
                                    break; 
                                }
                            case UserLocator.ActivityType.Running:
                                {
                                    lineOptionsRunning.Add(route.ElementAt(i).ToLatLng());
                                    lineOptionsRunning.Add(route.ElementAt(i+1).ToLatLng());

                                    lineOptionsRunning.InvokeColor(Color.FromHex("5285C7").ToAndroid());
                                    lineOptionsRunning.InvokeWidth(4);
                                    lineOptionsRunning.Visible(true);
                                    _map.AddPolyline(lineOptionsRunning);

                                    lineOptionsRunning = new PolylineOptions();
                                    lineOptionsRunning.InvokeWidth(4);
                                    lineOptionsRunning.Visible(true);
                                    break; 
                                }
                            case UserLocator.ActivityType.OnBicycle:
                                {
                                    lineOptionsOnBicycle.Add(route.ElementAt(i).ToLatLng());
                                    lineOptionsOnBicycle.Add(route.ElementAt(i+1).ToLatLng());

                                    lineOptionsOnBicycle.InvokeColor(Color.FromHex("BF9EF7").ToAndroid());
                                    lineOptionsOnBicycle.InvokeWidth(4);
                                    lineOptionsOnBicycle.Visible(true);
                                    _map.AddPolyline(lineOptionsOnBicycle);

                                    lineOptionsOnBicycle = new PolylineOptions();
                                    lineOptionsOnBicycle.InvokeWidth(4);
                                    lineOptionsOnBicycle.Visible(true);
                                    break;
                                }
                            case UserLocator.ActivityType.InVehicle:
                                {
                                    lineOptionsInVehicle.Add(route.ElementAt(i).ToLatLng());
                                    lineOptionsInVehicle.Add(route.ElementAt(i+1).ToLatLng());

                                    lineOptionsInVehicle.InvokeColor(Color.FromHex("C1D397").ToAndroid());
                                    lineOptionsInVehicle.InvokeWidth(4);
                                    lineOptionsInVehicle.Visible(true);
                                    _map.AddPolyline(lineOptionsInVehicle);

                                    lineOptionsInVehicle = new PolylineOptions();
                                    lineOptionsInVehicle.InvokeWidth(4);
                                    lineOptionsInVehicle.Visible(true);
                                    break;
                                }
                            default:
                                {
                                    lineOptions.Add(route.ElementAt(i).ToLatLng());
                                    lineOptions.Add(route.ElementAt(i + 1).ToLatLng());

                                    // same as walking
                                    lineOptions.InvokeColor(Color.FromHex("666666").ToAndroid());
                                    lineOptions.InvokeWidth(4);
                                    lineOptions.Visible(true);
                                    _map.AddPolyline(lineOptions);

                                    lineOptions = new PolylineOptions();
                                    lineOptions.InvokeWidth(4);
                                    lineOptions.Visible(true);
                                    break; 
                                }
                        }
                    }

                    _map.AddPolyline(lineOptionsWalking);
                    _map.AddPolyline(lineOptionsRunning);
                    _map.AddPolyline(lineOptionsOnBicycle);
                    _map.AddPolyline(lineOptionsInVehicle);
                    _map.AddPolyline(lineOptions);
                });
            }
            catch (System.Exception exc)
            {
                Log.Debug("DrawRoute", exc.Message);
            }
        }

        private static int GetMyPinIcon()
        {
            return Resource.Drawable.blueMapPin;
        }

        private static int GetPinToVoteIcon()
        {
            return Resource.Drawable.purpleMapPin;
        }

        private static int GetVotedPinIcon()
        {
            return Resource.Drawable.votedMapPin;
        }

        private async void HandleMapLongClick(object sender, GoogleMap.MapLongClickEventArgs e)
        {
            lock (_lock)
            {
                /*if (!_online)
                    return;*/

                if (_annotationDialogIsOpening)
                    return;

                _annotationDialogIsOpening = true;
            }

            MapLocation selectedPoint = new MapLocation(e.Point.Latitude, e.Point.Longitude);
            if (selectedPoint.DistanceFrom(UserLocator.Instance.CurrentLocation) > UserLocator.Instance.UserAreaRadius)
            {
                _annotationDialogIsOpening = false;
                return;
            }
            selectedPoint.InUserArea = true;

            var androidMapView = (MapView)Control;
            BitmapDescriptor pinIcon = BitmapDescriptorFactory.FromResource(GetMyPinIcon());

            var markerWithIcon = new MarkerOptions();
            markerWithIcon.SetPosition(e.Point);
            markerWithIcon.SetTitle("Inserisci Titolo");
            //markerWithIcon.SetSnippet("Untagged annotation!");

            markerWithIcon.InvokeIcon(pinIcon);

            _currentAnnotationMarker = _map.AddMarker(markerWithIcon);
            _sharedMapView.CurrentState = CustomMapView.State.Ready;

            Annotation newAnnotation = new Annotation(selectedPoint) { UserId = UsersModel.CurrentUser.Id, Title = "Inserisci Titolo", CreatedFromCurrentUser = true };

            try
            {
                await newAnnotation.LoadTags().ContinueWith((t) =>
                {
                    (Context as Activity).RunOnUiThread(async () =>
                    {
                        _sharedMapView.Items.Add(newAnnotation);

                        _sharedMapView.SelectedPinAnnotation = newAnnotation;
                        await OpenAnnotationModalDialog(true);
                    });
                });
            }
            catch {}
        }

        private void HandleMarkerClick(object sender, GoogleMap.MarkerClickEventArgs e)
        {
            _currentAnnotationMarker = e.Marker;
            var map = this.Element as CustomMapView;
            if (map == null)
                return;

            Annotation currentAnnotation = null;
            if (map.Items != null && map.Items.Count > 0)
            {
                List<Annotation> annotationsWithCoords = map.Items.Where(x => x.Latitude == e.Marker.Position.Latitude &&
                                                                         x.Longitude == e.Marker.Position.Longitude).ToList();
                if (annotationsWithCoords != null && annotationsWithCoords.Count > 0)
                    currentAnnotation = annotationsWithCoords.First();
            }

            if (currentAnnotation != null)
            {
                _currentAnnotationMarker.Title = currentAnnotation.Title;
                _currentAnnotationMarker.Snippet = currentAnnotation.Description;
                MarkerOptions opt = new MarkerOptions();
                
                _currentAnnotationMarker.ShowInfoWindow();

                var formsPin = new CustomPinView(_currentAnnotationMarker.Title, _currentAnnotationMarker.Snippet, _currentAnnotationMarker.Position.Latitude, _currentAnnotationMarker.Position.Longitude);
                map.SelectedPin = formsPin;
            }
        }

        private async void HandleInfoWindowClick(object sender, GoogleMap.InfoWindowClickEventArgs e)
        {
            lock (_lock)
            {
                /*if (!_online)
                    return;*/

                if (_annotationDialogIsOpening)
                    return;

                _annotationDialogIsOpening = true;
            }

            Marker clickedMarker = e.Marker;

            var formsMap = (CustomMapView)Element;

            if (formsMap.Items.Count == 0)
                return;

            var annotations = formsMap.Items.Where(x => x.Latitude == e.Marker.Position.Latitude && x.Longitude == e.Marker.Position.Longitude);

            if (annotations != null && annotations.Count() > 0)
            {
                formsMap.SelectedPinAnnotation = annotations.First();
                await OpenAnnotationModalDialog(false);
            }
        }

        private async Task OpenAnnotationModalDialog(bool createNew)
        {
            var formsMap = (CustomMapView)Element;

            if (formsMap.IsLoading)
                return;

            _map.InfoWindowClick -= HandleInfoWindowClick;

            await formsMap.ShowAnnotationDialog(createNew);

            lock(_lock)
            {
                _map.InfoWindowClick += HandleInfoWindowClick;
                _annotationDialogIsOpening = false;
            }
        }

        private void HandleOnUpdated(object sender, EventArgs e)
        {
            Update();
        }

        private void HandleOnAnnotationCreated(object sender, EventArgs e)
        {
            _annotationDialogIsOpening = false;
        }

        private void HandleOnAnnotationModified(object sender, EventArgs e)
        {
            _annotationDialogIsOpening = false;
        }

        private void HandleOnAnnotationDismissed(object sender, EventArgs e)
        {
            _annotationDialogIsOpening = false;
        }

        private void HandleOnCreateAnnotationAborted(object sender, EventArgs e)
        {
            _annotationDialogIsOpening = false;
            _currentAnnotationMarker.Remove();
        }

        /*private void OnUserLocationChanged(ILocatable location, double distance)
        {
            Update();

            if (_routeStart != null)
            {
                if (_routeEnd != null)
                    _routeStart = new LatLng(_routeEnd.Latitude, _routeEnd.Longitude);

                _routeEnd = new LatLng(location.Latitude, location.Longitude);
            }

            if (_routeStart == null)
                _routeStart = new LatLng(location.Latitude, location.Longitude);

            if (_routeStart != null && _routeEnd != null && _firstRouteDrawn == true)
            {
                //_route.Add(new RoutePosition(_routeStart.Latitude, _routeStart.Longitude));
                //_route.Add(new RoutePosition(_routeEnd.Latitude, _routeEnd.Longitude));
                //(this.Context as Activity).RunOnUiThread(() => DrawRoute(_route));

                // FROM GOOGLE DIRECTIONS API
                //Task.Run(async () =>
                //{
                //    // Getting URL to the Google Directions API
                //    string url = GetDirectionsUrl(_routeStart, _routeEnd, "walking");
                //    // Start downloading json data from Google Directions API
                //    string json = await DownloadJSONRouteDirections(url);
                //    // Parse the JSON to get the route directions
                //    if (!string.IsNullOrEmpty(json))
                //    {
                //        List<List<Dictionary<string, string>>> route = ParseJSONRouteDirections(json);
                //        // Draw the route
                //        (this.Context as Activity).RunOnUiThread(() => DrawRoute(route));

                //        //List<RoutePosition> route = ParseJSONRoutePositions(json);
                //        //// Draw the route
                //        //(this.Context as Activity).RunOnUiThread(() => DrawRoute(route));
                //    }
                //});
            }
            
        }*/

        //private string GetDirectionsUrl(LatLng originCoords, LatLng destitationCoords, string mode)
        //{
        //    // Origin of route
        //    string origin = "origin=" + originCoords.Latitude.ToString().Replace(',', '.') + "," + originCoords.Longitude.ToString().Replace(',', '.');
        //    // Destination of route
        //    string dest = "destination=" + destitationCoords.Latitude.ToString().Replace(',', '.') + "," + destitationCoords.Longitude.ToString().Replace(',', '.');
        //    // Sensor enabled
        //    string sensor = "sensor=false";
        //    // Building the parameters to the web service
        //    string parameters = origin + "&" + dest + "&" + sensor + "&" + mode;

        //    // Output format
        //    string output = "json";
        //    // Building the url to the web service
        //    string url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        //    return url;
        //}

        //private async Task<string> DownloadJSONRouteDirections(string url)
        //{
        //    // For storing data from web service
        //    string data = "";
        //    try
        //    {
        //        // Fetching the data from web service
        //        data = await Task.Run<string>(() => DownloadUrlContent(url));
        //    }
        //    catch (System.Exception e)
        //    {
        //        Log.Debug("DownloadJSONRouteDirections", "Background Task Failed : " + e.Message);
        //    }
        //    return data; 
        //}

        //private string DownloadUrlContent(string urlToDownload)
        //{
        //    string data = "";
        //    Stream stream = null;
        //    Java.Net.HttpURLConnection urlConnection = null;
        //    try
        //    {
        //        URL url = new URL(urlToDownload);

        //        // Creating an http connection to communicate with url
        //        urlConnection = (HttpURLConnection)url.OpenConnection();

        //        // Connecting to url
        //        urlConnection.Connect();

        //        // Reading data from url
        //        stream = urlConnection.InputStream;

        //        Java.IO.BufferedReader br = new Java.IO.BufferedReader(new InputStreamReader(stream));

        //        StringBuffer sb = new StringBuffer();

        //        string line = "";
        //        while ((line = br.ReadLine()) != null)
        //        {
        //            sb.Append(line);
        //        }

        //        data = sb.ToString();

        //        br.Close();

        //    }
        //    catch (System.Exception e)
        //    {
        //        Log.Debug("DownloadUrlContent", "Exception while downloading url : " + e.Message);
        //    }
        //    finally
        //    {
        //        stream.Close();
        //        urlConnection.Disconnect();
        //    }
        //    return data;
        //}

        //private List<List<Dictionary<string, string>>> ParseJSONRouteDirections(string json)
        //{
        //    JSONObject jObject;
        //    List<List<Dictionary<string, string>>> routes = null;

        //    try
        //    {
        //        jObject = new JSONObject(json);
        //        JSONRouteDirectionsParser parser = new JSONRouteDirectionsParser();

        //        // Starts parsing data
        //        routes = parser.Parse(jObject);
        //    }
        //    catch (Java.Lang.Exception e)
        //    {
        //        Log.Debug("ParseJSONRouteDirections", e.Message);
        //    }

        //    return routes;
        //}

        //private List<RoutePosition> ParseJSONRoutePositions(string json)
        //{
        //    JSONObject jObject;
        //    List<RoutePosition> routes = null;

        //    try
        //    {
        //        jObject = new JSONObject(json);
        //        JSONRouteDirectionsParser parser = new JSONRouteDirectionsParser();

        //        // Starts parsing data
        //        routes = parser.ParseToRoutePositions(jObject);
        //    }
        //    catch (Java.Lang.Exception e)
        //    {
        //        Log.Debug("ParseJSONRouteDirections", e.Message);
        //    }

        //    return routes;
        //}

        //private void DrawRoute(List<List<Dictionary<string, string>>> route)
        //{
        //    if (route == null)
        //        return;

        //    List<LatLng> points = null;
        //    PolylineOptions lineOptions = null;
        //    MarkerOptions markerOptions = new MarkerOptions();

        //    // Traversing through all the routes
        //    for (int i = 0; i < route.Count; i++)
        //    {
        //        points = new List<LatLng>();
        //        lineOptions = new PolylineOptions();

        //        // Fetching i-th route
        //        List<Dictionary<string, string>> path = route[i];

        //        // Fetching all the points in i-th route
        //        for (int j = 0; j < path.Count; j++)
        //        {
        //            Dictionary<string, string> point = path[j];

        //            double lat = double.Parse(point["lat"]);
        //            double lng = double.Parse(point["lng"]);
        //            LatLng position = new LatLng(lat, lng);

        //            points.Add(position);
        //        }

        //        // Adding all the points in the route to LineOptions
        //        foreach (LatLng point in points)
        //            lineOptions.Add(point);

        //        lineOptions.InvokeWidth(5);
        //        lineOptions.InvokeColor(Color.Green.ToAndroid().ToArgb());
        //        //lineOptions.Geodesic(true);
        //    }

        //    // Drawing polyline in the Google Map for the i-th route
        //    _map.AddPolyline(lineOptions);
        //}
    }
}
