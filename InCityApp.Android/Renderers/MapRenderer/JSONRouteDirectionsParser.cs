using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Org.Json;
using Android.Gms.Maps.Model;
using InCityApp.Entities;

namespace InCityApp_Android.Renderers
{
    public class JSONRouteDirectionsParser
    {
        /// <summary>
        /// Receives a JSONObject and returns a list of lists containing latitude and longitude
        /// </summary> 
        public List<List<Dictionary<String, String>>> Parse(JSONObject jObject)
        {
            List<List<Dictionary<String, String>>> routes = new List<List<Dictionary<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;

            try
            {
                jRoutes = jObject.GetJSONArray("routes");

                // Traversing all routes 
                for (int i = 0; i < jRoutes.Length(); i++)
                {
                    jLegs = ((JSONObject)jRoutes.Get(i)).GetJSONArray("legs");
                    List<Dictionary<String, String>> path = new List<Dictionary<String, String>>();

                    // Traversing all legs 
                    for (int j = 0; j < jLegs.Length(); j++)
                    {
                        jSteps = ((JSONObject)jLegs.Get(j)).GetJSONArray("steps");

                        // Traversing all steps 
                        for (int k = 0; k < jSteps.Length(); k++)
                        {
                            String polyline = "";
                            polyline = (String)((JSONObject)((JSONObject)jSteps.Get(k)).Get("polyline")).Get("points");
                            List<LatLng> list = DecodePoly(polyline);

                            // Traversing all points 
                            for (int l = 0; l < list.Count; l++)
                            {
                                Dictionary<String, String> hm = new Dictionary<String, String>();
                                hm.Add("lat", list[l].Latitude.ToString());
                                hm.Add("lng", list[l].Longitude.ToString());
                                path.Add(hm);
                            }
                        }
                        routes.Add(path);
                    }
                }
            }
            catch (JSONException e)
            {
                e.PrintStackTrace();
            }
            catch (Exception e)
            {
            }

            return routes;
        }

        /// <summary>
        /// Receives a JSONObject and returns a list of MapLocation
        /// </summary> 
        public List<RoutePosition> ParseToRoutePositions(JSONObject jObject)
        {
            List<RoutePosition> routes = new List<RoutePosition>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;

            try
            {
                jRoutes = jObject.GetJSONArray("routes");

                // Traversing all routes 
                for (int i = 0; i < jRoutes.Length(); i++)
                {
                    jLegs = ((JSONObject)jRoutes.Get(i)).GetJSONArray("legs");

                    // Traversing all legs 
                    for (int j = 0; j < jLegs.Length(); j++)
                    {
                        jSteps = ((JSONObject)jLegs.Get(j)).GetJSONArray("steps");

                        // Traversing all steps 
                        for (int k = 0; k < jSteps.Length(); k++)
                        {
                            String polyline = "";
                            polyline = (String)((JSONObject)((JSONObject)jSteps.Get(k)).Get("polyline")).Get("points");
                            List<LatLng> list = DecodePoly(polyline);

                            // Traversing all points 
                            for (int l = 0; l < list.Count; l++)
                            {
                                routes.Add(new RoutePosition(list[l].Latitude, list[l].Longitude));
                            }
                        }
                    }
                }
            }
            catch (JSONException e)
            {
                e.PrintStackTrace();
            }
            catch (Exception e)
            {
            }

            return routes;
        }

        /// <summary>
        /// Method to decode polyline points
        /// Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
        /// </summary>
        private List<LatLng> DecodePoly(String encoded)
        {
            List<LatLng> poly = new List<LatLng>();
            int index = 0, len = encoded.Length;
            int lat = 0, lng = 0;

            while (index < len)
            {
                int b, shift = 0, result = 0;
                do
                {
                    b = encoded[index++] - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);

                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do
                {
                    b = encoded[index++] - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double)lat / 1E5)), (((double)lng / 1E5)));
                poly.Add(p);
            }

            return poly;
        }
    }
}