﻿using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using InCityApp.Shared.Views;
using InCityApp_Android;
using Android.Widget;
using Android.Graphics.Drawables.Shapes;
using Android.Graphics.Drawables;
using Android.Graphics;

using Color = Xamarin.Forms.Color;
using View = global::Android.Views.View;
using ViewGroup = global::Android.Views.ViewGroup;
using Context = global::Android.Content.Context;
using ListView = global::Android.Widget.ListView;
using Android.App;

[assembly: ExportRenderer(typeof(LongLabel), typeof(LongLabelRenderer))]
namespace InCityApp_Android
{
    public class LongLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            Control.SetMaxLines(1000);
        }
    }
}
