using System;
using Android.App;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Object = Java.Lang.Object;
using View = Android.Views.View;

using InCityApp.Views;
using InCityApp_Android.Activities;
using InCityApp.Entities;

[assembly: ExportRenderer(typeof(FixedLabel), typeof(InCityApp_Android.Renderers.FixedLabelRenderer))]
namespace InCityApp_Android.Renderers
{
    public class FixedLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
        }

        protected override bool DrawChild(Android.Graphics.Canvas canvas, View child, long drawingTime)
        {
            Control.Text = Control.Text;
            return base.DrawChild(canvas, child, drawingTime);
        }
    }
}