﻿using System;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Platform.Android;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Util;

using InCityApp.Shared.Pages;
using InCityApp.Views;
using InCityApp.Entities;
using InCityApp.Helpers;
using InCityApp.DependencyServiceInterfaces;

[assembly: Xamarin.Forms.Dependency(typeof(InCityApp_Android.EditableImageCustomRenderer))]
[assembly: ExportRenderer(typeof(InCityApp.Views.EditableImage), typeof(InCityApp_Android.EditableImageCustomRenderer))]
namespace InCityApp_Android
{
    /// <summary>
    /// Editable Image View 
    /// Currently only support the rastering of another image on it.
    /// In the future we'll add drawing functionalities and so on.
    /// </summary>
    public class EditableImageCustomRenderer : ImageRenderer, IEditableImage
    {
        private WeakReference _imageViewRef;
        private Canvas _canvas;
        private Bitmap _sourceBitmap;
        //private EditableImage.EditMode _currentEditMode;
        private static bool _canSetHotspot;

        #region IEditableImage implementation

        public void SetEditMode(EditableImage.EditMode editMode)
        {
            switch (editMode)
            {
                case EditableImage.EditMode.None: _canSetHotspot = false; break;
                case EditableImage.EditMode.HotSpot: _canSetHotspot = true; break;
            }
        }

        #endregion

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Image> e)
        {
            base.OnElementChanged(e);
            EditableImage formsImage = (EditableImage)Element;
            
            // Use a WeakReference to ensure the ImageView can be garbage collected
            _imageViewRef = new WeakReference((ImageView)Control);

            if (_sourceBitmap != null && _sourceBitmap.IsRecycled == false)
            {
                _sourceBitmap.Recycle();
                _sourceBitmap.Dispose();
            }






            //BitmapDrawable drawableBmp = ((BitmapDrawable)(Control as ImageView).Drawable.Current);
            
           // _sourceBitmap = DrawableToBitmap((Control as ImageView).Drawable, formsImage.ImageWidth / 4, formsImage.ImageHeight / 4);
            //_sourceBitmap = LoadBitmapFromView((Control as ImageView), formsImage.ImageWidth, formsImage.ImageHeight); // (Control as ImageView).GetDrawingCache(true); //await GetBitmapAsync(e.NewElement.Source);
        }

        //public Bitmap DrawableToBitmap(Drawable drawable, int width, int height) 
        //{
        //    if (drawable is BitmapDrawable) {
        //        return ((BitmapDrawable) drawable).Bitmap;
        //    }

        //    // Now we check we are > 0
        //    Bitmap bitmap = Bitmap.CreateBitmap(width <= 0 ? 1 : width, height <= 0 ? 1 : height, Bitmap.Config.Argb8888);
        //    _canvas = new Canvas(bitmap);
        //    drawable.SetBounds(0, 0, _canvas.Width, _canvas.Height);
        //    drawable.Draw(_canvas);

        //    return bitmap;
        //}

        //protected override bool DrawChild(Canvas canvas, global::Android.Views.View child, long drawingTime)
        //{
        //    return base.DrawChild(canvas, child, drawingTime);
        //}

        public override bool OnTouchEvent(MotionEvent e)
        {
            MotionEventActions action = e.Action & MotionEventActions.Mask;
                            
            float x, y;
            switch (action)
            {
                case MotionEventActions.Down:
                    x = e.GetX();
                    y = e.GetY();

                    if (_canSetHotspot)
                    {
                        _canSetHotspot = false;
                        int bmpX, bmpY;
                        GetBitmapCoords((_imageViewRef.Target as ImageView), e, out bmpX, out bmpY);
                        DisplayMetrics metrics = Resources.DisplayMetrics;
                        float density = metrics.Density;
                        (Element as EditableImage).Tap((int)(x / density), (int)(y / density), (int)bmpX, (int)bmpY);
                    }
                    break;
            }

            return base.OnTouchEvent(e);
        }

        private void GetBitmapCoords(ImageView view, MotionEvent e, out int x, out int y)
        {
            int index = e.ActionIndex;
            float[] coords = new float[] { e.GetX(index), e.GetY(index) };
            Matrix matrix = new Matrix();
            view.ImageMatrix.Invert(matrix);
            matrix.PostTranslate(view.ScrollX, view.ScrollY);
            matrix.MapPoints(coords);
            x = (int)coords[0];
            y = (int)coords[1];
        }

        //public async Task DrawImage(string sharedResourceName, int x, int y)
        //{
        //    Assembly assembly = Assembly.GetExecutingAssembly();
        //    string[] resources = assembly.GetManifestResourceNames();
        //    foreach (string resource in resources)
        //    {
        //        if(resource.EndsWith(sharedResourceName))
        //        {
        //            Stream imageStream = assembly.GetManifestResourceStream(resource);

        //            //Bitmap newBitmap = Bitmap.CreateBitmap(_sourceBitmap.Width, _sourceBitmap.Height, Bitmap.Config.Argb8888);
        //            Canvas tempCanvas = new Canvas(_sourceBitmap);
                    
        //            BitmapFactory.Options options = new BitmapFactory.Options();
        //            // Calculate inSampleSize
        //            //options.InSampleSize = ImageResizer.CalculateInSampleSize(options, 50, 50);
        //            // Decode bitmap with inSampleSize set
        //            //options.InJustDecodeBounds = false;
        //            Android.Graphics.Rect paddingRect = new Rect(0, 0, 0, 0);

        //            var bitmap = await BitmapFactory.DecodeStreamAsync(imageStream, paddingRect, options);
        //            //bitmap.SetConfig(Bitmap.Config.Argb8888);
        //            //Paint paint = new Paint(PaintFlags.FilterBitmap);
        //            tempCanvas.DrawBitmap(bitmap, x, y, null);
        //            //tempCanvas.DrawBitmap(bitmap, x, y, null);
        //            //(_imageViewRef.Target as ImageView).Drawable
                    
        //            //BitmapDrawable drawableBitmap = new BitmapDrawable(Resources, _sourceBitmap);
        //            //(_imageViewRef.Target as ImageView).SetImageDrawable(drawableBitmap);
        //            (Control as ImageView).Drawable.Draw(tempCanvas);
        //            bitmap.Recycle();
        //            bitmap.Dispose();
        //            //newBitmap.Recycle();
        //            //newBitmap.Dispose();
        //        }
        //    }
        //}

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);
        }
    }
}
