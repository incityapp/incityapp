using System;
using Android.App;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Object = Java.Lang.Object;
using View = Android.Views.View;

using InCityApp.Views;
using InCityApp_Android.Activities;
using InCityApp.Entities;
using Android.OS;

[assembly: ExportRenderer(typeof(FacebookPublishSwitch), typeof(InCityApp_Android.Renderers.FacebookPublishSwitchRenderer))]
namespace InCityApp_Android.Renderers
{
    public class FacebookPublishSwitchRenderer : SwitchRenderer
    {
        private static Activity _activity;

        protected override void OnElementChanged(ElementChangedEventArgs<Switch> e)
        {
            base.OnElementChanged(e);

            _activity = this.Context as Activity;

            FacebookInterface.Instance.OnPostMessage += OnPostCustomMessage;
        }

        void OnPostCustomMessage(string obj)
        {
            Intent intent = new Intent(_activity, typeof(FacebookActivity));
            Bundle parameters = new Bundle();
            parameters.PutString("msg", obj);
            intent.PutExtras(parameters);
            _activity.StartActivityForResult(intent, 0);
        }
    }
}