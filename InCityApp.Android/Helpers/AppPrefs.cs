using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;

namespace InCityApp_Android.Helpers
{
    /// <summary>
    /// Helper class to use Android SharedPreferences. 
    /// Allows to read and write data in an app-dedicated context (a file internally managed by SO).
    /// </summary>
    public class AppPrefs
    {
        static readonly AppPrefs _instance = new AppPrefs();

        public static AppPrefs Instance
        {
            get
            {
                return _instance;
            }
        }

        public static ISharedPreferences SharedPreferences { get; set; }
        private static ISharedPreferencesEditor _sharedPreferencesEditor { get; set; }
        private readonly object _locker = new object();

        AppPrefs() { }

        static AppPrefs ()
        {
             SharedPreferences = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
            _sharedPreferencesEditor = SharedPreferences.Edit();
        }

        /// <summary>
        /// Gets the current value or the default that you specify.
        /// </summary>
        /// <typeparam name="T">Vaue of t (bool, int, float, long, string)</typeparam>
        /// <param name="key">Key for settings</param>
        /// <param name="defaultValue">default value if not set</param>
        /// <returns>Value or default</returns>
        public T GetValueOrDefault<T>(string key, T defaultValue = default(T))
        {
            lock (_locker)
            {
                Type typeOf = typeof(T);
                if (typeOf.IsGenericType && typeOf.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    typeOf = Nullable.GetUnderlyingType(typeOf);
                }

                object value = null;
                var typeCode = Type.GetTypeCode(typeOf);

                try
                {
                    switch (typeCode)
                    {
                        case TypeCode.Boolean:
                            value = SharedPreferences.GetBoolean(key, Convert.ToBoolean(defaultValue));
                            break;
                        case TypeCode.Int64:
                            value = SharedPreferences.GetLong(key, Convert.ToInt64(defaultValue));
                            break;
                        case TypeCode.String:
                            value = SharedPreferences.GetString(key, Convert.ToString(defaultValue));
                            break;
                        case TypeCode.Int32:
                            value = SharedPreferences.GetInt(key, Convert.ToInt32(defaultValue));
                            break;
                        case TypeCode.Single:
                            value = SharedPreferences.GetFloat(key, Convert.ToSingle(defaultValue));
                            break;
                        case TypeCode.DateTime:
                            var ticks = SharedPreferences.GetLong(key, -1);
                            if (ticks == -1)
                                value = defaultValue;
                            else
                                value = new DateTime(ticks);
                            break;
                    }
                }
                catch (Exception e)
                {
                    return null != value ? (T)value : defaultValue;
                };

                return null != value ? (T)value : defaultValue;
            }
        }

        /// <summary>
        /// Adds or updates a value
        /// </summary>
        /// <param name="key">key to update</param>
        /// <param name="value">value to set</param>
        /// <returns>True if added or update and you need to save</returns>
        public bool AddOrUpdateValue(string key, object value)
        {
            lock (_locker)
            {
                Type typeOf = value.GetType();
                if (typeOf.IsGenericType && typeOf.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    typeOf = Nullable.GetUnderlyingType(typeOf);
                }
                var typeCode = Type.GetTypeCode(typeOf);

                try
                {
                    switch (typeCode)
                    {
                        case TypeCode.Boolean:
                            _sharedPreferencesEditor.PutBoolean(key, Convert.ToBoolean(value));
                            break;
                        case TypeCode.Int64:
                            _sharedPreferencesEditor.PutLong(key, Convert.ToInt64(value));
                            break;
                        case TypeCode.String:
                            _sharedPreferencesEditor.PutString(key, Convert.ToString(value));
                            break;
                        case TypeCode.Int32:
                            _sharedPreferencesEditor.PutInt(key, Convert.ToInt32(value));
                            break;
                        case TypeCode.Single:
                            _sharedPreferencesEditor.PutFloat(key, Convert.ToSingle(value));
                            break;
                        case TypeCode.DateTime:
                            _sharedPreferencesEditor.PutLong(key, ((DateTime)(object)value).Ticks);
                            break;
                    }

                }
                catch (Exception e)
                {
                    return false;
                };
            }

            return true;
        }

        /// <summary>
        /// Saves out all current settings
        /// </summary>
        public void Save()
        {
            lock (_locker)
            {
                _sharedPreferencesEditor.Commit();
            }
        }
    }
}