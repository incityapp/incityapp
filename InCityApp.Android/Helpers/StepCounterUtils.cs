using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using System.Globalization;
using InCityApp_Android.Services;

namespace InCityApp_Android.Helpers
{
    public static class StepCounterUtils
    {
        public static bool IsKitKatWithStepCounter(PackageManager pm)
        {
            // Require at least Android KitKat
            int currentApiVersion = (int)Build.VERSION.SdkInt;
            // Check that the device supports the step counter and detector sensors
            return currentApiVersion >= 19
                && pm.HasSystemFeature(Android.Content.PM.PackageManager.FeatureSensorStepCounter)
                && pm.HasSystemFeature(Android.Content.PM.PackageManager.FeatureSensorStepDetector);
        }

        public static string DateString
        {
            get
            {
                var day = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DateTime.Now.DayOfWeek);
                var month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
                var dayNum = DateTime.Now.Day;
                if (StepCounterSettings.UseKilometeres)
                    return day + " " + dayNum + " " + month;

                return day + " " + month + " " + dayNum;
            }
        }

        public static string GetDateStaring(DateTime date)
        {
            string day = date.ToString("ddd");
            string month = date.ToString("MMM");
            int dayNum = date.Day;
            if (StepCounterSettings.UseKilometeres)
                return day + " " + dayNum + " " + month;

            return day + " " + month + " " + dayNum;
        }

        public static bool IsSameDay
        {
            get
            {
                return DateTime.Today.DayOfYear == StepCounterSettings.CurrentDay.DayOfYear &&
                  DateTime.Today.Year == StepCounterSettings.CurrentDay.Year;
            }
        }

        public static string FormatSteps(Int64 steps)
        {
            return steps.ToString("N0");
        }
    }
}