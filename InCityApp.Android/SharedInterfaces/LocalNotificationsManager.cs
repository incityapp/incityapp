using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using InCityApp.DependencyServiceInterfaces;
using Xamarin.Forms;
using System.Threading;
using InCityApp_Android.Activities;
using InCityApp.Entities;

[assembly: Xamarin.Forms.Dependency(typeof(InCityApp_Android.LocalNotificationsManager))]
namespace InCityApp_Android
{
    public class LocalNotificationsManager : Java.Lang.Object, ILocalNotifications
    {
        #region ILocalNofications Methods

        public void Notify(string title, string text, bool emitSound = true, bool emitVibration = true)
        {
            Activity activity = Forms.Context as Activity;

            var intent = activity.PackageManager.GetLaunchIntentForPackage(activity.PackageName);
            intent.AddFlags(ActivityFlags.ClearTop);

            var pendingIntent = PendingIntent.GetActivity(activity, 0, intent, PendingIntentFlags.UpdateCurrent);

            Notification.Builder builder = new Notification.Builder(activity)
            .SetContentIntent(pendingIntent)
            .SetContentTitle(title)
            .SetContentText(text)
            //.SetOngoing(true)
            .SetSmallIcon(Resource.Drawable.NotifyIcon);

            Notification notification = builder.Build();

            NotificationManager notificationManager = activity.GetSystemService(Context.NotificationService) as NotificationManager;

            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);
        }

        public void NotifyAnnotation(string annotationId, string title, string text, bool emitSound = true, bool emitVibration = true)
        {
            Activity activity = Forms.Context as Activity;

            var resultIntent = new Intent(Forms.Context, typeof(MainActivity));
            resultIntent.PutExtra("annotationId", annotationId);
            var stackBuilder = TaskStackBuilder.Create(Forms.Context);
            stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(MainActivity)));
            stackBuilder.AddNextIntent(resultIntent);
            var pendingIntent = stackBuilder.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);

            Notification.Builder builder = new Notification.Builder(activity)
            .SetAutoCancel(true)
            .SetContentIntent(pendingIntent)
            .SetContentTitle(title)
            .SetContentText(text)
                //.SetOngoing(true)
            .SetSmallIcon(Resource.Drawable.NotifyIcon);

            Notification notification = builder.Build();

            NotificationManager notificationManager = activity.GetSystemService(Context.NotificationService) as NotificationManager;

            const int notificationId = 1;
            notificationManager.Notify(notificationId, notification);
        }

        #endregion
    }
}