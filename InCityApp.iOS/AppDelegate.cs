﻿using System;
using System.Collections.Generic;
using System.Linq;
//using MonoTouch.Foundation;
//using MonoTouch.UIKit;
using Xamarin.Forms;
using Xamarin;
using Xamarin.Forms.Platform.iOS;
using InCityApp.Shared.Views;
using InCityApp.Shared.Pages;
using Foundation;
using UIKit;
using InCityApp;
using InCityApp.Entities;
using System.Diagnostics;
using System.Threading.Tasks;

namespace InCityApp_iOS
{
    [Register ("AppDelegate")]
    public partial class AppDelegate : FormsApplicationDelegate
    {
        UIWindow _window;
		StepManager _stepManager; 

        public override bool FinishedLaunching (UIApplication app, NSDictionary options)
        {
			InCityApplication.ScreenWidth = (int) UIScreen.MainScreen.Bounds.Width;
			InCityApplication.ScreenHeight = (int) UIScreen.MainScreen.Bounds.Height;

			IosAudioTracker pippo = new IosAudioTracker();
			pippo.OnVolumeChanged += HandleOnVolumeChanged;

			// Necessary for loading the chart. Does not do anything.
			new XLabs.Forms.Charting.Controls.ChartRenderer();

			_stepManager = new StepManager();
			_stepManager.DailyStepCountChanged += stepManager_DailyStepCountChanged;
//			_stepManager.StartCountingFrom(DateTime.Today);

            _window = new UIWindow(UIScreen.MainScreen.Bounds);

            InCityApplication.Init(typeof(InCityApplication).Assembly);
            Forms.Init();
            FormsMaps.Init();

			UIApplication.SharedApplication.SetStatusBarHidden(true, true);
			//UINavigationBar.Appearance.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0);
			//UINavigationBar.Appearance.TintColor = InCityApp.Shared.Helpers.Color.Blue.ToUIColor();
			//UINavigationBar.Appearance.BarTintColor = UIColor.White;
			//UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
			//{
			//	TextColor = UIColor.White
			//});

            LoadApplication(new App());  // method is new in 1.3

            return base.FinishedLaunching(app, options);
        }

		void stepManager_DailyStepCountChanged(nint obj)
		{
//			Debug.Print("Steps: " + obj.ToString());
			StepCounter.Instance.StepCount = obj;
		}

        static UIViewController BuildView()
        {
            var root = new RootPage();
            var controller = root.CreateViewController();
            return controller;
        }

		private void HandleOnVolumeChanged(double db)
		{
			InvokeOnMainThread(() =>
			{
				AudioRecorder.Instance.AddSoundSample(db);
			});
		}
    }
}

