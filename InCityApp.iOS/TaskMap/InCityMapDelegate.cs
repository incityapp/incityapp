using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using MapKit;

namespace InCityApp_iOS
{
	class InCityMapDelegate : MKMapViewDelegate
	{
		protected string annotationIdentifier = "IosAnnotation";
		UIButton detailButton; // avoid GC

		public event EventHandler OnAnnotationClicked;

		public override MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
		{
			// try and dequeue the annotation view
			MKAnnotationView annotationView = mapView.DequeueReusableAnnotation(annotationIdentifier);
			// if we couldn't dequeue one, create a new one
			if (annotationView == null)
				annotationView = new MKPinAnnotationView(annotation, annotationIdentifier);
			else // if we did dequeue one for reuse, assign the annotation to it
				annotationView.Annotation = annotation;

			// configure our annotation view properties
			//annotationView.CanShowCallout = true;
			//annotationView.AnimatesDrop = true;
			//annotationView.PinColor = MKPinAnnotationColor.Green;
			//annotationView.Selected = true;

			// you can add an accessory view; in this case, a button on the right and an image on the left
			detailButton = UIButton.FromType(UIButtonType.DetailDisclosure);
//			detailButton.TouchUpInside += Handle_OnClickAnnotation;
			annotationView.RightCalloutAccessoryView = detailButton;

			//        annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromBundle("29_icon.png"));
			return annotationView;
		}

		void Handle_OnClickAnnotation(object sender, EventArgs e)
		{
			if (OnAnnotationClicked != null)
				OnAnnotationClicked(sender, e);
		}
	}
}