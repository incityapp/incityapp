using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using MapKit;
using CoreLocation;

namespace InCityApp_iOS
{
	class InCityAnnotation : MKAnnotation
	{
		CLLocationCoordinate2D _coordinates;
		public override CLLocationCoordinate2D Coordinate { get { return _coordinates; } }
		string title, subtitle;
		public override string Title { get { return title; } }
		public override string Subtitle { get { return subtitle; } }

		public override void SetCoordinate(CLLocationCoordinate2D value)
		{
			_coordinates = value;
		}

		public InCityAnnotation(CLLocationCoordinate2D coordinate, string title, string subtitle = "")
		{
			SetCoordinate(coordinate);
			this.title = title;
			this.subtitle = subtitle;
		}
	}
}