using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using MapKit;
using System.Drawing;

namespace InCityApp_iOS
{
	class InCityAnnotationView : MKAnnotationView
	{
		UIButton detailButton; // avoid GC
		static UIImage _img;
		public InCityAnnotation _annotation;

		public InCityAnnotationView(IMKAnnotation annotation)
			: base(annotation, "InCityAnnotation")
		{
			_annotation = (InCityAnnotation)annotation;
			if (_img == null)
			{
				_img = UIImage.FromBundle("mapPin.png");
			}
			this.Image = _img;
			this.Frame = new RectangleF(0, 0, (float)_img.Size.Width, (float)_img.Size.Height);
			detailButton = UIButton.FromType(UIButtonType.DetailDisclosure);
			RightCalloutAccessoryView = detailButton;
			CanShowCallout = true;
		}

	}
}