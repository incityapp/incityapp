using CoreLocation;

using MonoTouch;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Foundation;
using UIKit;
using MapKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps.iOS;

using InCityApp_iOS;
using InCityApp.Entities;
using InCityApp.Helpers;
using InCityApp.Views;
using InCityApp.DataModels;

[assembly: ExportRenderer(typeof(InCityApp.Views.CustomMapView), typeof(InCityApp_iOS.MapCustomRenderer))]

namespace InCityApp_iOS
{
    class MapCustomRenderer : MapRenderer
    {
        #region Declarations

        // This asks for the permissions to use location services
		CustomMapView _formsMap = null;
		CLLocationManager _manager;
		MKMapView _mapView;
		MKAnnotation _currentAnnotation;

		//protected string annotationIdentifier = "IosAnnotation";
		//UIButton detailButton; // avoid GC


		static object _lock = new object();
		private bool _annotationPopupIsOpen;

        #endregion

        #region Construction

        public MapCustomRenderer()
        {
            _manager = new CLLocationManager();
        }

		// Called on creation, therefore in Construction and not in Events.
		protected override void OnElementChanged(Xamarin.Forms.Platform.iOS.ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);

			if (_formsMap == null) // Already initialized
			{
				_formsMap = (e.NewElement == null) ? (CustomMapView)e.OldElement : (CustomMapView)e.NewElement;
				if (_formsMap == null)
				{
					throw new Exception("Form is null");
				}
				_formsMap.OnAnnotationCreated += HandleOnAnnotationCreated;
				_formsMap.OnAnnotationModified += HandleOnAnnotationModified;
				_formsMap.OnAnnotationDismissed += HandleOnAnnotationDismissed;
				_formsMap.OnCreateAnnotationAborted += HandleOnCreateAnnotationAborted;

//				((ObservableCollection<Pin>)_formsMap.Pins).CollectionChanged += OnCollectionChanged;
			}

			if (_mapView == null)
			{
//				SetNativeControl(new MKMapView());
				_mapView = (MKMapView)Control;
//				_mapView.Delegate = new InCityMapDelegate();

				if (_mapView == null)
				{
					throw new Exception("Map View null!");
				}

				if(UIDevice.CurrentDevice.CheckSystemVersion(8,0))
					_manager.RequestWhenInUseAuthorization();	

				_mapView.ShowsUserLocation = true;
				if (_mapView.UserLocation != null)
					UserLocator.Instance.CurrentLocation = new MapLocation(_mapView.UserLocation.Coordinate.Latitude, _mapView.UserLocation.Coordinate.Longitude);
				else
					Debug.Print("WARNING: Map current location is null");

				//if (_mapView.Delegate is InCityMapDelegate)
				//{
				//	((InCityMapDelegate)_mapView.Delegate).OnAnnotationClicked -= Handle_OnAnnotationClicked;
				//	((InCityMapDelegate)_mapView.Delegate).OnAnnotationClicked += Handle_OnAnnotationClicked;
				//}
			}

			//Debug.Print("Map View al costruttore: " + _mapView.ToString());
			//Debug.Print("Map Delegate al costruttore: " + _mapView.Delegate);

			// Questo fa in modo di centrare la mappa sull'utente ogni volta che viene percepito un cambiamento di location.
			_mapView.DidUpdateUserLocation += Handle_OnUserLocationChanged;

			_mapView.AddGestureRecognizer(new UITapGestureRecognizer(this, new ObjCRuntime.Selector("MapTapSelector:")));
			_mapView.CalloutAccessoryControlTapped += Handle_OnControlTapped;
			_mapView.GetViewForAnnotation += GetViewForAnnotation;
			Update(false);
		}

        #endregion

        #region Events

		#region Events - Annotation Dialog

		void HandleOnAnnotationCreated(object sender, EventArgs e)
		{
			_annotationPopupIsOpen = false;
			Update(true);
		}

		void HandleOnAnnotationModified(object sender, EventArgs e)
		{
			_annotationPopupIsOpen = false;
			Update(true);
		}

		void HandleOnAnnotationDismissed(object sender, EventArgs e)
		{
			_annotationPopupIsOpen = false;
		}

		void HandleOnCreateAnnotationAborted(object sender, EventArgs e)
		{
			_annotationPopupIsOpen = false;
			_mapView.RemoveAnnotation(_currentAnnotation);
//			Debug.Print("Chiamata chiusura Abort");
			Update(false);
		}

		#endregion

		void Handle_OnUserLocationChanged(object sender, EventArgs e)
		{
			Update(false);
		}

		//void Handle_OnClickAnnotation(object sender, EventArgs e)
		//{
		//	Debug.Print("Chiamata OnAnnotationClicked" + sender.ToString());
		//}

		void Handle_OnControlTapped(object sender, MKMapViewAccessoryTappedEventArgs e)
		{
//			Debug.Print("Chiamata OnControlTapped" + ((InCityAnnotationView)e.View)._annotation.ToString());
			InCityAnnotation ann = ((InCityAnnotationView)e.View)._annotation;
			_formsMap.SelectedPinAnnotation = _formsMap.Items.Where(x => x.Latitude == ann.Coordinate.Latitude &&
														   x.Longitude == ann.Coordinate.Longitude).First();
			OpenAnnotationModalDialog(false);
 
		}

		[Export("MapTapSelector:")]
		protected void OnMapTapped(UIGestureRecognizer sender)
		{
//			Debug.Print("Percepito tap su mappa");
			if (_formsMap.CurrentState != CustomMapView.State.InsertingAnnotation)
				return;

			CLLocationCoordinate2D tappedLocationCoord = _mapView.ConvertPoint(sender.LocationInView(_mapView), _mapView);
			MapLocation selectedPoint = new MapLocation(tappedLocationCoord.Latitude, tappedLocationCoord.Longitude);
			if (selectedPoint.DistanceFrom(UserLocator.Instance.CurrentLocation) > UserLocator.Instance.UserAreaRadius)
				return;

			selectedPoint.InUserArea = true;

//			BitmapDescriptor pinIcon = BitmapDescriptorFactory.FromResource(GetInAreaPinIcon());

			//var markerWithIcon = new MarkerOptions();
			//markerWithIcon.SetPosition(e.P0);
			//markerWithIcon.SetTitle("New Annotation");
			////markerWithIcon.SetSnippet("Untagged annotation!");

			//markerWithIcon.InvokeIcon(pinIcon);

			_currentAnnotation = new InCityAnnotation(tappedLocationCoord, "New Annotation");// = androidMapView.Map.AddMarker(markerWithIcon);
			_formsMap.CurrentState = CustomMapView.State.Ready;

			Annotation newAnnotation = new Annotation(selectedPoint.Latitude, selectedPoint.Longitude) { UserId = UsersModel.CurrentUser.Id, Title = "New Annotation" };
			_formsMap.Items.Add(newAnnotation);

			_formsMap.SelectedPinAnnotation = newAnnotation;
			OpenAnnotationModalDialog(true);
        }

        #endregion

        #region Engine

		private void OpenAnnotationModalDialog(bool createNew)
		{
			lock (_lock)
			{
				if (_annotationPopupIsOpen)
					return;

				_annotationPopupIsOpen = true;

				_formsMap.ShowAnnotationDialog(createNew);
			}
		}

		private void Update(bool forceChange)
		{
			bool hasChanged = UpdateUserArea();

			if(forceChange || hasChanged)
				UpdatePins();
		}

		public MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
		{
			MKAnnotationView annotationView = null;
			if (annotation is InCityAnnotation)
				annotationView = new InCityAnnotationView(annotation);
			//				detailButton = UIButton.FromType(UIButtonType.DetailDisclosure);
			//				detailButton.TouchUpInside += Handle_OnClickAnnotation;
			//				annotationView.RightCalloutAccessoryView = detailButton;
			else
				return null;
			// try and dequeue the annotation view
//			MKAnnotationView annotationView = mapView.DequeueReusableAnnotation(annotationIdentifier);
			// if we couldn't dequeue one, create a new one
			//if (annotationView == null)
			//else // if we did dequeue one for reuse, assign the annotation to it
			//	annotationView.Annotation = annotation;

			// configure our annotation view properties
//			annotationView.AnimatesDrop = true;
//			annotationView.PinColor = MKPinAnnotationColor.Green;
//			annotationView.Selected = true;

			// you can add an accessory view; in this case, a button on the right and an image on the left

			//        annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromBundle("29_icon.png"));
			return annotationView;
		}

		private void UpdatePins()
		{
			_mapView.RemoveAnnotations(_mapView.Annotations);

//			Debug.Print("Number of annotations " + _formsMap.Items.Count.ToString());
			//Debug.Print("Map Delegate all'update: " + _mapView.Delegate);
			//Debug.Print("Map View all'update: " + _mapView.ToString());

			foreach (Annotation item in _formsMap.Items)
				_mapView.AddAnnotation(new InCityAnnotation(new CLLocationCoordinate2D(item.Latitude, item.Longitude), item.Title));
		}

		private bool UpdateUserArea()
		{
			if (UserLocator.Instance.CurrentLocation != null)
				if (UserLocator.Instance.CurrentLocation.Latitude == _mapView.UserLocation.Coordinate.Latitude &&
					UserLocator.Instance.CurrentLocation.Longitude == _mapView.UserLocation.Coordinate.Longitude)
					return false;

			if (_mapView.UserLocation != null)
			{
				UserLocator.Instance.CurrentLocation = new MapLocation(_mapView.UserLocation.Coordinate.Latitude, _mapView.UserLocation.Coordinate.Longitude);
				CLLocationCoordinate2D coords = _mapView.UserLocation.Coordinate;
				MKCoordinateSpan span = new MKCoordinateSpan(DistanceCalculator.KmsToLatitudeDegrees(UserLocator.Instance.UserAreaRadius / 1000), DistanceCalculator.KmsToLongitudeDegrees(UserLocator.Instance.UserAreaRadius / 1000, coords.Latitude));
//				MKCoordinateSpan span = new MKCoordinateSpan(DistanceCalculator.KmsToLatitudeDegrees(2), DistanceCalculator.KmsToLongitudeDegrees(2, coords.Latitude));
				_mapView.Region = new MKCoordinateRegion(coords, span);
				Debug.Print("richiesta coordinata alla mappa: " + coords.Latitude.ToString() + " - " + coords.Longitude.ToString() + " Span: " + span.LatitudeDelta.ToString() + " - " + span.LongitudeDelta.ToString());
			}

			// Fallback
			else
			{
				// user denied permission, or device doesn't have GPS/location ability
				CLLocationCoordinate2D coords = new CLLocationCoordinate2D(37.33233141, -122.0312186); // cupertino
				MKCoordinateSpan span = new MKCoordinateSpan(DistanceCalculator.MilesToLatitudeDegrees(20), DistanceCalculator.MilesToLongitudeDegrees(20, coords.Latitude));
				_mapView.Region = new MKCoordinateRegion(coords, span);
			}
			return true;		
		}

        #endregion

    }
}