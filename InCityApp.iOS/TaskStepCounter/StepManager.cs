using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using InCityApp.Helpers;
using CoreMotion;
using System.Threading.Tasks;
using System.Diagnostics;

namespace InCityApp_iOS
{
	public class StepManager
	{
		public event Action<nint> DailyStepCountChanged;
		
		private NSOperationQueue _queue;
//		private DateTime _resetTime;
		private CMStepCounter _stepCounter;

		public StepManager()
		{
			_queue = NSOperationQueue.CurrentQueue;
			_stepCounter = new CMStepCounter();
			_stepCounter.StartStepCountingUpdates(_queue, 1, Updater);
			StepsRefresh();
		}

		void ForceUpdate()
		{
			//If the last reset date wasn't today then we should update this.
			//if (_resetTime.Date.Day != DateTime.Now.Date.Day)
			//	_resetTime = DateTime.Today; //Forces update as the day may have changed.

//			NSDate sMidnight = DateConverter.DateTimeToNSDate(_resetTime);
			NSDate sMidnight = DateConverter.DateTimeToNSDate(DateTime.Today);

			_queue = _queue ?? NSOperationQueue.CurrentQueue;
			if (_stepCounter == null)
				_stepCounter = new CMStepCounter();
			_stepCounter.QueryStepCount(sMidnight, NSDate.Now, _queue, DailyStepQueryHandler);
		}

		//public void StartCountingFrom(DateTime date)
		//{
		//	_resetTime = date;
		//	StepsRefresh();
		//}

		private void DailyStepQueryHandler(nint stepCount, NSError error)
		{
			if (DailyStepCountChanged != null)
				DailyStepCountChanged(stepCount);
		}

		private void Updater(nint stepCount, NSDate date, NSError error)
		{
//			NSDate sMidnight = DateConverter.DateTimeToNSDate(_resetTime);
			NSDate sMidnight = DateConverter.DateTimeToNSDate(DateTime.Today);
			Debug.Print("Toccato Updater!");
			_stepCounter.QueryStepCount(sMidnight, NSDate.Now, _queue, DailyStepQueryHandler);
		}

		async void StepsRefresh()
		{
			while (true)
			{
				await Task.Delay(5000);
				ForceUpdate();
			}
		}

	}
}